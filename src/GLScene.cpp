#include "StdAfx.h"
#include "GLScene.h"


GLScene::GLScene(void)
{
	m_pViewportCamera = nullptr;
	m_iWidth = 0;
	m_iHeight = 0;
}

GLScene::~GLScene(void)
{
	
}

void GLScene::Init()
{
	// Set the ColorBuffer clear color
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	// Set the DepthBuffer clear depth
	glClearDepth(1.0f);

	// Set shademodel to Smooth
	glShadeModel(GL_SMOOTH);

	// Enable depthtest (z buffer)
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// Set perspective correction to nicest
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Switch to modelview as default
	glMatrixMode(GL_MODELVIEW);

	// Set backface culling as default
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Init the GLEW extension library
	InitGLEWExtension();
	
	// Create the root debug object
	m_RootDebugObject = new SceneObject("rootDebug");

	// Print some info about our environment
	StdConsole::LogInfo( _STR"Vendor             : " << glGetString( GL_VENDOR ) );
	StdConsole::LogInfo( _STR"Renderer           : " << glGetString( GL_RENDERER ) );
	StdConsole::LogInfo( _STR"OpenGL Version     : " << glGetString( GL_VERSION ) );
	StdConsole::LogInfo( _STR"Shading Language   : " << glGetString( GL_SHADING_LANGUAGE_VERSION ) );
	
}

void GLScene::InitGLEWExtension()
{
	GLenum err = glewInit();
	if (GLEW_OK != err)
		StdConsole::LogError( _STR"Failed to initialize GLEW" );

	StdConsole::LogInfo( _STR"Using GLEW Version: " << glewGetString(GLEW_VERSION) );
}

void GLScene::SetViewport( int Width, int Height )
{
	// Store the values used
	m_iWidth = Width, m_iHeight = Height;
	
	if (m_pViewportCamera != nullptr)
		m_pViewportCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );

	// Update the viewport
	glViewport(0,0,Width,Height);
}

void GLScene::GetBounds( glm::vec3 &min, glm::vec3 &max )
{
	min = glm::vec3(-1.f, -1.f, -1.f);
	max = glm::vec3(1.f, 1.f, 1.f);
}

void GLScene::Clear( GLbitfield bufferMask )
{
	glClear(bufferMask);
}

void GLScene::AssertGLError()
{
	GLenum err = glGetError();
	assert( err == 0 );
}

bool GLScene::Event_ProcessCommand( std::string command, std::string &response )
{

	return false;
}