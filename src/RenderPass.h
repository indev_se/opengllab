#pragma once
#include "SceneObject.h"
#include "RenderJob.h"
#include "GLScene.h"

class SceneObject;

class RenderPass
{
public:
	RenderPass(void);
	~RenderPass(void);

	virtual void Init( GLScene *scene );
	virtual void Bind() = 0;
	virtual void Unbind() = 0;
	
	virtual void BindObject( SceneObject *object ) {}
	virtual void UnbindObject( SceneObject *object ) {}

	virtual void BindJob( RenderJob *job ) {}
	virtual void UnbindJob( RenderJob *job ) {}
	
	bool BindTextures() { return m_bBindTextures; }

	void RenderScreenQuad();

protected:

	GLScene *m_pScene;

	bool m_bBindTextures;

};

