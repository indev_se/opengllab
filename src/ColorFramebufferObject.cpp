#include "StdAfx.h"
#include "ColorFramebufferObject.h"


ColorFramebufferObject::ColorFramebufferObject(void)
{

}


ColorFramebufferObject::~ColorFramebufferObject(void)
{
}

void ColorFramebufferObject::Create2D( unsigned int Width, unsigned int Height, GLenum attachment )
{
	m_Width = Width;
	m_Height = Height;

	// Create the texture
	m_pTexture = new TextureAsset();

	TextureInfo *info = new TextureInfo();
	info->genMipMap = false;
	info->format = GL_RGBA16F;
	info->valueType = GL_FLOAT;

	TextureInfoParam infoParam1 = { GL_TEXTURE_WRAP_S, GL_CLAMP };
	info->params.push_back( &infoParam1 );
	TextureInfoParam infoParam2 = { GL_TEXTURE_WRAP_T, GL_CLAMP };
	info->params.push_back( &infoParam2 );
	TextureInfoParam infoParam3 = { GL_TEXTURE_MAG_FILTER, GL_NEAREST };
	info->params.push_back( &infoParam3 );
	TextureInfoParam infoParam4	= { GL_TEXTURE_MIN_FILTER, GL_NEAREST };
	info->params.push_back( &infoParam4 );

	m_pTexture->CreateEmpty( Width, Height, GL_RGBA, info);

	// Create the Framebuffer object
	glGenFramebuffers(1, &m_FBOID);
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBOID );

	// Instruct openGL that we won't bind a color texture with the currently binded FBO
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	// Attach the texture to the framebuffer
	glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, m_pTexture->TextureID(), 0 );

	// Check status to make sure everything went ok
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if ( status != GL_FRAMEBUFFER_COMPLETE ) {
		StdConsole::LogError( _STR"GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO");
	}

	// Switch back to the normal framebuffer (window)
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}