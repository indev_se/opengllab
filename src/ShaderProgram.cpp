#include "StdAfx.h"
#include "ShaderProgram.h"


ShaderProgram::ShaderProgram(void)
{
}


ShaderProgram::~ShaderProgram(void)
{
	glDetachShader(m_Program, m_VertexShader);
	glDetachShader(m_Program, m_FragmentShader);

	glDeleteShader(m_VertexShader);
	glDeleteShader(m_FragmentShader);

	glDeleteProgram(m_Program);
}

GLhandleARB ShaderProgram::CreateProgram( const char *vertexShaderFile, const char *fragmentShaderFile )
{
	const char *vertexSource;
	const char *fragSource;
	int compiled = 0;
	int linked = 0;
	char str[4096];

	// Get the sourcecode of the shaders
	vertexSource = GetSource( vertexShaderFile );
	fragSource = GetSource( fragmentShaderFile );

	m_VertexShader = glCreateShader(GL_VERTEX_SHADER_ARB);
	m_FragmentShader = glCreateShader(GL_FRAGMENT_SHADER_ARB);

	glShaderSource(m_VertexShader, 1, &vertexSource, NULL);
	glShaderSource(m_FragmentShader, 1, &fragSource, NULL);

	// Compile the vertex shader
	glCompileShader(m_VertexShader);
	glGetShaderiv(m_VertexShader, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE) {
		glGetShaderInfoLog( m_VertexShader, sizeof(str), NULL, str );
		StdConsole::LogError( _STR"Vertex Shader Compile Error: " << vertexShaderFile );
		StdConsole::LogError( _STR str );
	    return 0;
	}

	// Compile the fragment shader
	glCompileShader(m_FragmentShader);
	glGetShaderiv(m_FragmentShader, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE) {
		glGetShaderInfoLog( m_FragmentShader, sizeof(str), NULL, str );
		StdConsole::LogError( _STR"Fragment Shader Compile Error: " << fragmentShaderFile );
		StdConsole::LogError( _STR str );
	    return 0;
	}

	// Create the program and attach the compiled shaders
	m_Program = glCreateProgram();
	
	glAttachShader(m_Program, m_VertexShader);
	glAttachShader(m_Program, m_FragmentShader);

	// Link the program
	glLinkProgram(m_Program);
	glGetProgramiv(m_Program, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE) {
		StdConsole::LogError( _STR"Can't link shaders");
	    return 0;
	}

	// Cleanup
	//glDeleteShader(m_VertexShader);
	//glDeleteShader(m_FragmentShader);

	PrintProgramInfoLog();

	//free(vertexSource);
	//free(fragSource);

	return m_Program;
}

void ShaderProgram::SendUniform1i(char variable[60], int value)
{
	GLint loc = glGetUniformLocationARB(m_Program, variable);
    if (loc==-1) {
		StdConsole::LogError( _STR"Can't find uniform variable: " << variable);
		return;  // can't find variable
	}
    
    glUniform1iARB(loc, value);
}

void ShaderProgram::SendUniform1f(char variable[60], float value)
{
	GLint loc = glGetUniformLocationARB(m_Program, variable);
    if (loc==-1) {
		StdConsole::LogError( _STR"Can't find uniform variable: " << variable);
		return;  // can't find variable
	}
    
    glUniform1fARB(loc, value);
}

void ShaderProgram::SendUniform2f(char variable[60], float value[2])
{
	GLint loc = glGetUniformLocationARB(m_Program, variable);
    if (loc==-1) {
		StdConsole::LogError( _STR"Can't find uniform variable: " << variable);
		return;  // can't find variable
	}
    
    glUniform2fARB(loc, value[0], value[1]);
}

void ShaderProgram::SendUniform3f(char variable[60], float value[3])
{
	GLint loc = glGetUniformLocationARB(m_Program, variable);
    if (loc==-1) {
		StdConsole::LogError( _STR"Can't find uniform variable: " << variable);
		return;  // can't find variable
	}
    
    glUniform3fARB(loc, value[0], value[1], value[2]);
}

void ShaderProgram::SendUniform4f(char variable[60], float value[4])
{
	GLint loc = glGetUniformLocationARB(m_Program, variable);
    if (loc==-1) {
		StdConsole::LogError( _STR"Can't find uniform variable: " << variable);
		return;  // can't find variable
	}
    
    glUniform4fARB(loc, value[0], value[1], value[2], value[3]);
}

void ShaderProgram::SendUniformMatrix4fv(char variable[60], int count, bool transpose, float *value)
{
	GLint loc = glGetUniformLocationARB(m_Program, variable);
    if (loc==-1) {
		StdConsole::LogError( _STR"Can't find uniform variable: " << variable);
		return;  // can't find variable
	}
    
    glUniformMatrix4fvARB(loc, count, transpose, value);
}

void ShaderProgram::BindFragDataLocation(char variable[60], int value)
{
	glBindFragDataLocation(m_Program, value, variable);
}

int32 ShaderProgram::GetAttribLocation(char variable[60] )
{
	GLint loc = glGetAttribLocation(m_Program, variable);
	if (loc==-1) {
		//printf("ERROR: Can't find attribute location: %s\n", variable);
		return -1;  // can't find variable
	}

	return loc;
}

void ShaderProgram::PrintProgramInfoLog()
{
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;

    glGetObjectParameterivARB(m_Program, GL_OBJECT_INFO_LOG_LENGTH_ARB, &infologLength);

    if (infologLength > 0)
    {
		infoLog = (char *)malloc(infologLength);
		glGetInfoLogARB(m_Program, infologLength, &charsWritten, infoLog);
		StdConsole::LogInfo( _STR infoLog);
		free(infoLog);
    }
}

char *ShaderProgram::GetSource( const char *sourceFile )
{
	FILE *f;
	long len;
	char *buffer;

	fopen_s(&f, sourceFile, "r");

	fseek(f, 0, SEEK_END);
	len = ftell(f);
	fseek(f, 0, SEEK_SET);

	buffer = (char *)malloc(len);
	memset( buffer, 0, len);
	fread(buffer, len, 1, f); //read into buffer
	fclose(f);

	return buffer;
}