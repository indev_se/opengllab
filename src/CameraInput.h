#pragma once
#include "GLCamera.h"
#include "InputManager.h"

#define CAMERA_UNITS_PER_SEC METER

class CameraInput
{
public:
	CameraInput(void);
	~CameraInput(void);

	void SetCamera( GLCamera *Camera );
	GLCamera *GetCamera();

	void Update( float tickMS );

protected:
	GLCamera *m_pCamera;
	InputManager *m_pInputManger;

	void CheckKeys( float tickMS );
	void CheckMouse( float tickMS );
};

