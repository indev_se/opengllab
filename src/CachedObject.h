#pragma once

#define CACHE_UNKNOWN 0
#define CACHE_STATICMESH 1
#define CACHE_ANIMATEDMESH 2
#define CACHE_MODELRENDERBATCH 3

class CachedObject
{
public:
	CachedObject(void);
	~CachedObject(void);

	virtual bool LoadFromCache( const char *cachedFilename ) = 0;

protected:

	virtual void SaveCachedVersion( const char *filename ) = 0;

	uint32 CacheType;
};

