#include "StdAfx.h"
#include "TextureAsset.h"

static TextureInfo* defaultTextureInfo = nullptr;

TextureAsset::TextureAsset(void)
{
	if ( defaultTextureInfo == nullptr )
	{
		defaultTextureInfo = new TextureInfo();
		defaultTextureInfo->genMipMap = true;

		TextureInfoParam infoParam1 = { GL_TEXTURE_WRAP_S, GL_REPEAT };
		defaultTextureInfo->params.push_back( &infoParam1 );
		TextureInfoParam infoParam2 = { GL_TEXTURE_WRAP_T, GL_REPEAT };
		defaultTextureInfo->params.push_back( &infoParam2 );
		TextureInfoParam infoParam3 = { GL_TEXTURE_MAG_FILTER, GL_LINEAR };
		defaultTextureInfo->params.push_back( &infoParam3 );
		TextureInfoParam infoParam4	= { GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR };
		defaultTextureInfo->params.push_back( &infoParam4 );
	}
}


TextureAsset::~TextureAsset(void)
{
	glDeleteTextures( 1, &m_TextureID );
}

void TextureAsset::CreateFromBitmapData( BitmapDataAsset *bitmap, TextureInfo *inf )
{
	if ( bitmap == NULL )
	{
		StdConsole::LogError( _STR"TextureAsset::CreateFromBitmapData - Bitmap is NULL");
		return;
	}

	m_Info = inf == nullptr ? defaultTextureInfo : inf;

	glGenTextures( 1, &m_TextureID );

	glBindTexture( inf->target, m_TextureID );

	std::list<TextureInfoParam *>::iterator iter;
	TextureInfoParam *param;

	for(iter = inf->params.begin(); iter != inf->params.end(); iter++) 
	{
		param = (TextureInfoParam*)(*iter);
		glTexParameteri( inf->target, param->paramName, param->paramValue );
	}

	if ( inf->genMipMap )
	{
		if ( glewIsSupported("GL_VERSION_3_0 GL_ARB_framebuffer_object") ) {
			// Do this after glTexImage2D instead
		}
		else if ( glewIsSupported("GL_VERSION_1_4") ) {
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE); //Requires GL 1.4. Deprecated in GL 3.0 and above.
		}
	}
	
	m_TextureFormat = inf->format == 0 ? bitmap->GetGLDestFormat() : inf->format;
	glTexImage2D(GL_TEXTURE_2D, 0, m_TextureFormat, bitmap->Width(), bitmap->Height(), 0, bitmap->GetGLFormat(), inf->valueType, bitmap->Bits());

	if ( inf->genMipMap )
	{
		if ( glewIsSupported("GL_VERSION_3_0 GL_ARB_framebuffer_object") ) {
			glEnable(GL_TEXTURE_2D); // ati requires this
			glGenerateMipmap(inf->target); // 3.1 and above
		}
	}

	glBindTexture( inf->target, 0 );
}

void TextureAsset::CreateEmpty( unsigned int Width, unsigned int Height, GLint format, TextureInfo *inf )
{
	m_Info = inf;

	glGenTextures( 1, &m_TextureID );

	glBindTexture( inf->target, m_TextureID );
	
	std::list<TextureInfoParam *>::iterator iter;
	TextureInfoParam *param;

	for(iter = inf->params.begin(); iter != inf->params.end(); iter++) 
	{
		param = (TextureInfoParam*)(*iter);
		glTexParameteri( inf->target, param->paramName, param->paramValue );
	}

	// No mipmap support for empty texture, nothing to generate from

	if ( inf->format == 0 )
	{
		StdConsole::LogError( _STR"No texture format specified");
		return;
	}

	m_TextureFormat = inf->format;
	glTexImage2D(GL_TEXTURE_2D, 0, inf->format, Width, Height, 0, format, inf->valueType, 0);

	glBindTexture( inf->target, 0 );
}

void TextureAsset::Bind( GLenum texture )
{
	glActiveTexture(texture);
	glBindTexture(GL_TEXTURE_2D, m_TextureID);

	if ( m_TextureFormat == GL_RGBA )
	{
		glEnable(GL_BLEND);
		glAlphaFunc(GL_GREATER, 0.6f);
		glEnable(GL_ALPHA_TEST) ;
	}
}

void TextureAsset::Unbind( GLenum texture )
{
	glActiveTexture(texture);
	glBindTexture(GL_TEXTURE_2D, 0);

	if ( m_TextureFormat == GL_RGBA )
	{
		glDisable(GL_BLEND);
		glDisable(GL_ALPHA_TEST) ;
	}
}