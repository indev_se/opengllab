#include "StdAfx.h"
#include "GLGameWindow.h"
#include "InputManager.h"
#include "SceneManager.h"
#include "TextObject.h"
#include "GameApplication.h"


GameApplication::GameApplication(void)
{
}


GameApplication::~GameApplication(void)
{
}

void GameApplication::Run( GLGameWindow *gameWindow, GLScene *glScene )
{
	m_pGameWindow = gameWindow;
	m_pGLScene = glScene;
	SceneManager::Inst()->SetScene( m_pGLScene );

	m_pGameWindow->SetResizeEventListener( this, &GLGameWindowEventListener::Event_Resize );

	StdConsole::AddCommandListener( glScene, &StdConsoleEventListener::Event_ProcessCommand );

	m_pGLScene->SetViewport( gameWindow->Width(), gameWindow->Height() );
	m_pGLScene->Init();

	m_pTimer = new GameTimer();
	m_pTimer->Init();
	m_pTimer->Tick();

	char timeStr[128];
	int lastTickMs = 0, curTickMs = 0;

	MSG msg;
	bool done = false;

	while(!done)
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if (msg.message==WM_QUIT)
			{
				done = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{

			if ( InputManager::Inst()->GetKey(VK_ESCAPE) )
			{
				done = true;
			}
			else
			{
				
				m_pTimer->Tick();
				
				curTickMs = (int)(m_pTimer->GetDelta() * 1000);
				if ( lastTickMs != curTickMs )
				{
					lastTickMs = curTickMs;
					sprintf_s( timeStr, 128, "FPS: %d\nMS: %d", m_pTimer->GetFPS(), lastTickMs );
					if ( SceneManager::Inst()->GetSceneObject( "timeText" ) != nullptr )
						((TextObject*)SceneManager::Inst()->GetSceneObject( "timeText" ))->SetText(timeStr);
				}
				
				m_pGLScene->Update( m_pTimer->GetDelta() );

				m_pGLScene->Render();

				SwapBuffers( gameWindow->GetHDC() ); // flip
			}
		}
	}
}

void GameApplication::Event_Resize( int Width, int Height )
{
	m_pGLScene->SetViewport( Width, Height );
}