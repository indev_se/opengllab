#pragma once
class AnimationState;

class AnimationStateEventListener
{
public:
	AnimationStateEventListener(void);
	~AnimationStateEventListener(void);

	virtual void Event_AnimationEndReached( AnimationState *state ) {}
};

