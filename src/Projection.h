#pragma once
class Projection
{
public:
	Projection(void);
	~Projection(void);

	void SetPerspective( float Fov, float AspectRatio, float zNear, float zFar );
	void SetNearFar( float zNear, float zFar );
	void SetZNear( float zNear );
	void SetZFar( float zFar );
	void SetFov( float Fov );
	void SetAspectRatio( float AspectRatio );

	glm::mat4x4 GetMatrix();
	
	inline float GetFov() { return m_Fov; }
	inline float GetAspectRatio() { return m_AspectRatio; }
	inline float GetZNear() { return m_zNear; }
	inline float GetZFar() { return m_zFar; }

	Projection* Clone();

private:

	float m_Fov;
	float m_AspectRatio;
	float m_zNear;
	float m_zFar;

	glm::mat4x4 m_matProjection;
};

