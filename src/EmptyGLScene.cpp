#include "StdAfx.h"
#include "EmptyGLScene.h"


EmptyGLScene::EmptyGLScene(void)
{
}


EmptyGLScene::~EmptyGLScene(void)
{
}

void EmptyGLScene::SetViewport( int Width, int Height )
{
	GLScene::SetViewport( Width, Height );
}

void EmptyGLScene::Init()
{
	GLScene::Init();
	
	// Create the camera
	m_pViewportCamera = new GLCamera();
	m_pViewportCamera->Init();

	if ( m_iWidth != 0 && m_iHeight != 0)
		m_pViewportCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );
	else
		m_pViewportCamera->SetPerspective( 45.0f, 4.f/3.f, 1.0f, 4000.0f );
}

void EmptyGLScene::GetBounds( glm::vec3 &min, glm::vec3 &max )
{
	min = glm::vec3(-2000.f, -200.f, -2000.f);
	max = glm::vec3(2000.f, 2000.f, 2000.f);
	//m_RootObject->GetBounds(min, max);
}

void EmptyGLScene::Update( float tickMS )
{
}

void EmptyGLScene::Render()
{
	Clear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	
	AssertGLError();
}