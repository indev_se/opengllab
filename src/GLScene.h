#pragma once
#include "GLCamera.h"
#include "Projection.h"
#include "AssetsManager.h"

class SceneObject;

class GLScene : 
	public StdConsoleEventListener
{
public:
	GLScene(void);
	~GLScene(void);

	virtual void SetViewport( int Width, int Height );
	virtual void Clear( GLbitfield bufferMask );

	virtual void Init();
	virtual void Update( float tickMS ) {;}
	virtual void Render() {;}

	void AssertGLError();

	inline int Width() { return m_iWidth; }
	inline int Height() { return m_iHeight; }

	inline GLCamera* GetCamera() { return m_pViewportCamera; }
	
	inline AssetsManager *GetAssetsManager() { return m_AssetsManager; }

	glm::mat4x4 GetProjectionMatrix() { return m_pViewportCamera->GetProjection()->GetMatrix(); }
	Projection *GetProjection() { return m_pViewportCamera->GetProjection(); }

	SceneObject *GetRootDebugObject() { return m_RootDebugObject; }

	virtual void GetBounds( glm::vec3 &min, glm::vec3 &max );

protected:

	void InitGLEWExtension();

	GLCamera *m_pViewportCamera;
	
	AssetsManager *m_AssetsManager;

	int m_iWidth, m_iHeight;
	
	SceneObject *m_RootDebugObject;

public:
	bool Event_ProcessCommand( std::string command, std::string &response );
};

