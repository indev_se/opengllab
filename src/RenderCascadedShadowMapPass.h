#pragma once
#include "RenderPass.h"
#include "LightObject.h"
#include "CascadedShadowMap.h"

class RenderCascadedShadowMapPass :
	public RenderPass
{
public:
	RenderCascadedShadowMapPass(void);
	~RenderCascadedShadowMapPass(void);

	void Init( LightObject *light, GLScene *scene, uint32 NumSplits );
	void Bind();
	void Unbind();

	void BindObject( SceneObject *object );
	void UnbindObject( SceneObject *object );

	void BindJob( RenderJob *job );
	void UnbindJob( RenderJob *job );

	void SetSplitIndex( uint32 SplitIndex );
	uint32 GetNumSplits();

	CascadedShadowMap *GetShadowMap() { return m_pShadowMap; }

protected:

	CascadedShadowMap *m_pShadowMap;

	uint32 m_CurrentBindSplit;
};

