#include "StdAfx.h"
#include "SceneObjectFactory.h"
#include "InputManager.h"
#include "UserPlayerObject.h"


UserPlayerObject::UserPlayerObject( const char *objectName ) : 
	SceneObject(objectName)
{
	m_PendingState = STATE_NONE;
	m_pPhysics = nullptr;
}

UserPlayerObject::~UserPlayerObject(void)
{
	if( m_pPhysics != nullptr )
		delete m_pPhysics;

	m_pPhysics = nullptr;
}

void UserPlayerObject::Init( GLScene *scene )
{
	SceneObject::Init(scene, nullptr);
	
	m_pModel = SceneObjectFactory::Inst()->CreateAnimatedMeshObject("player", "Data/Models/Characters/player01.fbx", m_pScene);
	this->AddChild(m_pModel);

	m_pModel->SetScale( glm::vec3( METER * .5, METER * .5, METER * .5 ) );

	m_pCurrentAction = new UserPlayerAction(STATE_IDLE);

	m_pModel->SetAnimationByName( m_pCurrentAction->GetAnimationName(), true);
	m_pModel->GetAnimationState()->Play();

	InputManager::Inst()->AddKeyDownListener(this, &InputEventListener::Event_KeyDown);
	InputManager::Inst()->AddKeyUpListener(this, &InputEventListener::Event_KeyUp);

	m_pPhysics = new PlayerPhysics();
	m_pPhysics->Init( m_vecPosition );
}

void UserPlayerObject::Event_AnimationEndReached( AnimationState *state )
{
	m_PendingState = STATE_IDLE;
}

void UserPlayerObject::Update( float tickMS )
{
	if ( m_PendingState != STATE_NONE ) {
		SetPlayerState(m_PendingState);
		m_PendingState = STATE_NONE;
	}

	m_pModel->GetAnimationState()->Update(tickMS);

	CheckInput();

	m_pPhysics->Simulate( tickMS );

	UpdateMovement();

	SceneObject::Update(tickMS);
}

void UserPlayerObject::Render(RenderPass *pass)
{
	SceneObject::Render(pass);
}

void UserPlayerObject::SetPlayerState( PlayerActionState state )
{
	if ( state != m_pCurrentAction->GetState() ) {

		// should queue and stuff here later on
		delete m_pCurrentAction;

		m_pCurrentAction = new UserPlayerAction(state);

		// nasty and ugly :)
		delete m_pModel->GetAnimationState();

		m_pModel->SetAnimationByName( m_pCurrentAction->GetAnimationName(), m_pCurrentAction->IsEndless() );
		if ( m_pCurrentAction->IsEndless() == false )
			m_pModel->GetAnimationState()->SetEventEndReachedListener( this, &AnimationStateEventListener::Event_AnimationEndReached);
		m_pModel->GetAnimationState()->Play();
	}
}

void UserPlayerObject::Event_KeyUp( uint16 key )
{
	if ( key == VK_UP ) {
		m_pPhysics->StopRun();
		SetPlayerState( PlayerActionState::STATE_IDLE );
	}
}

void UserPlayerObject::Event_KeyDown( uint16 key )
{
	InputManager* input = InputManager::Inst();

	if ( key == VK_UP ) {
		glm::vec3 dir = glm::vec3( glm::toMat4(m_quatRotY) * glm::vec4( 0.0f, 0.0f, 1.0f, 1.0f ) );

		m_pPhysics->StartRun(dir);

		SetPlayerState( PlayerActionState::STATE_RUN_FORWARD );
	}

	if ( key == VK_CONTROL ) {
		m_pPhysics->DoJump();
		SetPlayerState( PlayerActionState::STATE_JUMP );
	}
}

void UserPlayerObject::CheckInput()
{
	InputManager* input = InputManager::Inst();
	
	if ( input->GetKey( VK_LEFT ) )
	{
		TurnLeft();
		if ( input->GetKey( VK_UP ) )
			m_pPhysics->ChangeDirection( glm::vec3( glm::toMat4(m_quatRotY) * glm::vec4( 0.0f, 0.0f, 1.0f, 1.0f ) ) );
	}
	else if ( input->GetKey( VK_RIGHT ) )
	{
		TurnRight();
		if ( input->GetKey( VK_UP ) )
			m_pPhysics->ChangeDirection( glm::vec3( glm::toMat4(m_quatRotY) * glm::vec4( 0.0f, 0.0f, 1.0f, 1.0f ) ) );
	}
	
}

void UserPlayerObject::UpdateMovement()
{
	glm::vec3 position = m_pPhysics->GetPosition();

	SetTransformMatrix( glm::translate( position.x, position.y, position.z ) * glm::toMat4(m_quatRotY) );
}

void UserPlayerObject::RunForward()
{
	//m_movementVector += m_quatRotY * glm::vec3(AXIS_Z * 4.0f);
}

void UserPlayerObject::RunBackward()
{
	//m_movementVector += m_quatRotY * glm::vec3(AXIS_Z * -4.0f);
}

void UserPlayerObject::TurnLeft()
{
	m_directionVector += 0.4f * AXIS_Y;
	m_quatRotY = glm::angleAxis( m_directionVector.y, AXIS_Y );
}

void UserPlayerObject::TurnRight()
{
	m_directionVector -= 0.4f * AXIS_Y;
	m_quatRotY = glm::angleAxis( m_directionVector.y, AXIS_Y );
}