#pragma once
#include "InputEventListener.h"

#define KEY_W 87
#define KEY_S 83
#define KEY_A 65
#define KEY_D 68

#define KEY_O 79
#define KEY_P 80

typedef std::pair< InputEventListener*, InputEventKeyFuncPtr> EventKeyPair;
typedef std::vector< EventKeyPair > EventKeyListeners;

typedef std::pair< InputEventListener*, InputEventDeltaFuncPtr> EventDeltaPair;
typedef std::vector< EventDeltaPair > EventDeltaListeners;

class InputManager
{
public:
	~InputManager(void);
	static InputManager* Inst();

	LRESULT ProcessMessage( UINT uMsg, WPARAM wParam, LPARAM lParam );

	inline bool GetKey( BYTE key ) { return m_bKeys[key]; }
	void GetCurrentMousePos( int &X, int &Y );
	
	void SetWindowSize( int Width, int Height );
	void SetWindowPosition( int PosX, int PosY );

public:

	void AddKeyUpListener( InputEventListener*, InputEventKeyFuncPtr );
	void RemoveKeyUpListener( InputEventListener*, InputEventKeyFuncPtr );
	
	void AddKeyDownListener( InputEventListener*, InputEventKeyFuncPtr );
	void RemoveKeyDownListener( InputEventListener*, InputEventKeyFuncPtr );

	void AddDeltaListener( InputEventListener*, InputEventDeltaFuncPtr );
	void RemoveDeltaListener( InputEventListener*, InputEventDeltaFuncPtr );

	EventKeyListeners m_Listeners_KeyUp;
	EventKeyListeners m_Listeners_KeyDown;
	EventDeltaListeners m_Listeners_Delta;

	void DispatchKeyUpEvent( uint16 );
	void DispatchKeyDownEvent( uint16 );
	void DispatchDeltaEvent( uint16, uint16 );
	
protected:
	InputManager(void);

	bool m_bKeys[256];

	int m_iCurrentMouseX;
	int m_iCurrentMouseY;

	int m_iWindowWidth;
	int m_iWindowHeight;

	int m_iWindowPosX;
	int m_iWindowPosY;

	int m_iWindowCenterX;
	int m_iWindowCenterY;

	bool m_bIsMouseNavigationActive;
	bool m_bIgnoreMouseMoveEvent;

private:
	static InputManager* m_pInst;


};

