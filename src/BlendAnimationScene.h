#pragma once
#include "GLScene.h"

class CameraInput;
class MaterialImporter;
class ForwardRenderPass;
class AnimationStateEventListener;

class BlendAnimationScene :
	public GLScene,
	public AnimationStateEventListener
{
public:
	BlendAnimationScene(void);
	~BlendAnimationScene(void);

	void SetViewport( int Width, int Height );

	void Init();
	void Update( float tickMS );
	void Render();

	void GetBounds( glm::vec3 &min, glm::vec3 &max );

	void Event_AnimationEndReached( AnimationState *state );

protected:
	GLCamera *m_pDefaultFPSCamera;
	CameraInput *m_pCameraInput;
	
	MaterialImporter *m_MaterialImporter;

	SceneObject *m_RootObject;
	SceneObject *m_RootLightObject;

	ForwardRenderPass *passForward;
	
public:
	bool Event_ProcessCommand( std::string command, std::string &response );
};

