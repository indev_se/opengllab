#include "StdAfx.h"
#include "DeferredFramebuffers.h"


DeferredFramebuffers::DeferredFramebuffers(void)
{
	m_pDepthTexture = NULL;
	m_pColorTexture = NULL;
	m_pNormalTexture = NULL;
	m_pPositionTexture = NULL;
}


DeferredFramebuffers::~DeferredFramebuffers(void)
{
	if ( m_pDepthTexture != NULL )
		delete m_pDepthTexture;
	if ( m_pColorTexture != NULL )
		delete m_pColorTexture;
	if ( m_pNormalTexture != NULL )
		delete m_pNormalTexture;
	if ( m_pPositionTexture != NULL )
		delete m_pPositionTexture;

	m_pDepthTexture = NULL;
	m_pColorTexture = NULL;
	m_pNormalTexture = NULL;
	m_pPositionTexture = NULL;

	glDeleteFramebuffers(1, &m_FBOID);
}

void DeferredFramebuffers::Create2D( unsigned int Width, unsigned int Height )
{
	m_Width = Width;
	m_Height = Height;

	m_pDepthTexture = CreateDepthTexture();
	m_pColorTexture = CreateColorTexture();
	m_pNormalTexture = CreateColorTexture();
	m_pPositionTexture = CreateColorTexture();
	
	// Create the Framebuffer object
	glGenFramebuffers(1, &m_FBOID);
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBOID );

	// Instruct openGL that we won't bind a color texture with the currently binded FBO
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_pDepthTexture->TextureID(), 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_pColorTexture->TextureID(), 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_pNormalTexture->TextureID(), 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_pPositionTexture->TextureID(), 0);
	
	// Check status to make sure everything went ok
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if ( status != GL_FRAMEBUFFER_COMPLETE ) {
		StdConsole::LogError( _STR"GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO");
	}

	// Switch back to the normal framebuffer (window)
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

TextureAsset *DeferredFramebuffers::CreateDepthTexture()
{
	TextureAsset *texture = new TextureAsset();

	TextureInfo *info = new TextureInfo();
	info->genMipMap = false;
	info->format = GL_DEPTH_COMPONENT32;
	info->valueType = GL_UNSIGNED_INT;

	TextureInfoParam infoParam1 = { GL_TEXTURE_WRAP_S, GL_CLAMP };
	info->params.push_back( &infoParam1 );
	TextureInfoParam infoParam2 = { GL_TEXTURE_WRAP_T, GL_CLAMP };
	info->params.push_back( &infoParam2 );
	TextureInfoParam infoParam3 = { GL_TEXTURE_MAG_FILTER, GL_NEAREST };
	info->params.push_back( &infoParam3 );
	TextureInfoParam infoParam4	= { GL_TEXTURE_MIN_FILTER, GL_NEAREST };
	info->params.push_back( &infoParam4 );

	texture->CreateEmpty( m_Width, m_Height, GL_DEPTH_COMPONENT, info);

	return texture;
}

TextureAsset *DeferredFramebuffers::CreateColorTexture()
{
	TextureAsset *texture = new TextureAsset();

	TextureInfo *info = new TextureInfo();
	info->genMipMap = false;
	info->format = GL_RGBA16F;
	info->valueType = GL_FLOAT;

	TextureInfoParam infoParam1 = { GL_TEXTURE_WRAP_S, GL_CLAMP };
	info->params.push_back( &infoParam1 );
	TextureInfoParam infoParam2 = { GL_TEXTURE_WRAP_T, GL_CLAMP };
	info->params.push_back( &infoParam2 );
	TextureInfoParam infoParam3 = { GL_TEXTURE_MAG_FILTER, GL_NEAREST };
	info->params.push_back( &infoParam3 );
	TextureInfoParam infoParam4	= { GL_TEXTURE_MIN_FILTER, GL_NEAREST };
	info->params.push_back( &infoParam4 );

	texture->CreateEmpty( m_Width, m_Height, GL_RGBA, info);

	return texture;
}