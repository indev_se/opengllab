#include "StdAfx.h"
#include "GLCamera.h"


GLCamera::GLCamera(void)
{
	Reset();

	m_pFrustum = nullptr;
	m_pProjection = nullptr;
}


GLCamera::~GLCamera(void)
{
	if ( m_pProjection != nullptr )
		delete m_pProjection;

	if ( m_pFrustum != nullptr )
		delete m_pFrustum;

	m_pFrustum = nullptr;
	m_pProjection = nullptr;
}

void GLCamera::Init()
{
	m_pFrustum = new Frustum();
	m_pProjection = new Projection();

	Reset();
}

void GLCamera::SetPerspective( float Fov, float AspectRatio, float zNear, float zFar )
{
	m_pProjection->SetPerspective( Fov, AspectRatio, zNear, zFar );
}

void GLCamera::Reset()
{
	m_matView = glm::mat4();
	m_matRotation = glm::mat4();
	m_matTranslation = glm::mat4();

	m_vecEye = glm::vec3(0,0,0);
	m_vecDirection = glm::vec3(0,0,1);

	m_qRotation = glm::quat();
}

void GLCamera::RotateEyeByAxis( glm::vec3 &axis, float radians )
{
	m_vecDirection += axis * radians;

	InvalidateRotationQuaternions();
}

void GLCamera::InvalidateRotationQuaternions()
{
	
	glm::quat q_right = glm::angleAxis( m_vecDirection.x, 1.0f, .0f, .0f );
	glm::quat q_up = glm::angleAxis( m_vecDirection.y, .0f, 1.0f, .0f );
	glm::quat q_look = glm::angleAxis( m_vecDirection.z, .0f, .0f, 1.0f );
	
	m_qRotation = q_up * q_right; // the order here is very important
}

void GLCamera::MoveEyeByAxis( glm::vec3 &axis, float distance )
{
	glm::mat4x4 matRotation = glm::toMat4(m_qRotation);
	
	glm::vec3 move = glm::vec3( matRotation * glm::vec4( glm::vec3(axis * distance), 1.0f ) );

	m_matTranslation = glm::translate(m_matTranslation, move);
	m_vecEye += move;
}

glm::mat4x4 GLCamera::GetViewNormalMatrix()
{
	glm::mat4x4 matRotation = glm::toMat4(m_qRotation);
	matRotation = glm::inverse(matRotation);

	return matRotation;
}

glm::mat4x4 GLCamera::GetViewMatrix()
{
	glm::mat4x4 matRotation = glm::toMat4(m_qRotation);
	m_matView = glm::inverse(matRotation) * m_matTranslation;

	return m_matView;
}

Projection *GLCamera::GetProjection()
{
	return m_pProjection;
}

Frustum* GLCamera::GetFrustum()
{
	m_pFrustum->SetFrustum( m_pProjection->GetMatrix() * this->GetViewMatrix() );

	return m_pFrustum;
}