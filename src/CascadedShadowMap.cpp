#include "StdAfx.h"
#include "DirectionalLight.h"
#include "CascadedShadowMap.h"


CascadedShadowMap::CascadedShadowMap(void)
{
	m_bIsAtlas = false;
	m_pFBOAtlas = nullptr;

	m_AtlasOffsets[0] = glm::vec2(0.0f,0.0f);
	m_AtlasOffsets[1] = glm::vec2(0.5f,0.0f);
	m_AtlasOffsets[2] = glm::vec2(0.0f,0.5f);
	m_AtlasOffsets[3] = glm::vec2(0.5f,0.5f);
}

CascadedShadowMap::~CascadedShadowMap(void)
{
	m_TextureMatrices.clear();
	m_ProjectionMatrices.clear();
	m_ViewMatrices.clear();

	for ( uint32 i = 0; i < m_FBOs.size(); ++i )
		delete m_FBOs[i];

	if ( m_pFBOAtlas != nullptr )
		delete m_pFBOAtlas;

	m_FBOs.clear();
	m_pFBOAtlas = nullptr;
}

void CascadedShadowMap::Create( LightObject *light, GLScene *scene, uint32 numSplits, unsigned int FBWidth, unsigned int FBHeight, bool useAtlas )
{
	m_pLight = light;
	m_pScene = scene;
	m_bIsAtlas = useAtlas;

	m_NumSplits = numSplits;

	memset(m_fDepths, 0, sizeof(float)*MAX_NUM_SHADOW_CASCADES);
	memset(m_fClipDepths, 0, sizeof(float)*MAX_NUM_SHADOW_CASCADES);
	
	m_TextureMatrices.resize(m_NumSplits);
	m_ProjectionMatrices.resize(m_NumSplits);
	m_ViewMatrices.resize(m_NumSplits);
	m_CropMatrices.resize(m_NumSplits);

	m_pDebugCubes.resize(m_NumSplits);
	m_LightMinVolumes.resize(m_NumSplits);
	m_LightMaxVolumes.resize(m_NumSplits);

	switch ( m_pLight->GetType() )
	{
		case LIGHT_SPOT:
		{
			StdConsole::LogWarning( _STR"Cascaded shadow mapping with Point light not implemented yet");
		} break;

		case LIGHT_DIRECTIONAL:
		{
			if ( m_bIsAtlas ) {
				m_pFBOAtlas = new DepthFramebufferObject();
				m_pFBOAtlas->Create2D( FBWidth, FBHeight );
			}
			else {
				for ( uint32 i = 0; i < m_NumSplits; ++i ) {
					m_FBOs.push_back( new DepthFramebufferObject() );
					m_FBOs[i]->Create2D( FBWidth, FBHeight );
				}
			}

		} break;

		case LIGHT_POINT:
		{
			StdConsole::LogWarning( _STR"Cascaded shadow mapping  with Point light not implemented yet");
		} break;

		default: 
		{
			StdConsole::LogError( _STR"CascadedShadowMap::SetLight, unknown light type");
		} break;
	}
}

void CascadedShadowMap::BindFramebuffer(uint32 SplitIndex)
{
	m_BoundSplit = SplitIndex;

	DepthFramebufferObject *fbo = m_bIsAtlas ? m_pFBOAtlas : m_FBOs[SplitIndex];

	// Bind the Framebuffer and make sure it succeded
	if ( m_bIsAtlas ) {
		if ( SplitIndex == 0 ) // Only want to bind this once
			glBindFramebuffer(GL_FRAMEBUFFER, fbo->GetFBOID());
	}
	else {
		glBindFramebuffer(GL_FRAMEBUFFER, fbo->GetFBOID());

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if ( status != GL_FRAMEBUFFER_COMPLETE ) {
			StdConsole::LogError( _STR"GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO");
			return;
		}
	}

	// Set the viewport to the size of the Framebuffer
	glm::vec2 offset;
	if ( m_bIsAtlas ) {
		offset = m_AtlasOffsets[SplitIndex];
		glViewport(fbo->Width() * offset.x, fbo->Height()*0.5 - (fbo->Height() * offset.y), fbo->Width() * .5f, fbo->Height() * .5f);

		if ( SplitIndex == 0 ) {
			glClear(GL_DEPTH_BUFFER_BIT);
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
			glShadeModel(GL_FLAT);
		}
	}
	else {
		glViewport(0,0,fbo->Width(),fbo->Height());
		glClear(GL_DEPTH_BUFFER_BIT);
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glShadeModel(GL_FLAT);
	}

	// Calculate the split depths before calculating the light values
	Projection *projection = m_pScene->GetProjection();
	float zNear = projection->GetZNear();
	float zFar = projection->GetZFar();

	m_fDepths[0] = 500.f;
	m_fDepths[1] = 1000.f;
	m_fDepths[2] = 2000.f;
	m_fDepths[3] = 4000.f;
	
	float f = 1.0f;
	for ( uint32 i = 0; i < MAX_NUM_SHADOW_CASCADES-1; ++i, f += 1.0f ) {
		m_fDepths[i] = LERP( zNear + (f/MAX_NUM_SHADOW_CASCADES) * (zFar-zNear),
							 zNear * powf( zFar / zNear, f/MAX_NUM_SHADOW_CASCADES ),
							 0.5);
	}
	m_fDepths[MAX_NUM_SHADOW_CASCADES-1] = zFar;

	switch ( m_pLight->GetType() )
	{
		case LIGHT_SPOT:
		{
			StdConsole::LogWarning( _STR"Cascaded shadow mapping with Point light not implemented yet" );
		} break;

		case LIGHT_DIRECTIONAL:
		{
			SetDirectionalLightValues(SplitIndex);
		} break;

		case LIGHT_POINT:
		{
			StdConsole::LogWarning( _STR"Cascaded shadow mapping  with Point light not implemented yet");
		} break;

		default: 
		{
			StdConsole::LogError( _STR"CascadedShadowMap::SetLight, unknown light type");
		} break;
	}

	//glPolygonOffset(4.8f,4.0f); // Magic values, these should not be static here
	glPolygonOffset(4.8f,4.0f); // Magic values, these should not be static here
	glEnable(GL_POLYGON_OFFSET_FILL);

	CalculateTextureMatrix(SplitIndex);
}

void CascadedShadowMap::UnbindFramebuffer()
{
	// Disable the polygon offset
	glDisable(GL_POLYGON_OFFSET_FILL);

	// Unbind the Framebuffer
	if ( m_bIsAtlas ) {
		// Only unbind if we're done with the last split
		if ( m_BoundSplit < m_NumSplits-1 )
			return;
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Enable colorwrite that was disabled by the Shadowmap
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	// Reset the viewport
	glViewport(0,0,m_pScene->Width(),m_pScene->Height());

	m_BoundSplit = 0;
}

void CascadedShadowMap::SetDirectionalLightValues(uint32 SplitIndex)
{
	Projection *projection = m_pScene->GetProjection();
	float zNear = projection->GetZNear();
	float zFar = projection->GetZFar();

	// set the new near and far plane values
	zFar = m_fDepths[SplitIndex];
	if ( SplitIndex > 0 )
		zNear = m_fDepths[SplitIndex-1];

	Projection *splitProjection = projection->Clone();
	splitProjection->SetNearFar( zNear, zFar );

	// Calculate the split index in camera homogeneous coordinates as we need to send this to the shader
	glm::mat4x4 camProj = projection->GetMatrix(); 
	glm::vec4 zFarClip =  camProj * glm::vec4(.0f,.0f,zFar,1.0);
	float tmpZFar = (zFarClip.z / zFarClip.w);
	
	//tmpZFar = 0.5f * (-zFar*camProj[2][2]+camProj[2][3])/zFar + 0.5f;
	tmpZFar = 0.5f * (-zFar*camProj[2][2]+camProj[3][2])/zFar + 0.5f; // cam_proj * (0, 0, far_z, 1)^t and then normalize to [0; 1]

	m_fClipDepths[SplitIndex] = tmpZFar;

	// Now that we have the projection for this split we can calculate the lights transform and projection
	glm::mat4x4 cameraViewMatrix = m_pScene->GetCamera()->GetViewMatrix();
	glm::vec3 cameraDirection;
	m_pScene->GetCamera()->GetDirection( cameraDirection );

	Frustum frustum;
	frustum.SetFrustum( splitProjection->GetMatrix() * cameraViewMatrix );

	BoundingBox *bb = frustum.GetBB();
	glm::vec3 bbMin, bbMax;
	bb->GetBounds(bbMin, bbMax);

	// Find the center of the Frustum
	glm::vec3 frustumCenter(0.0,0.0,0.0);
	frustumCenter = bbMin + (bbMax - bbMin) * .5f; //bbMax - bbMin;

	float nearPlaneZOffset = 0.0f;
	float viewDistanceFromCenter = (2000.f ) + nearPlaneZOffset;

	glm::vec3 lightDirection;
	((DirectionalLight*)m_pLight)->GetDirection(lightDirection);

	// Calculate the up vector
	glm::vec3 up(0.0f,1.0f,0.0f);
	if ( fabs( glm::dot(up,lightDirection)) >= 0.99f)
		up = glm::vec3(0.0f,0.0f,1.0f);

	glm::vec3 right = glm::cross( lightDirection, up );
	right = glm::normalize(right);
	up = glm::normalize( glm::cross( right, lightDirection ) );
	
	glm::mat4x4 lightViewMatrix = glm::lookAt( frustumCenter - (lightDirection * viewDistanceFromCenter), frustumCenter, up );
	
	// Convert the frustum bb to light space
	glm::vec4 lsMax, lsMin;
	lsMin = glm::vec4( lightViewMatrix * glm::vec4(bbMin, 1.0f) );
	lsMax = glm::vec4( lightViewMatrix * glm::vec4(bbMax, 1.0f) );

	// these will represent the shadow casters bb in world coords
	glm::vec3 zMin(.0f);// = glm::vec3(-1.0f, -8000.f,-1.0f);
	glm::vec3 zMax(.0f);// = glm::vec3(1.0f, 8000.f, 1.0f);

	m_pScene->GetBounds(zMin, zMax);

	zMin = glm::vec3( lightViewMatrix * glm::vec4(zMin, 1.0f) );
	zMax = glm::vec3( lightViewMatrix * glm::vec4(zMax, 1.0f) );

	//glm::mat4x4 lightProjMatrix = glm::ortho(lsMin.x, lsMax.x, lsMin.y, lsMax.y, -zMax.z, -zMin.z);
	glm::mat4x4 lightProjMatrix = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, -zMax.z, -zMin.z);
	
	// This has hardcoded scaling and is not beatiful, should take the world bb into consideration
	float scaleX = (2.0f / (lsMax.x - lsMin.x)) * .5f;
	float scaleY = (2.0f / (lsMax.x - lsMin.x)) * .5f; // this is cheating.. but what the hell :)
	float offsetX = -.5f * (lsMax.x + lsMin.x) * scaleX;
	float offsetY = -.5f * (lsMax.x + lsMin.x) * scaleY; // this is cheating.. but what the hell :)
	glm::mat4x4 cropMatrix;
	cropMatrix[0][0] = scaleX;
	cropMatrix[1][1] = scaleY;
	cropMatrix[0][3] = offsetX;
	cropMatrix[1][3] = offsetY;
	
	cropMatrix = glm::transpose(cropMatrix);

	// Store the perspective and view matrix
	m_ViewMatrices[SplitIndex] = lightViewMatrix;
	m_ProjectionMatrices[SplitIndex] = lightProjMatrix * cropMatrix;
	m_CropMatrices[SplitIndex] = cropMatrix;

	// Store the volume values so that we can render them later
	m_LightMinVolumes[SplitIndex] = glm::vec3(lsMin.x, lsMin.y, zMin.z);
	m_LightMaxVolumes[SplitIndex] = glm::vec3(lsMax.x, lsMax.y, zMax.z);
	
	// clean up
	delete splitProjection;
}

void CascadedShadowMap::CalculateTextureMatrix( uint32 SplitIndex )
{
	// This is matrix transform every coordinate x,y,z
	// x = x* 0.5 + 0.5 
	// y = y* 0.5 + 0.5 
	// z = z* 0.5 + 0.5 
	// Moving from unit cube [-1,1] to [0,1]  
	glm::mat4x4 bias(	
		0.5, 0.0, 0.0, 0.0, 
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0);
	
	// store the original textureMatrix
	m_TextureMatrices[SplitIndex] = bias * m_ProjectionMatrices[SplitIndex] * m_ViewMatrices[SplitIndex];
}

void CascadedShadowMap::CreateDebugVolumes()
{
	uint32 i;
	// Clear the old cube objects before we create new ones
	for ( i = 0; i < m_pDebugCubes.size(); ++i ) {
		if ( m_pDebugCubes[i] != nullptr )
			delete m_pDebugCubes[i];
	}


	glm::vec3 vmin, vmax;
	float tmpz;
	BoundingBox *bb;
	for ( i = 0; i < m_pDebugCubes.size(); ++i )
	{
		vmin = m_LightMinVolumes[i];
		vmax = m_LightMaxVolumes[i];
		
		tmpz = vmin.z;
		vmin.z = -m_LightMaxVolumes[i].z;
		vmax.z = -tmpz;
		
		bb = new BoundingBox(vmin, vmax);

		m_pDebugCubes[i] = bb;
	}
}

void CascadedShadowMap::RenderDebug()
{
	uint32 i;
	
	int width = m_pScene->Width();
	int height = m_pScene->Height();

	float widthOffset = (float) width / 4.f;
	float heightOffset = (float) height / (float) m_NumSplits;

	glUseProgramObjectARB(0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,width,0,height,1,20);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor4f(1,1,1,1);
	glEnable(GL_TEXTURE_2D);
	glActiveTextureARB(GL_TEXTURE0);

	if ( m_bIsAtlas )
	{
		glBindTexture(GL_TEXTURE_2D, m_pFBOAtlas->GetTextureId());
	
		glLoadIdentity();
		glTranslated( width * .5f, 0.0f, -1);

		glBegin(GL_QUADS);
		glTexCoord2d(0,0);glVertex3f(0,0,0);
		glTexCoord2d(1,0);glVertex3f( width * .5f , 0, 0);
		glTexCoord2d(1,1);glVertex3f( width * .5f, height * .5f, 0);
		glTexCoord2d(0,1);glVertex3f( 0, height * .5f, 0);
	 
		glEnd();
	}
	else
	{
		for ( i = 0; i < m_NumSplits; ++i )
		{
			glBindTexture(GL_TEXTURE_2D, m_FBOs[i]->GetTextureId());
	
			glLoadIdentity();
			glTranslated( width-widthOffset, heightOffset*i ,-1);

			glBegin(GL_QUADS);
			glTexCoord2d(0,0);glVertex3f(0,0,0);
			glTexCoord2d(1,0);glVertex3f(widthOffset,0,0);
			glTexCoord2d(1,1);glVertex3f(widthOffset,heightOffset,0);
			glTexCoord2d(0,1);glVertex3f(0,heightOffset,0);
	 
			glEnd();
		}
	}
	glDisable(GL_TEXTURE_2D);
	/*
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST); 

	glClear( GL_DEPTH_BUFFER_BIT );

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glLoadMatrixf( glm::value_ptr(m_pScene->GetProjectionMatrix()) );

	// Setup the camera matrix
	glMatrixMode( GL_MODELVIEW );
	
	for ( i = 0; i < m_pDebugCubes.size(); ++i )
	{
		glLoadIdentity();

		glm::mat4x4 mat = m_pScene->GetCamera()->GetViewMatrix() * m_ViewMatrices[i];
		//glLoadMatrixf( glm::value_ptr(m_ViewMatrices[i]) );
		glLoadMatrixf( glm::value_ptr( mat ) );

		if ( m_pDebugCubes[i] )
			m_pDebugCubes[i]->Render();
	}

	glEnable(GL_DEPTH_TEST); 
	glDepthMask(GL_TRUE);
	*/
}

LightObject *CascadedShadowMap::GetLight()
{
	return m_pLight;
}

float* CascadedShadowMap::GetDepths()
{
	return m_fDepths;
}

float *CascadedShadowMap::GetClipDepths()
{
	return m_fClipDepths;
}

float CascadedShadowMap::GetMaxDepth()
{
	return std::max( m_fDepths[0], std::max( m_fDepths[1], std::max( m_fDepths[2], m_fDepths[3] ) ) );
}

glm::mat4x4 CascadedShadowMap::GetViewMatrix(uint32 SplitIndex)
{
	return m_ViewMatrices[SplitIndex];
}

glm::mat4x4 CascadedShadowMap::GetProjectionMatrix(uint32 SplitIndex)
{
	return m_ProjectionMatrices[SplitIndex];
}

glm::mat4x4 CascadedShadowMap::GetTextureMatrix(uint32 SplitIndex)
{
	return m_TextureMatrices[SplitIndex];
}

uint32 CascadedShadowMap::GetTextureId(uint32 SplitIndex)
{
	if ( m_bIsAtlas )
		return m_pFBOAtlas->GetTextureId();
	else
		return m_FBOs[SplitIndex]->GetTextureId();
}