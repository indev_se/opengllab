#include "StdAfx.h"
#include "SceneObject.h"
#include "RenderPass.h"


RenderPass::RenderPass(void)
{
	m_bBindTextures = true;
}

RenderPass::~RenderPass(void)
{
}

void RenderPass::Init( GLScene *scene )
{
	m_pScene = scene;
}

void RenderPass::RenderScreenQuad()
{
	uint32 width = m_pScene->Width();
	uint32 height = m_pScene->Height();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,width,0,height,1,20);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor4f(1,1,1,1);

	glTranslated(0,0,-1);

	glBegin(GL_QUADS);
	glTexCoord2d(0,0);glVertex3f(0,0,0);
	glTexCoord2d(1,0);glVertex3f(width,0,0);
	glTexCoord2d(1,1);glVertex3f(width,height,0);
	glTexCoord2d(0,1);glVertex3f(0,height,0);
	glEnd();
}