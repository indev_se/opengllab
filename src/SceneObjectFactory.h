#pragma once
#include "StaticMeshObject.h"
#include "AnimatedMeshObject.h"
#include "ModelRenderBatchObject.h"

class SceneObjectFactory
{
public:
	~SceneObjectFactory(void);

	static SceneObjectFactory* Inst();

	StaticMeshObject* CreateStaticMeshObject( const char *name, const char *filename, GLScene *scene );
	AnimatedMeshObject* CreateAnimatedMeshObject( const char *name, const char *filename, GLScene *scene );
	ModelRenderBatchObject* CreateModelRenderBatchObject( const char *name, const char *filename, GLScene *scene );

	std::string GetCacheFilepath(const char *filename);

protected:
	SceneObjectFactory(void);

private:
	static SceneObjectFactory* m_pInst;

	std::string GetCachedFile(const char *filename);

};

