#pragma once
#include "SceneObject.h"
#include "Mesh.h"
#include "Bone.h"
#include "GeometryBuffer.h"
#include "AnimationState.h"

class AnimatedMeshGroupObject :
	public SceneObject
{
public:
	AnimatedMeshGroupObject(const char *objectName);
	~AnimatedMeshGroupObject(void);

	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);
	
	void Create( MeshGroup *mesh, Mesh *parent, GeometryBuffer *gb );

	glm::mat4x4 GetModelMatrix();
	
	void SetAnimationState( AnimationState* state );

private:
	void ApplyAnimation();
	void RenderSkeleton( MeshAnimation *animation, uint32 frame, RenderPass *pass );

	void PreApplyAnimation();
	void PreApplyBlendAnimation();

	void PreApplyNoAnimation();

	std::vector<glm::mat4x4> m_BoneMatrices;
	std::vector< std::vector<glm::mat4x4> > m_BoneFrameMatrices;

	MeshGroup *m_pMesh;
	Mesh *m_pMeshParent;
	GeometryBuffer *m_pGB;

	AnimationState *m_pAnimationState;
};

