#pragma once
#include "FramebufferObject.h"

class EmptyFramebufferObject :
	public FramebufferObject
{
public:
	EmptyFramebufferObject(void);
	~EmptyFramebufferObject(void);

	void Create2D( unsigned int Width, unsigned int Height );

	void BindTexture( uint32 textureId, GLenum attachment );
};

