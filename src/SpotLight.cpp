#include "StdAfx.h"
#include "SceneManager.h"
#include "CubeObject.h"
#include "SpotLight.h"


SpotLight::SpotLight( const char *objectName = nullptr) : DirectionalLight(objectName)
{
	m_LightType = LIGHT_SPOT;
}


SpotLight::~SpotLight(void)
{
}

void SpotLight::Render()
{
	CubeObject *cube = (CubeObject*)SceneManager::Inst()->GetSceneObject("normalizedCube");

}

void SpotLight::SetShaderUniform( char LightVar[60], ShaderProgram *shader )
{
	char LightVarStruct[60];
	
	// LightSource.Type
	sprintf_s(LightVarStruct, "%s.Type", LightVar);
	shader->SendUniform1i( LightVarStruct, m_LightType );

	// LightSource.Position
	sprintf_s(LightVarStruct, "%s.Position", LightVar);
	shader->SendUniform3f( LightVarStruct, (float*)&m_vecPosition );
	
	// LightSource.Position
	sprintf_s(LightVarStruct, "%s.Direction", LightVar);
	shader->SendUniform3f( LightVarStruct, (float*)&m_vDirection );
	/*
	// LightSource.Color
	sprintf_s(LightVarStruct, "%s.Color", LightVar);
	shader->SendUniform3f( LightVarStruct, (float*)&Vector3f(m_vDiffuse.x, m_vDiffuse.y, m_vDiffuse.z) );
	*/
}