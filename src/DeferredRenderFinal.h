#pragma once
#include "DeferredFramebuffers.h"
#include "ColorFramebufferObject.h"
#include "ShadowMapping.h"
#include "LightObject.h"
#include "RenderPass.h"

class DeferredRenderFinal :
	public RenderPass
{
public:
	DeferredRenderFinal(void);
	~DeferredRenderFinal(void);

	void Init( GLScene *scene, DeferredFramebuffers *fbo, ColorFramebufferObject *shadowFBO, ColorFramebufferObject *lightFBO, std::vector<LightObject*> globalLights );
	void Bind();
	void Unbind();
	
	void BindObject( SceneObject *object );
	void UnbindObject( SceneObject *object );

	void BindJob( RenderJob *job ) {}
	void UnbindJob( RenderJob *job ) {}

private:

	DeferredFramebuffers *m_pFBOs;
	ColorFramebufferObject *m_pLightFBO;
	ColorFramebufferObject *m_pShadowFBO;

	TextureAsset *m_pSSAONoiseTexture;

	ShaderProgram *m_pShader;

	std::vector<LightObject*> m_GlobalLights;
};