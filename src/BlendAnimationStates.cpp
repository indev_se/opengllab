#include "StdAfx.h"
#include "AnimationState.h"
#include "BlendAnimationStates.h"


BlendAnimationStates::BlendAnimationStates( AnimationState *_pSource, AnimationState *_pDest ) : 
	AnimationState(nullptr, false)
{
	m_pSourceState = _pSource;
	m_pDestState = _pDest;

	m_TotalFrames = FPS * 10; // We want the blend to last 10s for now

	m_CurrentFrame = 0;
	m_LastHandledFrame = -1;
	m_DurationSinceLastFrame = .0f;

	m_Evt_EndReachedScope = nullptr;
	m_Evt_EndReachedListener = nullptr;

	m_IsBlend = true;
}

BlendAnimationStates::~BlendAnimationStates(void)
{
	if ( nullptr != m_pSourceState )
		delete m_pSourceState;
	
	m_pSourceState = nullptr;
}

void BlendAnimationStates::GetAnimationStates( AnimationState **_pSource, AnimationState **_pDest )
{
	*_pSource = m_pSourceState;
	*_pDest = m_pDestState;
}