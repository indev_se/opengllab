#include "StdAfx.h"
#include "BlendAnimationStates.h"
#include "AnimationState.h"


AnimationState::AnimationState( MeshAnimation* animation, bool loop )
{
	m_Animation = animation;
	m_AnimationName = m_Animation ? m_Animation->Name : "";
	m_Loop = loop;

	m_TotalFrames = m_Animation ? m_Animation->GetNumKeyFrames() : 0;

	m_CurrentFrame = 0;
	m_LastHandledFrame = -1;
	m_DurationSinceLastFrame = .0f;

	m_Evt_EndReachedScope = nullptr;
	m_Evt_EndReachedListener = nullptr;

	m_IsBlend = false;
}

AnimationState::~AnimationState(void)
{
	m_Evt_EndReachedScope = nullptr;
	m_Evt_EndReachedListener = nullptr;
}

MeshAnimation* AnimationState::GetMeshAnimation()
{
	return m_Animation;
}

void AnimationState::SetEventEndReachedListener( AnimationStateEventListener *scope, AnimationStateEventFuncPtr evt_ptr )
{
	this->m_Evt_EndReachedScope = scope;
	this->m_Evt_EndReachedListener = evt_ptr;
}

void AnimationState::Play()
{
	m_Active = true;
}

void AnimationState::Pause()
{
	m_Active = false;
}

void AnimationState::Stop()
{
	m_Active = false;
	Reset();
}

void AnimationState::Reset()
{
	m_CurrentFrame = 0;
	m_LastHandledFrame = -1;
	m_DurationSinceLastFrame = 0;
}

void AnimationState::Update( float tickMS )
{
	if ( ! m_Active )
		return;

	m_DurationSinceLastFrame += tickMS;
	if ( m_DurationSinceLastFrame > MS_PER_FRAME )
	{
		m_CurrentFrame++;
		m_DurationSinceLastFrame = 0;
		if ( m_CurrentFrame >= m_TotalFrames )
			EndReached();
	}
}

uint32 AnimationState::GetCurrentFrame()
{
	m_LastHandledFrame = m_CurrentFrame;
	return m_CurrentFrame;
}

int32 AnimationState::GetTotalFrames()
{
	return m_TotalFrames;
}

bool AnimationState::NeedUpdate()
{
	return m_LastHandledFrame != m_CurrentFrame;
}

bool AnimationState::IsBlend()
{
	return m_IsBlend;
}

void AnimationState::EndReached()
{
	if ( m_Loop )
		Reset();
	else {
		Stop();
		DispatchEvent_EndReached();
	}
}

void AnimationState::DispatchEvent_EndReached()
{
	if ( m_Evt_EndReachedScope != nullptr && m_Evt_EndReachedListener != nullptr )
		(m_Evt_EndReachedScope->*m_Evt_EndReachedListener)(this);
}
