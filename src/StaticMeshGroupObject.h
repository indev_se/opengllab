#pragma once
#include "SceneObject.h"
#include "Mesh.h"
#include "GeometryBuffer.h"

class StaticMeshGroupObject :
	public SceneObject
{
public:
	StaticMeshGroupObject(const char *objectName);
	~StaticMeshGroupObject(void);

	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);
	
	void Create( MeshGroup *mesh, GeometryBuffer *gb );

	glm::mat4x4 GetModelMatrix();
	

private:

	MeshGroup *m_pMesh;
	GeometryBuffer *m_pGB;
};