#pragma once
#include "GLScene.h"
#include "Material.h"
#include "BoundingBox.h"
#include "RenderPass.h"

class RenderPass;

class SceneObject
{
public:
	SceneObject(const char *objectName);
	~SceneObject(void);

	virtual void Init( GLScene *scene, Material *material = NULL );
	virtual void Update( float tickMS );
	virtual void Render( RenderPass *pass );

	void RenderBoundingBox( glm::mat4x4 viewMatrix, glm::mat4x4 projection );
	void RenderBoundingBoxChildren( glm::mat4x4 viewMatrix, glm::mat4x4 projection );

	void Identity();

	void SetRotation( const glm::vec3 axis, float radians );
	inline void SetRotationX( float radians ) { SetRotation( AXIS_X, radians ); }
	inline void SetRotationY( float radians ) { SetRotation( AXIS_Y, radians ); }
	inline void SetRotationZ( float radians ) { SetRotation( AXIS_Z, radians ); }
	
	void SetScale( glm::vec3 &scale );
	void SetPosition( glm::vec3 &position );

	void SetTransformMatrix( glm::mat4x4 &mat );

	inline void GetPosition( glm::vec3 &position ) { position = m_vecPosition; }
	inline glm::vec3 GetPosition() { return m_vecPosition; }

	inline void GetObjectMatrix( glm::mat4x4 &out ) { out = m_matTransform; }
	glm::mat4x4 &GetObjectMatrix() { return m_matTransform; }

	virtual glm::mat4x4 GetModelMatrix();
	virtual glm::mat4x4 GetMVNormalMatrix();

	void Visible( bool Visible ) { m_Visible = Visible; }
	bool Visible() { return m_Visible; }

	void AddChild( SceneObject *object );
	void RemoveChild( SceneObject *object );

	std::vector<SceneObject *> GetChildren() { return m_Children; }

	Material *GetMaterial() { return m_pMaterial; }

	void SetParent( SceneObject *parent ) { m_pParent = parent; }
	SceneObject *GetParent() { return m_pParent; }

	void GetBounds( glm::vec3 &min, glm::vec3 &max );

	bool InFrustum();

protected:

	void UpdateChildren(float tickMS);
	void RenderChildren(RenderPass *pass);

	glm::vec3 m_vecRotation;
	glm::vec3 m_vecPosition;
	glm::vec3 m_vecScale;

	glm::mat4x4 m_matTransform;

	GLScene *m_pScene;

	const char *m_sObjectName;

	Material *m_pMaterial;

	bool m_Visible;

	BoundingBox *m_pAABB;

	SceneObject *m_pParent;

	std::vector<SceneObject *> m_Children;
};


