#include "StdAfx.h"
#include "SceneManager.h"
#include "TextObject.h"
#include "PlayerPhysics.h"

static std::stringstream strDebugStatsStream;
static std::string strDebugStats;

PlayerPhysics::PlayerPhysics(void)
{
	m_PositionSim = nullptr;
}

PlayerPhysics::~PlayerPhysics(void)
{
	if ( m_PositionSim != nullptr )
		delete m_PositionSim;

	m_PositionSim = nullptr;
}

void PlayerPhysics::Init( glm::vec3 initialPosition )
{
	m_PositionSim = new PlayerPhysicsSimulation();
	
	m_PositionSim->Create( glm::vec3(0.0f), glm::vec3(0.0f), glm::vec3(PLAYER_RUN_DEACCELERATION, PLAYER_GRAVITY,PLAYER_RUN_DEACCELERATION));
	
	m_PositionSim->SetVelocityBounds( glm::vec3(0.0f,-5.0f,0.0f), glm::vec3(10.672f, 5.0f, 10.627f) );

	m_PositionSim->Pause();
}

void PlayerPhysics::DoJump()
{
	if ( m_PositionSim->GetForceY() > 0.0f )
		return;

	m_PositionSim->SetVelocity( AXIS_Y, PLAYER_JUMP_VELOCITY );
	m_PositionSim->SetForceY( 1.0f );

	m_PositionSim->PushCurrentState();

	m_PositionSim->Run();
}

void PlayerPhysics::StartRun( glm::vec3 direction )
{
	m_PositionSim->SetAccelerationX( PLAYER_RUN_ACCELERATION );
	m_PositionSim->SetAccelerationZ( PLAYER_RUN_ACCELERATION );

	m_PositionSim->SetForceX( direction.x );
	m_PositionSim->SetForceZ( direction.z );

	m_PositionSim->PushCurrentState();

	m_PositionSim->Run();
}

void PlayerPhysics::StopRun()
{
	m_PositionSim->PushCurrentState();

	m_PositionSim->SetAccelerationX( PLAYER_RUN_DEACCELERATION );
	m_PositionSim->SetAccelerationZ( PLAYER_RUN_DEACCELERATION );
}

void PlayerPhysics::ChangeDirection( glm::vec3 direction )
{
	m_PositionSim->SetForceX( direction.x );
	m_PositionSim->SetForceZ( direction.z );

	m_PositionSim->PushCurrentState();

	m_PositionSim->Run();
}

glm::vec3 PlayerPhysics::GetPosition()
{
	return m_PositionSim->GetPosition();
}

void PlayerPhysics::Simulate( float tickMS )
{
	strDebugStatsStream.str(std::string());
	strDebugStatsStream.precision(3);

	float tickSec = tickMS;// / 1000.f;

	// Update the velocity
	if ( m_PositionSim != nullptr )
		m_PositionSim->Simulate(tickSec);

	strDebugStats = strDebugStatsStream.str();
	((TextObject*)SceneManager::Inst()->GetSceneObject("statsText"))->SetText(strDebugStats.c_str());
}

/**
 * Player physics simulation
 **/
PlayerPhysicsSimulation::PlayerPhysicsSimulation()
{
	m_SimulationTime = 0.0f;
	m_Velocity = glm::vec3(0.0f);
	m_Acceleration = glm::vec3(0.0f);
	m_HalfAcceleration = glm::vec3(0.0f);
	m_Position = glm::vec3(0.0f);

	m_InitialVelocity = glm::vec3(0.0f);
	m_InitialPosition = glm::vec3(0.0f);

	m_VelocityMin = glm::vec3(0.0f);
	m_VelocityMax = glm::vec3(10.0f);

	m_Force = glm::vec3(0.0f);

	m_Paused = true;
}

PlayerPhysicsSimulation::~PlayerPhysicsSimulation()
{
}

void PlayerPhysicsSimulation::SetVelocityBounds( glm::vec3 velocityMin, glm::vec3 velocityMax )
{
	m_VelocityMin = velocityMin;
	m_VelocityMax = velocityMax;
}

void PlayerPhysicsSimulation::Create( glm::vec3 initialVelocity, glm::vec3 initialPosition, glm::vec3 acceleration )
{
	m_InitialVelocity = initialVelocity;
	m_InitialPosition = initialPosition;
	m_Acceleration = acceleration;
	m_HalfAcceleration = m_Acceleration * .5f;
}

void PlayerPhysicsSimulation::SetAcceleration( glm::vec3 acceleration )
{
	m_Acceleration = acceleration;
	m_HalfAcceleration = m_Acceleration * .5f;
}

void PlayerPhysicsSimulation::SetAccelerationX( float acceleration )
{
	m_Acceleration.x = acceleration;
	m_HalfAcceleration.x = m_Acceleration.x * .5f;
}

void PlayerPhysicsSimulation::SetAccelerationY( float acceleration )
{
	m_Acceleration.y = acceleration;
	m_HalfAcceleration.y = m_Acceleration.y * .5f;
}

void PlayerPhysicsSimulation::SetAccelerationZ( float acceleration )
{
	m_Acceleration.z = acceleration;
	m_HalfAcceleration.z = m_Acceleration.z * .5f;
}

void PlayerPhysicsSimulation::SetForce( glm::vec3 force )
{
	m_Force = force;
}

void PlayerPhysicsSimulation::SetForceX( float force )
{
	m_Force.x = force;
}

void PlayerPhysicsSimulation::SetForceY( float force )
{
	m_Force.y = force;
}

void PlayerPhysicsSimulation::SetForceZ( float force )
{
	m_Force.z = force;
}

float PlayerPhysicsSimulation::GetForceY()
{
	return m_Force.y;
}

void PlayerPhysicsSimulation::PushCurrentState()
{
	m_InitialPosition = m_Position;
	m_InitialVelocity = m_Velocity;
	m_SimulationTime = 0.0f;
}

void PlayerPhysicsSimulation::Run()
{
	m_Paused = false;
}

void PlayerPhysicsSimulation::Pause()
{
	m_Paused = true;
}

void PlayerPhysicsSimulation::Reset()
{
	m_SimulationTime = 0.0f;
	m_Velocity = glm::vec3(0.0f);
	m_Position = glm::vec3(0.0f);
}

void PlayerPhysicsSimulation::SetVelocity( const glm::vec3 axis, float velocity )
{
	if ( axis.x == 1.0f ) m_Velocity.x = velocity;
	if ( axis.y == 1.0f ) m_Velocity.y = velocity;
	if ( axis.z == 1.0f ) m_Velocity.z = velocity;
}

void PlayerPhysicsSimulation::Simulate(float tickSec)
{
	if ( m_Paused ) {
		strDebugStatsStream << "Simulation paused";
		return;
	}
	
	//if ( glm::length(m_Force) == 0.0f ) {
	//	strDebugStatsStream << "No force";
	//	return;
	//}

	m_SimulationTime += std::min(0.05f, tickSec);
	
	//m_Velocity = glm::abs(m_Force) * ((m_InitialVelocity * m_SimulationTime) + ( m_HalfAcceleration * pow(m_SimulationTime, 2.0f) ));
	m_Velocity = ((m_InitialVelocity /** m_SimulationTime*/) + ( m_HalfAcceleration * pow(m_SimulationTime, 2.0f) ));
	
	if ( m_Velocity.x < m_VelocityMin.x ) {
		m_Velocity.x = m_VelocityMin.x;
		m_Force.x = 0.0f;
	}
	if ( m_Velocity.y < m_VelocityMin.y ) m_Velocity.y = m_VelocityMin.y;
	if ( m_Velocity.z < m_VelocityMin.z ) {
		m_Velocity.z = m_VelocityMin.z;
		m_Force.z = 0.0f;
	}

	if ( m_Velocity.x > m_VelocityMax.x ) m_Velocity.x = m_VelocityMax.x;
	if ( m_Velocity.y > m_VelocityMax.y ) m_Velocity.y = m_VelocityMax.y;
	if ( m_Velocity.z > m_VelocityMax.z ) m_Velocity.z = m_VelocityMax.z;

	//m_Position = m_InitialPosition + ( m_Force * (m_Velocity * METER) * m_SimulationTime );
	m_Position += ( m_Force * (m_Velocity * METER) * tickSec );

	// Fulfix for jump, orkar inte right now
	if( m_Position.y < 0.0f ) {
		m_Position.y = 0.0f;
		m_Force.y = 0.0f;
	}

	strDebugStatsStream << "Simulation:\n";
	strDebugStatsStream.precision(3);
	strDebugStatsStream << "Time: " << m_SimulationTime << "\n";
	strDebugStatsStream << "Vel: " << m_Velocity.x << ", " << m_Velocity.y << ", " << m_Velocity.z << "\n";
	strDebugStatsStream.precision(6);
	strDebugStatsStream << "Pos: " << m_Position.x << ", " << m_Position.y << ", " << m_Position.z << "\n";
	strDebugStatsStream.precision(3);
	strDebugStatsStream << "Force: " << m_Force.x << ", " << m_Force.y << ", " << m_Force.z << "\n";
	strDebugStatsStream.precision(3);
	strDebugStatsStream << "Acc: " << m_Acceleration.x << ", " << m_Acceleration.y << ", " << m_Acceleration.z << "\n";
	
}