#pragma once
#include "TextureAsset.h"
#include "GeometryBuffer.h"
#include "BoundingBox.h"
#include "MeshAnimation.h"
#include "Skeleton.h"

struct MeshGroup
{
	MeshGroup() : m_aaBoundingBox(nullptr) { }

	std::string name;
	std::string materialName;

	GeometryBuffer *pGeometryBuffer;
	
	uint16 *indices;
	uint32 numIndices;

	glm::mat4x4 transform;
	glm::mat4x4 transformInv;

	BoundingBox *m_aaBoundingBox;
	
	// Insertion operator
	uint32 GBCacheID;

	friend std::ostream& operator<<(std::ostream& os, const MeshGroup& s)
	{
		uint32 len;
		
		len = s.name.length();
		SWRITE(os,len);
		SWRITE_C(os,s.name[0],len);

		len = s.materialName.length();
		SWRITE(os,len);
		SWRITE_C(os,s.materialName[0],len);

		os << *s.m_aaBoundingBox;

		SWRITE(os,s.pGeometryBuffer->UniqueCacheID);

		SWRITE(os,s.numIndices);

		os.write((char*)&s.indices[0], s.numIndices * sizeof(uint16));

		//for ( i = 0; i < s.numIndices; ++i )
		//	SWRITE(os,s.indices[i]);

		streamwrite_glm_mat4x4(os, s.transform);
		streamwrite_glm_mat4x4(os, s.transformInv);

		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, MeshGroup& s)
	{
		uint32 len;

		SREAD(is,len);
		s.name.resize(len);
		SREAD_C(is,s.name[0],len);

		SREAD(is,len);
		s.materialName.resize(len);
		SREAD_C(is,s.materialName[0],len);

		s.m_aaBoundingBox = new BoundingBox();
		is >> *s.m_aaBoundingBox;

		SREAD(is,s.GBCacheID);

		SREAD(is,s.numIndices);
		s.indices = new uint16[s.numIndices];

		is.read((char*)&s.indices[0], s.numIndices * sizeof(uint16));
		//for ( i = 0; i < s.numIndices; ++i )
		//	SREAD(is,s.indices[i]);

		s.transform = streamread_glm_mat4x4(is);
		s.transformInv = streamread_glm_mat4x4(is);

		return is;
	}
};

class Mesh
{
public:
	Mesh(void);
	~Mesh(void);

	std::string Name;

	void SetSkeleton( Skeleton *skeleton );
	void SetGeometryBuffer( MeshGroup *group, GeometryBuffer *buffer );

	void SetBounds( const glm::vec3 &min, const glm::vec3 &max );
	BoundingBox* GetBoundingBox();
	void CalculateBoundsFromGroups();

	GeometryBuffer* GetGeometryBuffer( MeshGroup *group );
	std::vector<MeshGroup*> GetGroups();
	std::vector<MeshAnimation*> GetAnimations();
	Skeleton* GetSkeleton();
	MeshAnimation* GetAnimation( std::string name );

	void AddAnimation( MeshAnimation *animation );
	void AddGroup( MeshGroup *group );

	uint16 GetNumIndices();
	uint16 GetNumIndices( GeometryBuffer *gb );

private:

	Skeleton *m_Skeleton;
	BoundingBox *m_aaBoundingBox;

	std::vector<MeshGroup*> m_Groups;
	std::vector<GeometryBuffer*> m_GeometryBuffers;
	std::vector<MeshAnimation*> m_Animations;

public:

	// Insertion operator
	friend std::ostream& operator<<(std::ostream& os, const Mesh& s)
	{
		uint32 i, count;

		bool hasSkeleton = s.m_Skeleton != NULL;
		SWRITE(os,hasSkeleton);
		if (hasSkeleton)
			os << *s.m_Skeleton;

		count = s.m_GeometryBuffers.size();
		SWRITE(os, count);
		for ( i = 0; i < count; ++i ) {
			s.m_GeometryBuffers[i]->UniqueCacheID = i;
			os << *s.m_GeometryBuffers[i];
		}

		count = s.m_Animations.size();
		SWRITE(os, count);
		for ( i = 0; i < count; ++i )
			os << *s.m_Animations[i];

		count = s.m_Groups.size();
		SWRITE(os, count);
		for ( i = 0; i < count; ++i ) {
			os << *s.m_Groups[i];
		}

		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, Mesh& s)
	{
		uint32 i, count;

		bool hasSkeleton = false;
		SREAD(is, hasSkeleton);
		if (hasSkeleton) {
			s.m_Skeleton = new Skeleton();
			is >> *s.m_Skeleton;
		}

		SREAD(is, count);
		for ( i = 0; i < count; ++i ) {
			GeometryBuffer *gb = new GeometryBuffer();
			is >> *gb;

			s.m_GeometryBuffers.push_back(gb);
		}

		SREAD(is, count);
		for ( i = 0; i < count; ++i ) {
			MeshAnimation *animation = new MeshAnimation();
			animation->SetSkeleton(s.m_Skeleton);

			is >> *animation;
			s.m_Animations.push_back(animation);
		}

		uint32 numGroups;
		SREAD(is, numGroups);
		for ( i = 0; i < numGroups; ++i ) {
			MeshGroup *group = new MeshGroup();
			is >> *group;

			group->pGeometryBuffer = s.m_GeometryBuffers[group->GBCacheID];
			s.m_Groups.push_back(group);
		}

		return is;
	}
};

