#pragma once
#include "SceneObject.h"
#include "RenderBatch.h"
#include "CachedObject.h"

class FbxFileContext;

class ModelRenderBatchObject :
	public SceneObject, public CachedObject

{
public:
	ModelRenderBatchObject( const char *objectName );
	~ModelRenderBatchObject(void);

	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);
	
	bool LoadFromFile( const char *filename );
	bool LoadFromCache( const char *cachedFilename );

protected:

	void SaveCachedVersion(const char *filename);

private:

	RenderBatch* m_pRenderBatch;
	Mesh *m_pMesh;
	FbxFileContext *m_pFbxContext;
};

