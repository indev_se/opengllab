#include "StdAfx.h"
#include "CameraInput.h"
#include "MaterialImporter.h"
#include "ForwardRenderPass.h"
#include "AnimatedMeshObject.h"
#include "SceneObjectFactory.h"
#include "SceneManager.h"
#include "BlendAnimationStates.h"
#include "BlendAnimationScene.h"

BlendAnimationScene::BlendAnimationScene(void)
{
	m_pDefaultFPSCamera = nullptr;
}

BlendAnimationScene::~BlendAnimationScene(void)
{
}

void BlendAnimationScene::SetViewport( int Width, int Height )
{
	GLScene::SetViewport( Width, Height );

	if (m_pDefaultFPSCamera != nullptr)
		m_pDefaultFPSCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );
}

void BlendAnimationScene::Init()
{
	GLScene::Init();
	
	// Setup the assetsmanager
	m_AssetsManager = new AssetsManager();

	// Create the camera
	m_pViewportCamera = m_pDefaultFPSCamera = new GLCamera();
	m_pViewportCamera->Init();

	if ( m_iWidth != 0 && m_iHeight != 0)
		m_pViewportCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );
	else
		m_pViewportCamera->SetPerspective( 45.0f, 4.f/3.f, 1.0f, 4000.0f );

	m_pViewportCamera->MoveEyeZ( -20 );

	// Setup the camera input
	m_pCameraInput = new CameraInput();
	m_pCameraInput->SetCamera( m_pViewportCamera );

	// Read all materials
	m_MaterialImporter = new MaterialImporter();
	m_MaterialImporter->ImportAssetsFromFile("Data/Materials/blendscene.mtrl", m_AssetsManager);
	
	// Create the scene objects
	m_RootObject = new SceneObject("root");

	// Create the forward rendering pass
	passForward = new ForwardRenderPass();
	passForward->Init(this);

	// Load the humanoid character
	AnimatedMeshObject *humanoid = SceneObjectFactory::Inst()->CreateAnimatedMeshObject("humanoid", "Data/Models/Characters/humanoid.fbx", this);
	humanoid->SetAnimationByName("run", true);
	humanoid->GetAnimationState()->Play();
	m_RootObject->AddChild(humanoid);
}

void BlendAnimationScene::GetBounds( glm::vec3 &min, glm::vec3 &max )
{
	m_RootObject->GetBounds(min, max);
}

void BlendAnimationScene::Update( float tickMS )
{
	m_pCameraInput->Update( std::min(tickMS, 50.0f) );

	m_RootObject->Update( std::min(tickMS, 50.0f) );
}

void BlendAnimationScene::Render()
{
	Clear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	passForward->Bind();
	m_RootObject->Render( passForward );
	passForward->Unbind();
	
	AssertGLError();
}

void BlendAnimationScene::Event_AnimationEndReached( AnimationState *state )
{
	if ( state->IsBlend() ) {
		AnimationState *pSourceState = nullptr, *pDestState = nullptr;
		((BlendAnimationStates*)state)->GetAnimationStates( &pSourceState, &pDestState );
		pSourceState = nullptr;
		delete state;

		AnimatedMeshObject *humanoid = (AnimatedMeshObject*)SceneManager::Inst()->GetSceneObject("humanoid");
		humanoid->SetAnimationState(pDestState);
	}
}

bool BlendAnimationScene::Event_ProcessCommand( std::string command, std::string &response )
{
	uint32 i;
	std::stringstream stream(command);
	std::string val;
	std::vector<std::string> values;
	std::stringstream responseStream;

	while ( std::getline(stream, val, ' ') ) {
		if ( !val.empty() )
			values.push_back(val);
	}

	if ( values.size() >= 2 ) {

		// parse the command
		if ( values[0] == "anim" && values[1] == "set" ) {
			if ( values.size() == 3 ) {
				AnimatedMeshObject *humanoid = (AnimatedMeshObject*)SceneManager::Inst()->GetSceneObject("humanoid");
				
				humanoid->DeleteAnimationState();
				humanoid->SetAnimationByName(values[2].c_str(), true);
				humanoid->GetAnimationState()->SetEventEndReachedListener(this, &AnimationStateEventListener::Event_AnimationEndReached);
				humanoid->GetAnimationState()->Play();

				responseStream << "Animation changed to " << values[2] << std::endl;
				response = responseStream.str();

				return true;
			}
			else response = "Please specify a name for the animation";
		}
		else if ( values[0] == "anim" && values[1] == "blend" ) {
			if ( values.size() == 3 ) {
				AnimatedMeshObject *humanoid = (AnimatedMeshObject*)SceneManager::Inst()->GetSceneObject("humanoid");

				AnimationState *pDestState = new AnimationState(humanoid->GetMesh()->GetAnimation(values[2].c_str()), true);
				BlendAnimationStates *pBlendStates = new BlendAnimationStates( humanoid->GetAnimationState(), pDestState );
				humanoid->SetAnimationState(pBlendStates);
				pBlendStates->SetEventEndReachedListener(this, &AnimationStateEventListener::Event_AnimationEndReached);
				pBlendStates->Play();
				
				responseStream << "Animation will blend to " << values[2] << std::endl;
				response = responseStream.str();

				return true;
			}
			else response = "Please specify a name for the animation";
		}
		else if ( values[0] == "anim" && values[1] == "list" ) {
			std::vector<MeshAnimation*> animations = ((AnimatedMeshObject*)SceneManager::Inst()->GetSceneObject("humanoid"))->GetMesh()->GetAnimations();
			responseStream << "Mesh has " << animations.size() << " animations" << std::endl;
			for ( i = 0; i < animations.size(); ++i )
				responseStream << animations[i]->Name << std::endl;

			response = responseStream.str();
			return false;
		}
	}
	else response = "Wrong number of arguments";

	return GLScene::Event_ProcessCommand(command, response);
}