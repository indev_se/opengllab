#include "StdAfx.h"
#include "TextObject.h"


TextObject::TextObject(const char *objectName) : SceneObject(objectName)
{
}

TextObject::~TextObject(void)
{
}

void TextObject::Init( GLScene *scene )
{
	SceneObject::Init( scene );
}

void TextObject::SetFont( GLFont *font, const glm::vec3 &color )
{
	m_pFont = font;
	m_vecColor = color;
}

void TextObject::SetText( const char *text )
{
	m_sText = text;
}

const char *TextObject::GetText()
{
	return m_sText;
}

void TextObject::Update( float tickMS )
{
	
}

void TextObject::Render(RenderPass *pass)
{
	glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT | GL_TRANSFORM_BIT);	
	
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	
	
	glPushMatrix();
	glMultMatrixf(glm::value_ptr(m_matTransform));
	glTranslatef(0,0,-1);

	//glColor3fv( m_vecColor );
	glColor4f(m_vecColor.r,m_vecColor.g,m_vecColor.b,1);

	RenderText();

	glPopMatrix();
	
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	glPopAttrib();
}

void TextObject::RenderText()
{
	GLuint fontList = m_pFont->GetGlyphList();

	glm::mat4x4 preMatrix;
	glGetFloatv(GL_MODELVIEW_MATRIX, glm::value_ptr(preMatrix) );
	
	// line height, fontsize and some space
	float lineHeight = (float)m_pFont->GetFontSize() / .9f;

	glListBase(fontList);

	std::vector<std::string> lines;
	std::string str(m_sText);
	std::stringstream stream( str );
	std::string curline;

	while ( std::getline(stream, curline) )
	{
		if ( !curline.empty() )
			lines.push_back(curline);
	}

	for(unsigned int i = 0; i < lines.size(); i++) {
		
		glLoadIdentity();
		glLoadMatrixf(glm::value_ptr(preMatrix));

		glTranslatef(0,-(float)m_pFont->GetFontSize() * (i+1),0);

		glCallLists(lines[i].length(), GL_UNSIGNED_BYTE, lines[i].c_str() );
    }

}