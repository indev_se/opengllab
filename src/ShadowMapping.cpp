#include "StdAfx.h"
#include "SpotLight.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "ShadowMapping.h"


ShadowMapping::ShadowMapping(void)
{
	m_pFrustum = nullptr;
}

ShadowMapping::~ShadowMapping(void)
{
	if ( m_pFrustum != nullptr )
		delete m_pFrustum;

	m_pFrustum = nullptr;
}

void ShadowMapping::Create( LightObject *light, GLScene *Scene, unsigned int FBWidth, unsigned int FBHeight )
{
	m_pScene = Scene;
	m_pLight = light;

	switch ( m_pLight->GetType() )
	{
		case LIGHT_SPOT:
		{
			m_pFPO = new DepthFramebufferObject();
			m_pFPO->Create2D( FBWidth, FBHeight );
		} break;

		case LIGHT_DIRECTIONAL:
		{
			m_pFPO = new DepthFramebufferObject();
			m_pFPO->Create2D( FBWidth, FBHeight );

			m_pFrustum = new Frustum();
		} break;

		case LIGHT_POINT:
		{
			StdConsole::LogWarning( _STR"ShadowMapping with Point light not implemented yet");
		} break;

		default: 
		{
			StdConsole::LogError( _STR"ShadowMapping::SetLight, unknown light type");
		} break;
	}
}

void ShadowMapping::BindFramebuffer()
{
	// Bind the Framebuffer and make sure it succeded
	glBindFramebuffer(GL_FRAMEBUFFER, m_pFPO->GetFBOID());

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if ( status != GL_FRAMEBUFFER_COMPLETE ) {
		StdConsole::LogError( _STR"GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO");
		return;
	}

	// Set the viewport to the size of the Framebuffer
	glViewport(0,0,m_pFPO->Width(),m_pFPO->Height());

	glClear(GL_DEPTH_BUFFER_BIT);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glShadeModel(GL_FLAT);

	switch ( m_pLight->GetType() )
	{
		case LIGHT_SPOT:
		{
			SetSpotLightValues();
		} break;

		case LIGHT_DIRECTIONAL:
		{
			SetDirectionalLightValues();
		} break;

		case LIGHT_POINT:
		{
			StdConsole::LogWarning( _STR"ShadowMapping with Point light not implemented yet");
			return;
		} break;

		default: 
		{
			StdConsole::LogError( _STR"ShadowMapping::SetLight, unknown light type");
			return;
		} break;
	}

	glPolygonOffset(1.1f,4.0f); // Magic values, these should not be static here
	glEnable(GL_POLYGON_OFFSET_FILL);

	CalculateTextureMatrix();
}

void ShadowMapping::UnbindFramebuffer()
{
	// Disable the polygon offset
	glDisable(GL_POLYGON_OFFSET_FILL);

	// Unbind the Framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Enable colorwrite that was disabled by the Shadowmap
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	// Clear the depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset the viewport
	glViewport(0,0,m_pScene->Width(),m_pScene->Height());
}

void ShadowMapping::SetSpotLightValues()
{
	// Setup the projection matrix for the light
	m_ProjectionMatrix = glm::perspective(45.0f, (float)m_pFPO->Width()/(float)m_pFPO->Height(), 1.0f, 1000.0f);

	// Setup the model matrix for the light
	glm::vec3 pos;
	glm::vec3 dir;
	glm::vec3 lookAt;
	m_pLight->GetPosition(pos);
	((SpotLight*)m_pLight)->GetDirection(dir);
	
	lookAt = dir;//Vector3f();//dir * 100;

	m_ViewMatrix = glm::lookAt( pos, lookAt, glm::vec3(.0f, 1.0f, .0f) );
}

void ShadowMapping::SetDirectionalLightValues()
{
	Projection *projection = m_pScene->GetProjection();
	Projection *cloneProjection = projection->Clone();
	
	float zFar = 250.0f;
	cloneProjection->SetZFar( zFar );

	glm::mat4x4 sceneViewMatrix = m_pScene->GetCamera()->GetViewMatrix();
	glm::mat4x4 sceneProjMatrix = cloneProjection->GetMatrix();
	glm::mat4x4 sceneViewProjMatrix = sceneProjMatrix * sceneViewMatrix;

	m_pFrustum->SetFrustum( sceneViewProjMatrix );

	BoundingBox *bb = m_pFrustum->GetBB();
	glm::vec3 bbMin, bbMax;
	bb->GetBounds(bbMin, bbMax);

	// Find the center of the Frustum
	glm::vec3 frustumCenter(0.0,0.0,0.0);
	frustumCenter = bbMin - bbMax;

	float nearPlaneZOffset = 0.0f;
	float viewDistanceFromCenter = zFar + nearPlaneZOffset;

	glm::vec3 lightDirection;
	((DirectionalLight*)m_pLight)->GetDirection(lightDirection);

	// Calculate the up vector
	glm::vec3 up(0.0f,1.0f,0.0f);
	if ( fabs( glm::dot(up,lightDirection)) >= 0.99f)
		up = glm::vec3(0.0f,0.0f,1.0f);

	glm::vec3 right = glm::cross( up, lightDirection );
	right = glm::normalize(right);
	up = glm::cross( lightDirection, right );
	up = glm::normalize(up);

	//glm::vec3 up = glm::vec3(viewDir.x,(-lightDirection.z*viewDir.z-lightDirection.x*viewDir.x)/lightDirection.y,viewDir.z);

	// Transform the Frustum to the lights view and calculate the max/min  x,y,z to create the ortho view
	glm::mat4x4 lightViewMatrix = glm::lookAt( frustumCenter - (lightDirection * viewDistanceFromCenter), frustumCenter, up );
	//glm::mat4x4 lightViewMatrix = glm::lookAt( frustumCenter - (lightDirection * viewDistanceFromCenter), frustumCenter, up );

	// Convert the frustum bb to light space
	glm::vec3 lsMax, lsMin;
	lsMin = glm::vec3( lightViewMatrix * glm::vec4(bbMin, 1.0f) );
	lsMax = glm::vec3( lightViewMatrix * glm::vec4(bbMax, 1.0f) );

	// these will represent the shadow casters bb in world coords
	glm::vec3 zMin = glm::vec3(-1.f,-150.0f,-1.0f);
	glm::vec3 zMax = glm::vec3(1.0f,200.0f,1.0f);

	zMin = glm::vec3( lightViewMatrix * glm::vec4(zMin, 1.0f) );
	zMax = glm::vec3( lightViewMatrix * glm::vec4(zMax, 1.0f) );

	glm::mat4x4 lightProjMatrix = glm::ortho(lsMin.x, lsMax.x, lsMin.y, lsMax.y, zMin.z, zMax.z);
	
	// Store the perspective and view matrix
	m_ViewMatrix = lightViewMatrix;
	m_ProjectionMatrix = lightProjMatrix;
}

void ShadowMapping::CalculateTextureMatrix()
{
	glm::mat4x4 modelView;
	glm::mat4x4 projection;

	// This is matrix transform every coordinate x,y,z
	// x = x* 0.5 + 0.5 
	// y = y* 0.5 + 0.5 
	// z = z* 0.5 + 0.5 
	// Moving from unit cube [-1,1] to [0,1]  
	glm::mat4x4 bias(	
		0.5, 0.0, 0.0, 0.0, 
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0);
	
	// Grab modelview and transformation matrices
	//glGetFloatv(GL_MODELVIEW_MATRIX, glm::value_ptr(modelView));
	//glGetFloatv(GL_PROJECTION_MATRIX, glm::value_ptr(projection));

	// store the original textureMatrix
	m_TextureMatrix = bias * m_ProjectionMatrix * m_ViewMatrix;
}

void ShadowMapping::RenderDepthBuffer()
{
	glDisable(GL_LIGHTING);
	glDisable(GL_LIGHT0);
	glDisable(GL_NORMALIZE);

	int width = 800;
	int height = 600;

	glUseProgramObjectARB(0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-width/2,width/2,-height/2,height/2,1,20);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor4f(1,1,1,1);
	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,m_pFPO->GetTextureId());
	glEnable(GL_TEXTURE_2D);
	glTranslated(0,0,-1);
	glBegin(GL_QUADS);
	glTexCoord2d(0,0);glVertex3f(0,0,0);
	glTexCoord2d(1,0);glVertex3f(width/2,0,0);
	glTexCoord2d(1,1);glVertex3f(width/2,height/2,0);
	glTexCoord2d(0,1);glVertex3f(0,height/2,0);
	 
	glEnd();
	glDisable(GL_TEXTURE_2D);
}