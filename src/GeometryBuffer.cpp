#include "StdAfx.h"
#include "VertexBuffer.h"
#include "GeometryBuffer.h"


GeometryBuffer::GeometryBuffer(void)
{
	m_VertexDesc = nullptr;
	m_VertexBuffer = nullptr;
	m_IsDirty = false;
}

GeometryBuffer::~GeometryBuffer(void)
{
}

VertexBuffer *GeometryBuffer::GetVertexBuffer()
{
	if ( m_IsDirty || m_VertexBuffer == nullptr )
		RebuildVertexBuffer();

	return m_VertexBuffer;
}

void GeometryBuffer::RebuildVertexBuffer()
{
	if ( m_VertexBuffer == nullptr )
		m_VertexBuffer = new VertexBuffer();

	uint32 numVertices = Count();

	m_VertexBuffer->CreateVertexBuffer( m_VertexDesc, numVertices, &m_Data[0], GL_STATIC_DRAW );
	
	m_IsDirty = false;
}

void GeometryBuffer::ReplaceDescription( VertexDescription *vertexDesc )
{
	m_VertexDesc = vertexDesc;
}

void GeometryBuffer::SetDescription( VertexDescription *vertexDesc )
{
	Clear();
	m_VertexDesc = vertexDesc;
}

VertexDescription* GeometryBuffer::GetDescription()
{
	return m_VertexDesc;
}

void GeometryBuffer::Set(VertexData *buffer, uint32 size)
{
	Clear();
	Add(buffer, size);
}

void GeometryBuffer::Add(VertexData *buffer, uint32 size)
{
	m_Data.resize( m_Data.size() + size );
	std::copy(buffer->data(), buffer->data()+size, m_Data.end()-size);
	m_IsDirty = true;
}

void GeometryBuffer::Add(uint8 *buffer, uint32 size)
{
	m_Data.resize( m_Data.size() + size );
	std::copy(buffer, buffer+size, m_Data.end()-size);
	m_IsDirty = true;
}

void GeometryBuffer::Clear()
{
	m_Data.clear();
	m_IsDirty = true;
}

uint8 *GeometryBuffer::GetAt( uint32 offset )
{
	return &m_Data.at( offset * m_VertexDesc->VertexSize() );
}

uint32 GeometryBuffer::Count()
{
	return m_Data.size() / m_VertexDesc->VertexSize();
}