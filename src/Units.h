#pragma once

#define MILLIMETER 0.1f
#define CENTIMETER 1.0f
#define METER CENTIMETER * 100.0f
#define KILOMETER METER * 1000.0f