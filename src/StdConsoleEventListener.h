#pragma once
class StdConsoleEventListener
{
public:
	StdConsoleEventListener(void);
	~StdConsoleEventListener(void);

	virtual bool Event_ProcessCommand( std::string command, std::string &response ) { return false; };
};

typedef bool (StdConsoleEventListener::*ProcessCommandFuncPtr) ( std::string, std::string & );

typedef std::pair< StdConsoleEventListener*, ProcessCommandFuncPtr> StdConsoleListenerPair;
typedef std::vector< StdConsoleListenerPair > StdConsoleListenerList;