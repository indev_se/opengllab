#pragma once
#include "VertexBuffer.h"

class RenderJob
{
public:
	RenderJob(void);
	~RenderJob(void);

	void AddIndexRange( uint16 *indices, size_t size );

	void SetVertexBuffer( VertexBuffer *vb );

	void SetMaterial( Material *material );
	Material* GetMaterial();

	glm::mat4x4 GetMatrix();

	void Render();

private:

	Material *m_pMaterial;
	VertexBuffer *m_pVB;

	glm::mat4x4 m_TransformMatrix;
};

