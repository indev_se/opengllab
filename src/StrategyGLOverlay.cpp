#include "StdAfx.h"
#include "TextObject.h"
#include "StrategyGLOverlay.h"


StrategyGLOverlay::StrategyGLOverlay(void)
{
}


StrategyGLOverlay::~StrategyGLOverlay(void)
{
}

void StrategyGLOverlay::Init()
{
	GLOverlay::Init();

	m_Font_Arial16 = new GLFont();
	m_Font_Arial16->Init( "Data/fonts/arial.ttf", 16 );

	m_pStatsText = new TextObject("statsText");
	m_pStatsText->SetFont(m_Font_Arial16, glm::vec3(1.0f,1.0f,1.0f));
	m_pStatsText->SetText( "0" );
	m_pStatsText->SetPosition( glm::vec3(0.0f,-32.0f,0.0f) );
	
	m_pTimeText = new TextObject("timeText");
	m_pTimeText->SetFont(m_Font_Arial16, glm::vec3(1.0f,1.0f,1.0f));
	m_pTimeText->SetText( "" );
	m_pTimeText->SetPosition( glm::vec3(0.0,0.0,0.0) );
}

void StrategyGLOverlay::Update( float tickMS )
{
	m_pStatsText->Update( tickMS );
	m_pTimeText->Update( tickMS );
}

void StrategyGLOverlay::Render()
{
	BeginRender();
	
	// We want Y = 0 at the top
	glTranslatef( 0.0f, m_iHeight, 0.0f );
	
	m_pStatsText->Render(nullptr);
	m_pTimeText->Render(nullptr);

	EndRender();
}