#include "StdAfx.h"
#include "LightObject.h"


LightObject::LightObject( const char *objectName = nullptr ) : SceneObject(objectName)
{
	m_vDiffuse = glm::vec3(1.0,1.0,1.0);
	m_vAmbient = glm::vec3(0.4,0.4,0.4);
	m_vSpecular = glm::vec3(0.0,0.0,0.0);
	m_fShininess = 0.0;
}

LightObject::~LightObject(void)
{
}