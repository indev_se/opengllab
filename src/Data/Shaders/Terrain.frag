varying vec3 lightVec;

// textures
uniform sampler2D maskMap;
uniform sampler2D tex01Map;
uniform sampler2D tex02Map;
uniform sampler2D tex03Map;
uniform sampler2D tex04Map;
uniform sampler2D normalMap;

float getLamberFactor()
{
	// lookup normal from normal map, move from [0,1] to  [-1, 1] range, normalize
	vec3 normal = 2.0 * texture2D (normalMap, gl_TexCoord[1].st).rgb - 1.0;
	normal = normalize (normal);
	
	// compute diffuse lighting
	float lamberFactor= max (dot (lightVec, normal), 0.0) ;
	return lamberFactor;
}



void main (void)
{
	vec4 maskTexel = texture2D(maskMap,gl_TexCoord[1].st);
	
	vec4 finalTexel = texture2D(tex01Map,gl_TexCoord[0].st) * ( maskTexel.r);
	finalTexel += texture2D(tex03Map,gl_TexCoord[0].st) * ( maskTexel.g);
	finalTexel += texture2D(tex04Map,gl_TexCoord[0].st) * ( maskTexel.b);
	
	float lamberFactor = getLamberFactor();
	if (lamberFactor >= 0.0)
		finalTexel = finalTexel * lamberFactor;
	
	finalTexel += gl_LightSource[0].ambient;
	
	gl_FragColor = finalTexel;
}