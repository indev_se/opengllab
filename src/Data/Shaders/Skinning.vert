uniform mat4 boneMatrices[60];

void main()
{
	gl_TexCoord[0] = gl_MultiTexCoord0;
	vec4 boneIndices = gl_MultiTexCoord1; // bone indices (4)
	vec4 weights = gl_MultiTexCoord2; // weights (2)
	
	vec4 pos = vec4(0,0,0,0);

	for(int i=0; i<4; i++)
	{
		int index = int(boneIndices[i]);
		if ( index > -1 )
		{
			pos = pos + (boneMatrices[index]*gl_Vertex)*weights[i];
		}
	}


	gl_Position = gl_ModelViewProjectionMatrix * pos;
}