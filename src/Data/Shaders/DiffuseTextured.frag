varying vec3 normal, lightDir, eyeVec;

// texutre
//uniform sampler2D colorMap;

void main (void)
{
	// handle the texture
	//vec4 texel = vec4(1.0,1.0,1.0,1.0);//texture2D(colorMap,gl_TexCoord[0].st);

	vec4 final_color = vec4(1.0,0.0,0.0,1.0);
	//(gl_FrontLightModelProduct.sceneColor * gl_FrontMaterial.ambient) + 
	//(gl_LightSource[0].ambient * gl_FrontMaterial.ambient);
							
	vec3 N = normalize(normal);
	vec3 L = normalize(lightDir);
	
	float lambertTerm = dot(N,L);
	
	if(lambertTerm > 0.0)
	{
		final_color = gl_LightSource[0].diffuse * gl_FrontMaterial.diffuse * lambertTerm;	
	}

	gl_FragColor = final_color;			
}