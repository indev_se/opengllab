attribute vec3 InPosition;
attribute vec2 InTex0;
attribute vec3 InNormal;
attribute vec3 InTangent;

varying vec4 worldPosition;
varying vec2 uv0;
varying vec3 normal;
varying vec3 tangent;
varying vec3 bitangent;
varying vec4 shadowTexCoord;

uniform int isDepthPass;
uniform mat4 matShadowTex;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

void main()
{
	vec4 pos = vec4(InPosition.xyz, 1.0);
	
	if ( isDepthPass == 1 ) {
		gl_Position = gl_ModelViewProjectionMatrix * pos;
	}
	else {
		uv0 = InTex0;

		vec4 localNormal = matModel * vec4(InNormal.xyz, 0.0);
		localNormal.w = 0.0;
		normal = normalize( (matView * localNormal).xyz  );
		tangent = normalize( InTangent );
		bitangent = cross(normal, tangent);
		
		vec4 localPos = matModel * pos;
		worldPosition = matView * localPos;
		gl_Position = matPersp * worldPosition;
		shadowTexCoord = matShadowTex * pos;
	}
}