varying vec3 normal;
varying vec2 uv0;
varying vec4 position;
varying vec3 tangent;
varying vec3 bitangent;

uniform sampler2D InColorTexture;
uniform sampler2D InNormalTexture;

uniform int IsShadowDepthPass;

void main()
{
	vec4 clr = texture2D(InColorTexture,uv0.st);

	if (clr.a < 0.1) 
		discard;

	vec3 tsTexNrm = texture2D(InNormalTexture,uv0.st).xyz*2.0-1.0; 
	vec3 wsTexNrm = tangent * tsTexNrm.x + bitangent* tsTexNrm.y + normal * tsTexNrm.z;

	gl_FragColor = clr + (vec4(wsTexNrm * 0.5 + 0.5, 1.0) * 0.01);
}