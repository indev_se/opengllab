varying vec3 normal;
varying vec2 uv0;

uniform sampler2D InColorTexture;

void main()
{
	vec4 clr = texture2D(InColorTexture,uv0.st);

	if (clr.a < 0.1) 
		discard;

	gl_FragColor = clr;
}