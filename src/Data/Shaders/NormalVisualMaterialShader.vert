attribute vec3 InPosition;
attribute vec3 InNormal;

varying vec4 worldPosition;
varying vec3 normal;

varying vec4 shadowTexCoord;
uniform int isDepthPass;
uniform mat4 matShadowTex;

uniform mat4 matNrmObject;
uniform mat4 matNrmWorld;

void main()
{
	vec4 pos = vec4(InPosition.xyz, 1.0);

	if ( isDepthPass == 1 ) {
		gl_Position = gl_ModelViewProjectionMatrix * pos;
	}
	else {
		vec4 localNormal = matNrmObject * vec4(InNormal.xyz, 1.0);
		normal = normalize( (matNrmWorld * localNormal).xyz );

		worldPosition = gl_ModelViewMatrix * pos;
		gl_Position = gl_ModelViewProjectionMatrix * pos;
		shadowTexCoord = matShadowTex * pos;
	}
}