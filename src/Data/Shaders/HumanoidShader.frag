varying vec3 normal;
varying vec4 position;

void main()
{
	vec4 clr = vec4(1.0,1.0,1.0,1.0);

	if (clr.a < 0.1) 
		discard;

	gl_FragColor = clr;
}