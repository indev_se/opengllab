attribute vec3 InPosition;
attribute vec2 InTex0;

varying vec2 uv0;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

void main()
{
	vec4 localPosition = matModel * vec4(InPosition.xyz, 1.0);
	vec4 worldPosition = matView * localPosition;
	
	uv0 = InTex0;

	gl_Position = matPersp * worldPosition;
}