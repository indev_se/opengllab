attribute vec3 InPosition;
attribute vec3 InNormal;
attribute vec3 InColor;

// Used for shadow lookup
varying vec4 ShadowCoord;
varying vec3 normal;
varying vec3 color;

void main()
{
	// Store this for the Fragment shader
 	ShadowCoord = gl_TextureMatrix[7] * gl_Vertex;
  
	normal = InNormal;
	color = vec4(InColor.xyz, 1.0);

	gl_Position = gl_ModelViewProjectionMatrix * vec4(InPosition.xyz, 1.0);
}