varying vec4 worldPosition;
varying vec2 uv0;
varying vec3 normal;
varying vec3 tangent;
varying vec3 bitangent;
varying vec4 shadowTexCoord;

uniform sampler2D InColorTexture;
uniform sampler2D InNormalTexture;

uniform sampler2DShadow ShadowMapTexture;
uniform int isDepthPass;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;


//http://forum.openscenegraph.org/viewtopic.php?t=9526

/*
#define LIGHT_POINT			1
#define LIGHT_SPOT			2
#define LIGHT_DIRECTIONAL	3
*/
struct LightSource
{
	int Type;
	vec3 Position;
	vec3 Direction;
};
uniform LightSource light0;

float getShadow() {
	vec4 shadowCoordinateWdivide = shadowTexCoord / shadowTexCoord.w;
	
	vec4 shadow = shadow2D(ShadowMapTexture, shadowCoordinateWdivide.xyz ); 
	
	if( shadow.z < shadowCoordinateWdivide.z ) 
		return 0.5;
	else 
		return 1.0;
}

float getLambert( vec3 nrm ) {
	
	float lambert = 0;
	if ( light0.Type == 1 )
	{
		//vec4 localLight = matModel * vec4(light0.Position,1.0);
		vec4 worldLight = matView * vec4(light0.Position,1.0);
		vec3 LightDir = normalize( (worldLight - worldPosition).xyz );

		lambert = clamp(dot(nrm, LightDir), 0, 1);//max( dot(nrm, LightDir), 0.0 );
	}
	else if ( light0.Type == 2 )
	{
		vec4 worldLight = matView * vec4(light0.Position,1.0);
		vec3 posDir = normalize( (worldLight - worldPosition).xyz );

		vec4 worldDir = matView * vec4(-light0.Direction,0.0);
		float ang = dot(posDir, worldDir.xyz);

		if ( ang > 0.8 ) {

			lambert = clamp(dot(nrm, normalize(worldDir.xyz)), 0, 1);//max( dot(nrm, LightDir), 0.0 );
			//lambert = lambert * (0.2 - (1.0-ang)) / 0.2;
		}
		else {
			lambert = 0;
		}
	}

	return lambert;
}


void main()
{
	if ( isDepthPass == 1 ) {
		gl_FragColor = vec4(1.0,1.0,1.0,1.0);
	}
	else {
		vec4 clr = texture2D(InColorTexture,uv0.st);

		if ( clr.a < 0.1 )
			discard;

		vec3 texNrm = texture2D(InNormalTexture, uv0.st).xyz * 2.0 -1.0; // rgb to xyz
		vec3 texNrmClr = tangent * texNrm.x + bitangent * texNrm.y + normal * texNrm.z;

		gl_FragColor = clr  * getLambert(texNrmClr) * getShadow(); // xyz to rgb
	}
}