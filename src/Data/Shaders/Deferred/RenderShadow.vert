varying vec2 uv0;

void main()
{
	uv0 = gl_MultiTexCoord0.st;
	
	gl_Position = ftransform();
}