varying vec2 uv0;
varying vec2 blurUVs[14];

uniform int IsHorizontal;

// http://xissburg.com/faster-gaussian-blur-in-glsl/

void main()
{
	gl_Position = ftransform();
	
	uv0 = gl_MultiTexCoord0.st;

	if ( IsHorizontal == 1 )
	{
		blurUVs[0] = uv0 + vec2(-0.028, 0.0);
		blurUVs[1] = uv0 + vec2(-0.024, 0.0);
		blurUVs[2] = uv0 + vec2(-0.020, 0.0);
		blurUVs[3] = uv0 + vec2(-0.016, 0.0);
		blurUVs[4] = uv0 + vec2(-0.012, 0.0);
		blurUVs[5] = uv0 + vec2(-0.008, 0.0);
		blurUVs[6] = uv0 + vec2(-0.004, 0.0);
		blurUVs[7] = uv0 + vec2( 0.004, 0.0);
		blurUVs[8] = uv0 + vec2( 0.008, 0.0);
		blurUVs[9] = uv0 + vec2( 0.012, 0.0);
		blurUVs[10] = uv0 + vec2( 0.016, 0.0);
		blurUVs[11] = uv0 + vec2( 0.020, 0.0);
		blurUVs[12] = uv0 + vec2( 0.024, 0.0);
		blurUVs[13] = uv0 + vec2( 0.028, 0.0);
	}
	else
	{
		blurUVs[0] = uv0 + vec2(0.0, -0.028);
		blurUVs[1] = uv0 + vec2(0.0, -0.024);
		blurUVs[2] = uv0 + vec2(0.0, -0.020);
		blurUVs[3] = uv0 + vec2(0.0, -0.016);
		blurUVs[4] = uv0 + vec2(0.0, -0.012);
		blurUVs[5] = uv0 + vec2(0.0, -0.008);
		blurUVs[6] = uv0 + vec2(0.0, -0.004);
		blurUVs[7] = uv0 + vec2(0.0, 0.004);
		blurUVs[8] = uv0 + vec2(0.0, 0.008);
		blurUVs[9] = uv0 + vec2(0.0, 0.012);
		blurUVs[10] = uv0 + vec2(0.0, 0.016);
		blurUVs[11] = uv0 + vec2(0.0, 0.020);
		blurUVs[12] = uv0 + vec2(0.0, 0.024);
		blurUVs[13] = uv0 + vec2(0.0, 0.028);
	}
	
}