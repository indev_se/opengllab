varying vec2 uv0;

uniform mat4 matLight;

uniform sampler2D InPositionTexture;
uniform sampler2D InDepthTexture;

//uniform sampler2D InShadowTexture;
uniform sampler2DShadow InShadowTexture;

uniform mat4 matView;
uniform mat4 matPerspInv;

void main()
{
	float DepthScreen = texture2D(InDepthTexture, uv0).z;
	float Depth = DepthScreen * 2.0 - 1.0;

	if (DepthScreen == 1.0)
	{
		gl_FragColor = vec4(1.0,1.0,1.0,1.0);
	}
	else
	{
		vec4 ScreenPosition = texture2D(InPositionTexture, uv0);
		vec4 WorldPosition = ScreenPosition;//matView * vec4(ScreenPosition.xyz,1.0);

		vec4 ShadowCoord = matLight * WorldPosition;
		vec4 UnitShadowCoord = ShadowCoord / ShadowCoord.w;

		float bias = 0.0001;
		float ShadowFactor = 1.0;
		float ShadowSum = 0.0;
		vec4 CurrentShadowSample = vec4(0.0);

		vec2 Offset = vec2(1.0 / 1024.0); //vec2(0.00048828,0.00048828); // 1 / 2048

		//float ShadowZ = shadow2D(InShadowTexture, UnitShadowCoord.xyz ).z;
		//if (ShadowZ < UnitShadowCoord.z )
		//{
			float halfSampleSize = 2.0;
			for(float x = -halfSampleSize; x <= halfSampleSize; x += 1.0) 
			{
				for(float y = -halfSampleSize; y <= halfSampleSize; y += 1.0) 
				{
					//CurrentShadowSample = texture2D(InShadowTexture, UnitShadowCoord.xy + vec2(Offset.x*x, Offset.y*y) );
					vec3 shadowTexCoord = UnitShadowCoord.xyz + vec3(Offset.x*x, Offset.y*y, 0.0);
					CurrentShadowSample = shadow2D(InShadowTexture, shadowTexCoord);
					if ( CurrentShadowSample.z < UnitShadowCoord.z+bias ) {
						ShadowSum += 1.0;//CurrentShadowSample / CurrentShadowSample.w;
					}
				}
			}
			float numSamples = (halfSampleSize*2.0) * (halfSampleSize*2.0);
			ShadowFactor = 1 - (ShadowSum / 16.0);
			
		//}
	
		//vec4 ShadowClr = texture2D(InShadowTexture, UnitShadowCoord.xy );
		//vec4 ShadowClr = shadow2D(InShadowTexture, UnitShadowCoord.xyz );
		
		//CurrentShadowSample = shadow2D(InShadowTexture, UnitShadowCoord.xyz );
		//if ( CurrentShadowSample.z < UnitShadowCoord.z+0.0005 )
		//	ShadowFactor = 0.0;// - ShadowClr.z;
		
		gl_FragColor = vec4(ShadowFactor);
	}
}