#version 130
precision mediump float;

uniform sampler2D InTexture;

varying vec2 uv0;
varying vec2 blurUVs[14];

void main()
{
	gl_FragColor = vec4(0.0);
	gl_FragColor += texture2D(InTexture, blurUVs[0])*0.0044299121055113265;
	gl_FragColor += texture2D(InTexture, blurUVs[1])*0.00895781211794;
	gl_FragColor += texture2D(InTexture, blurUVs[2])*0.0215963866053;
	gl_FragColor += texture2D(InTexture, blurUVs[3])*0.0443683338718;
	gl_FragColor += texture2D(InTexture, blurUVs[4])*0.0776744219933;
	gl_FragColor += texture2D(InTexture, blurUVs[5])*0.115876621105;
	gl_FragColor += texture2D(InTexture, blurUVs[6])*0.147308056121;
	gl_FragColor += texture2D(InTexture, uv0 )*0.159576912161;
	gl_FragColor += texture2D(InTexture, blurUVs[7])*0.147308056121;
	gl_FragColor += texture2D(InTexture, blurUVs[8])*0.115876621105;
	gl_FragColor += texture2D(InTexture, blurUVs[9])*0.0776744219933;
	gl_FragColor += texture2D(InTexture, blurUVs[10])*0.0443683338718;
	gl_FragColor += texture2D(InTexture, blurUVs[11])*0.0215963866053;
	gl_FragColor += texture2D(InTexture, blurUVs[12])*0.00895781211794;
	gl_FragColor += texture2D(InTexture, blurUVs[13])*0.0044299121055113265;
}