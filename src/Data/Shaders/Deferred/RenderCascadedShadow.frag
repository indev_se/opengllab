#version 120

varying vec2 uv0;

varying vec4 shadowUV0;
varying vec4 shadowUV1;
varying vec4 shadowUV2;
varying vec4 shadowUV3;

uniform mat4 matLight0;
uniform mat4 matLight1;
uniform mat4 matLight2;
uniform mat4 matLight3;

uniform sampler2D InPositionTexture;
uniform sampler2D InDepthTexture;

uniform sampler2D InShadowTexture0;
uniform sampler2D InShadowTexture1;
uniform sampler2D InShadowTexture2;
uniform sampler2D InShadowTexture3;

uniform mat4 matView;

uniform vec4 splitDepths;

struct LightSource
{
	int Type;
	vec3 Position;
	vec3 Direction;
};
uniform LightSource Light0;

varying vec4 Light0ViewPosition;
varying vec4 Light0ViewDirection;

float getShadowFactor()
{
	vec4 NrmColor = texture2D(InNormalTexture, uv0);
	vec3 Normal = normalize( NrmColor.xyz * 2.0 - 1.0 );
	
	if ( Light0.Type == 3 ) {
		return dot(Normal, normalize(Light0ViewDirection.xyz) ) > 0.0 ? 1.0 : 0.0;
	}

	return 0.0;
}

float getShadowSample( vec4 uv, sampler2D shadowTexture, float depth, float samples )
{
	
	float ShadowSum = 1.0;
	float halfSampleSize = samples / 2.0;
	vec2 Offset = vec2(1.0 / 1024.0);
	float numSamples = (samples+1.0) * (samples+1.0);

	numSamples = 0.0;
	for(float x = -halfSampleSize; x <= halfSampleSize; x += 1.0) 
	{
		for(float y = -halfSampleSize; y <= halfSampleSize; y += 1.0) 
		{
			numSamples += 1.0;
			vec2 shadowTexCoord = uv.xy + vec2(Offset.x*x, Offset.y*y);
			vec4 CurrentShadowSample = texture2D(shadowTexture, shadowTexCoord.xy);
			if ( CurrentShadowSample.z < depth ) {
				ShadowSum += 1.0;
			}
		}
	}

	
	return 1 - ( (ShadowSum / numSamples)  );
	
/*
	vec4 CurrentShadowSample = texture2D(shadowTexture, uv.xy);
	if ( CurrentShadowSample.z < depth )
		return 0.0;

	return 1.0;*/
}

void main()
{
	float DepthScreen = texture2D(InDepthTexture, uv0).z;
	float Depth = DepthScreen;// * 2.0 - 1.0; // Convert to clipspace
	float bias = 0.0000005;

	if (DepthScreen == 1.0)
	{
		gl_FragColor = vec4(1.0,1.0,1.0,1.0);
	}
	else
	{
		vec4 WorldPosition = texture2D(InPositionTexture, uv0);
		
		vec4 ShadowCoord;
		vec4 UnitShadowCoord;
		vec4 CurrentShadowSample;
		vec4 Clr;
		vec4 ShadowClr;
		float ShadowFactor = 1.0;

		if (Depth < splitDepths.x ) // Near
		{
			Clr = vec4(1.0,0.0,0.0,1.0);

			ShadowCoord = matLight0 * WorldPosition;
			UnitShadowCoord = ShadowCoord / ShadowCoord.w;
			
			ShadowFactor = getShadowSample(UnitShadowCoord, InShadowTexture0, UnitShadowCoord.z + bias, 4.0 );
		}
		else if (Depth < splitDepths.y )
		{  
			Clr = vec4(0.0,1.0,0.0,1.0);

			ShadowCoord = matLight1 * WorldPosition;
			UnitShadowCoord = ShadowCoord / ShadowCoord.w;

			ShadowFactor = getShadowSample(UnitShadowCoord, InShadowTexture1, UnitShadowCoord.z + bias, 3.0 );
		}
		else if (Depth < splitDepths.z )
		{  
			Clr = vec4(0.0,0.0,1.0,1.0);
			
			ShadowCoord = matLight2 * WorldPosition;
			UnitShadowCoord = ShadowCoord / ShadowCoord.w;

			CurrentShadowSample = texture2D(InShadowTexture2, UnitShadowCoord.xy);
			if ( CurrentShadowSample.z < UnitShadowCoord.z + bias )
				ShadowFactor = 0.0;

		}
		else if (Depth < splitDepths.w ) // Very far
		{  
			Clr = vec4(1.0,0.0,1.0,1.0);
			
			ShadowCoord = matLight3 * WorldPosition;
			UnitShadowCoord = ShadowCoord / ShadowCoord.w;

			CurrentShadowSample = texture2D(InShadowTexture3, UnitShadowCoord.xy);
			if ( CurrentShadowSample.z < UnitShadowCoord.z + bias )
				ShadowFactor = 0.0;
		}
		else // Outside of shadow
		{
			Clr = vec4(0.0);
			ShadowFactor = 0.0;
		}

		gl_FragColor = ShadowFactor;
		//gl_FragColor = (ShadowFactor*0.01) + clamp( 1-(ssao/10.0), 0.0, 1.0 );
	}


	/*
	vec4 localShadow = shadowUV / shadowUV.w;
	vec4 shadow = shadow2D(InShadowTexture, localShadow.xyz ); 
	float shadowFactor = 1.0;
	if( shadow.z < localShadow.z )
		shadowFactor = 0.3;

	gl_FragColor = vec4(shadowFactor);
	*/
	
}