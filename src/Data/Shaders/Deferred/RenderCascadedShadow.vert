varying vec2 uv0;

struct LightSource
{
	int Type;
	vec3 Position;
	vec3 Direction;
};
uniform LightSource Light0;

varying vec4 Light0ViewPosition;
varying vec4 Light0ViewDirection;

uniform mat4 matView;

void main()
{
	uv0 = gl_MultiTexCoord0.st;
	
	//Light0ViewPosition = matView * vec4(Light0.Position.xyz, 1.0);
	//Light0ViewDirection = matView * vec4(-Light0.Direction.xyz, 0.0);

	gl_Position = ftransform();
}