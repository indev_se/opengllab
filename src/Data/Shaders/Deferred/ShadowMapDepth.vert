attribute vec3 InPosition;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

void main()
{
	gl_Position = matPersp * matView * matModel * vec4(InNormal.xyz,0.0);
}