#version 120

varying vec2 uv0;

varying vec4 shadowUV0;
varying vec4 shadowUV1;
varying vec4 shadowUV2;
varying vec4 shadowUV3;

uniform mat4 matLight0;
uniform mat4 matLight1;
uniform mat4 matLight2;
uniform mat4 matLight3;

uniform sampler2D InPositionTexture;
uniform sampler2D InDepthTexture;

uniform sampler2D InShadowTexture;

uniform mat4 matView;

uniform vec4 splitDepths;

float getShadowSample( vec4 uv, sampler2D shadowTexture, float depth, float samples )
{
	if ( uv.x < 0.0 || uv.x > 1.0 ||
			 uv.y < 0.0 || uv.y > 1.0 )
		return 1.0;

	float ShadowSum = 0.0;
	vec2 Offset = vec2(1.0 / 1024.0);
	float numSamples = 1.0;

	// PCF 4x4
	if ( samples == 4.0 )
	{
		numSamples = 16;
	
		for(float x = -1.5; x <= 1.5; x += 1.0) 
		{
			for(float y = -1.5; y <= 1.5; y += 1.0) 
			{
				vec2 shadowTexCoord = uv.xy + vec2(Offset.x*x, Offset.y*y);
				vec4 CurrentShadowSample = texture2D(shadowTexture, shadowTexCoord.xy);
				if ( CurrentShadowSample.z < depth ) {
					ShadowSum += 1.0;
				}
			}
		}
	}
	else if ( samples == 2.0 )
	{
		numSamples = 4;

		for(float x = -1.5; x <= 1.5; x += 2.0) 
		{
			for(float y = -1.5; y <= 1.5; y += 2.0) 
			{
				vec2 shadowTexCoord = uv.xy + vec2(Offset.x*x, Offset.y*y);
				vec4 CurrentShadowSample = texture2D(shadowTexture, shadowTexCoord.xy);
				if ( CurrentShadowSample.z < depth ) {
					ShadowSum += 1.0;
				}
			}
		}
	}
	else {
			vec4 CurrentShadowSample = texture2D(shadowTexture, uv.xy);
			if ( CurrentShadowSample.z < depth ) {
				ShadowSum = 1.0;
			}
			numSamples = 1.0;
	}
	
	return 1.0 - ( (ShadowSum / numSamples)  );
	
/*
	vec4 CurrentShadowSample = texture2D(shadowTexture, uv.xy);
	if ( CurrentShadowSample.z < depth )
		return 0.0;

	return 1.0;*/
}

void main()
{
	float DepthScreen = texture2D(InDepthTexture, uv0).z;
	float Depth = DepthScreen;// * 2.0 - 1.0; // Convert to clipspace
	float bias = 0.0000005;

	if (DepthScreen == 1.0)
	{
		gl_FragColor = vec4(1.0,1.0,1.0,1.0);
	}
	else
	{
		vec4 WorldPosition = texture2D(InPositionTexture, uv0);
		
		vec4 ShadowCoord;
		vec4 UnitShadowCoord;
		vec4 CurrentShadowSample;
		vec4 Clr;
		vec4 ShadowClr;
		float ShadowFactor = 1.0;
		float ssao = 1.0;

		if (Depth < splitDepths.x ) // Near
		{
			Clr = vec4(1.0,0.0,0.0,1.0);

			ShadowCoord = matLight0 * WorldPosition;
			UnitShadowCoord = ShadowCoord / ShadowCoord.w;

			UnitShadowCoord.x = (0.5 * UnitShadowCoord.x);
			UnitShadowCoord.y = (0.5 * UnitShadowCoord.y) + 0.5;

			ShadowFactor = getShadowSample(UnitShadowCoord, InShadowTexture, UnitShadowCoord.z + bias, 4.0 );
		}
		else if (Depth < splitDepths.y )
		{  
			Clr = vec4(0.0,1.0,0.0,1.0);

			ShadowCoord = matLight1 * WorldPosition;
			UnitShadowCoord = ShadowCoord / ShadowCoord.w;

			UnitShadowCoord.x = (0.5 * UnitShadowCoord.x) + 0.5;
			UnitShadowCoord.y = (0.5 * UnitShadowCoord.y) + 0.5;

			ShadowFactor = getShadowSample(UnitShadowCoord, InShadowTexture, UnitShadowCoord.z + bias, 4.0 );
		}
		else if (Depth < splitDepths.z )
		{  
			Clr = vec4(0.0,0.0,1.0,1.0);
			
			ShadowCoord = matLight2 * WorldPosition;
			UnitShadowCoord = ShadowCoord / ShadowCoord.w;

			UnitShadowCoord.x = (0.5 * UnitShadowCoord.x);
			UnitShadowCoord.y = 0.5 * UnitShadowCoord.y;

			if ( UnitShadowCoord.x < 0.0 || UnitShadowCoord.x > 1.0 ||
					 UnitShadowCoord.y < 0.0 || UnitShadowCoord.y > 1.0 ) {
				ShadowFactor = 1.0;
			}
			else {
				
				CurrentShadowSample = texture2D(InShadowTexture, UnitShadowCoord.xy);
				if ( CurrentShadowSample.z < UnitShadowCoord.z + bias )
					ShadowFactor = 0.0;
				
				//ShadowFactor = getShadowSample(UnitShadowCoord, InShadowTexture, UnitShadowCoord.z + bias, 4.0 );
			}

		}
		else if (Depth < splitDepths.w ) // Very far
		{  
			Clr = vec4(1.0,0.0,1.0,1.0);
			
			ShadowCoord = matLight3 * WorldPosition;
			UnitShadowCoord = ShadowCoord / ShadowCoord.w;

			UnitShadowCoord.x = (0.5 * UnitShadowCoord.x)  + 0.5;
			UnitShadowCoord.y = 0.5 * UnitShadowCoord.y;
			
			if ( UnitShadowCoord.x < 0.0 || UnitShadowCoord.x > 1.0 ||
					 UnitShadowCoord.y < 0.0 || UnitShadowCoord.y > 1.0 ) {
				ShadowFactor = 1.0;
			}
			else {
				
				CurrentShadowSample = texture2D(InShadowTexture, UnitShadowCoord.xy);
				if ( CurrentShadowSample.z < UnitShadowCoord.z + bias )
					ShadowFactor = 0.0;
				
				//ShadowFactor = getShadowSample(UnitShadowCoord, InShadowTexture, UnitShadowCoord.z + bias, 4.0 );
			}
		}
		else // Outside of shadow
		{
			Clr = vec4(0.0);
			ShadowFactor = 0.0;
		}

		
		gl_FragColor = vec4(ShadowFactor);
		//gl_FragColor = (ShadowFactor*0.01) + clamp( 1-(ssao/10.0), 0.0, 1.0 );
	}


	/*
	vec4 localShadow = shadowUV / shadowUV.w;
	vec4 shadow = shadow2D(InShadowTexture, localShadow.xyz ); 
	float shadowFactor = 1.0;
	if( shadow.z < localShadow.z )
		shadowFactor = 0.3;

	gl_FragColor = vec4(shadowFactor);
	*/
	
}