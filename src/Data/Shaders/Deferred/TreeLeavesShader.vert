attribute vec3 InPosition;
attribute vec2 InTex0;
attribute vec3 InNormal;

varying vec3 normal;
varying vec2 uv0;
varying vec4 position;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

uniform int IsShadowDepthPass;

void main()
{
	vec4 localPosition = matModel * vec4(InPosition.xyz, 1.0);
	vec4 worldPosition = matView * localPosition;
		
	if ( IsShadowDepthPass == 1 )
	{
		gl_Position = matPersp * worldPosition;
	}
	else
	{
		uv0 = InTex0;

		vec4 localNormal = matModel * vec4(InNormal.xyz, 0.0);
		localNormal.w = 0.0;
		normal = normalize( (matView * localNormal).xyz );

		position = worldPosition;
		gl_Position = matPersp * worldPosition;
	}
}