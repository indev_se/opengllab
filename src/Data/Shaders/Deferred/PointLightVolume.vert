attribute vec3 InPosition;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

uniform vec3 lightColor;
uniform vec3 lightPosition;
uniform float lightSize;

varying vec4 perspectivePosition;
varying vec4 worldPosition;
varying vec4 worldLightPosition;

void main()
{
	vec4 localPos = matModel * vec4(InPosition.xyz,1.0);
	worldPosition = matView * localPos;

	worldLightPosition = matView * vec4(lightPosition, 1.0);

	perspectivePosition = matPersp * worldPosition;
	gl_Position = perspectivePosition;
}