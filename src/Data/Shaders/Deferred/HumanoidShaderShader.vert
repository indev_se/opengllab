attribute vec3 InPosition;
attribute vec3 InNormal;
attribute vec4 InSkinIndices;
attribute vec4 InSkinWeights;

varying vec3 normal;
varying vec4 position;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;
uniform mat4 SkinMatrices[80];

uniform int IsShadowDepthPass;

void main()
{
	vec4 Pos = vec4(0,0,0,0);
	vec4 OrigPos = vec4(InPosition.xyz, 1.0);

	for(int i=0; i<4; i++)
	{
		int BoneIndex = int(InSkinIndices[i]);
		if ( BoneIndex > -1 )
		{
			Pos = Pos + (SkinMatrices[BoneIndex]*OrigPos)*InSkinWeights[i];
		}
	}

	vec4 localPosition = matModel * Pos;
	vec4 worldPosition = matView * localPosition;
		
	if ( IsShadowDepthPass == 1 )
	{
		gl_Position = matPersp * worldPosition;
	}
	else
	{
		vec4 localNormal = matModel * vec4(InNormal.xyz, 0.0);
		normal = normalize( (matView * localNormal).xyz );

		position = worldPosition;
		gl_Position = matPersp * worldPosition;
	}
}