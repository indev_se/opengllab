varying vec4 color;
varying vec3 normal;
varying vec4 position;

uniform int IsShadowDepthPass;

void main()
{
	if ( IsShadowDepthPass == 1 )
	{
		gl_FragData[0] = vec4(1.0,1.0,1.0,1.0);
	}
	else
	{
		gl_FragData[0] = color;
		gl_FragData[1] = vec4(normal.x*0.5+0.5,normal.y*0.5+0.5,normal.z*0.5+0.5,1.0);
		gl_FragData[2] = vec4(position.x, position.y, position.z, 1.0);
	}
}