uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

uniform vec3 lightColor;
uniform vec3 lightPosition;
uniform float lightSize;

uniform sampler2D InNormalTexture;
uniform sampler2D InPositionTexture;

varying vec4 perspectivePosition;
varying vec4 worldPosition;
varying vec4 worldLightPosition;


void main()
{
	vec3 ScreenPos = perspectivePosition.xyz / perspectivePosition.w;
	vec2 UV = ScreenPos.xy * 0.5 + 0.5;

	vec3 Normal = normalize( texture2D(InNormalTexture, UV).xyz * 2.0 - 1.0 );
	vec3 Position = texture2D(InPositionTexture, UV).xyz;
	
	vec3 LightDir = (worldLightPosition.xyz - Position).xyz;
  float Len = length(LightDir);

  float Dist = 1.0 - pow( clamp(Len, 0.0, lightSize) / lightSize , 2.0 );

	float Lambert = max( dot(Normal, normalize(LightDir)), 0.0 );

	gl_FragColor = vec4(lightColor * Dist * Lambert, 1.0);
}