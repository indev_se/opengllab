varying vec3 normal;
varying vec2 uv0;
varying vec4 position;

uniform sampler2D InColorTexture;

uniform int IsShadowDepthPass;

void main()
{
	if ( IsShadowDepthPass == 1 )
	{
		gl_FragData[0] = vec4(1.0,1.0,1.0,1.0);
	}
	else
	{
		vec4 clr = texture2D(InColorTexture,uv0.st);

		if (clr.a < 0.1) 
			discard;

		gl_FragData[0] = clr;
		gl_FragData[1] = vec4(normal.x*0.5+0.5,normal.y*0.5+0.5,normal.z*0.5+0.5,1.0);
		gl_FragData[2] = vec4(position.x, position.y, position.z, 1.0);
	}
}