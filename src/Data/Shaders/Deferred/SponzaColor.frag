varying vec3 normal;
varying vec2 uv0;
varying vec4 position;
varying vec3 tangent;
varying vec3 bitangent;

uniform sampler2D InColorTexture;

uniform int IsShadowDepthPass;

void main()
{
	if ( IsShadowDepthPass == 1 )
	{
		gl_FragData[0] = vec4(1.0,1.0,1.0,1.0);
	}
	else
	{
		vec4 clr = texture2D(InColorTexture,uv0.st);

		gl_FragData[0] = clr;
		gl_FragData[1] = vec4(normal * 0.5 + 0.5, 0.0);
		gl_FragData[2] = vec4(position.x, position.y, position.z, 1.0);
	}
}