attribute vec3 InPosition;
attribute vec3 InNormal;

varying vec3 normal;
varying vec4 position;

uniform mat4 matObject;
uniform mat4 matWorld;
uniform mat4 matPerspective;

uniform mat4 matNrmObject;
uniform mat4 matNrmWorld;

void main()
{
	vec4 localNormal = matNrmObject * vec4(InNormal.xyz, 1.0);
	normal = normalize( (matNrmWorld * localNormal).xyz );

	vec4 localPosition = matObject * vec4(InPosition.xyz, 1.0);
	vec4 worldPosition = matWorld * localPosition;
	
	position = worldPosition;
	gl_Position = matPerspective * worldPosition;
}