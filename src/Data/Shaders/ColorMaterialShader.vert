attribute vec3 InPosition;
attribute vec3 InColor;
attribute vec3 InNormal;

varying vec4 worldPosition;
varying vec4 color;
varying vec3 normal;

varying vec4 shadowTexCoord;
uniform int isDepthPass;
uniform mat4 matShadowTex;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

void main()
{
	vec4 pos = vec4(InPosition.xyz, 1.0);
	
	if ( isDepthPass == 1 ) {
		gl_Position = gl_ModelViewProjectionMatrix * pos;
	}
	else {
		vec4 localNormal = matModel * vec4(InNormal.xyz, 0.0);
		normal = normalize( (matView * vec4(localNormal.xyz,0.0)).xyz );

		color = vec4(InColor.xyz, 1.0);

		vec4 localPos = matModel * pos;
		worldPosition = matView * localPos;
		gl_Position = matPersp * worldPosition;//gl_ModelViewProjectionMatrix * pos;
		shadowTexCoord = matShadowTex * pos;
	}

}