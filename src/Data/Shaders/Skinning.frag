uniform sampler2D color;

void main()
{
	gl_FragColor = texture2D(color,gl_TexCoord[0].st);;
}