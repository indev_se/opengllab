uniform sampler2DShadow ShadowMap;

varying vec4 ShadowCoord;
varying vec3 normal;
varying vec3 color;

void main()
{
	vec4 shadowCoordinateWdivide = ShadowCoord / ShadowCoord.w;
	
	vec4 shadow = shadow2D(ShadowMap, shadowCoordinateWdivide.xyz ); 
	
	float fudge = 0;
	if( shadow.z < (shadowCoordinateWdivide.z + fudge) ) 
		gl_FragColor = 0.5 * gl_Color;
	else 
		gl_FragColor = 1 * gl_Color;

}