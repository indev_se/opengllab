varying vec4 worldPosition;
varying vec4 color;
varying vec3 normal;

varying vec4 shadowTexCoord;
uniform sampler2DShadow ShadowMapTexture;
uniform int isDepthPass;


uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

/*
#define LIGHT_POINT			1
#define LIGHT_SPOT			2
#define LIGHT_DIRECTIONAL	3
*/
struct LightSource
{
	int Type;
	vec3 Position;
	vec3 Direction;
};
uniform LightSource light0;

float getShadow() {
	vec4 shadowCoordinateWdivide = shadowTexCoord / shadowTexCoord.w;
	
	vec4 shadow = shadow2D(ShadowMapTexture, shadowCoordinateWdivide.xyz ); 
	
	if( shadow.z < shadowCoordinateWdivide.z ) 
		return 0.5;
	else 
		return 1.0;
}

float getLambert() {
	
	float lambert = 0;
	if ( light0.Type == 1 )
	{
		//vec4 localLight = matModel * vec4(light0.Position,1.0);
		vec4 worldLight = matView * vec4(light0.Position,1.0);
		vec3 LightDir = normalize( (worldLight - worldPosition).xyz );

		lambert = clamp(dot(normal, LightDir), 0, 1);//max( dot(normal, LightDir), 0.0 );
	}
	else if ( light0.Type == 2 )
	{
		vec4 worldLight = matView * vec4(light0.Position,1.0);
		vec3 posDir = normalize( (worldLight - worldPosition).xyz );

		vec4 worldDir = matView * vec4(-light0.Direction,0.0);
		float ang = dot(posDir, worldDir.xyz);

		if ( ang > 0.8 ) {

			lambert = clamp(dot(normal, normalize(worldDir.xyz)), 0, 1);//max( dot(normal, LightDir), 0.0 );
			//lambert = lambert * (0.2 - (1.0-ang)) / 0.2;
		}
		else {
			lambert = 0;
		}
	}

	return lambert;
}

void main()
{
	if ( isDepthPass == 1 )
		gl_FragColor = vec4(1.0,1.0,1.0,1.0);
	else
		gl_FragColor = color * getLambert() * getShadow();
}