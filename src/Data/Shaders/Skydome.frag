varying vec2 uv0;

uniform sampler2D InColorTexture;

void main()
{
	vec4 clr = texture2D(InColorTexture,uv0.st);

	gl_FragColor = clr;
}