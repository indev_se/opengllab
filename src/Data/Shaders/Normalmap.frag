varying vec3 lightVec;
varying vec3 halfVec;
varying vec3 eyeVec;

uniform sampler2D normalMap;

void main (void)
{
	// lookup normal from normal map, move from [0,1] to  [-1, 1] range, normalize
	vec3 normal = 2.0 * texture2D (normalMap, gl_TexCoord[1].st).rgb - 1.0;
	normal = normalize (normal);
	
	// compute diffuse lighting
	float lamberFactor= max (dot (lightVec, normal), 0.0) ;
	vec4 diffuseMaterial = vec4(0.0,0.0,0.0,1.0);
	vec4 diffuseLight  = vec4(0.0,0.0,0.0,1.0);
  
	// compute ambient
	vec4 ambientLight = gl_LightSource[0].ambient;	
	
	if (lamberFactor > 0.0)
	{
		diffuseMaterial = gl_FrontMaterial.diffuse;
		diffuseLight  = gl_LightSource[0].diffuse;
		 
		gl_FragColor =	diffuseMaterial * diffuseLight * lamberFactor ;
	}
	
	gl_FragColor +=	ambientLight;
	
	// debug tangent * normalmap
//	vec3 color = texture2D(normalMap,gl_TexCoord[1].st).rgb * gl_TexCoord[2].xyz;
//	gl_FragColor = vec4(color.r,color.g,color.b,1);
}