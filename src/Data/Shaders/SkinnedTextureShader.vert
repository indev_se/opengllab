attribute vec3 InPosition;
attribute vec2 InTex0;
attribute vec3 InNormal;
attribute vec4 InSkinIndices;
attribute vec4 InSkinWeights;

uniform mat4 SkinMatrices[60];

varying vec3 normal;
varying vec2 uv0;

void main()
{
	normal = (InNormal).xyz;
	uv0 = InTex0;

	vec4 Pos = vec4(0,0,0,0);
	vec4 OrigPos = vec4(InPosition.xyz, 1.0);

	for(int i=0; i<4; i++)
	{
		int BoneIndex = int(InSkinIndices[i]);
		if ( BoneIndex > -1 )
		{
			Pos = Pos + (SkinMatrices[BoneIndex]*OrigPos)*InSkinWeights[i];
		}
	}

	gl_Position = gl_ModelViewProjectionMatrix * Pos;
}