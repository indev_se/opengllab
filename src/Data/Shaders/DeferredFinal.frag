#version 120

varying vec2 uv0;

uniform sampler2D InColorTexture;
uniform sampler2D InPositionTexture;
uniform sampler2D InNormalTexture;
uniform sampler2D InDepthTexture;
uniform sampler2D InLightTexture;
uniform sampler2D InShadowTexture;
uniform sampler2D InSSAONoiseTexture;

uniform mat4 matView;

struct LightSource
{
	int Type;
	vec3 Position;
	vec3 Direction;
};
uniform LightSource GlobalLight0;

varying vec4 GlobalLight0Position;
varying vec4 GlobalLight0Direction;

float getLambert(vec4 position, vec3 normal) {
	
	float lambert = 0.0;
	if ( GlobalLight0.Type == 1 ) // Point light
	{
		vec3 LightDir = normalize( (GlobalLight0Position - position).xyz );

		lambert = clamp(dot(normal, LightDir), 0.0, 1.0);
	}
	else if ( GlobalLight0.Type == 2 ) // Spotlight
	{
		vec3 posDir = normalize( (GlobalLight0Position - position).xyz );

		float ang = dot(posDir, GlobalLight0Direction.xyz);

		if ( ang > 0.8 ) {
			lambert = clamp(dot(normal, normalize(GlobalLight0Direction.xyz)), 0.0, 1.0);
		}
		else {
			lambert = 0.0;
		}
	}
	else if ( GlobalLight0.Type == 3 ) {
			lambert = clamp(dot(normal, normalize(GlobalLight0Direction.xyz)), 0.0, 1.0);
	}
	else {
		lambert = 1.0;
	}

	return lambert;
}

float getSpecular(vec4 position, vec3 normal, float specularIntensity, float specularPower)
{
	// Vector between eye and position (since we're in eye-space the eye is located ad 0,0,0)
	vec3 eyeVec = normalize(-position.xyz);

	// The reflective vector between the light and positions normal
	vec3 surfaceReflect = normalize( reflect(normal, -GlobalLight0Direction.xyz) );

	// Calculate the specular reflection intensity
	return specularIntensity * pow( max(dot(surfaceReflect, eyeVec), 0.0), specularPower );
}

const float strength = 0.09;
const float offsetAO = 50.0;
const float falloff = 0.00002;
const float rad = 0.01;
float getSSAO( float DepthScreen, vec3 normal )
{

	vec3 pSphere[10] = vec3[](vec3(-0.010735935, 0.01647018, 0.0062425877),vec3(-0.06533369, 0.3647007, -0.13746321),vec3(-0.6539235, -0.016726388, -0.53000957),vec3(0.40958285, 0.0052428036, -0.5591124),vec3(-0.1465366, 0.09899267, 0.15571679),vec3(-0.44122112, -0.5458797, 0.04912532),vec3(0.03755566, -0.10961345, -0.33040273),vec3(0.019100213, 0.29652783, 0.066237666),vec3(0.8765323, 0.011236004, 0.28265962),vec3(0.29264435, -0.40794238, 0.15964167));
 
	vec3 fres = normalize( texture2D(InSSAONoiseTexture,uv0*offsetAO).xyz * 2.0 - 1.0);
	
	float occluderDepth, depthDifference;
	vec4 occluderNormal;
	vec3 ray;
	vec2 uv_Sample;
	float radD = rad*DepthScreen;

	float bl = 0.0;
	for(int i=0; i<10;++i)
	{
    // get a vector (randomized inside of a sphere with radius 1.0) from a texture and reflect it
		ray = radD * reflect(pSphere[i],fres);

		uv_Sample = uv0 + (sign(dot(ray,normal.xyz) ))*ray.xy;
		occluderDepth = texture2D(InDepthTexture,uv_Sample).r;
		occluderNormal = normalize( texture2D(InNormalTexture,uv_Sample)  * 2.0 - 1.0 );
		depthDifference = DepthScreen-occluderDepth;


		bl += step(falloff,depthDifference)*(1.0-dot(occluderNormal.xyz,normal))*(1.0-smoothstep(falloff,strength,depthDifference));
	}

	return bl;
}

void main()
{
	float DepthScreen = texture2D(InDepthTexture, uv0).x;
	
	if (DepthScreen==1.0)
	{  
		gl_FragColor = texture2D(InColorTexture, uv0);
	}
	else
	{
		vec4 NrmColor = texture2D(InNormalTexture, uv0);
		vec3 Normal = normalize( NrmColor.xyz * 2.0 - 1.0 );
		vec4 Position = texture2D(InPositionTexture, uv0);
		vec3 LightColor = texture2D(InLightTexture, uv0).xyz;
		vec3 Color = texture2D(InColorTexture, uv0).xyz;
		vec4 WorldPosition = matView * vec4(Position.xyz,1.0);
		vec3 Ambient = vec3(0.3);//vec3(0.3,0.3,0.3);

		float SpecularIntensity = NrmColor.a;
		float SSAO = clamp( 1.0-( getSSAO(DepthScreen, Normal) /10.0), 0.0, 1.0 );
		float Shadow = (texture2D(InShadowTexture, uv0).x * SSAO);

		float Specular = 0.0;
		if ( SpecularIntensity > 0.0 )
			Specular = getSpecular(WorldPosition, Normal, SpecularIntensity, 1.5);


		float Lambert = getLambert(WorldPosition, Normal);

		vec3 GlobalLightColor = vec3(1.0);//vec3( 68.0 / 255.0, 62.0 / 255.0, 92.0 / 255.0 );
		vec3 GlobalColor = (GlobalLightColor * Shadow * Lambert) + (Ambient*Lambert);

		//Color = vec3(1.0,1.0,1.0);
		vec3 FinalColor = Color * (GlobalColor + LightColor);
		FinalColor += Specular;

		gl_FragColor = vec4(FinalColor, 1.0);
	}
}