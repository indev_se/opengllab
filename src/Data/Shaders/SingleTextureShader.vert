attribute vec3 InPosition;
attribute vec2 InTex0;
attribute vec3 InNormal;

varying vec3 normal;
varying vec2 uv0;

void main()
{
	normal = (InNormal).xyz;
	uv0 = InTex0;

	gl_Position = gl_ModelViewProjectionMatrix * vec4(InPosition.xyz, 1.0);
}