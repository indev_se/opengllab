varying vec4 worldPosition;
varying vec3 normal;

varying vec4 shadowTexCoord;
uniform sampler2DShadow ShadowMapTexture;
uniform int isDepthPass;
uniform mat4 matWorld;

/*
#define LIGHT_POINT			1
#define LIGHT_SPOT			2
#define LIGHT_DIRECTIONAL	3
*/
struct LightSource
{
	int Type;
	vec3 Position;
};
uniform LightSource light0;

float getShadow() {
	vec4 shadowCoordinateWdivide = shadowTexCoord / shadowTexCoord.w;
	
	vec4 shadow = shadow2D(ShadowMapTexture, shadowCoordinateWdivide.xyz ); 
	
	if( shadow.z < shadowCoordinateWdivide.z ) 
		return 0.5;
	else 
		return 1.0;
}

float getLambert() {
	
	float lambert = 0.0;
	if ( light0.Type == 2 )
	{
		vec3 lpos = (matWorld * vec4(light0.Position,1.0)).xyz;
		vec3 LightDir = normalize( lpos - worldPosition.xyz );
 
		lambert = max( dot(normal, LightDir), 0.0 );
	}

	return lambert;
}

void main()
{
	if ( isDepthPass == 1 )
		gl_FragColor = vec4(1.0,1.0,1.0,1.0);
	else
		gl_FragColor = vec4(1.0,1.0,1.0, 1.0 ) * getLambert() * getShadow();
	 
}