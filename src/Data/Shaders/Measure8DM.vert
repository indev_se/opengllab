attribute vec3 InPosition;
attribute vec2 InTex0;
attribute vec3 InNormal;
attribute vec3 InTangent;

varying vec3 normal;
varying vec2 uv0;
varying vec3 tangent;
varying vec3 bitangent;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

void main()
{
	vec4 localPosition = matModel * vec4(InPosition.xyz, 1.0);
	vec4 worldPosition = matView * localPosition;
		
	uv0 = InTex0;

	vec4 localNormal = matModel * vec4(InNormal.xyz, 0.0);
	localNormal.w = 0.0;
	normal = normalize( (matView * localNormal).xyz );

	vec4 localTangent = matModel * vec4(InTangent.xyz, 0.0);
	localTangent.w = 0.0;
	tangent = normalize( (matView * localTangent).xyz );
	bitangent = normalize( cross(normal, tangent) );

	gl_Position = matPersp * worldPosition;
}