attribute vec3 InPosition;
attribute vec3 InColor;
attribute vec3 InNormal;

varying vec4 color;
varying vec3 normal;

uniform mat4 matModel;
uniform mat4 matView;
uniform mat4 matPersp;

void main()
{
	normal = normalize( (matView * matModel * vec4(InNormal.xyz, 0.0)).xyz  );

	color = vec4(InColor.xyz, 1.0);

	gl_Position = matPersp * matView * matModel * vec4(InPosition.xyz, 1.0);
}