varying vec2 uv0;

uniform mat4 matView;

struct LightSource
{
	int Type;
	vec3 Position;
	vec3 Direction;
};
uniform LightSource GlobalLight0;

varying vec4 GlobalLight0Position;
varying vec4 GlobalLight0Direction;

void main()
{
	uv0 = gl_MultiTexCoord0.st;
	
	GlobalLight0Position = matView * vec4(GlobalLight0.Position.xyz, 1.0);
	GlobalLight0Direction = matView * vec4(-GlobalLight0.Direction.xyz,0.0);
		
	gl_Position = ftransform();
}