#pragma once
#include "FbxManager.h"

#include "MeshAnimation.h"
#include "Mesh.h"
#include "Skeleton.h"
#include "TextureAsset.h"
#include "VertexBufferFixed.h"
#include "CubeObject.h"
#include "SceneObject.h"

class FbxFileContext;

struct FbxVertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec3 color;
	glm::vec2 uv0;

	float bones[4]; // Only support 4 bones per vertex
	float weights[4]; // same with weights
};

class FbxImporter
{
public:
	FbxImporter(void);
	~FbxImporter(void);

	FbxFileContext *LoadFromFile( const char *filename, GLScene *glscene, bool includeAnimation );

	//void RenderSkeleton( );
	//void Update( float tickMS );

private:

	void ProcessBoneAnimation( Bone *bone, KFbxNode *node );

	void ReadMeshNodes(KFbxNode *node);

	void ReadSubMesh( KFbxNode *node );
	void ReadSubMeshGroup( KFbxNode *node, const int materialIndex );
	
	void ReadSkeleton( KFbxNode *node );
	void ReadSkeletonRecursive( KFbxNode *node, int parentIndex, Skeleton *skeleton );

	void ReadLight( KFbxNode *_pNode );

	void TriangulateNode( KFbxNode *node );
	KFbxMatrix GetNodeTransform(KFbxNode *node);

	std::string FindMaterialName( KFbxMesh *fbxMesh, const int materialIndex );
	
	Material *GetMaterial( KFbxMesh *fbxMesh, const int materialIndex, std::string &materialName );

	KFbxNode* FindRoot(KFbxNode* root, KFbxNodeAttribute::EAttributeType attrib);

	glm::mat4x4 GetAbsoluteTransformFromCurrentTake(KFbxNode* node, KTime time);
	glm::mat4x4 ConvertMatrix( KFbxMatrix src );

	glm::vec3 CalculateTangentBiNormal( uint8 *buf0, uint8 *buf1, uint8 *buf2, VertexDescription *desc );

	std::map<VertexDescription*, GeometryBuffer*> m_GBCache;

	std::map<KFbxNode*, Bone*> m_FbxSkeletonMap;

	FbxFileContext *m_pContext;
	Mesh *m_pMesh;
	Skeleton *m_pSkeleton;
	std::map<std::string, MeshAnimation*> m_Animations;
	
	KFbxScene *m_pFbxScene;
	KFbxNode *m_pRootNode;

	GLScene *m_pScene;
	bool m_bIncludeAnimation;
};

