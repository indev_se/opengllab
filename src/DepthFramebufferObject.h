#pragma once
#include "FramebufferObject.h"

class DepthFramebufferObject :
	public FramebufferObject
{
public:
	DepthFramebufferObject(void);
	~DepthFramebufferObject(void);

	void Create2D( unsigned int Width, unsigned int Height );
};

