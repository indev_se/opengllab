#pragma once
#include "SceneObject.h"
#include "VertexBuffer.h"

struct CubeVertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec3 uv;
};

class CubeObject :
	public SceneObject
{
public:
	CubeObject( float SizeX, float SizeY, float SizeZ, const char *objectName );
	~CubeObject(void);

	inline void SetColor( glm::vec3 color ) { m_vecColor = color; }
	inline void GetColor( glm::vec3 &color ) { color = m_vecColor; }

	void Init( GLScene *scene, Material *material, VertexDescription *description = nullptr );
	void Update( float tickMS );
	void Render(RenderPass *pass);

protected:

	glm::vec3 m_vecColor;
	glm::vec3 m_vecSize;

	VertexBuffer *m_pVB;

private:
	void CubeObject::SetColorByPoint( const glm::vec4 &v );
};

