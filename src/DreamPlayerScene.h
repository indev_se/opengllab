#pragma once
#include "GLScene.h"
#include "StrategyGLOverlay.h"
#include "CameraInput.h"
#include "MaterialImporter.h"

#include "DeferredRenderPass.h"
#include "DeferredRenderLightPass.h"
#include "RenderCascadedShadowMapPass.h"
#include "DeferredRenderShadowPass.h"
#include "DeferredRenderFinal.h"

class DreamPlayerScene :
	public GLScene
{
public:
	DreamPlayerScene(void);
	~DreamPlayerScene(void);
	
	void SetViewport( int Width, int Height );

	void Init();
	void Update( float tickMS );
	void Render();

	void GetBounds( glm::vec3 &min, glm::vec3 &max );

private:

	void RenderDebugOverlay( uint32 textureId );

	GLCamera *m_pDefaultFPSCamera;
	CameraInput *m_pCameraInput;
	StrategyGLOverlay *m_pOverlay;

	MaterialImporter *m_MaterialImporter;

	SceneObject *m_RootObject;
	SceneObject *m_RootLightObject;

	// Render passes
	DeferredRenderPass *passDeferred;
	DeferredRenderLightPass *passDeferredLight;
	RenderCascadedShadowMapPass *passRenderCascadedShadowMap;
	DeferredRenderShadowPass *passDeferredRenderShadow;

	DeferredRenderFinal *passDeferredFinal;
};

