#pragma once
#include "GLGameWindowEventListener.h"

#define MAX_GPU 4

typedef void (GLGameWindowEventListener::*GameWindowEventFuncPtr) ( int a, int b );

class GLGameWindow
{
public:
	GLGameWindow(void);
	~GLGameWindow(void);

	bool Create( int Width, int Height, bool fullscreen );

	void ActivateConsole();

	inline HDC GetHDC() { return m_hDC; }

	int Width() { return m_iWidth; }
	int Height() { return m_iHeight; }

	void SetResizeEventListener( GLGameWindowEventListener *scope, GameWindowEventFuncPtr evt_ptr );

	void EnterConsole();
	void ExitConsole();

protected:
	static LRESULT CALLBACK StaticWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

private:
	
	GLGameWindowEventListener *m_Evt_ResizeScope;
	GameWindowEventFuncPtr m_Evt_ResizeListener;

	HDC m_hDC;
	HGLRC m_hRC;
	HWND m_hWnd;
	HINSTANCE m_hInstance;

	bool m_bFullscreen;
	int m_iWidth, m_iHeight;
	
	bool m_bActive;
};

