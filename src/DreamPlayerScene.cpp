#include "StdAfx.h"
#include "DirectionalLight.h"
#include "DreamPlayerScene.h"
#include "AnimatedMeshObject.h"
#include "SceneObjectFactory.h"
#include "SceneManager.h"
#include "UserPlayerObject.h"
#include "FbxTakeImporter.h"
#include "MeasureObject.h"

DreamPlayerScene::DreamPlayerScene(void)
{
	m_pDefaultFPSCamera = nullptr;
}

DreamPlayerScene::~DreamPlayerScene(void)
{
}

void DreamPlayerScene::SetViewport( int Width, int Height )
{
	m_pOverlay = new StrategyGLOverlay();
	m_pOverlay->SetOrtho( Width, Height );

	GLScene::SetViewport( Width, Height );
	
	if (m_pDefaultFPSCamera != nullptr)
		m_pDefaultFPSCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );
}

void DreamPlayerScene::Init()
{
	GLScene::Init();
	
	m_pOverlay->Init();

	// Setup the assetsmanager
	m_AssetsManager = new AssetsManager();

	// Create the camera
	m_pViewportCamera = m_pDefaultFPSCamera = new GLCamera();
	m_pViewportCamera->Init();

	if ( m_iWidth != 0 && m_iHeight != 0)
		m_pViewportCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );
	else
		m_pViewportCamera->SetPerspective( 45.0f, 4.f/3.f, 1.0f, 4000.0f );

	// Setup the camera input
	m_pCameraInput = new CameraInput();
	m_pCameraInput->SetCamera( m_pViewportCamera );

	// Read all materials
	m_MaterialImporter = new MaterialImporter();
	m_MaterialImporter->ImportAssetsFromFile("Data/Materials/builtin.mtrl", m_AssetsManager);
	m_MaterialImporter->ImportAssetsFromFile("Data/Materials/player.mtrl", m_AssetsManager);

	// Create the scene objects
	m_RootObject = new SceneObject("root");

	/*
	AnimatedMeshObject* humanoid = SceneObjectFactory::Inst()->CreateAnimatedMeshObject("humanoid", "Data/Models/Characters/humanoid.fbx", this);
	m_RootObject->AddChild(humanoid);
	
	FbxTakeImporter* importer = new FbxTakeImporter();
	importer->LoadTake( humanoid->GetMesh(), "Data/Models/Characters/animations/CLIP_STOP_2_RUN.FBX" );
	importer->LoadTake( humanoid->GetMesh(), "Data/Models/Characters/animations/CLIP_RUN_LEFT_45DEG_CYCLE.FBX" );
	importer->LoadTake( humanoid->GetMesh(), "Data/Models/Characters/animations/GENERIC_RUN.FBX" );
	importer->LoadTake( humanoid->GetMesh(), "Data/Models/Characters/animations/HERO_RUNJUMP.FBX" );

	humanoid->SetAnimationByName( "Hero_RunJump", true );
	*/
	// Create the lights
	m_RootLightObject = new SceneObject("rootlight");

	// all lights
	std::vector<LightObject *> globalLights;

	// shadow casting light
	DirectionalLight *sun = new DirectionalLight("sun01");
	glm::vec3 dir( glm::vec3(0,0,0) - glm::vec3(0.f,50.f,30.f) );
	sun->SetDirection(glm::normalize(dir));
	globalLights.push_back(sun); // add to global lights so that it will cast lights

	// Create all the rendering passes, deferred rendering
	passRenderCascadedShadowMap = new RenderCascadedShadowMapPass();
	passRenderCascadedShadowMap->Init(sun, this, 4);

	// Create thje deferred rendering pass
	passDeferred = new DeferredRenderPass();
	passDeferred->Init(this);
	
	// Create the pass that will render the shadow to a deferred fbo
	passDeferredRenderShadow = new DeferredRenderShadowPass();
	passDeferredRenderShadow->Init(this, passDeferred->GetFramebuffer(), nullptr, passRenderCascadedShadowMap->GetShadowMap() );
	
	// Create the pass that will apply the static lights to the scene
	passDeferredLight = new DeferredRenderLightPass();
	passDeferredLight->Init(this, passDeferred->GetFramebuffer());

	// The final render pass, makes it all come together
	passDeferredFinal = new DeferredRenderFinal();
	passDeferredFinal->Init(this, passDeferred->GetFramebuffer(), passDeferredRenderShadow->GetFramebuffer(), passDeferredLight->GetFBO(), globalLights );

	
	// Load the models
	
	UserPlayerObject *player = new UserPlayerObject("DreamPlayer");
	player->Init(this);
	m_RootObject->AddChild(player);
	
	MeasureObject *measure = new MeasureObject( "MeasurementFloor");
	measure->Init(this, 50.f );
	m_RootObject->AddChild(measure);	
}

void DreamPlayerScene::GetBounds( glm::vec3 &min, glm::vec3 &max )
{
	m_RootObject->GetBounds(min, max);
}

void DreamPlayerScene::Update( float tickMS )
{
	if ( InputManager::Inst()->GetKey( KEY_O ) ) return;

	m_pCameraInput->Update( tickMS );

	m_pOverlay->Update( tickMS );

	m_RootObject->Update( tickMS );

	//((AnimatedMeshObject*)SceneManager::Inst()->GetSceneObject("humanoid"))->GetAnimationState()->Update(tickMS);
}

void DreamPlayerScene::Render()
{
	Clear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	passDeferred->Bind();
	m_RootObject->Render( passDeferred );
	passDeferred->Unbind();
	
	for ( uint32 i = 0; i < passRenderCascadedShadowMap->GetNumSplits(); ++i )
	{
		passRenderCascadedShadowMap->SetSplitIndex(i);
		passRenderCascadedShadowMap->Bind();

		m_RootObject->Render( passRenderCascadedShadowMap );

		passRenderCascadedShadowMap->Unbind();
	}
	/*
	passDeferredLight->Bind();
	m_RootLightObject->Render( passDeferredLight );
	passDeferredLight->Unbind();
	*/
	passDeferredRenderShadow->Bind();
	passDeferredRenderShadow->RenderScreenQuad();
	passDeferredRenderShadow->Unbind();
	
	passDeferredFinal->Bind();
	passDeferredFinal->RenderScreenQuad();
	passDeferredFinal->Unbind();
	

	//RenderDebugOverlay( passDeferred->GetColorTextureId() );
	//RenderDebugOverlay( passDeferredRenderShadow->GetFramebuffer()->GetTextureId() );
	//passRenderCascadedShadowMap->GetShadowMap()->RenderDebug();

	m_pOverlay->Render();
	AssertGLError();
}

void DreamPlayerScene::RenderDebugOverlay( uint32 textureId )
{
	glUseProgramObjectARB(0);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,m_iWidth,0,m_iHeight,1,20);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor4f(1,1,1,1);
	
	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,textureId);
	
	glEnable(GL_TEXTURE_2D);
	glTranslated(0,0,-1);
	glBegin(GL_QUADS);
	glTexCoord2d(0,0);glVertex3f(0,0,0);
	glTexCoord2d(1,0);glVertex3f(m_iWidth,0,0);
	glTexCoord2d(1,1);glVertex3f(m_iWidth,m_iHeight,0);
	glTexCoord2d(0,1);glVertex3f(0,m_iHeight,0);
	glEnd();
	
	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glDisable(GL_TEXTURE_2D);
}