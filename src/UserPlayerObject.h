#pragma once
#include "SceneObject.h"
#include "UserPlayerAction.h"
#include "AnimatedMeshObject.h"
#include "AnimationStateEventListener.h"
#include "PlayerPhysics.h"
#include "InputEventListener.h"

class UserPlayerObject :
	public SceneObject, 
	public AnimationStateEventListener,
	public InputEventListener
{
public:
	UserPlayerObject( const char *objectName );
	~UserPlayerObject(void);

	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);

private:

	void CheckInput();

	AnimatedMeshObject* m_pModel;

	UserPlayerAction* m_pCurrentAction;

	void SetPlayerState( PlayerActionState state );

	PlayerActionState m_PendingState;

private:

	void Event_AnimationEndReached( AnimationState *state );

	void Event_KeyUp( uint16 key );
	void Event_KeyDown( uint16 key );

private:
	
	void TurnLeft();
	void TurnRight();

	void RunForward();
	void RunBackward();

	glm::mat4x4 m_matRotate;
	glm::mat4x4 m_matTranslate;
	glm::mat4x4 m_matScale;

	glm::vec3 m_velocityVector;
	glm::vec3 m_movementVector;
	glm::vec3 m_directionVector;

	glm::quat m_quatRotY;
	
	PlayerPhysics *m_pPhysics;

	void UpdateMovement();
};

