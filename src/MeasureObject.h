#pragma once
#include "SceneObject.h"
#include "BitmapDataAsset.h"
#include "TextureAsset.h"
#include "Material.h"

class MeasureObject :
	public SceneObject
{
public:
	MeasureObject( const char *objectName );
	~MeasureObject(void);

	void Init( GLScene *scene, float sizeMeter );
	void Update( float tickMS );
	void Render(RenderPass *pass);

private:

	VertexBuffer *m_pVB;
};

