#pragma once
#include "SceneObject.h"
#include "GLFont.h"

class TextObject :
	public SceneObject
{
public:
	TextObject(const char *objectName);
	~TextObject(void);

	void SetFont( GLFont *font, const glm::vec3 &color );
	void SetText( const char *text );
	const char *GetText();

	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);

protected:
	void TextObject::RenderText();

private:

	GLFont *m_pFont;
	glm::vec3 m_vecColor;
	const char *m_sText;
};

