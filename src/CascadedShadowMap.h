#pragma once
#include "LightObject.h"
#include "CubeObject.h"
#include "DepthFramebufferObject.h"

#define MAX_NUM_SHADOW_CASCADES 4

typedef std::vector<DepthFramebufferObject*> DepthBuffers;

typedef std::vector<glm::vec3> LightVolumes;
typedef std::vector<glm::mat4x4> SplitMatrices;
typedef std::vector<BoundingBox*> DebugCubes;


class CascadedShadowMap
{
public:
	CascadedShadowMap(void);
	~CascadedShadowMap(void);

	void Create( LightObject *light, GLScene *scene, uint32 numSplits, unsigned int FBWidth, unsigned int FBHeight, bool useAtlas );

	void BindFramebuffer(uint32 SplitIndex);
	void UnbindFramebuffer();

	void SetDirectionalLightValues(uint32 SplitIndex);

	uint32 GetSplitCount() { return m_NumSplits; }

	LightObject *GetLight();

	float* GetDepths();
	float* GetClipDepths();
	float GetMaxDepth();

	uint32 GetTextureId(uint32 SplitIndex);

	glm::mat4x4 GetViewMatrix(uint32 SplitIndex);
	glm::mat4x4 GetProjectionMatrix(uint32 SplitIndex);
	glm::mat4x4 GetTextureMatrix(uint32 SplitIndex);

	void RenderDebug();
	void CreateDebugVolumes();

	bool IsAtlas() { return m_bIsAtlas; }

protected:

	void CalculateTextureMatrix( uint32 SplitIndex );

	LightObject *m_pLight;

	uint32 m_NumSplits;
	uint32 m_BoundSplit;

	float m_fDepths[MAX_NUM_SHADOW_CASCADES];
	float m_fClipDepths[MAX_NUM_SHADOW_CASCADES];

	glm::vec2 m_AtlasOffsets[MAX_NUM_SHADOW_CASCADES];
	
	DebugCubes m_pDebugCubes;

	bool m_bIsAtlas;

	DepthBuffers m_FBOs;
	DepthFramebufferObject *m_pFBOAtlas;

	SplitMatrices m_ViewMatrices;
	SplitMatrices m_ProjectionMatrices;
	SplitMatrices m_TextureMatrices;
	SplitMatrices m_CropMatrices;

	LightVolumes m_LightMinVolumes;
	LightVolumes m_LightMaxVolumes;

	GLScene *m_pScene;
};

