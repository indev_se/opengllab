#include "StdAfx.h"
#include "EmptyFramebufferObject.h"


EmptyFramebufferObject::EmptyFramebufferObject(void)
{
}


EmptyFramebufferObject::~EmptyFramebufferObject(void)
{
}

void EmptyFramebufferObject::Create2D( unsigned int Width, unsigned int Height )
{
	m_Width = Width;
	m_Height = Height;

	// Create the Framebuffer object
	glGenFramebuffers(1, &m_FBOID);
	
}

void EmptyFramebufferObject::BindTexture( uint32 textureId, GLenum attachment )
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBOID );

	// Instruct openGL that we won't bind a color texture with the currently binded FBO
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	
	// Attach the texture to the framebuffer
	glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, textureId, 0 );

	// Check status to make sure everything went ok
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if ( status != GL_FRAMEBUFFER_COMPLETE ) {
		StdConsole::LogError( _STR"GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}