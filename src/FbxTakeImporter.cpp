#include "StdAfx.h"
#include "FbxImporter.h"
#include "FbxTakeImporter.h"


FbxTakeImporter::FbxTakeImporter(void)
{
}


FbxTakeImporter::~FbxTakeImporter(void)
{
}

bool FbxTakeImporter::LoadTake( Mesh* mesh, const char *filename )
{
	FbxManager *mgr = FbxManager::Inst();
    
	// Allocate and load the scene from file
	KFbxScene* scene = NULL;
	mgr->AllocateScene(scene);

	mgr->LoadScene(scene, filename, true);

	if ( scene == NULL )
	{
		StdConsole::LogError( _STR"Failed to load scene: " << filename);
		return nullptr;
	}

	KFbxAxisSystem sceneAxisSystem = scene->GetGlobalSettings().GetAxisSystem();
	KFbxAxisSystem engineAxisSystem(KFbxAxisSystem::OpenGL);
	
	if (sceneAxisSystem != engineAxisSystem)
	{
		engineAxisSystem.ConvertScene(scene);     // convert the scene to engine specs
	}

	KFbxNode *rootNode = scene->GetRootNode();

	if(rootNode)
	{
		ReadSkeleton( FindRoot(rootNode, KFbxNodeAttribute::eSKELETON), scene, mesh);
		return true;
	}

	return false;
}

void FbxTakeImporter::ReadSkeleton( KFbxNode *node, KFbxScene *scene, Mesh* mesh )
{
	// http://www.assembla.com/code/vapor/subversion/nodes/src/Pipeline/ImporterFBX.cpp
	// http://code.google.com/p/meshimport/source/browse/trunk/src/MeshImportFBX/FBXSceneFuncs.cpp
	KFbxSkeleton *fbxSkeleton = (KFbxSkeleton*) node->GetSkeleton();

	ReadSkeletonRecursive( fbxSkeleton->GetNode(), -1, mesh->GetSkeleton(), scene, mesh );
}

void FbxTakeImporter::ReadSkeletonRecursive( KFbxNode *node, int parentIndex, Skeleton *skeleton, KFbxScene *scene, Mesh* mesh )
{
	int numChildren;

	KFbxSkeleton *fbxSkeleton = (KFbxSkeleton*) node->GetNodeAttribute();
	if ( fbxSkeleton == nullptr ) {
		numChildren = node->GetChildCount();
		return;
	}

	Bone *bone = skeleton->GetBoneByName( node->GetName() );

	if ( bone == nullptr ) {
		StdConsole::LogInfo( _STR"Skeleton has no bone with name: " << node->GetName());
		return;
	}

	ProcessBoneAnimation(bone, node, scene, mesh);

	numChildren = node->GetChildCount();
	for ( int i = 0; i < numChildren; ++i )
	{
		KFbxNode *child = node->GetChild( i );
		ReadSkeletonRecursive( child, bone->Index, skeleton, scene, mesh );
	}
}

void FbxTakeImporter::ProcessBoneAnimation( Bone *bone, KFbxNode *node, KFbxScene *scene, Mesh* mesh )
{
	//http://apoc3dcpp.googlecode.com/svn/trunk/APBuild/MeshBuild/FbxImporter.cpp

	// Read the animation
	KArrayTemplate<KString*> animStackNameArray;
	scene->FillAnimStackNameArray(animStackNameArray);
	const int animStackCount = animStackNameArray.GetCount();
	int i;

	float fFrameRate = (float)KTime::GetFrameRate(scene->GetGlobalSettings().GetTimeMode());

	for(i = 0; i < animStackCount; i++)
	{
		KFbxAnimStack *stack = scene->FindMember(FBX_TYPE(KFbxAnimStack), animStackNameArray[i]->Buffer());
		
		if ( stack == NULL ) continue;

		scene->GetEvaluator()->SetContext(stack);
		scene->ActiveAnimStackName.Set(*animStackNameArray[i]);
		
		KFbxTakeInfo *takeInfo = scene->GetTakeInfo(*(animStackNameArray[i]));
		//printf("Process take: \"%s\"\n", takeInfo->mName.Buffer());
		
		MeshAnimation *animation = mesh->GetAnimation(takeInfo->mName.Buffer());
		if ( animation == nullptr ) {
			animation = new MeshAnimation();
			animation->Name = takeInfo->mName.Buffer();
			animation->Duration = 0;

			mesh->AddAnimation( animation );
		}

		KFbxAnimLayer *layer = stack->GetMember(FBX_TYPE(KFbxAnimLayer), 0);
		
		KTime timeStart = takeInfo->mLocalTimeSpan.GetStart();
		KTime timeEnd = takeInfo->mLocalTimeSpan.GetStop();

		float fStart = static_cast<float>(timeStart.GetSecondDouble());
		float fEnd = static_cast<float>(timeEnd.GetSecondDouble());

		if( fStart < fEnd )
		{
			double fTime = 0;
			while( fTime <= fEnd )
			{
				KTime takeTime;
				takeTime.SetSecondDouble(fTime);

				AnimationKeyFrame *frame = new AnimationKeyFrame();
				frame->Time = static_cast<float>(fTime);
				frame->LocalTransform = ConvertMatrix(scene->GetEvaluator()->GetNodeLocalTransform(node, takeTime)); //GetAbsoluteTransformFromCurrentTake(node, takeTime);
				frame->GlobalTransform = ConvertMatrix(scene->GetEvaluator()->GetNodeGlobalTransform(node, takeTime)); //GetAbsoluteTransformFromCurrentTake(node, takeTime);
				fTime += 1.0f/fFrameRate;

				animation->AddKeyFrame( bone, frame );
			}
		}
	}
}

glm::mat4x4 FbxTakeImporter::ConvertMatrix( KFbxMatrix src )
{
	glm::mat4x4 dst = glm::mat4();
	
	for( int r = 0; r < 4; ++r )
	{
		for( int c = 0; c < 4; ++c )
		{
			dst[c][r] = static_cast<float>(src.Get(c,r));
		}
	}
	return dst;
}

KFbxNode* FbxTakeImporter::FindRoot(KFbxNode* root, KFbxNodeAttribute::EAttributeType attrib)
{
	if ( root->GetNodeAttribute() )
	{
		if ( root->GetNodeAttribute()->GetAttributeType() == attrib )
			return root;
	}

    uint32 numChildren = root->GetChildCount();
    if(!numChildren)
        return NULL;

    KFbxNode* child = NULL;
    for(unsigned c = 0; c < numChildren; c++)
    {
        child = root->GetChild(c);
        if(!child->GetNodeAttribute())
            continue;
        if(child->GetNodeAttribute()->GetAttributeType() != attrib)
            continue;
        
        return child;
    }

    KFbxNode* rootJoint = NULL;
    for(unsigned c = 0; c < numChildren; c++)
    {
        child = root->GetChild(c);
        rootJoint = FindRoot(child, attrib);
        if(rootJoint)
            break;
    }
    return rootJoint;
}