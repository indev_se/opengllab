#include "StdAfx.h"
#include "md5.h"
#include "AssetsManager.h"

AssetsManager::AssetsManager(void)
{
	m_bIgnoreCache = false;
}

AssetsManager::~AssetsManager(void)
{
}

BitmapDataAsset *AssetsManager::CreateBitmapDataAsset( const char *name, const char *filename )
{
	BitmapDataAsset *asset = (BitmapDataAsset*)LoadCache(filename);
	if ( asset != nullptr )
		return asset;

	asset = new BitmapDataAsset();

	if ( filename != NULL )
	{
		if ( asset->CreateFromFile( filename ) )
		{
			m_Assets[name] = asset;
		}
		else 
		{
			StdConsole::LogError( _STR"Unable to create BitmapDataAsset " << name << ", filename: " << filename );
		}
	}
	else 
	{
		StdConsole::LogError( _STR"Unable to create BitmapDataAsset " << std::string(name) << ", filename is NULL");
		m_Assets[name] = asset;
	}

	SaveCache(filename, asset);
	return asset;
}

TextureAsset *AssetsManager::CreateTextureAsset( const char *name )
{
	TextureAsset* asset = new TextureAsset();
	this->AddAsset(name, asset);
	return asset;
}

ShaderProgram *AssetsManager::CreateShaderProgram( const char *name )
{
	ShaderProgram* asset = new ShaderProgram();
	this->AddAsset(name, asset);
	return asset;
}

Material *AssetsManager::CreateMaterial( const char *name )
{
	Material* asset = new Material();
	this->AddAsset(name, asset);
	return asset;
}

void AssetsManager::AddAsset( const char *name, Asset *obj )
{
	obj->SetManager(this);
	obj->SetName(name);
	m_Assets[name] = obj;
}

void AssetsManager::RemoveAsset( const char *name )
{
	m_Assets[name] = NULL;
}

void AssetsManager::RemoveAndDestroyAsset( const char *name )
{
	if( m_Assets[name] != NULL )
	{
		delete m_Assets[name];
		m_Assets[name] = NULL;
	}
	else
		StdConsole::LogError( _STR"Unable to destroy asset " << std::string(name) << ", it doesn't exist");
}

void AssetsManager::RemoveAndDestroyAsset( Asset *obj )
{
	std::map<std::string, Asset*>::iterator iter = m_Assets.begin();
	while ( iter != m_Assets.end() ) {
		if ( iter->second == obj ) {
			m_Assets.erase(iter++);
		}
		else
			++iter;
	}

	delete obj;
}

Asset *AssetsManager::GetAsset( const char *name )
{
	if ( m_Assets[name] == NULL ) {
		StdConsole::LogWarning( _STR"Asset " << std::string(name) << " doesn't exist");
		return NULL;
	}

	return m_Assets[name];
}

BitmapDataAsset *AssetsManager::GetBitmapDataAsset( const char *name )
{
	return (BitmapDataAsset*)this->GetAsset(name);
}

TextureAsset *AssetsManager::GetTextureAsset( const char *name )
{
	return (TextureAsset*)this->GetAsset(name);
}

ShaderProgram *AssetsManager::GetShaderProgram( const char *name )
{
	return (ShaderProgram*)this->GetAsset(name);
}

Material *AssetsManager::GetMaterial( const char *name )
{
	return (Material*)this->GetAsset(name);
}

bool AssetsManager::AssetExists( const char *name )
{
	return (m_Assets[name] != nullptr);
}

void AssetsManager::SaveCache( const char* filename, Asset *obj )
{
	if ( m_bIgnoreCache )
		return;

	m_AssetsFileCache[ GetCacheName(filename) ] = obj;
}

Asset* AssetsManager::LoadCache( const char* filename )
{
	if ( m_bIgnoreCache )
		return nullptr;

	return m_AssetsFileCache[ GetCacheName(filename) ];
}

std::string AssetsManager::GetCacheName( const char *filename )
{
	return md5(filename);
}

void AssetsManager::ClearCache()
{
	m_AssetsFileCache.clear();
}

void AssetsManager::SetIgnoreCache( bool ignore )
{
	m_bIgnoreCache = ignore;
}