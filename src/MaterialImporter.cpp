#include "StdAfx.h"
#include "MaterialImporter.h"


MaterialImporter::MaterialImporter(void)
{
}


MaterialImporter::~MaterialImporter(void)
{
}

void MaterialImporter::ImportAssetsFromFile( const char *filename, AssetsManager *assets )
{
	m_pAssetsManager = assets;

	FILE *fileHandle = NULL;
	fopen_s(&fileHandle, filename, "r");

	if ( fileHandle == NULL )
	{
		StdConsole::LogError( _STR"MaterialImporter::ImportAssetsFromFile - File not found " << filename);
		return;
	}

	if( ParseMaterials(fileHandle) == false )
		StdConsole::LogError( _STR"MaterialImporter::ParseMaterials - Failed!");
	
	fclose(fileHandle);
}

bool MaterialImporter::ParseMaterials( FILE *fileHandle )
{
	char line[512];
	
	m_iCurrentLine = 0;

	std::string currentMaterialName("");
	std::string strLine;

	while( fgets(line, sizeof(line), fileHandle) != NULL )
	{
		++m_iCurrentLine;

		if ( strcmp("\n", line) == 0 )
			continue;

		strLine = line;
		TrimSpecial(strLine);

		if ( strLine == "{" )
		{
			if ( currentMaterialName == "" ) {
				StdConsole::LogError( _STR"MaterialImporter::ParseMaterials - Expected material name first, Line: " << m_iCurrentLine);
				return false;
			}

			if ( ParseMaterial(currentMaterialName, fileHandle) == false ) {
				StdConsole::LogError( _STR"MaterialImporter::ParseMaterials - Parse material failed Line: " << m_iCurrentLine);
				return false;
			}
			currentMaterialName = "";
		}
		else if ( strLine == "}" )
		{
			StdConsole::LogError( _STR"MaterialImporter::ParseMaterials - Unexpected }, Line: " << m_iCurrentLine);
			return false;
		}
		else // Should me the material name
		{
			currentMaterialName = strLine;
		}
	}

	return true;
}

bool MaterialImporter::ParseMaterial( std::string Name, FILE *fileHandle )
{
	char line[512];
	std::string strLine;

	Material *material = m_pAssetsManager->CreateMaterial( Name.c_str() );

	while( fgets(line, sizeof(line), fileHandle) != NULL )
	{
		++m_iCurrentLine;
		
		if ( strcmp("\n", line) == 0 )
			continue;

		strLine = line;
		TrimSpecial(strLine);
		
		if ( strLine == "vertex" )
		{
			if ( ParseMaterialVertex(material, fileHandle) == false ) {
				StdConsole::LogError( _STR"MaterialImporter::ParseMaterial - Failed parsing vertex data, Line: " << m_iCurrentLine);
				return false;
			}
		}
		else if ( strLine == "shader_forward" )
		{
			if ( ParseMaterialShaderForward(material, fileHandle) == false ) {
				StdConsole::LogError( _STR"MaterialImporter::ParseMaterial - Failed parsing forward shader data, Line: " << m_iCurrentLine);
				return false;
			}
		}
		else if ( strLine == "shader_deferred" )
		{
			if ( ParseMaterialShaderDeferred(material, fileHandle) == false ) {
				StdConsole::LogError( _STR"MaterialImporter::ParseMaterial - Failed parsing deferred shader data, Line: " << m_iCurrentLine);
				return false;
			}
		}
		else if ( strLine == "texture" )
		{
			if ( ParseMaterialTexture(material, fileHandle) == false ) {
				StdConsole::LogError( _STR"MaterialImporter::ParseMaterial - Failed parsing texture data, Line: " << m_iCurrentLine);
				return false;
			}
		}
		else if ( strLine == "texturecoord" )
		{
		}
		else if ( strLine == "}" )
		{
			break;
		}
	}

	material->BindShaderAttribs();
	material->GetDescription()->Lock();

	return true;
}

bool MaterialImporter::ParseMaterialVertex( Material *material, FILE *fileHandle )
{
	char line[512];
	std::string strLine;
	
	while( fgets(line, sizeof(line), fileHandle) != NULL )
	{
		++m_iCurrentLine;
		
		if ( strcmp("\n", line) == 0 )
			continue;

		strLine = line;
		TrimSpecial(strLine);
		

		if ( strLine == "}" )
			break;
		else if ( strLine == "{" )
			continue;
		
		// Must be a data value

		// Parse string in format AttribName:AttribSize
		std::stringstream stream(strLine);
		std::string val;
		std::vector<std::string> values;

		while ( std::getline(stream, val, ':') )
		{
			if ( !val.empty() )
			{
				values.push_back(val);
			}
		}

		if ( values.size() != 2 )
		{
			StdConsole::LogError( _STR"MaterialImporter::ParseMaterialVertex - Failed parsing vertex attrib, Line: " << m_iCurrentLine);
			return false;
		}

		VertexDataElement *elem = new VertexDataElement();
		elem->Name = values[0];
		TrimWhitespace(values[1]);
		elem->ParseTypeSize( values[1] );

		material->GetDescription()->AddElement(elem);
	}

	return true;
}

bool MaterialImporter::ParseMaterialShaderForward( Material *material, FILE *fileHandle )
{
	ShaderProgram *shader = ParseMaterialShader(material, fileHandle);
	
	material->SetShaderForward(shader);

	return true;
}

bool MaterialImporter::ParseMaterialShaderDeferred( Material *material, FILE *fileHandle )
{
	ShaderProgram *shader = ParseMaterialShader(material, fileHandle);
	
	material->SetShaderDeferred(shader);

	return true;
}

ShaderProgram *MaterialImporter::ParseMaterialShader( Material *material, FILE *fileHandle )
{
	char line[512];
	std::string strLine;
	
	std::map<std::string, std::string> keyValue;

	while( fgets(line, sizeof(line), fileHandle) != NULL )
	{
		++m_iCurrentLine;
		
		if ( strcmp("\n", line) == 0 )
			continue;

		strLine = line;
		TrimSpecial(strLine);
		

		if ( strLine == "}" )
			break;
		else if ( strLine == "{" )
			continue;
		
		// Must be a data value

		// Parse string in format Key:Value
		// Parse string in format AttribName:AttribSize
		std::stringstream stream(strLine);
		std::string val;
		std::vector<std::string> values;

		while ( std::getline(stream, val, ':') )
		{
			if ( !val.empty() )
			{
				values.push_back(val);
			}
		}

		if ( values.size() != 2 )
		{
			StdConsole::LogError( _STR"MaterialImporter::ParseMaterialShader - Failed parsing shader key:value, Line: " << m_iCurrentLine);
			return false;
		}

		TrimWhitespace(values[0]);
		TrimWhitespace(values[1]);
		
		keyValue[values[0]] = values[1];
	}

	// Create the shader object if we have all necessary key values
	if ( keyValue["name"].empty() && keyValue["vertex"].empty() && keyValue["fragment"].empty() )
	{
		StdConsole::LogError( _STR"MaterialImporter::ParseMaterialShader - Missing one of the required values name, vertex or fragment, Line: " << m_iCurrentLine);
		return false;
	}

	ShaderProgram *shader = m_pAssetsManager->GetShaderProgram( keyValue["name"].c_str() );
	
	if ( shader == nullptr )
	{
		if ( keyValue["vertex"].empty() && keyValue["fragment"].empty() )
		{
			StdConsole::LogError( _STR"MaterialImporter::ParseMaterialShader - Supposed to create shader but files are missing, Line: " << m_iCurrentLine);
			return false;
		}
		shader = m_pAssetsManager->CreateShaderProgram( keyValue["name"].c_str() );
		shader->CreateProgram( keyValue["vertex"].c_str(), keyValue["fragment"].c_str() );
	}

	return shader;
}

bool MaterialImporter::ParseMaterialTexture( Material *material, FILE *fileHandle )
{
	char line[512];
	std::string strLine;
	
	TextureInfo *info = new TextureInfo();
	info->genMipMap = true;
	
	TextureInfoParam infoParam1 = { GL_TEXTURE_WRAP_S, GL_REPEAT };
	info->params.push_back( &infoParam1 );
	TextureInfoParam infoParam2 = { GL_TEXTURE_WRAP_T, GL_REPEAT };
	info->params.push_back( &infoParam2 );
	TextureInfoParam infoParam3 = { GL_TEXTURE_MAG_FILTER, GL_LINEAR };
	info->params.push_back( &infoParam3 );
	TextureInfoParam infoParam4	= { GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR };
	info->params.push_back( &infoParam4 );

	while( fgets(line, sizeof(line), fileHandle) != NULL )
	{
		++m_iCurrentLine;
		
		if ( strcmp("\n", line) == 0 )
			continue;

		strLine = line;
		TrimSpecial(strLine);
		
		if ( strLine == "}" )
			break;
		else if ( strLine == "{" )
			continue;
		
		// Must be a data value

		// Parse string in format AttribName:AttribSize
		std::stringstream stream(strLine);
		std::string val;
		std::vector<std::string> values;

		while ( std::getline(stream, val, ':') )
		{
			if ( !val.empty() )
			{
				values.push_back(val);
			}
		}

		if ( values.size() != 2 )
		{
			StdConsole::LogError( _STR"MaterialImporter::ParseMaterialTexture - Failed parsing texture key:value, Line: " << m_iCurrentLine);
			return false;
		}

		TrimWhitespace(values[0]);
		TrimWhitespace(values[1]);

		std::string bitmapName = material->GetName() + values[0] + "_bmp";
		std::string textureName = material->GetName() + values[0] + "_tex";

		BitmapDataAsset *bmp = m_pAssetsManager->CreateBitmapDataAsset( bitmapName.c_str() , values[1].c_str() );
		TextureAsset *tex = m_pAssetsManager->CreateTextureAsset( textureName.c_str() );
		tex->CreateFromBitmapData( bmp, info );

		material->AddTexture( values[0].c_str(), tex);
	}

	return true;
}

void MaterialImporter::TrimSpecial(std::string &inStr)
{
	inStr.erase(std::remove(inStr.begin(), inStr.end(), '\t'), inStr.end());
	inStr.erase(std::remove(inStr.begin(), inStr.end(), '\n'), inStr.end());
	inStr.erase(std::remove(inStr.begin(), inStr.end(), '\r'), inStr.end());
}

void MaterialImporter::TrimWhitespace(std::string &inStr)
{
	inStr.erase(std::remove(inStr.begin(), inStr.end(), ' '), inStr.end());
}