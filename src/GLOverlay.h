#pragma once
#include "GLScene.h"
class GLOverlay :
	public GLScene
{
public:
	GLOverlay(void);
	~GLOverlay(void);

	virtual void SetOrtho( int Width, int Height );
	
	virtual void Clear( GLbitfield bufferMask );

	virtual void Init();
	virtual void Update( float tickMS ) {;}
	virtual void Render() {;}

protected:

	void BeginRender();
	void EndRender();

	glm::mat4x4 m_matOrthoProjection;
	int m_iWidth, m_iHeight;
};

