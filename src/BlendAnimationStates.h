#pragma once
#include "AnimationState.h"
class AnimationState;

class BlendAnimationStates : 
	public AnimationState
{
public:
	BlendAnimationStates( AnimationState *_pSource, AnimationState *_pDest );
	~BlendAnimationStates(void);

	void GetAnimationStates( AnimationState **_pSource, AnimationState **_pDest );

private:

	AnimationState *m_pSourceState;
	AnimationState *m_pDestState;
};

