#pragma once
#include "glscene.h"
class EmptyGLScene :
	public GLScene
{
public:
	EmptyGLScene(void);
	~EmptyGLScene(void);

	void SetViewport( int Width, int Height );

	void Init();
	void Update( float tickMS );
	void Render();

	void GetBounds( glm::vec3 &min, glm::vec3 &max );
};

