#include "StdAfx.h"
#include "RenderBatch.h"


RenderBatch::RenderBatch(void)
{
}


RenderBatch::~RenderBatch(void)
{
}

void RenderBatch::AddJob( RenderJob* job )
{
	m_Jobs.push_back(job);
}

void RenderBatch::RemoveJob( RenderJob* job )
{
	std::vector<RenderJob *>::iterator iter = std::find(m_Jobs.begin(), m_Jobs.end(), job);
	if(iter != m_Jobs.end()) {
		m_Jobs.erase(iter);
	}
}

void RenderBatch::Render( RenderPass *pass )
{
	RenderJob *job;
	for( uint32 i = 0; i < m_Jobs.size(); ++i ) {
		job = m_Jobs[i];
		
		if ( pass ) pass->BindJob(job);

		job->Render();

		if ( pass ) pass->UnbindJob(job);
	}
}