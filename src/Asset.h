#pragma once

class AssetsManager;

class Asset
{
public:
	Asset(void);
	~Asset(void);

	void SetManager( AssetsManager *mngr ) { m_pManager = mngr; }
	
	std::string GetName() { return Name; }
	void SetName(std::string name) { Name = name; }

protected:

	std::string Name;
	AssetsManager *m_pManager;

};

