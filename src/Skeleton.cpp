#include "StdAfx.h"
#include "Skeleton.h"


Skeleton::Skeleton(void)
{
}

Skeleton::~Skeleton(void)
{
}

void Skeleton::AddBone( Bone* bone )
{
	m_Bones.push_back( bone );
}

uint32 Skeleton::Size()
{
	return m_Bones.size();
}

const Bone* Skeleton::GetBone( uint32 index )
{
	for( uint32 i = 0; i < m_Bones.size(); i++ )
	{
		const Bone* bone = m_Bones[i];
		if ( bone->Index == index )
			return bone;
	}

	return NULL;
}

Bone* Skeleton::GetBoneByName( std::string name )
{
	for ( uint32 i = 0; i < m_Bones.size(); ++i )
		if ( m_Bones[i]->Name == name )
			return m_Bones[i];

	return nullptr;
}

std::vector<Bone*> Skeleton::GetBones()
{
	return m_Bones;
}
