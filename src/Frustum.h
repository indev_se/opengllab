#pragma once
#include "BoundingBox.h"

#define FRUSTUMPLANE_NEAR	0
#define FRUSTUMPLANE_FAR	1
#define FRUSTUMPLANE_BOTTOM	2
#define FRUSTUMPLANE_TOP	3
#define FRUSTUMPLANE_LEFT	4
#define FRUSTUMPLANE_RIGHT	5

class Frustum
{
public:
	Frustum(void);
	~Frustum(void);

	// Extract the planes from a perspective matrix
	// m: projection matrix multiplied by the modelview matrix
	void SetFrustum(const glm::mat4x4 &matrix);

	bool PointInFrustum(const glm::vec3 &point);
	bool AABBInFrustum( BoundingBox *boundingBox, glm::mat4x4 transform );

	bool GetCorners( glm::vec3* corners );
	bool GetBoundingCorners( glm::vec3* corners, const glm::mat4x4 &matrix );

	BoundingBox *GetBB();

private:

	void CalculateBoundingBox();

	bool m_bFrustumBBDirty;

	glm::mat4x4 m_Matrix;

	BoundingBox *m_pBB;
	Plane m_Planes[6]; 
};