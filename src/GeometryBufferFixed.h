#pragma once
#include "VertexBufferFixed.h"

class GeometryBufferFixed
{
public:
	GeometryBufferFixed(void);
	~GeometryBufferFixed(void);

	void SetFormat( uint32 vertexFormat );
	uint32 GetFormat();

	VertexBufferFixed *GetVertexBuffer();

	size_t CalculateVertexSize( uint32 vertexFormat = 0 );

	void Set(uint8 *buffer, uint32 size);
	void Add(uint8 *buffer, uint32 size);
	void Clear();

	uint8 *GetAt( uint32 offset );

	uint32 Count();

	inline void SetDirty() { m_IsDirty = true; }

private:

	void RebuildVertexBuffer();

	bool m_IsDirty;

	uint32 m_VertexFormat;
	size_t m_VertexSize;

	VertexBufferFixed *m_VertexBuffer;

	// Vertex data
	std::vector<uint8> m_Data;
};

