#include "StdAfx.h"
#include "BoundingBox.h"


BoundingBox::BoundingBox(void)
{
}

BoundingBox::BoundingBox(const glm::vec3 &min, const glm::vec3 &max)
{
	m_vecMin = min;
	m_vecMax = max;
}

BoundingBox::~BoundingBox(void)
{
}

void BoundingBox::SetBounds(const glm::vec3 &min, const glm::vec3 &max)
{
	m_vecMin = min;
	m_vecMax = max;
}

void BoundingBox::GetBounds( glm::vec3 &min, glm::vec3 &max)
{
	min = m_vecMin;
	max = m_vecMax;
}

void BoundingBox::Multiply( const glm::mat4x4 &transformMatrix )
{
	glm::vec4 vmin( m_vecMin.x, m_vecMin.y, m_vecMin.z, 1 );
	glm::vec4 vmax( m_vecMax.x, m_vecMax.y, m_vecMax.z, 1 );

	vmin = transformMatrix * vmin;
	vmax = transformMatrix * vmax;

	m_vecMin.x = vmin.x;
	m_vecMin.y = vmin.y;
	m_vecMin.z = vmin.z;

	m_vecMax.x = vmax.x;
	m_vecMax.y = vmax.y;
	m_vecMax.z = vmax.z;

}

void BoundingBox::GetPoints( glm::vec3 points[8] )
{
	points[0] = glm::vec3(m_vecMin.x, m_vecMin.y, m_vecMin.z);
    points[1] = glm::vec3(m_vecMax.x, m_vecMin.y, m_vecMin.z);
    points[2] = glm::vec3(m_vecMax.x, m_vecMax.y, m_vecMin.z);
    points[3] = glm::vec3(m_vecMin.x, m_vecMax.y, m_vecMin.z);
    points[4] = glm::vec3(m_vecMin.x, m_vecMin.y, m_vecMax.z);
    points[5] = glm::vec3(m_vecMax.x, m_vecMin.y, m_vecMax.z);
    points[6] = glm::vec3(m_vecMax.x, m_vecMax.y, m_vecMax.z);
    points[7] = glm::vec3(m_vecMin.x, m_vecMax.y, m_vecMax.z);
}

void BoundingBox::GetPoints( glm::vec3 points[8], glm::mat4x4 transform )
{
	this->GetPoints( points );
	for ( uint32 i = 0; i < 8; ++i ) {
		points[i] = glm::vec3( transform * glm::vec4(points[i], 1.0f) );
	}
}

void BoundingBox::Render( glm::mat4x4 modelView, glm::mat4x4 projection )
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glLoadMatrixf( glm::value_ptr(projection) );

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glLoadMatrixf( glm::value_ptr(modelView) );
	
	// We push the attributes on the openGL attribute stack; so
	// we dont disturb the current values of color (current) or line width (line)
	glDisable(GL_CULL_FACE);
	
	glPushAttrib(GL_CURRENT_BIT | GL_LINE_BIT);
	glDisable(GL_LIGHTING);
	glLineWidth(2);

	glBegin(GL_LINES);
	{
		glColor3f(0.7f, 0.5f, 0.1f);

		glVertex3f(m_vecMin.x,m_vecMin.y,m_vecMin.z); glVertex3f(m_vecMax.x,m_vecMin.y,m_vecMin.z);
		glVertex3f(m_vecMin.x,m_vecMin.y,m_vecMin.z); glVertex3f(m_vecMin.x,m_vecMax.y,m_vecMin.z);
		glVertex3f(m_vecMin.x,m_vecMin.y,m_vecMin.z); glVertex3f(m_vecMin.x,m_vecMin.y,m_vecMax.z);
		glVertex3f(m_vecMax.x,m_vecMin.y,m_vecMin.z); glVertex3f(m_vecMax.x,m_vecMax.y,m_vecMin.z);
		glVertex3f(m_vecMax.x,m_vecMin.y,m_vecMin.z); glVertex3f(m_vecMax.x,m_vecMin.y,m_vecMax.z);
		glVertex3f(m_vecMax.x,m_vecMax.y,m_vecMin.z); glVertex3f(m_vecMin.x,m_vecMax.y,m_vecMin.z);
		glVertex3f(m_vecMax.x,m_vecMax.y,m_vecMin.z); glVertex3f(m_vecMax.x,m_vecMax.y,m_vecMax.z);
		glVertex3f(m_vecMin.x,m_vecMax.y,m_vecMin.z); glVertex3f(m_vecMin.x,m_vecMax.y,m_vecMax.z);
		glVertex3f(m_vecMin.x,m_vecMin.y,m_vecMax.z); glVertex3f(m_vecMax.x,m_vecMin.y,m_vecMax.z);
		glVertex3f(m_vecMin.x,m_vecMin.y,m_vecMax.z); glVertex3f(m_vecMin.x,m_vecMax.y,m_vecMax.z);
		glVertex3f(m_vecMax.x,m_vecMin.y,m_vecMax.z); glVertex3f(m_vecMax.x,m_vecMax.y,m_vecMax.z);
		glVertex3f(m_vecMin.x,m_vecMax.y,m_vecMax.z); glVertex3f(m_vecMax.x,m_vecMax.y,m_vecMax.z);
	}
	glEnd();

	// Revert the lighting state and the line state
	glPopAttrib();

	glEnable(GL_CULL_FACE);
}

BoundingBox *BoundingBox::Clone()
{
	return new BoundingBox( m_vecMin, m_vecMax );
}