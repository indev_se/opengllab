#include "StdAfx.h"
#include "Frustum.h"


Frustum::Frustum(void)
{
	m_bFrustumBBDirty = false;
	m_pBB = nullptr;
}


Frustum::~Frustum(void)
{
	if ( m_pBB != nullptr )
		delete m_pBB;

	m_pBB = nullptr;
}

// Extract the planes from a perspective matrix
// m: projection matrix multiplied by the modelview matrix
void Frustum::SetFrustum(const glm::mat4x4 &matrix)
{
	m_Planes[FRUSTUMPLANE_NEAR].SetCoefficients(
									matrix[0][2] + matrix[0][3],
									matrix[1][2] + matrix[1][3],
									matrix[2][2] + matrix[2][3],
									matrix[3][2] + matrix[3][3]);

	m_Planes[FRUSTUMPLANE_FAR].SetCoefficients(
									-matrix[0][2] + matrix[0][3],
									-matrix[1][2] + matrix[1][3],
									-matrix[2][2] + matrix[2][3],
									-matrix[3][2] + matrix[3][3]);

	m_Planes[FRUSTUMPLANE_BOTTOM].SetCoefficients(
									 matrix[0][1] + matrix[0][3],
									 matrix[1][1] + matrix[1][3],
									 matrix[2][1] + matrix[2][3],
									 matrix[3][1] + matrix[3][3]);

	m_Planes[FRUSTUMPLANE_TOP].SetCoefficients(
									-matrix[0][1] + matrix[0][3],
									-matrix[1][1] + matrix[1][3],
									-matrix[2][1] + matrix[2][3],
									-matrix[3][1] + matrix[3][3]);

	m_Planes[FRUSTUMPLANE_LEFT].SetCoefficients(
									 matrix[0][0] + matrix[0][3],
									 matrix[1][0] + matrix[1][3],
									 matrix[2][0] + matrix[2][3],
									 matrix[3][0] + matrix[3][3]);

	m_Planes[FRUSTUMPLANE_RIGHT].SetCoefficients(
									-matrix[0][0] + matrix[0][3],
									-matrix[1][0] + matrix[1][3],
									-matrix[2][0] + matrix[2][3],
									-matrix[3][0] + matrix[3][3]);

	m_bFrustumBBDirty = true;
	m_Matrix = matrix;
}

static const glm::vec3 cornerPoints[] =
{
	glm::vec3(-1,  1,  1), glm::vec3(1,  1,  1),
	glm::vec3(-1, -1,  1), glm::vec3(1, -1,  1),
	glm::vec3(-1,  1, -1), glm::vec3(1,  1, -1),
	glm::vec3(-1, -1, -1), glm::vec3(1, -1, -1)
};
bool Frustum::GetBoundingCorners( glm::vec3* corners, const glm::mat4x4 &matrix )
{
	glm::mat4x4 matInvClip = glm::inverse( matrix );

	for ( uint32 i = 0; i < 8; ++i )
	{
		const glm::vec3& corner = cornerPoints[i];
		glm::vec4 c = matInvClip * glm::vec4( glm::vec3(corner), 1.0);

		corners[i] = glm::vec3(c.x, c.y, c.z) / c.w;
	}

	return true;
}

BoundingBox *Frustum::GetBB()
{
	if ( m_bFrustumBBDirty ) {
		CalculateBoundingBox();
	}


	return m_pBB;
}

void Frustum::CalculateBoundingBox()
{
	glm::vec3 corners[8];
	GetBoundingCorners( &corners[0], m_Matrix );

	glm::vec3 frustumMax(corners[0]), frustumMin(corners[0]);
	for ( uint32 i = 1; i < 8; ++i )
	{
		if ( corners[i].x > frustumMax.x )
			frustumMax.x = corners[i].x;
		else if ( corners[i].x < frustumMin.x )
			frustumMin.x = corners[i].x;

		if ( corners[i].y > frustumMax.y )
			frustumMax.y = corners[i].y;
		else if ( corners[i].y < frustumMin.y )
			frustumMin.y = corners[i].y;

		if ( corners[i].z > frustumMax.z )
			frustumMax.z = corners[i].z;
		else if ( corners[i].z < frustumMin.z )
			frustumMin.z = corners[i].z;
	}

	if ( m_pBB == nullptr )
		m_pBB = new BoundingBox();

	m_pBB->SetBounds( frustumMin, frustumMax );

	m_bFrustumBBDirty = false;
}

bool Frustum::GetCorners( glm::vec3* corners )
{
	glm::vec3 intersection;
	uint32 idx = 0;
	
	Plane::GetPointIntersection( m_Planes[FRUSTUMPLANE_NEAR], m_Planes[FRUSTUMPLANE_BOTTOM], m_Planes[FRUSTUMPLANE_LEFT], &intersection );
	corners[idx++] = intersection;

	Plane::GetPointIntersection( m_Planes[FRUSTUMPLANE_NEAR], m_Planes[FRUSTUMPLANE_BOTTOM], m_Planes[FRUSTUMPLANE_RIGHT], &intersection );
	corners[idx++] = intersection;

	Plane::GetPointIntersection( m_Planes[FRUSTUMPLANE_NEAR], m_Planes[FRUSTUMPLANE_TOP], m_Planes[FRUSTUMPLANE_LEFT], &intersection );
	corners[idx++] = intersection;

	Plane::GetPointIntersection( m_Planes[FRUSTUMPLANE_NEAR], m_Planes[FRUSTUMPLANE_TOP], m_Planes[FRUSTUMPLANE_RIGHT], &intersection );
	corners[idx++] = intersection;

	Plane::GetPointIntersection( m_Planes[FRUSTUMPLANE_FAR], m_Planes[FRUSTUMPLANE_BOTTOM], m_Planes[FRUSTUMPLANE_LEFT], &intersection );
	corners[idx++] = intersection;

	Plane::GetPointIntersection( m_Planes[FRUSTUMPLANE_FAR], m_Planes[FRUSTUMPLANE_BOTTOM], m_Planes[FRUSTUMPLANE_RIGHT], &intersection );
	corners[idx++] = intersection;

	Plane::GetPointIntersection( m_Planes[FRUSTUMPLANE_FAR], m_Planes[FRUSTUMPLANE_TOP], m_Planes[FRUSTUMPLANE_LEFT], &intersection );
	corners[idx++] = intersection;

	Plane::GetPointIntersection( m_Planes[FRUSTUMPLANE_FAR], m_Planes[FRUSTUMPLANE_TOP], m_Planes[FRUSTUMPLANE_RIGHT], &intersection );
	corners[idx++] = intersection;

	return (8 == idx);
}

bool Frustum::PointInFrustum(const glm::vec3 &point)
{
	for ( int i = 0; i < 6; ++i )
	{
		if (  m_Planes[i].DistanceToPlane( point ) <= 0 )
		{
			return false;
		}
	}

	return true;
}

bool Frustum::AABBInFrustum( BoundingBox *boundingBox, glm::mat4x4 transform )
{
	glm::vec3 vPoints[8];
	int totalIn = 0;

	boundingBox->GetPoints( vPoints );

	for ( int p = 0; p < 6; ++p )
	{
		int inCount = 8;
		int pointIn = 1;

		for(int i = 0; i < 8; ++i) {
			// test this point against the planes
			if( m_Planes[p].DistanceToPlane( vPoints[i] ) <= 0) {
				pointIn = 0;
				--inCount;
			}
		}

		// were all the points outside of plane p?
		if ( inCount == 0 )
			return false;

		// check if they were all on the right side of the plane
		totalIn += pointIn;
	}

	if(totalIn == 6)
		return true; // all in

	return true; // intersect
}