#pragma once
#include "TextureAsset.h"

class FramebufferObject
{
public:
	FramebufferObject(void);
	~FramebufferObject(void);

	virtual void Create( unsigned int Width, unsigned int Height ) {}

	inline unsigned int GetFBOID() { return m_FBOID; }
	inline unsigned int Width() { return m_Width; }
	inline unsigned int Height() { return m_Height; }

	inline uint32 GetTextureId() { return m_pTexture ? m_pTexture->TextureID() : 0; }
	inline TextureAsset* GetTexture() { return m_pTexture; }

protected:
	TextureAsset *m_pTexture;

	unsigned int m_Width;
	unsigned int m_Height;

	unsigned int m_FBOID;
};

