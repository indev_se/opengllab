#pragma once

#define PLANE_DENOM 0

enum ClassificationPointsEnum { CP_FRONT, CP_BACK, CP_SPLIT, CP_ONPLANE };

class Plane
{
public:
	Plane(void);
	~Plane(void);

	void CalculatePlane( const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2 );
	void SetCoefficients( float a, float b, float c, float d );
	void Normalize();

	float DistanceToPlane(const glm::vec3 &v);
	ClassificationPointsEnum ClassifyPoint(const glm::vec3 &v);
	ClassificationPointsEnum ClassifyPoints( std::list<const glm::vec3> vertices );

	inline void SetNormal( const glm::vec3 &normal ) { m_vecNormal = normal; }
	inline void SetDistance( float distance ) { m_fDistance = distance; }

	inline glm::vec3 GetNormal() { return m_vecNormal; }
	inline float GetDistance() { return m_fDistance; }

	static bool GetPointIntersection( Plane &p1, Plane &p2, Plane &p3, glm::vec3 *intersectionPoint );

private:

	glm::vec3 m_vecNormal;
	float m_fDistance;

};

