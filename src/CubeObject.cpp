#include "StdAfx.h"
#include "StrategyGLScene.h"
#include "CubeObject.h"


CubeObject::CubeObject( float SizeX, float SizeY, float SizeZ, const char *objectName ) : SceneObject(objectName)
{
	m_vecSize = glm::vec3(SizeX, SizeY, SizeZ);
	m_pVB = nullptr;
}

CubeObject::~CubeObject(void)
{
	if ( m_pVB != nullptr )
		delete m_pVB;

	m_pVB = nullptr;
}

void CubeObject::Init( GLScene *scene, Material *material, VertexDescription *description )
{
	SceneObject::Init( scene, material );

	glm::vec3 vecHalvSize = m_vecSize / 2.0f;

	VertexDescription *usedDescription = nullptr;

	if ( material != nullptr )
		usedDescription = material->GetDescription();
	else if ( description != nullptr )
		usedDescription = description;
	else 
		usedDescription = VertexDescription::GetDefault("Position3Color3Normal3");

	// We'll expect it to have InPosition, InNormal and InColor elements
	VertexDataElement *POS = usedDescription->GetElement("InPosition");
	VertexDataElement *NOR = usedDescription->GetElement("InNormal");
	VertexDataElement *COL = usedDescription->GetElement("InColor");

	m_pVB = new VertexBuffer();

	VertexData *vertices = new VertexData[24];

	// Top Face
	vertices[0].set(POS, glm::vec3(-vecHalvSize.x, vecHalvSize.y,  -vecHalvSize.z) );
	vertices[1].set(POS, glm::vec3(-vecHalvSize.x, vecHalvSize.y,  vecHalvSize.z) );
	vertices[2].set(POS, glm::vec3( vecHalvSize.x, vecHalvSize.y,  vecHalvSize.z) );
	vertices[3].set(POS, glm::vec3( vecHalvSize.x, vecHalvSize.y, -vecHalvSize.z) );
	
	vertices[0].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[1].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[2].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[3].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	/*
	vertices[0].set(COL, Vector3f(1.0f, 0.0f, 0.0f) );
	vertices[1].set(COL, Vector3f(1.0f, 0.0f, 0.0f) );
	vertices[2].set(COL, Vector3f(1.0f, 0.0f, 0.0f) );
	vertices[3].set(COL, Vector3f(1.0f, 0.0f, 0.0f) );
	*/
	vertices[0].set(NOR, glm::vec3(0.0f,1.0f,0.0f) );
	vertices[1].set(NOR, glm::vec3(0.0f,1.0f,0.0f) );
	vertices[2].set(NOR, glm::vec3(0.0f,1.0f,0.0f) );
	vertices[3].set(NOR, glm::vec3(0.0f,1.0f,0.0f) );
	
	// Bottom Face
	vertices[4].set(POS, glm::vec3(-vecHalvSize.x, -vecHalvSize.y, -vecHalvSize.z) );
	vertices[5].set(POS, glm::vec3( vecHalvSize.x, -vecHalvSize.y, -vecHalvSize.z) );
	vertices[6].set(POS, glm::vec3( vecHalvSize.x, -vecHalvSize.y,  vecHalvSize.z) );
	vertices[7].set(POS, glm::vec3(-vecHalvSize.x, -vecHalvSize.y,  vecHalvSize.z) );
	
	
	vertices[4].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[5].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[6].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[7].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	/*
	vertices[4].set(COL, Vector3f(1.0f, 1.0f, 0.0f) );
	vertices[5].set(COL, Vector3f(1.0f, 1.0f, 0.0f) );
	vertices[6].set(COL, Vector3f(1.0f, 1.0f, 0.0f) );
	vertices[7].set(COL, Vector3f(1.0f, 1.0f, 0.0f) );
	*/
	
	vertices[4].set(NOR, glm::vec3(0.0f,-1.0f,0.0f) );
	vertices[5].set(NOR, glm::vec3(0.0f,-1.0f,0.0f) );
	vertices[6].set(NOR, glm::vec3(0.0f,-1.0f,0.0f) );
	vertices[7].set(NOR, glm::vec3(0.0f,-1.0f,0.0f) );
	

	// Front Face
	vertices[8].set(POS, glm::vec3(-vecHalvSize.x, -vecHalvSize.y,  vecHalvSize.z) );
	vertices[9].set(POS, glm::vec3( vecHalvSize.x, -vecHalvSize.y,  vecHalvSize.z) );
	vertices[10].set(POS, glm::vec3( vecHalvSize.x,  vecHalvSize.y,  vecHalvSize.z) );
	vertices[11].set(POS, glm::vec3(-vecHalvSize.x,  vecHalvSize.y,  vecHalvSize.z) );
	
	
	vertices[8].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[9].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[10].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[11].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	/*
	vertices[8].set(COL, Vector3f(0.0f, 1.0f, 0.0f) );
	vertices[9].set(COL, Vector3f(0.0f, 1.0f, 0.0f) );
	vertices[10].set(COL, Vector3f(0.0f, 1.0f, 0.0f) );
	vertices[11].set(COL, Vector3f(0.0f, 1.0f, 0.0f) );
	*/
	
	vertices[8].set(NOR, glm::vec3(0.0f,0.0f,1.0f) );
	vertices[9].set(NOR, glm::vec3(0.0f,0.0f,1.0f) );
	vertices[10].set(NOR, glm::vec3(0.0f,0.0f,1.0f) );
	vertices[11].set(NOR, glm::vec3(0.0f,0.0f,1.0f) );
	
	// Back Face
	vertices[12].set(POS, glm::vec3(-vecHalvSize.x, -vecHalvSize.y, -vecHalvSize.z) );
	vertices[13].set(POS, glm::vec3(-vecHalvSize.x,  vecHalvSize.y, -vecHalvSize.z) );
	vertices[14].set(POS, glm::vec3( vecHalvSize.x,  vecHalvSize.y, -vecHalvSize.z) );
	vertices[15].set(POS, glm::vec3( vecHalvSize.x, -vecHalvSize.y, -vecHalvSize.z) );
	
	
	vertices[12].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[13].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[14].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[15].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	/*
	vertices[12].set(COL, Vector3f(0.0f, 1.0f, 0.0f) );
	vertices[13].set(COL, Vector3f(0.0f, 1.0f, 0.0f) );
	vertices[14].set(COL, Vector3f(0.0f, 1.0f, 0.0f) );
	vertices[15].set(COL, Vector3f(0.0f, 1.0f, 0.0f) );
	*/
	vertices[12].set(NOR, glm::vec3(0.0f,0.0f,-1.0f) );
	vertices[13].set(NOR, glm::vec3(0.0f,0.0f,-1.0f) );
	vertices[14].set(NOR, glm::vec3(0.0f,0.0f,-1.0f) );
	vertices[15].set(NOR, glm::vec3(0.0f,0.0f,-1.0f) );
	
	// Right face
	vertices[16].set(POS, glm::vec3( vecHalvSize.x, -vecHalvSize.y, -vecHalvSize.z) );
	vertices[17].set(POS, glm::vec3( vecHalvSize.x,  vecHalvSize.y, -vecHalvSize.z) );
	vertices[18].set(POS, glm::vec3( vecHalvSize.x,  vecHalvSize.y,  vecHalvSize.z) );
	vertices[19].set(POS, glm::vec3( vecHalvSize.x, -vecHalvSize.y,  vecHalvSize.z) );
	
	
	vertices[16].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[17].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[18].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[19].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	/*
	vertices[16].set(COL, Vector3f(0.0f, 0.0f, 1.0f) );
	vertices[17].set(COL, Vector3f(0.0f, 0.0f, 1.0f) );
	vertices[18].set(COL, Vector3f(0.0f, 0.0f, 1.0f) );
	vertices[19].set(COL, Vector3f(0.0f, 0.0f, 1.0f) );
	*/
	vertices[16].set(NOR, glm::vec3(1.0f,0.0f,0.0f) );
	vertices[17].set(NOR, glm::vec3(1.0f,0.0f,0.0f) );
	vertices[18].set(NOR, glm::vec3(1.0f,0.0f,0.0f) );
	vertices[19].set(NOR, glm::vec3(1.0f,0.0f,0.0f) );
	

	// Left Face
	vertices[20].set(POS, glm::vec3(-vecHalvSize.x, -vecHalvSize.y, -vecHalvSize.z) );
	vertices[21].set(POS, glm::vec3(-vecHalvSize.x, -vecHalvSize.y,  vecHalvSize.z) );
	vertices[22].set(POS, glm::vec3(-vecHalvSize.x,  vecHalvSize.y,  vecHalvSize.z) );
	vertices[23].set(POS, glm::vec3(-vecHalvSize.x,  vecHalvSize.y, -vecHalvSize.z) );
	
	
	vertices[20].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[21].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[22].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	vertices[23].set(COL, glm::vec3(m_vecColor.r, m_vecColor.g, m_vecColor.b) );
	/*
	vertices[20].set(COL, Vector3f(0.0f, 0.0f, 1.0f) );
	vertices[21].set(COL, Vector3f(0.0f, 0.0f, 1.0f) );
	vertices[22].set(COL, Vector3f(0.0f, 0.0f, 1.0f) );
	vertices[23].set(COL, Vector3f(0.0f, 0.0f, 1.0f) );
	*/
	vertices[20].set(NOR, glm::vec3(-1.0f,0.0f,0.0f) );
	vertices[21].set(NOR, glm::vec3(-1.0f,0.0f,0.0f) );
	vertices[22].set(NOR, glm::vec3(-1.0f,0.0f,0.0f) );
	vertices[23].set(NOR, glm::vec3(-1.0f,0.0f,0.0f) );
	
	// Create the vertext buffer
	m_pVB->CreateVertexBuffer( material->GetDescription(), 24, vertices, GL_STATIC_DRAW );

	delete []vertices;
}

void CubeObject::Update( float tickMS )
{
	SceneObject::Update(tickMS);
}

void CubeObject::SetColorByPoint( const glm::vec4 &v )
{
	if ( ((StrategyGLScene*)m_pScene)->GetCamera()->GetFrustum()->PointInFrustum( glm::vec3(v.x, v.y, v.z) ) )
		glColor3f( 1.0, 0.0, 0.0 );
	else
		glColor3f( 0.0, 1.0, 0.0 );
}

void CubeObject::Render(RenderPass *pass)
{
	if ( pass != nullptr )
		pass->BindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->BindForward();

	glPushMatrix();
	
	glMultMatrixf(glm::value_ptr(m_matTransform));
	
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, glm::value_ptr(m_vecColor) );

	m_pVB->Render( GL_QUADS, (uint32)0, (uint32)0 );
	
	SceneObject::Render(pass);

	glPopMatrix();

	if ( pass != nullptr )
		pass->UnbindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->UnbindForward();
}
