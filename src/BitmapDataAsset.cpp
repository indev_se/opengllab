#include "StdAfx.h"
#include "FreeImage.h"
#include "BitmapDataAsset.h"

BitmapDataAsset::BitmapDataAsset(void)
{
	m_Width = m_Height = 0;
	m_Bits = NULL;
}

BitmapDataAsset::~BitmapDataAsset(void)
{
	if ( m_Bits != NULL && m_Bits != 0 )
	{
		delete m_Bits;
		m_Bits = NULL;
	}
}

bool BitmapDataAsset::CreateFromFile( const char *filename )
{
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	FIBITMAP *dib;
	
	//check the file signature and deduce its format
	fif = FreeImage_GetFileType(filename, 0);
	
	//if still unknown, try to guess the file format from the file extension
	if(fif == FIF_UNKNOWN) 
		fif = FreeImage_GetFIFFromFilename(filename);
	
	//if still unkown, return failure
	if(fif == FIF_UNKNOWN) {
		StdConsole::LogError( _STR"Unknown fileformat: " << filename);
		return false;
	}

	//check that the plugin has reading capabilities and load the file
	if(FreeImage_FIFSupportsReading(fif))
		dib = FreeImage_Load(fif, filename);
	
	//if the image failed to load, return failure
	if(!dib) {
		StdConsole::LogError( _STR"Unable to load file: " << filename);
		return false;
	}

	//get the image information
	m_BPP = FreeImage_GetBPP(dib);
	m_ScanWidth = FreeImage_GetPitch(dib);
	m_Width = FreeImage_GetWidth(dib);
	m_Height = FreeImage_GetHeight(dib);

	//retrieve the image data
	BYTE *bits = FreeImage_GetBits(dib);
	m_Bits = new BYTE[ m_Height * m_ScanWidth ];
	memcpy( m_Bits, bits, m_Height * m_ScanWidth );

	//if this somehow one of these failed (they shouldn't), return failure
	if((m_Bits == 0) || (m_Width == 0) || (m_Height == 0)) {
		StdConsole::LogError( _STR"Invalid bits: Bit: " << m_Bits << ", Width: " << m_Width << ", Height: " << m_Height);
		return false;
	}

	//Free FreeImage's copy of the data
	FreeImage_Unload(dib);

	StdConsole::LogInfo( _STR"Loaded BitmapDataAsset" << filename );
		
	return true;
}

bool BitmapDataAsset::CreateFromBits( BYTE *_Bits, UINT _BPP, UINT _Width, UINT _Height )
{
	m_BPP = _BPP;
	m_Width = _Width;
	m_Height = _Height;
	m_ScanWidth = (m_BPP / 8) * m_Width;
	
	m_Bits = new BYTE[ m_Height * m_ScanWidth ];
	memcpy( m_Bits, _Bits, m_Height * m_ScanWidth );

	return true;
}

void BitmapDataAsset::ConvertTo8Bits()
{
	if ( m_BPP <= 8 ) // Already 8 bit or less
		return;

	FIBITMAP *dib;

	// Create a new FreeImage Bitmap
	dib = FreeImage_ConvertFromRawBits(m_Bits, m_Width, m_Height, m_ScanWidth, m_BPP, 0xFF0000, 0x00FF00, 0x0000FF, TRUE);
	if(!dib) {
		StdConsole::LogError( _STR"Unable to create FIBITMAP from bits" );
		return;
	}

	// Delete the old bitmapdata
	delete m_Bits;

	// Convert to 8-bits
	dib = FreeImage_ConvertTo8Bits(dib);

	// retrieve the new image info
	m_BPP = FreeImage_GetBPP(dib);
	m_ScanWidth = FreeImage_GetPitch(dib);

	//retrieve the new image data
	BYTE *bits = FreeImage_GetBits(dib);
	m_Bits = new BYTE[ m_Height * m_ScanWidth ];
	memcpy( m_Bits, bits, m_Height * m_ScanWidth );
	
	//Free FreeImage's copy of the data
	FreeImage_Unload(dib);
}

GLenum BitmapDataAsset::GetGLFormat()
{
	switch(m_BPP)
	{
	case 8:
	case 16:
	case 24:
		return GL_BGR;
	case 32:
		return GL_BGRA;
	}

	return GL_BGR;
}

GLenum BitmapDataAsset::GetGLDestFormat()
{
	switch(m_BPP)
	{
	case 8:
	case 16:
	case 24:
		return GL_RGB;
	case 32:
		return GL_RGBA;
	}

	return GL_RGB;
}