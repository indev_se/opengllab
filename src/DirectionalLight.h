#pragma once
#include "lightobject.h"
class DirectionalLight :
	public LightObject
{
public:
	DirectionalLight(const char *objectName);
	~DirectionalLight(void);

	void SetDirection( const glm::vec3 &vDirection );
	glm::vec3 GetDirection();
	void GetDirection( glm::vec3 &direction );

	void SetShaderUniform( char LightVar[60], ShaderProgram *shader );

protected:

	glm::vec3 m_vDirection;
};

