#include "StdAfx.h"
#include "SceneObject.h"
#include "GLScene.h"
#include "SceneManager.h"

SceneManager *SceneManager::m_pInst = NULL;
SceneManager::SceneManager(void)
{
}

SceneManager::~SceneManager(void)
{
}

SceneManager *SceneManager::Inst()
{
	if ( m_pInst == NULL )
		m_pInst = new SceneManager();

	return m_pInst;
}

void SceneManager::RegisterObject( const char *objectName, SceneObject *obj )
{
	m_mapObjects[objectName] = obj;
}

void SceneManager::UnregisterObject( const char *objectName )
{
	m_mapObjects[objectName] = NULL;
}

SceneObject *SceneManager::GetSceneObject( const char *objectName )
{
	if ( m_mapObjects[objectName] == NULL ) {
		//printf("Object '%s' have not been registered\n", objectName);
		return NULL;
	}

	return m_mapObjects[objectName];
}