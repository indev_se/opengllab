// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <MMSystem.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <math.h>

// STD libraries
#include <map>
#include <algorithm>
#include <list>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>

#define GLEW_STATIC

// OpenGL
#include <gl\glew.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\wglew.h>

#include "Units.h"

// Math
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtx/transform.hpp>
#include <gtc/type_ptr.hpp>
#include <gtx/quaternion.hpp>

typedef char            int8;
typedef unsigned char   uint8;

typedef short           int16;
typedef unsigned short  uint16;

typedef int				int32;
typedef unsigned int	uint32;

typedef long            int64;
typedef unsigned long   uint64;

enum DataType
{
	Double,
	Float,
	Integer,
	String,
	Boolean,
	Byte
};

#pragma warning( disable : 4244 ) // 'argument' : conversion from 'type1' to 'type2', possible loss of data
#pragma warning( disable : 4482 ) // nonstandard extension used: enum 'enum' used in qualified name

// General
#include "Utils.h"
#include "StreamUtils.h"
#include "Plane.h"
#include "StdConsole.h"

// Scence
#include "RenderPass.h"
#include "SceneObject.h"
#include "RenderBatch.h"
#include "RenderJob.h"