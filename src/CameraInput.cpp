#include "StdAfx.h"
#include "InputManager.h"
#include "CameraInput.h"

CameraInput::CameraInput(void)
{
	m_pInputManger = InputManager::Inst();
	m_pCamera = nullptr;
}

CameraInput::~CameraInput(void)
{
}

void CameraInput::SetCamera( GLCamera *Camera )
{
	m_pCamera = Camera;
}

GLCamera *CameraInput::GetCamera()
{
	return m_pCamera;
}

void CameraInput::Update( float tickMS )
{
	if ( m_pCamera == nullptr )
		return;

	CheckKeys(tickMS);
	CheckMouse(tickMS);
}

void CameraInput::CheckKeys( float tickMS )
{
	/*
	if ( m_pInputManger->GetKey( VK_UP ) )
	{
		m_pCamera->RotateEyeX( 0.1f );
	}
	if ( m_pInputManger->GetKey( VK_DOWN ) )
	{
		m_pCamera->RotateEyeX( -0.1f );
	}
	if ( m_pInputManger->GetKey( VK_LEFT ) )
	{
		m_pCamera->RotateEyeY( 0.1f );
	}
	if ( m_pInputManger->GetKey( VK_RIGHT ) )
	{
		m_pCamera->RotateEyeY( -0.1f );
	}
	*/
	float units = 4.5f;//CAMERA_UNITS_PER_SEC * (tickMS);
	
	if ( m_pInputManger->GetKey( KEY_W ) )
	{
		m_pCamera->MoveEyeZ( units );
	}
	if ( m_pInputManger->GetKey( KEY_S ) )
	{
		m_pCamera->MoveEyeZ( -units );
	}
	if ( m_pInputManger->GetKey( KEY_A ) )
	{
		m_pCamera->MoveEyeX( units );
	}
	if ( m_pInputManger->GetKey( KEY_D ) )
	{
		m_pCamera->MoveEyeX( -units );
	}
}

void CameraInput::CheckMouse( float tickMS )
{
	int dx=0, dy=0;
	m_pInputManger->GetCurrentMousePos(dx,dy);

	if (dx!=0) {
		m_pCamera->RotateEyeY( -dx * .1 );
	}
	if (dy!=0) {
		m_pCamera->RotateEyeX( -dy * .1 );
	}
}