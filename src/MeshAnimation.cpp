#include "StdAfx.h"
#include "MeshAnimation.h"


MeshAnimation::MeshAnimation(void)
{
	m_pSkeleton = nullptr;
}


MeshAnimation::~MeshAnimation(void)
{
}

void MeshAnimation::SetKeyFrames( const Bone* bone, const KeyFramesVector& frames )
{
	m_KeyFrameMap[bone] = frames;

	for ( uint32 i = 0; i < frames.size(); ++i )
	{
		m_KeyFrames.push_back( frames[i] );
	}
}

void MeshAnimation::AddKeyFrame( const Bone* bone, AnimationKeyFrame* frame )
{
	m_KeyFrameMap[bone].push_back( frame );
	m_KeyFrames.push_back( frame );
}

KeyFramesVector MeshAnimation::GetKeyFrames( const Bone* bone )
{
	return m_KeyFrameMap[bone];
}

uint32 MeshAnimation::GetNumKeyFrames()
{
	KeyFramesMap::iterator iter = m_KeyFrameMap.begin();
	KeyFramesVector frames = iter->second;
	return frames.size();
}