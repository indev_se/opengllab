#pragma once
#include "Mesh.h"
#include "FbxManager.h"

class FbxTakeImporter
{
public:
	FbxTakeImporter(void);
	~FbxTakeImporter(void);

	bool LoadTake( Mesh* mesh, const char *filename );

private:

	void ProcessBoneAnimation( Bone *bone, KFbxNode *node, KFbxScene *scene, Mesh* mesh );

	void ReadSkeleton( KFbxNode *node, KFbxScene *scene, Mesh* mesh );
	void ReadSkeletonRecursive( KFbxNode *node, int parentIndex, Skeleton *skeleton, KFbxScene *scene, Mesh* mesh );

	KFbxNode* FindRoot(KFbxNode* root, KFbxNodeAttribute::EAttributeType attrib);

	glm::mat4x4 ConvertMatrix( KFbxMatrix src );

};