#pragma once
#include "GLScene.h"

class CameraInput;
class MaterialImporter;
class ForwardRenderPass;

class SkydomeScene :
	public GLScene
{
public:
	SkydomeScene(void);
	~SkydomeScene(void);

	void SetViewport( int Width, int Height );

	void Init();
	void Update( float tickMS );
	void Render();

	void GetBounds( glm::vec3 &min, glm::vec3 &max );
	
protected:
	GLCamera *m_pDefaultFPSCamera;
	CameraInput *m_pCameraInput;
	
	MaterialImporter *m_MaterialImporter;

	SceneObject *m_RootObject;
	SceneObject *m_RootLightObject;

	ForwardRenderPass *passForward;
};

