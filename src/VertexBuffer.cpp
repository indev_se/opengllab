#include "StdAfx.h"
#include "VertexBuffer.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

VertexBuffer::VertexBuffer(void)
{
	m_bHaveVBID = false;
	m_bHaveIBID = false;

	m_iIndexCount = 0;
	m_iVertexCount = 0;
}

VertexBuffer::~VertexBuffer(void)
{
	Destroy();
}

void VertexBuffer::Destroy()
{
	if ( m_bHaveVBID )
		glDeleteBuffers(1, &m_iVBID);
	if ( m_bHaveIBID )
		glDeleteBuffers(1, &m_iIBID);
	
	m_bHaveVBID = m_bHaveIBID = false;
}

void VertexBuffer::CreateVertexBuffer( VertexDescription *desc, size_t vCount, VertexData *data, GLenum usage ) 
{
	if ( m_bHaveVBID == false )
	{
		glGenBuffers( 1, &m_iVBID );
		m_bHaveVBID = true;
	}

	m_iVertexCount = vCount;
	m_VertexDesc = desc;
	m_iVertexSize = m_VertexDesc->VertexSize();

	// We want to create a memory aligned buffer for the vertex data
	std::vector<uint8> vertices;

	int size = m_iVertexSize;
	for ( uint32 i = 0; i < vCount; ++i )
	{
		vertices.resize( vertices.size() + size );
		std::copy(data[i].data(), data[i].data()+size, vertices.end()-size);
	}

	// Bind the buffer and start setting the data
	glBindBuffer( GL_ARRAY_BUFFER, m_iVBID );

	// Allocate memory and send the data to GL
	glBufferData( GL_ARRAY_BUFFER,  m_iVertexCount * m_iVertexSize, &vertices[0], usage);

	// unbind
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	// we can now erase the aligned memory buffer
	vertices.clear();

	/*

	// We want to create a memory aligned buffer for the vertex data
	std::vector<uint8> vertices;

	int size = desc->VertexSize();
	for ( uint32 i = 0; i < vCount; ++i )
	{
		vertices.resize( vertices.size() + size );
		std::copy(data[i].data(), data[i].data()+size, vertices.end()-size);
	}

	this->CreateVertexBuffer( desc, vCount, (void*)&vertices[0], usage);

	// we can now erase the aligned memory buffer
	vertices.clear();
	*/
}

void VertexBuffer::CreateVertexBuffer( VertexDescription *desc, size_t vCount, void *data, GLenum usage )
{
	if ( m_bHaveVBID == false )
	{
		glGenBuffers( 1, &m_iVBID );
		m_bHaveVBID = true;
	}

	m_iVertexCount = vCount;
	m_VertexDesc = desc;
	m_iVertexSize = m_VertexDesc->VertexSize();

	// Bind the buffer and start setting the data
	glBindBuffer( GL_ARRAY_BUFFER, m_iVBID );

	// Allocate memory and send the data to GL
	glBufferData( GL_ARRAY_BUFFER,  m_iVertexCount * m_iVertexSize, data, usage);

	// unbind
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

void VertexBuffer::SetVertexBuffer( int offset, size_t vCount, VertexData *data )
{
	if ( m_bHaveVBID == false )
		return;

	m_iVertexCount = vCount;
	
	glBindBuffer( GL_ARRAY_BUFFER, m_iVBID );

	// We want to create a memory aligned buffer for the vertex data
	std::vector<uint8> vertices;

	int size = m_iVertexSize;
	for ( uint32 i = 0; i < vCount; ++i )
	{
		vertices.resize( vertices.size() + size );
		std::copy(data[i].data(), data[i].data()+size, vertices.end()-size);
	}

	glBufferSubData( GL_ARRAY_BUFFER, 0, m_iVertexCount * m_iVertexSize, &vertices[0] );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	// we can now erase the aligned memory buffer
	vertices.clear();
}

void VertexBuffer::CreateIndexBuffer( size_t iCount, uint16 *indices, GLenum usage )
{
	if ( m_bHaveIBID == false )
	{
		glGenBuffers( 1, &m_iIBID );
		m_bHaveIBID = true;
	}

	m_iIndexCount = iCount;
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iIBID);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iCount * sizeof(uint16), indices, usage);

	// unbind
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}

void VertexBuffer::CreateIndexBuffer( size_t iCount, uint32 *indices, GLenum usage )
{
	if ( m_bHaveIBID == false )
	{
		glGenBuffers( 1, &m_iIBID );
		m_bHaveIBID = true;
	}

	m_iIndexCount = iCount;
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iIBID);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iCount * sizeof(uint32), indices, usage);

	// unbind
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}

void VertexBuffer::SetIndexBuffer( int offset, size_t iCount, uint16 *indices )
{
	if ( m_bHaveIBID == false )
		return;

	m_iIndexCount = m_iIndexCount + iCount;
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iIBID);

	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset * sizeof(uint16), iCount * sizeof(uint16), indices);
	assert( glGetError() == 0 );

	// unbind
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}

void VertexBuffer::Render( GLenum mode, uint16 indexOffset, uint16 indexCount )
{
	if ( m_bHaveVBID == false )
	{
		return;
	}

	BindBuffers();

	if ( m_iIndexCount > 0 && m_bHaveIBID )
	{
		glEnableClientState(GL_INDEX_ARRAY);

		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_iIBID );
		
		if ( indexOffset == 0 && indexCount == 0)
		{
			glDrawElements( mode, m_iIndexCount, GL_UNSIGNED_SHORT, 0 );
		}
		else
		{
			glDrawRangeElements( mode, indexOffset, indexOffset+indexCount, indexCount, GL_UNSIGNED_SHORT, (const GLvoid*)(indexOffset*sizeof(GLshort)) );
		}

		glDisableClientState(GL_INDEX_ARRAY);
	}
	else
	{
		glDrawArrays( mode, 0, m_iVertexCount );
	}

	// unbind 
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	DisableVertexAttribs();
}

void VertexBuffer::Render( GLenum mode, uint32 indexOffset, uint32 indexCount )
{
	if ( m_bHaveVBID == false )
	{
		return;
	}

	BindBuffers();

	if ( m_iIndexCount > 0 && m_bHaveIBID )
	{
		glEnableClientState(GL_INDEX_ARRAY);

		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_iIBID );
		
		if ( indexOffset == 0 && indexCount == 0)
		{
			glDrawElements( mode, m_iIndexCount, GL_UNSIGNED_INT, 0 );
		}
		else
		{
			glDrawRangeElements( mode, indexOffset, indexOffset+indexCount, indexCount, GL_UNSIGNED_INT, (const GLvoid*)(indexOffset*sizeof(GLint)) );
		}

		glDisableClientState(GL_INDEX_ARRAY);
	}
	else
	{
		glDrawArrays( mode, 0, m_iVertexCount );
	}

	// unbind 
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	DisableVertexAttribs();
}

void VertexBuffer::Render( GLenum mode, uint16* indices, uint32 indexCount )
{
	if ( m_bHaveVBID == false )
	{
		return;
	}

	BindBuffers();
	
	if ( indexCount > 0 && indices != NULL )
	{
		glDrawElements( mode, indexCount, GL_UNSIGNED_SHORT, indices );
	}
	
	// unbind 
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	DisableVertexAttribs();
}

void VertexBuffer::Render( GLenum mode, uint32* indices, uint32 indexCount )
{
	if ( m_bHaveVBID == false )
	{
		return;
	}

	BindBuffers();
	
	if ( indexCount > 0 && indices != NULL )
	{
		glDrawElements( mode, indexCount, GL_UNSIGNED_INT, indices );
	}
	
	// unbind 
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
    
	DisableVertexAttribs();
}

void VertexBuffer::Render( GLenum mode )
{
	if ( m_bHaveVBID == false )
		return;

	BindBuffers();

	glDrawArrays( mode, 0, m_iVertexCount );

	// unbind 
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	DisableVertexAttribs();
}

void VertexBuffer::BindBuffers()
{
	glBindBuffer( GL_ARRAY_BUFFER, m_iVBID );

	EnableVertexAttribs();

	std::vector<VertexDataElement*> elems = m_VertexDesc->GetElements();
	VertexDataElement *elem;

	for( uint32 i = 0; i < elems.size(); ++i )
	{
		elem = elems[i];

		if ( elem->Enabled )
			glVertexAttribPointer(elem->AttribLocation, elem->Size, elem->GetType(), GL_FALSE, m_iVertexSize, BUFFER_OFFSET(elem->Offset));
 	}
}

void VertexBuffer::EnableVertexAttribs()
{
	std::vector<VertexDataElement*> elems = m_VertexDesc->GetElements();
	for( uint32 i = 0; i < elems.size(); ++i )
	{
		if ( elems[i]->Enabled )
			glEnableVertexAttribArray( elems[i]->AttribLocation );
	}
}

void VertexBuffer::DisableVertexAttribs()
{
	std::vector<VertexDataElement*> elems = m_VertexDesc->GetElements();
	for( uint32 i = 0; i < elems.size(); ++i )
	{
		if ( elems[i]->Enabled )
			glDisableVertexAttribArray( elems[i]->AttribLocation );
	}
}