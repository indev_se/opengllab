#pragma once
#include "Bone.h"

class Skeleton
{
public:
	Skeleton(void);
	~Skeleton(void);

	void AddBone( Bone* bone );
	const Bone* GetBone( uint32 index );
	std::vector<Bone*> GetBones();

	Bone* GetBoneByName( std::string name );

	uint32 Size();

private:

	std::vector<Bone*> m_Bones;

public:

	friend std::ostream& operator<<(std::ostream& os, const Skeleton& s)
	{
		// NumBones
		uint32 numBones = s.m_Bones.size();
		SWRITE(os, numBones);

		// Write all bones
		for ( uint32 i = 0; i < s.m_Bones.size(); ++i )
			os << *s.m_Bones[i];

		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, Skeleton& s)
	{
		uint32 numBones;
		SREAD(is,numBones);

		for ( uint32 i = 0; i < numBones; ++i ) {
			Bone *bone = new Bone();
			is >> *bone;
			s.m_Bones.push_back(bone);
		}

		return is;
	}
};

