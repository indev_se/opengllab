#pragma once
#include "AssetsManager.h"
#include "Material.h"

class MaterialImporter
{
public:
	MaterialImporter(void);
	~MaterialImporter(void);

	void ImportAssetsFromFile( const char *filename, AssetsManager *assets );

private:

	bool ParseMaterials( FILE *fileHandle );

	bool ParseMaterial( std::string Name, FILE *fileHandle );
	bool ParseMaterialVertex( Material *material, FILE *fileHandle );
	bool ParseMaterialShaderForward( Material *material, FILE *fileHandle );
	bool ParseMaterialShaderDeferred( Material *material, FILE *fileHandle );
	ShaderProgram *ParseMaterialShader( Material *material, FILE *fileHandle );
	bool ParseMaterialTexture( Material *material, FILE *fileHandle );

	void TrimSpecial(std::string &inStr);
	void TrimWhitespace(std::string &inStr);

	AssetsManager *m_pAssetsManager;

	uint32 m_iCurrentLine;
};