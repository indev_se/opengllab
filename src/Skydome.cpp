#include "StdAfx.h"
#include "Skydome.h"

Skydome::Skydome( uint32 _iLatitude, uint32 _iLongitude, float _fRadius, const char *objectName ) : SceneObject(objectName)
{
	m_pVB = nullptr;
	m_pVertexDesc = nullptr;

	m_iLatitude = _iLatitude;
	m_iLongitude = _iLongitude;
	m_fRadius = _fRadius;
}

Skydome::~Skydome(void)
{
	if ( nullptr != m_pVB )
		delete m_pVB;
	if ( nullptr != m_pVertexDesc )
		delete m_pVertexDesc;

	m_pVB = nullptr;
	m_pVertexDesc = nullptr;
}

void Skydome::Init( GLScene *scene )
{
	m_pScene = scene;
	
	AssetsManager *manager = m_pScene->GetAssetsManager();

	m_pMaterial = manager->GetMaterial("Skydome");

	InitGeometry();
}

void Skydome::Update( float tickMS )
{
	glm::vec3 camEye;
	m_pScene->GetCamera()->GetEye(camEye);

	Identity();
	SetPosition( glm::vec3(-camEye.x, -camEye.y - (m_fRadius*0.05f), -camEye.z) );
}

void Skydome::Render(RenderPass *pass)
{
	if ( pass != nullptr )
		pass->BindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->BindForward();

	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST); 

	m_pVB->Render(GL_TRIANGLES, (uint16)0, (uint16)0 );

	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST); 

	SceneObject::Render(pass);

	if ( pass != nullptr )
		pass->UnbindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->UnbindForward();
}

void Skydome::InitGeometry()
{
	VertexDataElement *POS, *TEX0;
	uint32 numVertices, numIndices, iIndex, vIndex;
	int32 iLat, iLng;
	float fLat, fLng;
	glm::vec3 v;

	//InitVertexDescription();
	
	m_pVertexDesc = m_pMaterial->GetDescription();

	POS = m_pVertexDesc->GetElement("InPosition");
	TEX0 = m_pVertexDesc->GetElement("InTex0");
	
	numVertices = ((m_iLatitude + 1) * (m_iLongitude)) + 1;
	numIndices = (m_iLatitude * (m_iLongitude-1) * 6) + (m_iLatitude * 3);

	iIndex = 0;
	vIndex = 0;

	VertexData *vertices = new VertexData[numVertices]();
	uint16 *indices = new uint16[numIndices];

	// create the vertices
	for ( iLat = 0; iLat < m_iLatitude + 1.0; ++iLat ) {
		for ( iLng = 0; iLng < m_iLongitude; ++iLng ) {
			fLat = 2.0f * M_PI / m_iLatitude * iLat;
			fLng = M_PI * ( 0.5f / m_iLongitude * iLng );

			vertices[vIndex].set(TEX0, glm::vec2( iLat / (float)m_iLatitude, iLng / float(m_iLongitude+1) ) );
			vertices[vIndex++].set(POS, glm::vec3( cosf(fLng) * cosf(fLat), sinf(fLng), -cosf(fLng) * sinf(fLat) ) * m_fRadius );
		}
	}
	vertices[vIndex].set(TEX0, glm::vec2( .5f, 1.0f ) );
	vertices[vIndex++].set(POS, glm::vec3( 0.0f, 1.0f, 0.0f ) * m_fRadius );

	// create the indices
	for ( iLat = 0; iLat < m_iLatitude; ++iLat ) {
		for ( iLng = 1; iLng < m_iLongitude; ++iLng ) {
			
			uint16 v1 = iLat * m_iLongitude + iLng - 1;
			uint16 v2 = (iLat + 1.0) * m_iLongitude + iLng - 1;
			uint16 v3 = v2 + 1;
			uint16 v4 = v1 + 1;

			indices[iIndex++] = v1;
			indices[iIndex++] = v4;
			indices[iIndex++] = v2;

			indices[iIndex++] = v2;
			indices[iIndex++] = v4;
			indices[iIndex++] = v3;
		}
	}

	// create the indices by pols (stitch the end)
	uint16 solarIndex = vIndex-1;
	iLng = m_iLongitude - 1;
	for ( iLat = 0; iLat < m_iLatitude; ++iLat ) {
		
		uint16 v1 = iLat * m_iLongitude + iLng;
		uint16 v2 = (iLat + 1.0) * m_iLongitude + iLng;
		uint16 v3 = solarIndex;
		
		indices[iIndex++] = v1;
		indices[iIndex++] = v3;
		indices[iIndex++] = v2;
	}

	m_pVB = new VertexBuffer();
	m_pVB->CreateVertexBuffer( m_pVertexDesc, numVertices, vertices, GL_STATIC_DRAW );
	m_pVB->CreateIndexBuffer( numIndices, indices, GL_STATIC_DRAW );

	delete []vertices;
	delete []indices;
}

void Skydome::InitVertexDescription()
{
	VertexDataElement *POS, *TEX0;

	m_pVertexDesc = new VertexDescription();

	POS = new VertexDataElement();
	TEX0 = new VertexDataElement();

	POS->Name = "InPosition";
	TEX0->Name = "InTex0";
	
	POS->ParseTypeSize("float3");
	TEX0->ParseTypeSize("float2");
	
	m_pVertexDesc->AddElement(POS);
	m_pVertexDesc->AddElement(TEX0);

	m_pVertexDesc->Lock();
}