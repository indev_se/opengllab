#include "StdAfx.h"
#include "RenderCascadedShadowMapPass.h"


RenderCascadedShadowMapPass::RenderCascadedShadowMapPass(void)
{
	m_bBindTextures = false;
	m_CurrentBindSplit = 0;
	m_pShadowMap = nullptr;
}

RenderCascadedShadowMapPass::~RenderCascadedShadowMapPass(void)
{
	if ( m_pShadowMap != nullptr )
		delete m_pShadowMap;

	m_pShadowMap = nullptr;
}

void RenderCascadedShadowMapPass::Init( LightObject *light, GLScene *scene, uint32 NumSplits )
{
	RenderPass::Init(scene);

	// Create the shadowmap
	m_pShadowMap = new CascadedShadowMap();
	m_pShadowMap->Create( light, scene, NumSplits, 2048, 2048, true);
}

void RenderCascadedShadowMapPass::SetSplitIndex( uint32 SplitIndex )
{
	m_CurrentBindSplit = SplitIndex;
}

uint32 RenderCascadedShadowMapPass::GetNumSplits()
{
	return m_pShadowMap->GetSplitCount();
}

void RenderCascadedShadowMapPass::Bind()
{
	m_pShadowMap->BindFramebuffer(m_CurrentBindSplit);
	glCullFace(GL_FRONT);
}

void RenderCascadedShadowMapPass::Unbind()
{
	m_pShadowMap->UnbindFramebuffer();
	glCullFace(GL_BACK);
}

void RenderCascadedShadowMapPass::BindObject( SceneObject *object )
{
	// Bind the shader
	Material *mtrl = object->GetMaterial();

	if ( mtrl->GetShaderDeferred() ) {
		mtrl->BindDeferred(this);

		glm::mat4x4 matLightView = m_pShadowMap->GetViewMatrix(m_CurrentBindSplit);
		glm::mat4x4 matLightProjection = m_pShadowMap->GetProjectionMatrix(m_CurrentBindSplit);

		mtrl->GetBoundShader()->SendUniformMatrix4fv("matModel", 1, false, (float*)&object->GetModelMatrix());
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matView", 1, false, (float*)&matLightView);
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matPersp", 1, false, (float*)&matLightProjection);

		mtrl->GetBoundShader()->SendUniform1i("IsShadowDepthPass", 1);
	}
	else if ( mtrl->GetShaderForward() ) {
		mtrl->BindForward(this);

		mtrl->GetBoundShader()->SendUniform1i("IsShadowDepthPass", 1);
	}
}

void RenderCascadedShadowMapPass::UnbindObject( SceneObject *object )
{
}

void RenderCascadedShadowMapPass::BindJob( RenderJob *job )
{
	// Bind the shader
	Material *mtrl = job->GetMaterial();

	if ( mtrl->GetShaderDeferred() ) {
		mtrl->BindDeferred(this);

		glm::mat4x4 matLightView = m_pShadowMap->GetViewMatrix(m_CurrentBindSplit);
		glm::mat4x4 matLightProjection = m_pShadowMap->GetProjectionMatrix(m_CurrentBindSplit);

		mtrl->GetBoundShader()->SendUniformMatrix4fv("matModel", 1, false, (float*)&job->GetMatrix());
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matView", 1, false, (float*)&matLightView);
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matPersp", 1, false, (float*)&matLightProjection);

		mtrl->GetBoundShader()->SendUniform1i("IsShadowDepthPass", 1);
	}
	else if ( mtrl->GetShaderForward() ) {
		mtrl->BindForward(this);

		mtrl->GetBoundShader()->SendUniform1i("IsShadowDepthPass", 1);
	}
}

void RenderCascadedShadowMapPass::UnbindJob( RenderJob *job )
{
	Material *mtrl = job->GetMaterial();

	if ( mtrl->GetShaderDeferred() ) {
		mtrl->UnbindDeferred();
	}
	else if ( mtrl->GetShaderForward() ) {
		mtrl->UnbindForward();
	}

	glActiveTextureARB(GL_TEXTURE0);
}