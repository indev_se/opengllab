#include "StdAfx.h"
#include <fcntl.h>
#include <io.h> 

#include "InputManager.h"
#include "GLGameWindow.h"

GLGameWindow::GLGameWindow(void)
{
	m_Evt_ResizeListener = nullptr;
	m_Evt_ResizeScope = nullptr;
}


GLGameWindow::~GLGameWindow(void)
{
	
}

bool GLGameWindow::Create( int Width, int Height, bool Fullscreen )
{
	m_iWidth		= Width;
	m_iHeight		= Height;
	m_bFullscreen	= Fullscreen;

	WNDCLASS	wc;
	DWORD		dwExStyle;
	DWORD		dwStyle;
	RECT		WindowRect;

	WindowRect.left = (long)0;
	WindowRect.right = (long)m_iWidth;
	WindowRect.top = (long)0;
	WindowRect.bottom = (long)m_iHeight;

	m_hInstance			= GetModuleHandle(NULL);
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc		= (WNDPROC) GLGameWindow::StaticWindowProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= m_hInstance;
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= NULL;
	wc.lpszMenuName		= NULL;
	wc.lpszClassName	= L"StrategyGame";

	RegisterClass(&wc);

	if (m_bFullscreen)
	{
		DEVMODE dmScreenSettings;
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth	= m_iWidth;
		dmScreenSettings.dmPelsHeight	= m_iHeight;
		dmScreenSettings.dmBitsPerPel	= 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN);

		dwExStyle=WS_EX_APPWINDOW;
		dwStyle=WS_POPUP;
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		dwStyle=WS_OVERLAPPEDWINDOW;
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);

	m_hWnd = CreateWindowEx(dwExStyle,
						wc.lpszClassName,
						wc.lpszClassName,
						dwStyle |
						WS_CLIPSIBLINGS |
						WS_CLIPCHILDREN,
						0, 0,
						WindowRect.right-WindowRect.left,
						WindowRect.bottom-WindowRect.top,
						NULL,
						NULL,
						m_hInstance,
						NULL);

	unsigned int pixelFormat;

	static PIXELFORMATDESCRIPTOR pfd=
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		32,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		32,											// 32Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	m_hDC = GetDC(m_hWnd);	// get the device context

	pixelFormat = ChoosePixelFormat(m_hDC, &pfd);

	SetPixelFormat(m_hDC, pixelFormat, &pfd);

	m_hRC = wglCreateContext(m_hDC); // get the rendering context

	wglMakeCurrent(m_hDC, m_hRC); // activate rendering context

	ShowWindow(m_hWnd,SW_SHOW);		// Show The Window
	SetForegroundWindow(m_hWnd);	// Slightly Higher Priority
	SetFocus(m_hWnd);

	SetWindowLongPtr( m_hWnd, GWL_USERDATA, (LONG)this );

	InputManager::Inst()->SetWindowPosition(0,0);
	InputManager::Inst()->SetWindowSize(m_iWidth, m_iHeight);

	return true;
}

void GLGameWindow::SetResizeEventListener( GLGameWindowEventListener *scope, GameWindowEventFuncPtr evt_ptr )
{
	m_Evt_ResizeScope = scope;
	m_Evt_ResizeListener = evt_ptr;
}

LRESULT CALLBACK GLGameWindow::StaticWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	GLGameWindow *ptrParent;

	// Get pointer to window
   if(uMsg == WM_CREATE)
   {
      ptrParent = (GLGameWindow*)((LPCREATESTRUCT)lParam)->lpCreateParams;
      SetWindowLongPtr(hWnd,GWL_USERDATA,(LONG_PTR)ptrParent);
   } else {
      ptrParent = (GLGameWindow*)GetWindowLongPtr(hWnd,GWL_USERDATA);
   }

	if(!ptrParent) return DefWindowProc(hWnd,uMsg,wParam,lParam);
    return ptrParent->WindowProc(hWnd, uMsg,wParam,lParam);
}

LRESULT CALLBACK GLGameWindow::WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_ACTIVATE:
		{
			if ( LOWORD(wParam) == WA_ACTIVE || LOWORD(wParam) == WA_CLICKACTIVE )
				m_bActive = TRUE;
			else if ( LOWORD(wParam) == WA_INACTIVE )
				m_bActive = FALSE;

			return 0;
		}

		case WM_SYSCOMMAND:
		{
			switch (wParam)
			{
				case SC_SCREENSAVE:
				case SC_MONITORPOWER:
				return 0;
			}
			break;
		}

		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}

		case WM_MOVE:
		{
			InputManager::Inst()->SetWindowPosition( LOWORD(lParam), HIWORD(lParam) );
			return 0;
		}

		case WM_INPUT:
		case WM_MOUSEMOVE:
		{
			return InputManager::Inst()->ProcessMessage(uMsg, wParam, lParam);
		}

		
		case WM_KEYDOWN:
		{
			return InputManager::Inst()->ProcessMessage(uMsg, wParam, lParam);
		}

		case WM_KEYUP:
		{
			if ( wParam == VK_OEM_5 ) {
				// move to the console and wait for input
				EnterConsole();
				return 0;
			} else {
				return InputManager::Inst()->ProcessMessage(uMsg, wParam, lParam);
			}
		}

		case WM_SIZE:
		{
			InputManager::Inst()->SetWindowSize( LOWORD(lParam), HIWORD(lParam) );
			if ( m_Evt_ResizeListener != nullptr && m_Evt_ResizeScope != nullptr )
				(m_Evt_ResizeScope->*m_Evt_ResizeListener)(LOWORD(lParam), HIWORD(lParam));

			return 0;
		}
	}

	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

/** CONSOLE **/

void GLGameWindow::ActivateConsole()
{
	// if we're in window mode add a console window for outout
	if ( m_bFullscreen == false )
	{
		AllocConsole();

		// Set the stdout handle
		HANDLE stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		stdout->_file = _open_osfhandle((long)stdHandle,_O_TEXT);
		
		// Setup our console gateway
		StdConsole::SetStdoutHandle( stdHandle );

		// change the size of the screen buffer for stdout
		CONSOLE_SCREEN_BUFFER_INFO screenInfoBuffer;
		GetConsoleScreenBufferInfo(stdHandle, &screenInfoBuffer);
		screenInfoBuffer.dwSize.X = 150;
		screenInfoBuffer.dwSize.Y = 500;
		SetConsoleScreenBufferSize(stdHandle, screenInfoBuffer.dwSize);

		// Set the stdin handle
		stdHandle = GetStdHandle(STD_INPUT_HANDLE);
		stdin->_file = _open_osfhandle((long)stdHandle, _O_TEXT);

		// Move the console window 
		RECT r;
		GetWindowRect( GetConsoleWindow(), &r);
		MoveWindow( GetConsoleWindow(), m_iWidth + 10, 0, r.right - r.left, r.bottom - r.top, true );

		SetFocus(m_hWnd);
		SetForegroundWindow(m_hWnd);

		std::ios::sync_with_stdio();
	}
}

void GLGameWindow::EnterConsole()
{
	SetFocus( GetConsoleWindow() );
	SetForegroundWindow( GetConsoleWindow() );
	
	std::string cmd, response;
	std::cin.clear();
	char aCmd[128];
	
	bool exit = false;
	
	while( !exit )
	{
		std::cout << ">";
		std::cin.clear();
		memset(aCmd, 0, sizeof(char) * 128);

		std::cin.get(aCmd, 128);
		cmd = std::string(aCmd);
		std::cin.ignore();

		exit = StdConsole::ProcessCommand( cmd, response );

		if ( response.length() > 0 ) {
			std::cout << response << std::endl;
		} else {
			std::cout << "Unknown command: " << cmd << std::endl;
		}
	}

	ExitConsole();
}

void GLGameWindow::ExitConsole()
{
	SetFocus(m_hWnd);
	SetForegroundWindow(m_hWnd);
}