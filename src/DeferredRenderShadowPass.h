#pragma once
#include "RenderPass.h"
#include "ShadowMapping.h"
#include "CascadedShadowMap.h"
#include "DeferredFramebuffers.h"
#include "TextureAsset.h"
#include "ColorFramebufferObject.h"

#include "Material.h"

class DeferredRenderShadowPass :
	public RenderPass
{
public:
	DeferredRenderShadowPass(void);
	~DeferredRenderShadowPass(void);

	void Init( GLScene *scene, DeferredFramebuffers *fbos, ShadowMapping *shadowMap, CascadedShadowMap *cascadedShadowMap );
	void Bind();
	void Unbind();
	
	ColorFramebufferObject *GetFramebuffer() { return m_pFBO; }

protected:

	DeferredFramebuffers *m_pDeferredFBOs;
	ColorFramebufferObject *m_pFBO;
	
	ShaderProgram *m_pShader;

	ShadowMapping *m_pShadowMap;
	CascadedShadowMap *m_pCascadedShadowMap;
};