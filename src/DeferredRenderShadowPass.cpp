#include "StdAfx.h"
#include "DeferredRenderShadowPass.h"


DeferredRenderShadowPass::DeferredRenderShadowPass(void)
{
	m_pFBO = NULL;
}

DeferredRenderShadowPass::~DeferredRenderShadowPass(void)
{
	if ( m_pFBO != NULL )
		delete m_pFBO;

	m_pFBO = NULL;
}

void DeferredRenderShadowPass::Init( GLScene *scene, DeferredFramebuffers *fbos, ShadowMapping *shadowMap, CascadedShadowMap *cascadedShadowMap )
{
	RenderPass::Init(scene);

	m_pShadowMap = shadowMap;
	m_pCascadedShadowMap = cascadedShadowMap;
	m_pDeferredFBOs = fbos;

	m_pFBO = new ColorFramebufferObject();
	m_pFBO->Create2D(scene->Width(), scene->Height(), GL_COLOR_ATTACHMENT0);

	AssetsManager *assets = m_pScene->GetAssetsManager();

	if ( m_pCascadedShadowMap != nullptr ) {
		if ( m_pCascadedShadowMap->IsAtlas() )
		{
			m_pShader = assets->CreateShaderProgram("RenderCascadedShadowAtlasShader");
			m_pShader->CreateProgram( "Data/Shaders/Deferred/RenderCascadedShadowAtlas.vert", "Data/Shaders/Deferred/RenderCascadedShadowAtlas.frag" );
		}
		else
		{
			m_pShader = assets->CreateShaderProgram("RenderCascadedShadowShader");
			m_pShader->CreateProgram( "Data/Shaders/Deferred/RenderCascadedShadow.vert", "Data/Shaders/Deferred/RenderCascadedShadow.frag" );
		}
	}
	else if ( m_pShadowMap != nullptr ) {
		m_pShader = assets->CreateShaderProgram("RenderShadowShader");
		m_pShader->CreateProgram( "Data/Shaders/Deferred/RenderShadow.vert", "Data/Shaders/Deferred/RenderShadow.frag" );
	}
	else {
		printf("DeferredRenderShadowPass::Init - No shadow object\n");
	}

}

void DeferredRenderShadowPass::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_pFBO->GetFBOID() );

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	glEnable(GL_TEXTURE_2D);

	glUseProgramObjectARB(m_pShader->Program());

	// Set the uniform matrices
	glm::mat4x4 matView = m_pScene->GetCamera()->GetViewMatrix();
	glm::mat4x4 matViewInv = glm::inverse(matView);
	//glm::mat4x4 matPers = glm::inverse(m_pScene->GetProjectionMatrix());

	//m_pShader->SendUniformMatrix4fv("matView", 1, false, glm::value_ptr(matView));
	//m_pShader->SendUniformMatrix4fv("matPerspInv", 1, false, glm::value_ptr(matPers));
	
	// Bind the light to the shader (only one global light for now, the sun as a spotlight)
	//m_pCascadedShadowMap->GetLight()->SetShaderUniform( "Light0", m_pShader );

	m_pShader->SendUniform1i("InDepthTexture", 0);
	m_pShader->SendUniform1i("InPositionTexture", 1);
	
	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_pDeferredFBOs->GetDepthTexture()->TextureID());
	
	glActiveTextureARB(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_pDeferredFBOs->GetPositionTexture()->TextureID());
	
	glm::mat4x4 lightMatrix;
	if ( m_pCascadedShadowMap != nullptr )
	{
		uint32 i;
		float shaderDepths[4];
		float *depths;
		float maxDepth = m_pCascadedShadowMap->GetMaxDepth();
		GLenum textureIndices[] = { GL_TEXTURE2, GL_TEXTURE3, GL_TEXTURE4, GL_TEXTURE5 };
		
		char matLightUniformName[100], textureUniformName[100];

		depths = m_pCascadedShadowMap->GetClipDepths();
		memset(&shaderDepths, 1.0f, sizeof(float)*4);
		
		if ( m_pCascadedShadowMap->IsAtlas() )
		{
			for ( i = 0; i < m_pCascadedShadowMap->GetSplitCount(); ++i ) {
				shaderDepths[i] = depths[i];

				lightMatrix = m_pCascadedShadowMap->GetTextureMatrix(i) * matViewInv;

				sprintf_s(matLightUniformName, 100, "matLight%d", i);

				m_pShader->SendUniformMatrix4fv(matLightUniformName, 1, false, glm::value_ptr(lightMatrix));
			}
			
			glActiveTextureARB(textureIndices[0]);
			glBindTexture(GL_TEXTURE_2D, m_pCascadedShadowMap->GetTextureId(0) );
			m_pShader->SendUniform1i("InShadowTexture", 2);
		}
		else
		{
			for ( i = 0; i < m_pCascadedShadowMap->GetSplitCount(); ++i ) {
				shaderDepths[i] = depths[i];

				lightMatrix = m_pCascadedShadowMap->GetTextureMatrix(i) * matViewInv;

				sprintf_s(matLightUniformName, 100, "matLight%d", i);

				m_pShader->SendUniformMatrix4fv(matLightUniformName, 1, false, glm::value_ptr(lightMatrix));

				sprintf_s(textureUniformName, 100, "InShadowTexture%d", i);
				glActiveTextureARB(textureIndices[i]);
				glBindTexture(GL_TEXTURE_2D, m_pCascadedShadowMap->GetTextureId(i) );
				m_pShader->SendUniform1i(textureUniformName, i+2);
		
			}
		}

		m_pShader->SendUniform4f("splitDepths", shaderDepths);
	}
	else if ( m_pShadowMap != nullptr )
	{
		glActiveTextureARB(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, m_pShadowMap->GetFramebuffer()->GetTextureId() );
		m_pShader->SendUniform1i("InShadowTexture", 4);

		m_pShadowMap->GetTextureMatrix(lightMatrix);
		lightMatrix = lightMatrix * matView;
		m_pShader->SendUniformMatrix4fv("matLight", 1, false, glm::value_ptr( lightMatrix ));
	}
}

void DeferredRenderShadowPass::Unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0 );

	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0 );

	glActiveTextureARB(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0 );

	glActiveTextureARB(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0 );

	glActiveTextureARB(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, 0 );

	glActiveTextureARB(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, 0 );

	glActiveTextureARB(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, 0 );

	glActiveTextureARB(GL_TEXTURE0);

	glDisable(GL_TEXTURE_2D);
}