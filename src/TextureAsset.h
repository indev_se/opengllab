#pragma once
#include "Asset.h"
#include "BitmapDataAsset.h"

struct TextureInfoParam
{
	GLenum paramName;
	GLenum paramValue;
};

struct TextureInfo
{
	GLenum target;
	GLint format;
	GLenum valueType;

	bool genMipMap;

	std::list<TextureInfoParam *> params;

	TextureInfo() : target(GL_TEXTURE_2D), format(0), valueType(GL_UNSIGNED_BYTE) {}
};

class TextureAsset :
	public Asset
{
public:
	TextureAsset(void);
	~TextureAsset(void);

	void CreateFromBitmapData( BitmapDataAsset *bitmap, TextureInfo *inf );
	void CreateEmpty( unsigned int Width, unsigned int Height, GLint format, TextureInfo *inf );
	inline UINT TextureID() { return m_TextureID; }

	void Bind( GLenum texture );
	void Unbind( GLenum texture );

protected:

	TextureInfo *m_Info;
	UINT m_TextureID;
	GLint m_TextureFormat;
};