#pragma once
#define M_E         2.71828182845904523536028747135266250   /* e */
#define M_LOG2E     1.44269504088896340735992468100189214   /* log 2e */
#define M_LOG10E    0.434294481903251827651128918916605082  /* log 10e */
#define M_LN2       0.693147180559945309417232121458176568  /* log e2 */
#define M_LN10      2.30258509299404568401799145468436421   /* log e10 */
#define M_PI        3.14159265358979323846264338327950288   /* pi */
#define M_PI_2      1.57079632679489661923132169163975144   /* pi/2 */
#define M_PI_4      0.785398163397448309615660845819875721  /* pi/4 */
#define M_1_PI      0.318309886183790671537767526745028724  /* 1/pi */
#define M_2_PI      0.636619772367581343075535053490057448  /* 2/pi */
#define M_2_SQRTPI  1.12837916709551257389615890312154517   /* 2/sqrt(pi) */
#define M_SQRT2     1.41421356237309504880168872420969808   /* sqrt(2) */

#define DTOR(d) ((float)d*(M_PI/180.0f))
#define RTOD(d) ((float)d*(180.0f/M_PI))

#define SQR(x) ((x) * (x))
#define LERP(a,b,c) (((b) - (a)) * (c) + (a))

// ---------------------------------------------------------------------
// Discover the orientation of a triangle's points:
//
// Taken from "Programming Principles in Computer Graphics", L. Ammeraal (Wiley)
//
inline int TriangleOrientation( int pX, int pY, int qX, int qY, int rX, int rY )
{
	int aX, aY, bX, bY;
	float d;

	aX = qX - pX;
	aY = qY - pY;

	bX = rX - pX;
	bY = rY - pY;

	d = (float)aX * (float)bY - (float)aY * (float)bX;
	return (d < 0) ? (-1) : (d > 0);
}

inline glm::vec3 CalculateNormal( const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2 )
{
	glm::vec3 norm;
	float rx1 = v1.x - v0.x;
	float ry1 = v1.y - v0.y;
	float rz1 = v1.z - v0.z;
			
	float rx2 = v2.x - v0.x;
	float ry2 = v2.y - v0.y;
	float rz2 = v2.z - v0.z;

	norm.x = ry1 * rz2 - ry2 * rz1;
	norm.y = rz1 * rx2 - rz2 * rx1;
	norm.z = rx1 * ry2 - rx2 * ry1;
			
	norm /= norm.length();

	return norm;
}


static const glm::vec3 AXIS_X(1.0f,0.0f,0.0f);
static const glm::vec3 AXIS_Y(0.0f,1.0f,0.0f);
static const glm::vec3 AXIS_Z(0.0f,0.0f,1.0f);