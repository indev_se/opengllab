#pragma once
class SkydomeAtmosphere
{
public:
	SkydomeAtmosphere( uint32 _Width, uint32 _Height );
	~SkydomeAtmosphere(void);

	void Generate();

private:

	uint32 m_Width, m_Height;
};

