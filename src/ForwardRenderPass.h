#pragma once
#include "RenderPass.h"

class ForwardRenderPass :
	public RenderPass
{
public:
	ForwardRenderPass(void);
	~ForwardRenderPass(void);

	void Init( GLScene *scene );
	void Bind();
	void Unbind();
	
	void BindObject( SceneObject *object );
	void UnbindObject( SceneObject *object );

	void BindJob( RenderJob *job ) {}
	void UnbindJob( RenderJob *job ) {}

};

