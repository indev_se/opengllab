#pragma once
#include "Asset.h"
class BitmapDataAsset :
	public Asset
{
public:
	BitmapDataAsset(void);
	~BitmapDataAsset(void);

	bool CreateFromFile( const char *filename );
	bool CreateFromBits( BYTE *_Bits, UINT _BPP, UINT _Width, UINT _Height );

	void ConvertTo8Bits();

	inline const BYTE *Bits() { return m_Bits; } const
	inline UINT Width() { return m_Width; }
	inline UINT Height() { return m_Height; }

	GLenum GetGLFormat();
	GLenum GetGLDestFormat();

protected:

	UINT	m_Width, m_Height;
	BYTE*	m_Bits;
	UINT	m_BPP;
	UINT	m_ScanWidth; // Pitch

};

