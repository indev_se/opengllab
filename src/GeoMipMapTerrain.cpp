#include "StdAfx.h"
#include "VertexBufferFixed.h"
#include "GeoMipMapTerrain.h"

/**
 * PATCH
 */
GeoMipMapPatch::GeoMipMapPatch(void)
{
}


GeoMipMapPatch::~GeoMipMapPatch(void)
{
}

/**
 * TERRAIN
 */
GeoMipMapTerrain::GeoMipMapTerrain(void)
{
}


GeoMipMapTerrain::~GeoMipMapTerrain(void)
{
	Dispose();
}

void GeoMipMapTerrain::Dispose()
{
}

void GeoMipMapTerrain::Init(unsigned char *HeightMap, UINT Size, UINT PatchSize, float Step, float Scale, int SmoothingPasses )
{
	m_HeightMap = HeightMap;
	m_Size = Size;
	m_Step = Step;
	m_PatchSize = PatchSize;
	m_PatchCount = Size / (PatchSize-1);
	m_Scale = Scale;

	// Perform boxfilter smoothing on the heightmap
	BoxFilterHeightMap(SmoothingPasses);

	// Calculate the MaxLOD based on the size of a patch
	int div = m_PatchSize-1;
	m_MaxLOD = 0;
	while( div > 2 ) {
		div = div >> 1;
		m_MaxLOD++;
	}

	// Calculate LOD distances
	// Level 0 is closest
	m_LODDistances = new UINT[m_MaxLOD];
	for ( uint32 i = 0; i < m_MaxLOD; ++i ) {
		m_LODDistances[i] = ( (float)(m_Size/m_MaxLOD)/2 * (i+1)) * m_Step; //m_PatchSize * (i+1);
	}

	// Allocate memory for the vertices, count on that all fit into one buffer. 
	// This should be handled more beatiful later, meaning don't ignore max vertex buffer size

	m_VertexData = new PatchVertex[ m_Size * m_Size ];

	UINT x = 0;
	UINT z = 0;
	PatchVertex *v;

	for ( z = 0; z < m_Size; ++z )
	{
		for ( x = 0; x < m_Size; ++x ) {
			v = &m_VertexData[ (z * m_Size) + x ];

			// Set vertex position
			v->x = x * m_Step;
			v->z = z * m_Step;
			v->y = m_SmoothHeightMap[ (z * m_Size) + x ] * m_Scale;
			
			// calculate and set texture coordinates
			v->s0 = x / (float)(m_PatchSize-1)*18;
			v->t0 = z / (float)(m_PatchSize-1)*18;

			// set the texture mask coords
			v->s1 = x / (float)(m_Size);
			v->t1 = z / (float)(m_Size);
		}
	}

	// Calculate the normals
	glm::vec3 v0, v1, v2, n;

	for ( z = 0; z < m_Size; ++z )
	{
		for ( x = 0; x < m_Size; ++x ) {
			v = &m_VertexData[ (z * m_Size) + x ];

			if (x > 0 && x < m_Size - 1)
				n.x = m_SmoothHeightMap[ (z * m_Size) + x-1 ] - m_SmoothHeightMap[ (z * m_Size) + x+1 ];
			else if (x > 0)
				n.x = 2.0f * (m_SmoothHeightMap[ (z * m_Size) + x-1 ] - m_SmoothHeightMap[ (z * m_Size) + x ]);
			else
				n.x = 2.0f * (m_SmoothHeightMap[ (z * m_Size) + x ] - m_SmoothHeightMap[ (z * m_Size) + x+1 ]);

			if (z > 0 && z < m_Size - 1)
				n.z = m_SmoothHeightMap[ ((z-1) * m_Size) + x ] - m_SmoothHeightMap[ ((z+1) * m_Size) + x ];
			else if (z > 0)
				n.z = 2.0f * (m_SmoothHeightMap[ ((z-1) * m_Size) + x ] - m_SmoothHeightMap[ (z * m_Size) + x ]);
			else
				n.z = 2.0f * (m_SmoothHeightMap[ (z * m_Size) + x ] - m_SmoothHeightMap[ ((z+1) * m_Size) + x ]);

			n.y = 2.0f * m_Step;
			n = glm::normalize(n);
			
			v->nx = n.x;
			v->ny = n.y;
			v->nz = n.z;
		}
	}

	// Smooth the normals
	glm::vec3 sum;
	PatchVertex *curV;
	for ( z = 0; z < m_Size; ++z )
	{
		for ( x = 0; x < m_Size; ++x ) {
			curV = &m_VertexData[ (z * m_Size) + x ];
			sum = glm::vec3( curV->nx, curV->ny, curV->nz );

			if ( x > 0 ) {
				v = &m_VertexData[ (z * m_Size) + x-1 ];
				sum += glm::vec3( v->nx, v->ny, v->nz ) * 0.5f;
			}
			if ( x < m_Size-1 ) {
				v = &m_VertexData[ (z * m_Size) + x+1 ];
				sum += glm::vec3( v->nx, v->ny, v->nz ) * 0.5f;
			}
			if ( z > 0 ) {
				v = &m_VertexData[ ((z-1) * m_Size) + x ];
				sum += glm::vec3( v->nx, v->ny, v->nz ) * 0.5f;
			}
			if ( z < m_Size-1 ) {
				v = &m_VertexData[ ((z+1) * m_Size) + x ];
				sum += glm::vec3( v->nx, v->ny, v->nz ) * 0.5f;
			}

			if ( sum.length() == 0 ) {
				sum = glm::vec3(0.0f, 1.0f, 0.0f);
			}

			sum /= 4.0f;
			sum = glm::normalize(sum);

			curV->nx = sum.x;
			curV->ny = sum.y;
			curV->nz = sum.z;
		}
	}
	
	// Calculate the tangent
	glm::vec3 tangent;
	for ( z = 0; z < m_Size; ++z )
	{
		for ( x = 0; x < m_Size; ++x ) {
			curV = &m_VertexData[ (z * m_Size) + x ];

			int xp = (x < m_Size-1) ? x+1 : x;
			int xm = (x > 0) ? x-1 : x;
			int zp = (z < m_Size-1) ? z+1 : z;
			int zm = (z > 0) ? z-1 : z;

			//X filter:
			//-1 0 1
			//-2 0 2
			//-1 0 1
			/*float dhdx = -hm->HeightAt (xm,ym) + hm->HeightAt (xp, ym);
			dhdx += 2.0f * (-hm->HeightAt (xm,y) + hm->HeightAt (xp, y));
			dhdx += -hm->HeightAt (xm,yp) + hm->HeightAt (xp, yp);
			dhdx *= 0.25f;*/

			int dhdx = -m_SmoothHeightMap[ (zm * m_Size) + xm ] + m_SmoothHeightMap[ (zm * m_Size) + xp ];
			dhdx += 2 * (-m_SmoothHeightMap[ (z * m_Size) + xm ] + m_SmoothHeightMap[ (z * m_Size) + xp]);
			dhdx += -m_SmoothHeightMap[ (zp * m_Size) + xm ] + m_SmoothHeightMap[ (zp * m_Size) + xp ];

			// 
			//Z filter:
			//-1 -2 -1
			//0  0  0
			//1  2  1
			//
			/*float dhdz = hm->HeightAt (xm, yp) + 2.0f * hm->HeightAt (x, yp) + hm->HeightAt (xp, yp);
			dhdz -= hm->HeightAt (xm, ym) + 2.0f * hm->HeightAt (x, ym) + hm->HeightAt (xp, ym);
			dhdz *= 0.25f;*/
			int dhdz = m_SmoothHeightMap[ (zp * m_Size) + xm ] + 2 * m_SmoothHeightMap[ (zp * m_Size) + x ] + m_SmoothHeightMap[ (zp * m_Size) + xp ];
			dhdz -= m_SmoothHeightMap[ (zm * m_Size) + xm ] + 2 * m_SmoothHeightMap[ (zm * m_Size) + x ] + m_SmoothHeightMap[ (zm * m_Size) + xp ];

			tangent = glm::vec3(m_Step * 2.0f, m_Scale * dhdx * 0.25f, 0.0f);
			//binormal = Vector3(0.0f, dhdz * m_Scale * 0.25f, m_Step * 2.0f);

			tangent = glm::normalize(tangent);
			curV->tanx = tangent.x;
			curV->tany = tangent.y;
			curV->tanz = tangent.z;
		}
	}
	
	// Create a vertexbuffer object and bind the VB
	m_VBO = new VertexBufferFixed();
	m_VBO->CreateVertexBuffer( VDF_XYZ | VDF_NORMAL | VDF_TEX0 | VDF_TEX1 | VDF_TEX2 | VDF_TEXCOORDSIZE3(2), m_Size * m_Size, m_VertexData, sizeof(PatchVertex), GL_STATIC_DRAW );
	
	// Create the patches
	unsigned char maxY = 0;
	unsigned char minY = 255;
	unsigned char curY = 0;
	m_Patches = new GeoMipMapPatch[ m_PatchCount * m_PatchCount ];
	v = &m_VertexData[ (1 * m_Size) + 1 ];
	for ( z = 0; z < m_PatchCount; ++z )
	{
		for ( x = 0; x < m_PatchCount; ++x ) {

			GeoMipMapPatch *p = &m_Patches[ ( z * m_PatchCount ) + x ];

			p->m_LOD = 0;
			p->m_IndexCount = 0;

			// allocate memory for max amount of indices, waste of space but we never need to realloc
			p->m_Indices = new unsigned int[ (m_PatchSize-1) * (m_PatchSize-1) * 6 ];

			// calculate the center
			p->m_vCenter = glm::vec3( ((m_PatchSize*x) + (m_PatchSize/2))*m_Step, 0, ((m_PatchSize*z) + (m_PatchSize/2))*m_Step );
			
			// calculate the boundingbox
			maxY = 0;
			minY = 255;
			for ( uint32 vz = 0; vz < m_PatchSize; ++vz )
			for ( uint32 vx = 0; vx < m_PatchSize; ++vx )
			{
				curY = (char)m_HeightMap[ ((vz+((m_PatchSize-1)*z))*m_Size+(vx+((m_PatchSize-1)*x))) ];
				if ( curY < minY ) minY = curY;
				else if ( curY > maxY ) maxY = curY;
			}
			p->m_vMin = glm::vec3( x * m_PatchSize, minY * m_Scale, z * m_PatchSize );
			p->m_vMax = glm::vec3( (x * m_PatchSize) + m_PatchSize, maxY * m_Scale, (z * m_PatchSize) + m_PatchSize );

			p->m_vMin.x *= m_Step;
			p->m_vMin.z *= m_Step;
			p->m_vMax.x *= m_Step;
			p->m_vMax.z *= m_Step;

			p->m_BB = new BoundingBox( p->m_vMin, p->m_vMax );

			// set the neighbour patches
			if ( z > 0 ) // top
				p->m_Top = &m_Patches[ ( (z-1) * m_PatchCount ) + x ];
			else
				p->m_Top = NULL;

			if ( z < (m_PatchCount-1) ) // bottom
				p->m_Bottom = &m_Patches[ ( (z+1) * m_PatchCount ) + x ];
			else
				p->m_Bottom = NULL;

			if ( x > 0 ) // left
				p->m_Left = &m_Patches[ ( z * m_PatchCount ) + x - 1 ];
			else
				p->m_Left = NULL;

			if ( x < (m_PatchCount-1) ) // right
				p->m_Right = &m_Patches[ ( z * m_PatchCount ) + x + 1 ];
			else
				p->m_Right = NULL;
		}
	}
}

void GeoMipMapTerrain::BoxFilterHeightMap(int SmoothingPasses)
{
	UINT x, z;

	UINT bounds = m_Size * m_Size;

	float *smoothHeightMap = new float[bounds];
	for (z = 0; z < m_Size; ++z)
	{
		for (x = 0; x < m_Size; ++x)
		{
			smoothHeightMap[x + z * m_Size] = (float)m_HeightMap[x + z * m_Size] / 255.0f;
		}
	}

	for (int32 pass = 0; pass < SmoothingPasses; ++pass)
	{
	for (z = 0; z < m_Size; ++z)
	{
		for (x = 0; x < m_Size; ++x)
		{
			// Sample a 3x3 filtering grid based on surrounding neighbors
			float value = 0.0f;
			float cellAverage = 1.0f;

			// Sample top row
      
			if (((x - 1) + (z - 1) * m_Size) >= 0 &&
				((x - 1) + (z - 1) * m_Size) < bounds)
			{
			value += smoothHeightMap[(x - 1) + (z - 1) * m_Size];
			++cellAverage;
			}
      
			if (((x - 0) + (z - 1) * m_Size) >= 0 &&
				((x - 0) + (z - 1) * m_Size) < bounds)
			{
			value += smoothHeightMap[(x    ) + (z - 1) * m_Size];
			++cellAverage;
			}
      
			if (((x + 1) + (z - 1) * m_Size) >= 0 &&
				((x + 1) + (z - 1) * m_Size) < bounds)
			{
			value += smoothHeightMap[(x + 1) + (z - 1) * m_Size];
			++cellAverage;
			}
      
			// Sample middle row
      
			if (((x - 1) + (z - 0) * m_Size) >= 0 &&
				((x - 1) + (z - 0) * m_Size) < bounds)
			{
			value += smoothHeightMap[(x - 1) + (z    ) * m_Size];
			++cellAverage;
			}
      
			// Sample center point (will always be in bounds)
			value += m_HeightMap[x + z * m_Size];
      
			if (((x + 1) + (z - 0) * m_Size) >= 0 &&
				((x + 1) + (z - 0) * m_Size) < bounds)
			{
			value += smoothHeightMap[(x + 1) + (z    ) * m_Size];
			++cellAverage;
			}
      
			// Sample bottom row
      
			if (((x - 1) + (z + 1) * m_Size) >= 0 &&
				((x - 1) + (z + 1) * m_Size) < bounds)
			{
			value += smoothHeightMap[(x - 1) + (z + 1) * m_Size];
			++cellAverage;
			}
      
			if (((x - 0) + (z + 1) * m_Size) >= 0 &&
				((x - 0) + (z + 1) * m_Size) < bounds)
			{
			value += smoothHeightMap[(x    ) + (z + 1) * m_Size];
			++cellAverage;
			}
      
			if (((x + 1) + (z + 1) * m_Size) >= 0 &&
				((x + 1) + (z + 1) * m_Size) < bounds)
			{
			value += smoothHeightMap[(x + 1) + (z + 1) * m_Size];
			++cellAverage;
			}

			// Store the result
			smoothHeightMap[x + z * m_Size] = value / cellAverage;
		}
	}
	}
	
	// Release the old array
	//delete [] m_HeightMap;
	//m_HeightMap = NULL;

	// Store the new one
	m_SmoothHeightMap = smoothHeightMap;

}

void GeoMipMapTerrain::SetCamera( GLCamera *Camera )
{
	m_Camera = Camera;
}

void GeoMipMapTerrain::Update( float tickMS )
{
	CalculateLOD();
	UpdateIndices();
}

void GeoMipMapTerrain::CalculateLOD()
{
	UINT x = 0;
	UINT z = 0;
	GeoMipMapPatch *p;

	glm::vec3 vecCameraEye;
	m_Camera->GetEye(vecCameraEye);

	for ( z = 0; z < m_PatchCount; ++z )
	{
		for ( x = 0; x < m_PatchCount; ++x ) {

			p = &m_Patches[ ( z * m_PatchCount ) + x ];
			
			float f1 = SQR( ( (float)p->m_vCenter.x - (-vecCameraEye.x) ) );
			float f2 = SQR( ( (float)p->m_vCenter.z - (-vecCameraEye.z) ) );
		 	float distance = 1.0f + sqrtf( f1 + f2 );

			// Calculate LOD here
			for ( uint32 i = 0; i < m_MaxLOD; ++i ) {

				if( distance > m_LODDistances[i] )
					p->m_LOD = i;
			}

			p->m_Visible = m_Camera->GetFrustum()->AABBInFrustum(p->m_BB, glm::mat4x4());
			
			//p->m_LOD = 0;
/*			
			if ( x%2 == 0 && z%2 == 0)
				p->m_LOD = 2;
			else
				p->m_LOD = 2;
*/			
		}
	}
}

void GeoMipMapTerrain::UpdateIndices()
{
	uint32 j = 0, xS = 0;
	uint32 i = 0, zS = 0;
	int x = 0, z = 0;
	unsigned int index = 0;
	GeoMipMapPatch *p;
	int vertPerRow = m_PatchSize-1;
	int stepLOD = 1;
	int row = 0;
	int rows = 0;
	bool isStitching = false;
	
	//std::ofstream File("indices.txt");

	for ( i = 0; i < m_PatchCount; ++i )
	{
		for ( j = 0; j < m_PatchCount; ++j ) {
			p = &m_Patches[ ( i * m_PatchCount ) + j ];

			if ( !p->m_Visible ) continue;

			unsigned int *indexPtr = p->m_Indices;
			index = 0;
			stepLOD = 1<<p->m_LOD;
			
			rows = vertPerRow / stepLOD;

			for ( row = 0, z = 0; z < vertPerRow; z += stepLOD, ++row )
			{
				if ( row % 2 == 0 )
				{
					if ( row > 0 )
						p->m_Indices[index++] = getIndex( j, i, 0, z, p);

					for ( x = 0; x <= vertPerRow; x += stepLOD )
					{
						// Create a row
						p->m_Indices[index++] = getIndex( j, i, x, z, p);
						p->m_Indices[index++] = getIndex( j, i, x, z + stepLOD, p);
					}

					// Add degenerate triangles to stitch strips together.
					if ( row != rows-1 )
						p->m_Indices[index++] = p->m_Indices[index-1];
				}
				else
				{
					if ( row > 0 )
						p->m_Indices[index++] = getIndex( j, i, vertPerRow, z+ stepLOD, p);

					for ( x = vertPerRow; x >= 0; x -= stepLOD )
					{
						// Create a row
						p->m_Indices[index++] = getIndex( j, i, x, z + stepLOD, p);
						p->m_Indices[index++] = getIndex( j, i, x, z, p);
					}

					// Add degenerate triangles to stitch strips together.
					if ( row != rows-1 )
						p->m_Indices[index++] = p->m_Indices[index-1];
				}
			}
			

			p->m_IndexCount = index;
			/*
			for ( int test = 0; test < index; ++test ) {
				File << p->m_Indices[test] << ", ";
			}
			File << "\n";
			*/
		}
	}

	
}

inline unsigned int GeoMipMapTerrain::getIndex( UINT PatchX, UINT PatchZ, UINT X, UINT Z, GeoMipMapPatch *patch )
{
	if( patch->m_Top != NULL && Z == 0 && patch->m_Top->m_LOD != patch->m_LOD && patch->m_Top->m_LOD > patch->m_LOD )
	{
		X = X - (X % (1<<patch->m_Top->m_LOD));
	}
	if( patch->m_Bottom != NULL && Z == m_PatchSize-1 && patch->m_Bottom->m_LOD != patch->m_LOD && patch->m_Bottom->m_LOD > patch->m_LOD )
	{
		X = X - (X % (1<<patch->m_Bottom->m_LOD));
	}
	if( patch->m_Left != NULL && X == 0 && patch->m_Left->m_LOD != patch->m_LOD && patch->m_Left->m_LOD > patch->m_LOD )
	{
		Z = Z - (Z % (1<<patch->m_Left->m_LOD));
	}
	if( patch->m_Right != NULL && X == m_PatchSize-1 && patch->m_Right->m_LOD != patch->m_LOD && patch->m_Right->m_LOD > patch->m_LOD )
	{
		Z = Z - (Z % (1<<patch->m_Right->m_LOD));
	}

	// Don't really know why I did this, but it removes the crazy triangles that appear when on the border of the map size
	if(Z >= m_PatchSize-1 && PatchZ == m_PatchCount-1) Z = m_PatchSize-2;
    if(X >= m_PatchSize-1 && PatchX == m_PatchCount-1) X = m_PatchSize-2;

	return ((Z+((m_PatchSize-1)*PatchZ))*m_Size+(X+((m_PatchSize-1)*PatchX)));
}

void GeoMipMapTerrain::Render()
{
	UINT i = 0, j = 0;
	GeoMipMapPatch *p;

	for ( i = 0; i < m_PatchCount; ++i )
	{
		for ( j = 0; j < m_PatchCount; ++j ) 
		{
			p = &m_Patches[ ( i * m_PatchCount ) + j ];

			if ( p->m_Visible ) {
				m_VBO->Render( GL_TRIANGLE_STRIP, p->m_Indices, p->m_IndexCount );
			}
			
		}
	}
}

void GeoMipMapTerrain::RenderDebugBB()
{
	UINT i = 0, j = 0;
	GeoMipMapPatch *p;

	for ( i = 0; i < m_PatchCount; ++i )
	{
		for ( j = 0; j < m_PatchCount; ++j ) 
		{
			p = &m_Patches[ ( i * m_PatchCount ) + j ];

			if ( p->m_Visible ) {
				//p->m_BB->Render();
			}
			
		}
	}
}

void GeoMipMapTerrain::RenderDebugNormals()
{
	UINT x = 0;
	UINT z = 0;
	PatchVertex *v;
	glm::vec3 pos;

	// Normal
	for ( z = 0; z < 128; z += 1 )
	{
		for ( x = 0; x < 128; x += 1 ) {
			v = &m_VertexData[ (z * m_Size) + x ];

			pos = glm::vec3(v->x,v->y,v->z);

			glColor3f( 0.0,1.0,0.0 );
			glBegin(GL_LINES);
				glVertex3fv( glm::value_ptr(pos) );

				glVertex3fv( glm::value_ptr(pos + glm::vec3(v->nx * 60, v->ny * 60, v->nz * 60)) );
			glEnd();

			glColor3f( 1.0,0.0,0.0 );
			glBegin(GL_LINES);
				glVertex3fv( glm::value_ptr(pos) );

				glVertex3fv( glm::value_ptr(pos + glm::vec3(v->tanx * 60, v->tany * 60, v->tanz * 60)) );
			glEnd();

		}
	}
}