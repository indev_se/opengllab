#include "StdAfx.h"
#include "SceneObject.h"
#include "RenderShadowMapPass.h"

RenderShadowMapPass::RenderShadowMapPass(void)
{
	m_bBindTextures = false;
}


RenderShadowMapPass::~RenderShadowMapPass(void)
{
}

void RenderShadowMapPass::Init( LightObject *light, GLScene *scene )
{
	RenderPass::Init(scene);

	// Create the shadowmap
	m_pShadowMap = new ShadowMapping();
	m_pShadowMap->Create( light, scene, 1024, 1024);

}

void RenderShadowMapPass::BindObject( SceneObject *object )
{
	// Bind the shader
	Material *mtrl = object->GetMaterial();

	assert( glGetError() == 0 );


	if ( mtrl->GetShaderDeferred() ) {
		mtrl->BindDeferred(this);

		glm::mat4x4 matLightView = m_pShadowMap->GetViewMatrix();
		glm::mat4x4 matLightProjection = m_pShadowMap->GetProjectionMatrix();

		matLightView = matLightView;// * m_pScene->GetCamera()->GetViewMatrix();

		mtrl->GetBoundShader()->SendUniformMatrix4fv("matModel", 1, false, (float*)&object->GetModelMatrix());
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matView", 1, false, (float*)&matLightView);
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matPersp", 1, false, (float*)&matLightProjection);

		mtrl->GetBoundShader()->SendUniform1i("IsShadowDepthPass", 1);
	}
	else if ( mtrl->GetShaderForward() ) {
		mtrl->BindForward(this);

		mtrl->GetBoundShader()->SendUniform1i("IsShadowDepthPass", 1);
	}
}

void RenderShadowMapPass::UnbindObject( SceneObject *object )
{
	// Unbind the shader
	Material *mtrl = object->GetMaterial();

	if ( mtrl->GetShaderDeferred() )
		mtrl->UnbindDeferred(this);
	else if ( mtrl->GetShaderForward() )
		mtrl->UnbindForward(this);
}

void RenderShadowMapPass::Bind()
{
	m_pShadowMap->BindFramebuffer();
	//glCullFace(GL_FRONT);
}

void RenderShadowMapPass::Unbind()
{
	m_pShadowMap->UnbindFramebuffer();
	//glCullFace(GL_BACK);
}