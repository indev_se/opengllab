#pragma once
#include "GLScene.h"
#include "GameTimer.h"
#include "GLGameWindowEventListener.h"

class GameApplication : 
	public GLGameWindowEventListener
{
public:
	GameApplication(void);
	~GameApplication(void);

	void Run( GLGameWindow *gameWindow, GLScene *glScene );

	void Event_Resize( int Width, int Height );

protected:

	GLGameWindow *m_pGameWindow;
	GLScene		 *m_pGLScene;

	GameTimer	*m_pTimer;

};

