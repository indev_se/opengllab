#pragma once

void streamwrite_glm_vec3( std::ostream& os, glm::vec3 o );
void streamwrite_glm_quat( std::ostream& os, glm::quat o );
void streamwrite_glm_mat4x4( std::ostream& os, glm::mat4x4 o );

glm::vec3 streamread_glm_vec3( std::istream& is );
glm::quat streamread_glm_quat( std::istream& is );
glm::mat4x4 streamread_glm_mat4x4( std::istream& is );

#define SWRITE(stream, data) stream.write((char*)&data, sizeof(data));
#define SWRITE_C(stream, data, len) stream.write((char*)&data, sizeof(char)*len);

#define SREAD(stream, data) stream.read((char*)&data, sizeof(data));
#define SREAD_C(stream, data, len) stream.read((char*)&data, sizeof(char)*len);