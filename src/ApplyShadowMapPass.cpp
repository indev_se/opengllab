#include "StdAfx.h"
#include "AssetsManager.h"
#include "StrategyGLScene.h"
#include "ApplyShadowMapPass.h"


ApplyShadowMapPass::ApplyShadowMapPass(void)
{

}


ApplyShadowMapPass::~ApplyShadowMapPass(void)
{

}

void ApplyShadowMapPass::Init( ShadowMapping *shadowmap, GLScene *scene )
{
	RenderPass::Init(scene);

	m_pShadowMap = shadowmap;

	AssetsManager *assets = ((StrategyGLScene*) scene)->GetAssetsManager();

}

void ApplyShadowMapPass::BindObject( SceneObject *object )
{
	// Bind the shader
	Material *mtrl = object->GetMaterial();

	if ( mtrl->GetShaderForward() ) {
		mtrl->BindForward();

		glm::mat4x4 matTex, matObj, matShadowTex;
		m_pShadowMap->GetTextureMatrix(matTex);
		matObj = object->GetModelMatrix();

		matShadowTex = matTex * matObj;

		glm::mat4x4 matModel = object->GetModelMatrix();
		glm::mat4x4 matView = m_pScene->GetCamera()->GetViewMatrix();
		glm::mat4x4 matPersp = m_pScene->GetProjectionMatrix();

		mtrl->GetBoundShader()->SendUniform1i("isDepthPass", 0);
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matShadowTex", 1, false, glm::value_ptr(matShadowTex));
		mtrl->GetBoundShader()->SendUniform1i("ShadowMapTexture", 7);
		
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matModel", 1, false, glm::value_ptr(matModel));
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matView", 1, false, glm::value_ptr(matView));
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matPersp", 1, false, glm::value_ptr(matPersp));

		// Bind the light to the shader
		m_pShadowMap->GetLight()->SetShaderUniform( "light0", mtrl->GetBoundShader() );

		// Bind the shadowmap texture
		glActiveTextureARB(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, m_pShadowMap->GetFramebuffer()->GetTextureId() );
	}
}

void ApplyShadowMapPass::UnbindObject( SceneObject *object )
{
	// Unbind the shader
	Material *mtrl = object->GetMaterial();

	if ( mtrl->GetShaderForward() )
		mtrl->UnbindForward();
}

void ApplyShadowMapPass::Bind()
{
	glEnable(GL_TEXTURE_2D);
}

void ApplyShadowMapPass::Unbind()
{
	glDisable(GL_TEXTURE_2D);
	glUseProgramObjectARB(0);
}
