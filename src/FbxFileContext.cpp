#include "StdAfx.h"
#include "Mesh.h"
#include "FbxFileContext.h"


FbxFileContext::FbxFileContext(void)
{
	m_pMesh = nullptr;
}

FbxFileContext::~FbxFileContext(void)
{
	if ( m_pMesh != nullptr )
		delete m_pMesh;

	m_pMesh = nullptr;
}

void FbxFileContext::SetMesh( Mesh *_pMesh )
{
	m_pMesh = _pMesh;
}

Mesh *FbxFileContext::GetMesh( void )
{
	return m_pMesh;
}