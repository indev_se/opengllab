#pragma once

class Mesh;

class FbxFileContext
{
public:
	FbxFileContext(void);
	~FbxFileContext(void);

	void SetMesh( Mesh *_pMesh );
	Mesh *GetMesh( void );

private:

	Mesh *m_pMesh;

public:
	
	friend std::ostream& operator<<(std::ostream& os, const FbxFileContext& s)
	{
		os << *s.m_pMesh;
		
		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, FbxFileContext& s)
	{
		s.m_pMesh = new Mesh();
		is >> *s.m_pMesh;

		return is;
	}
};

