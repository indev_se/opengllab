#pragma once
#include "CubeObject.h"

class Bone
{
public:
	Bone(void);
	~Bone(void);

	std::string Name;
	uint32 Index;
	int32 ParentIndex;

	glm::vec3 Position;
	glm::quat Rotation;

	glm::mat4x4 GlobalMatrix;
	glm::mat4x4 GlobalMatrixInv;

	glm::mat4x4 LocalToGlobalMatrix;

	glm::vec3 DebugColor;
	CubeObject *DebugShape;

public:

	friend std::ostream& operator<<(std::ostream& os, const Bone& s)
	{
		uint32 len = s.Name.length();
		SWRITE(os, len);
		SWRITE_C(os, s.Name[0], len);
		SWRITE(os,s.Index)
		SWRITE(os,s.ParentIndex);

		streamwrite_glm_vec3(os, s.Position);
		streamwrite_glm_quat(os, s.Rotation);

		streamwrite_glm_mat4x4(os, s.GlobalMatrix);
		streamwrite_glm_mat4x4(os, s.GlobalMatrixInv);

		streamwrite_glm_mat4x4(os, s.LocalToGlobalMatrix);

		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, Bone& s)
	{
		uint32 len;
		SREAD(is,len);
		s.Name.resize(len);
		SREAD_C(is,s.Name[0],len);
		
		SREAD(is,s.Index);
		SREAD(is,s.ParentIndex);

		s.Position = streamread_glm_vec3(is);
		s.Rotation = streamread_glm_quat(is);

		s.GlobalMatrix = streamread_glm_mat4x4(is);
		s.GlobalMatrixInv = streamread_glm_mat4x4(is);

		s.LocalToGlobalMatrix = streamread_glm_mat4x4(is);

		return is;
	}
};

