#pragma once
#define MAP_SIZE (1024)
#define NUM_PATCHES_PER_SIDE (16)

#define MULT_SCALE (0.5f)

#define PATCH_SIZE (MAP_SIZE/NUM_PATCHES_PER_SIDE)

#define POOL_SIZE (25000)

#define VARIANCE_DEPTH (9)

#define MAX_SPLIT_DISTANCE (256)

struct TriTreeNode
{
	TriTreeNode *LeftChild;
	TriTreeNode *RightChild;
	TriTreeNode *BaseNeighbor;

	TriTreeNode *LeftNeighbor;
	TriTreeNode *RightNeighbor;
};

struct PatchVertex
{
	float x,y,z;
	float nx,ny,nz;
	float s0, t0;
};

// Forward declate the ROAMLandscape class
class ROAMLandscape;

class ROAMPatch
{
protected:
	unsigned char *m_HeightMap;									// Pointer to height map to use
	int m_WorldX, m_WorldY;										// World coordinate offset of this patch.

	unsigned char m_VarianceLeft[ 1<<(VARIANCE_DEPTH)];			// Left variance tree
	unsigned char m_VarianceRight[1<<(VARIANCE_DEPTH)];			// Right variance tree

	unsigned char *m_CurrentVariance;							// Which varience we are currently using. [Only valid during the Tessellate and ComputeVariance passes]
	unsigned char m_VarianceDirty;								// Does the Varience Tree need to be recalculated for this Patch?
	unsigned char m_isVisible;									// Is this patch visible in the current frame?

	TriTreeNode m_BaseLeft;										// Left base triangle tree node
	TriTreeNode m_BaseRight;									// Right base triangle tree node

	// Current camera, used for visibility and tesselation tests
	ROAMLandscape *m_pLandscape;

	// Boundingbox for this patch
	BoundingBox *m_pBoundingBox;
	
	// Scale of the map compared to the heightmap size
	unsigned int m_iMapScale;

	// Vertex buffer
	VertexBufferFixed *m_pVB;

	PatchVertex *m_pVertexData;
	unsigned short *m_pIndexData;

	unsigned int m_iNumIndices;

	bool m_bHasSplit;
	
public:
	ROAMPatch(void);
	~ROAMPatch(void);

	ROAMPatch *NeighborPatches[4];

	inline void SetHasSplit( bool isSplit ) { m_bHasSplit = isSplit; }

	// Some encapsulation functions & extras
	TriTreeNode *GetBaseLeft()  { return &m_BaseLeft; }
	TriTreeNode *GetBaseRight() { return &m_BaseRight; }
	char isDirty()     { return m_VarianceDirty; }
	int  isVisibile( ) { return true;/*m_isVisible;*/ }
	void CheckVisibility();

	// The static half of the Patch Class
	virtual void Init( int heightX, int heightY, int worldX, int worldY, unsigned char *hMap, ROAMLandscape *landscape, unsigned int mapScale );
	virtual void Reset();
	virtual void Tessellate();
	virtual void Render();
	virtual void ComputeVariance();

	// The recursive half of the Patch Class
	virtual void			Split( TriTreeNode *tri);
	virtual void			RecursTessellate( TriTreeNode *tri, int leftX, int leftY, int rightX, int rightY, int apexX, int apexY, int node );
	virtual void			RecursRender( TriTreeNode *tri, int leftX, int leftY, int rightX, int rightY, int apexX, int apexY );
	virtual unsigned char	RecursComputeVariance(	int leftX,  int leftY,  unsigned char leftZ,
													int rightX, int rightY, unsigned char rightZ,
													int apexX,  int apexY,  unsigned char apexZ,
													int node);
};

class ROAMLandscape
{
public:
	ROAMLandscape(void);
	~ROAMLandscape(void);

	static TriTreeNode *AllocateTri();

	virtual void Init(unsigned char *hMap, GLCamera *Camera, unsigned int mapScale);
	virtual void Reset();
	virtual void Tessellate();
	virtual void Render();

	inline GLCamera* GetCamera() { return m_pCamera; }
	inline float GetFrameTriangleVariance() { return m_fFrameTriangleVariance; }

protected:
	unsigned char *m_HeightMap;										// HeightMap of the Landscape
	ROAMPatch m_Patches[NUM_PATCHES_PER_SIDE][NUM_PATCHES_PER_SIDE];	// Array of patches

	static int	m_NextTriNode;										// Index to next free TriTreeNode
	static TriTreeNode m_TriPool[POOL_SIZE];						// Pool of TriTree nodes for splitting

	static int GetNextTriNode() { return m_NextTriNode; }
	static void SetNextTriNode( int nNextNode ) { m_NextTriNode = nNextNode; }

	// Current camera, used for visibility and tesselation tests
	GLCamera *m_pCamera;

	// Desired number of Binary Triangle tessellations per frame.
	// This is not the desired number of triangles rendered!
	// There are usually twice as many Binary Triangle structures as there are rendered triangles.
	int m_iNumDesiredTriangles;

	// Beginning frame varience (should be high, it will adjust automatically)
	float m_fFrameTriangleVariance;

	// Scale of the map compared to the heightmap size
	unsigned int m_iMapScale;
};




