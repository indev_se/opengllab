#include "StdAfx.h"
#include "VertexBufferFixed.h"
#include "GeometryBufferFixed.h"


GeometryBufferFixed::GeometryBufferFixed(void)
{
	m_VertexSize = 0;
	m_VertexFormat = 0;
	m_VertexBuffer = nullptr;
	m_IsDirty = false;
}

GeometryBufferFixed::~GeometryBufferFixed(void)
{
}

VertexBufferFixed *GeometryBufferFixed::GetVertexBuffer()
{
	if ( m_IsDirty || m_VertexBuffer == nullptr )
		RebuildVertexBuffer();

	return m_VertexBuffer;
}

void GeometryBufferFixed::RebuildVertexBuffer()
{
	if ( m_VertexBuffer == nullptr )
		m_VertexBuffer = new VertexBufferFixed();

	uint32 numVertices = Count();

	m_VertexBuffer->CreateVertexBuffer( m_VertexFormat, numVertices, &m_Data[0], m_VertexSize, GL_STATIC_DRAW );

	m_IsDirty = false;
}

void GeometryBufferFixed::SetFormat( uint32 vertexFormat )
{
	m_VertexFormat = vertexFormat;
	m_VertexSize = CalculateVertexSize(m_VertexFormat);

	m_IsDirty = true;
}

uint32 GeometryBufferFixed::GetFormat()
{
	return m_VertexFormat;
}

void GeometryBufferFixed::Set(uint8 *buffer, uint32 size)
{
	assert( size == m_VertexSize );

	Clear();
	Add(buffer, size);
}

void GeometryBufferFixed::Add(uint8 *buffer, uint32 size)
{
	assert( size == m_VertexSize ); 

	m_Data.resize( m_Data.size() + size );
	std::copy(buffer, buffer+size, m_Data.end()-size);
	m_IsDirty = true;
}

void GeometryBufferFixed::Clear()
{
	m_Data.clear();
	m_IsDirty = true;
}

uint8 *GeometryBufferFixed::GetAt( uint32 offset )
{
	return &m_Data.at( offset * m_VertexSize );
}

uint32 GeometryBufferFixed::Count()
{
	return m_Data.size() / m_VertexSize;
}

size_t GeometryBufferFixed::CalculateVertexSize( uint32 vertexFormat )
{
	if ( vertexFormat == 0 ) vertexFormat = m_VertexFormat;

	size_t size = 0;

	if ( m_VertexFormat & VDF_XYZ ) size += sizeof(float) * 3;
	if ( m_VertexFormat & VDF_NORMAL ) size += sizeof(float) * 3;
	if ( m_VertexFormat & VDF_COLOR_RGB ) size += sizeof(float) * 3;
	else if ( m_VertexFormat & VDF_COLOR_RGBA ) size += sizeof(float) * 4;

	if ( m_VertexFormat & VDF_TEX0 ) size += sizeof(float) * VertexBufferFixed::GetTextureCoordSize(vertexFormat, 0);
	if ( m_VertexFormat & VDF_TEX1 ) size += sizeof(float) * VertexBufferFixed::GetTextureCoordSize(vertexFormat, 1);
	if ( m_VertexFormat & VDF_TEX2 ) size += sizeof(float) * VertexBufferFixed::GetTextureCoordSize(vertexFormat, 2);
	if ( m_VertexFormat & VDF_TEX3 ) size += sizeof(float) * VertexBufferFixed::GetTextureCoordSize(vertexFormat, 3);
	if ( m_VertexFormat & VDF_TEX4 ) size += sizeof(float) * VertexBufferFixed::GetTextureCoordSize(vertexFormat, 4);
	if ( m_VertexFormat & VDF_TEX5 ) size += sizeof(float) * VertexBufferFixed::GetTextureCoordSize(vertexFormat, 5);
	if ( m_VertexFormat & VDF_TEX6 ) size += sizeof(float) * VertexBufferFixed::GetTextureCoordSize(vertexFormat, 6);
	if ( m_VertexFormat & VDF_TEX7 ) size += sizeof(float) * VertexBufferFixed::GetTextureCoordSize(vertexFormat, 7);

	return size;
}