#pragma once
#include "Asset.h"
#include "BitmapDataAsset.h"
#include "TextureAsset.h"
#include "ShaderProgram.h"
#include "Material.h"

class Asset;

class AssetsManager
{
public:
	AssetsManager(void);
	~AssetsManager(void);

	void AddAsset( const char *, Asset *obj );
	void RemoveAsset( const char * );
	void RemoveAndDestroyAsset( const char * );
	void RemoveAndDestroyAsset( Asset *obj );

	Asset *GetAsset( const char *name );
	BitmapDataAsset *GetBitmapDataAsset( const char *name );
	TextureAsset *GetTextureAsset( const char *name );
	ShaderProgram *GetShaderProgram( const char *name );
	Material *GetMaterial( const char *name );

	bool AssetExists( const char *name );

	BitmapDataAsset *CreateBitmapDataAsset( const char *name, const char *filename );
	TextureAsset *CreateTextureAsset( const char *name );
	ShaderProgram *CreateShaderProgram( const char *name );
	Material *CreateMaterial( const char *name );

	void ClearCache();
	void SetIgnoreCache( bool ignore );

protected:

	void SaveCache( const char* filename, Asset *obj );
	Asset* LoadCache( const char* filename );
	std::string GetCacheName( const char *filename );

	bool m_bIgnoreCache;

	std::map<std::string, Asset*> m_Assets;

	std::map<std::string, Asset*> m_AssetsFileCache;
};

