#pragma once
#include "SceneObject.h"
#include "GeometryBuffer.h"
#include "Mesh.h"
#include "CachedObject.h"
#include "AnimationState.h"

class FbxFileContext;
class BlendAnimationStates;

class AnimatedMeshObject :
	public SceneObject, public CachedObject
{
public:
	AnimatedMeshObject( const char *objectName );
	~AnimatedMeshObject(void);
	
	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);
	
	bool LoadFromFile( const char *filename );
	bool LoadFromCache( const char *cachedFilename );

	void SetAnimationState( AnimationState* state );
	void SetAnimationByName( std::string name, bool loop );
	AnimationState* GetAnimationState();
	void DeleteAnimationState();
	
	Mesh *GetMesh();

protected:

	void SaveCachedVersion(const char *filename);

private:

	Mesh *m_pMesh;
	FbxFileContext *m_pFbxContext;

	AnimationState *m_pAnimationState;

	//GeometryBuffer *m_TransformedGeometryBuffer;
};

