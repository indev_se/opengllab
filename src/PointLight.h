#pragma once
#include "LightObject.h"
#include "SphereObject.h"

class PointLight :
	public LightObject
{
public:
	PointLight( float radius, const char *objectName );
	~PointLight(void);

	void BindLight( GLenum lightID );
	void UnbindLight();

	void SetShaderUniform( char LightVar[60], ShaderProgram *shader );

	float LightSize() { return m_fLightSize; }

protected:

	float m_fLightSize;
};

