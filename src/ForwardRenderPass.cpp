#include "StdAfx.h"
#include "ForwardRenderPass.h"


ForwardRenderPass::ForwardRenderPass(void)
{
}


ForwardRenderPass::~ForwardRenderPass(void)
{
}

void ForwardRenderPass::Init( GLScene *scene )
{
	RenderPass::Init(scene);

}

void ForwardRenderPass::Bind()
{
}

void ForwardRenderPass::Unbind()
{
}

void ForwardRenderPass::BindObject( SceneObject *object )
{
	// Bind the shader
	Material *mtrl = object->GetMaterial();

	if ( mtrl->GetShaderForward() ) {
		mtrl->BindForward();

		mtrl->GetBoundShader()->SendUniformMatrix4fv("matModel", 1, false, (float*)&object->GetModelMatrix());
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matView", 1, false, (float*)&m_pScene->GetCamera()->GetViewMatrix());
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matPersp", 1, false, (float*)&m_pScene->GetProjectionMatrix());
	}
}

void ForwardRenderPass::UnbindObject( SceneObject *object )
{
	Material *mtrl = object->GetMaterial();

	if ( mtrl->GetShaderForward() ) {
		mtrl->UnbindForward();
	}
}