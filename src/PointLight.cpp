#include "StdAfx.h"
#include "PointLight.h"

PointLight::PointLight( float radius, const char *objectName = nullptr ) : LightObject(objectName)
{
	m_LightType = LIGHT_POINT;
	m_fLightSize = radius;
}

PointLight::~PointLight(void)
{
}

void PointLight::SetShaderUniform( char LightVar[60], ShaderProgram *shader )
{
	char LightVarStruct[60];
	
	// LightSource.Type
	sprintf_s(LightVarStruct, "%s.Type", LightVar);
	shader->SendUniform1i( LightVarStruct, m_LightType );
	
	// LightSource.Position
	sprintf_s(LightVarStruct, "%s.Position", LightVar);
	shader->SendUniform3f( LightVarStruct, (float*)&m_vecPosition );
	
	// LightSource.Color
	sprintf_s(LightVarStruct, "%s.Color", LightVar);
	shader->SendUniform3f( LightVarStruct, (float*)&glm::vec3(m_vDiffuse.x, m_vDiffuse.y, m_vDiffuse.z) );
}