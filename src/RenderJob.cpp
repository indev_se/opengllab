#include "StdAfx.h"
#include "RenderJob.h"


RenderJob::RenderJob(void)
{
	m_pVB = nullptr;
	m_pMaterial = nullptr;

	m_TransformMatrix = glm::mat4x4();
}

RenderJob::~RenderJob(void)
{
}

void RenderJob::AddIndexRange( uint16 *indices, size_t size )
{
	if ( m_pVB == nullptr ) {
		StdConsole::LogError( _STR"Trying to call AddIndexRange before SetVertexBuffer");
		return;
	}

	size_t offset = m_pVB->GetIndexSize();

	if ( offset == 0 )
		m_pVB->CreateIndexBuffer( size, indices, GL_STATIC_DRAW );
	else
		m_pVB->SetIndexBuffer( offset, size, indices );
}

void RenderJob::SetVertexBuffer( VertexBuffer *vb )
{
	m_pVB = vb;
}

glm::mat4x4 RenderJob::GetMatrix()
{
	return m_TransformMatrix;
}

void RenderJob::SetMaterial( Material *material )
{
	m_pMaterial = material;
}

Material* RenderJob::GetMaterial()
{
	return m_pMaterial;
}

void RenderJob::Render()
{
	m_pVB->Render( GL_TRIANGLES, (uint16)0, (uint16)0);

	assert( glGetError() == 0 );
}