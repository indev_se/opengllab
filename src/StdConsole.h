#pragma once

#include "StdConsoleEventListener.h"

#define _STR std::ostringstream().flush() << 

class StdConsole
{
public:
	StdConsole(void);
	~StdConsole(void);
	
	static void SetStdoutHandle( HANDLE stdoutHandle );

	static void LogInfo( std::ostream &stream );
	static void LogError( std::ostream &stream );
	static void LogWarning( std::ostream &stream );

	static HANDLE	stdoutHandle;

	static bool ProcessCommand( std::string command, std::string &response );

public:

	static void AddCommandListener( StdConsoleEventListener*, ProcessCommandFuncPtr );
	static void RemoveCommandListener( StdConsoleEventListener*, ProcessCommandFuncPtr );
	
protected:

	static StdConsoleListenerList m_Listeners_ProcessCommand;
	
private:

	static void SetTextColor(WORD wColor);
};

