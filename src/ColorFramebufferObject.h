#pragma once
#include "FramebufferObject.h"

class ColorFramebufferObject :
	public FramebufferObject
{
public:
	ColorFramebufferObject(void);
	~ColorFramebufferObject(void);

	void Create2D( unsigned int Width, unsigned int Height, GLenum attachment );
};

