#pragma once
#include "MeshAnimation.h"
#include "AnimationStateEventListener.h"

#define FPS 30.0f
#define MS_PER_FRAME FPS / 1000.0f

class AnimationState;
typedef void (AnimationStateEventListener::*AnimationStateEventFuncPtr) ( AnimationState *state );

class AnimationState
{
public:
	AnimationState( MeshAnimation* animation, bool loop );
	
	~AnimationState(void);

	void SetEventEndReachedListener( AnimationStateEventListener *scope, AnimationStateEventFuncPtr evt_ptr );

	void Update( float tickMS );

	uint32 GetCurrentFrame();
	int32 GetTotalFrames();

	MeshAnimation* GetMeshAnimation();

	bool IsBlend();

	bool NeedUpdate();

	void Play();
	void Stop();
	void Pause();
	void Reset();

protected:

	AnimationStateEventListener *m_Evt_EndReachedScope;
	AnimationStateEventFuncPtr m_Evt_EndReachedListener;

	int32 m_CurrentFrame;
	int32 m_LastHandledFrame;

	int32 m_TotalFrames;

	bool m_Active;

	float m_DurationSinceLastFrame;

	MeshAnimation* m_Animation;

	bool m_Loop;
	std::string m_AnimationName;

	bool m_IsBlend;

private:

	void EndReached();

	void DispatchEvent_EndReached();

};