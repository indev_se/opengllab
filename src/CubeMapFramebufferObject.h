#pragma once
#include "framebufferobject.h"
class CubeMapFramebufferObject :
	public FramebufferObject
{
public:
	CubeMapFramebufferObject(void);
	~CubeMapFramebufferObject(void);

	void Create( unsigned int Width, unsigned int Height );

private:

	TextureAsset *m_pTextures[6];

};

