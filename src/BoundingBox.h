#pragma once
class BoundingBox
{
public:
	BoundingBox(void);
	BoundingBox(const glm::vec3 &min, const glm::vec3 &max);

	~BoundingBox(void);

	void Render( glm::mat4x4 modelView, glm::mat4x4 projection );

	void Multiply( const glm::mat4x4 &transformMatrix );

	void SetBounds(const glm::vec3 &min, const glm::vec3 &max);
	void GetBounds( glm::vec3 &min, glm::vec3 &max);
	
	void GetPoints( glm::vec3 points[8] );
	void GetPoints( glm::vec3 points[8], glm::mat4x4 transform );

	BoundingBox *Clone();

private:

	glm::vec3 m_vecMin;
	glm::vec3 m_vecMax;

public:

	friend std::ostream& operator<<(std::ostream& os, const BoundingBox& s)
	{
		streamwrite_glm_vec3(os, s.m_vecMin);
		streamwrite_glm_vec3(os, s.m_vecMax);

		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, BoundingBox& s)
	{
		
		s.m_vecMin = streamread_glm_vec3(is);
		s.m_vecMax = streamread_glm_vec3(is);

		return is;
	}
};

