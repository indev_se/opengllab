#include "StdAfx.h"
#include "StreamUtils.h"

void streamwrite_glm_vec3( std::ostream& os, glm::vec3 o )
{
	SWRITE(os,o.x);
	SWRITE(os,o.y);
	SWRITE(os,o.z);
}

void streamwrite_glm_quat( std::ostream& os, glm::quat o )
{
	SWRITE(os,o.x);
	SWRITE(os,o.y);
	SWRITE(os,o.z);
	SWRITE(os,o.w);
}

void streamwrite_glm_mat4x4( std::ostream& os, glm::mat4x4 o )
{
	for ( uint32 x = 0; x < 4; ++x )
		for ( uint32 y = 0; y < 4; ++y )
			SWRITE(os,o[x][y]);
}

glm::vec3 streamread_glm_vec3( std::istream& is )
{
	glm::vec3 o;
	SREAD(is,o.x);
	SREAD(is,o.y);
	SREAD(is,o.z);

	return o;
}

glm::quat streamread_glm_quat( std::istream& is )
{
	glm::quat o;
	SREAD(is,o.x);
	SREAD(is,o.y);
	SREAD(is,o.z);
	SREAD(is,o.w);

	return o;
}

glm::mat4x4 streamread_glm_mat4x4( std::istream& is )
{
	glm::mat4x4 o;
	for ( uint32 x = 0; x < 4; ++x )
		for ( uint32 y = 0; y < 4; ++y )
			SREAD(is,o[x][y]);

	return o;
}