#pragma once
#include "RenderPass.h"
#include "EmptyFramebufferObject.h"
#include "ColorFramebufferObject.h"

class GaussianBlurPass :
	public RenderPass
{
public:
	GaussianBlurPass(void);
	~GaussianBlurPass(void);

	void Init( GLScene *scene );
	void Bind() {}
	void Unbind() {}

	void ApplyBlur( TextureAsset *texture );

	uint32 GetPass1TextureId();
	uint32 GetPass2TextureId();

private:

	ShaderProgram* m_pShader;
	ColorFramebufferObject* m_pPass1FBO;
	EmptyFramebufferObject* m_pPass2FBO;
};

