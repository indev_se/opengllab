#include "StdAfx.h"
#include "StaticMeshGroupObject.h"


StaticMeshGroupObject::StaticMeshGroupObject( const char *objectName ) : SceneObject(objectName)
{
}


StaticMeshGroupObject::~StaticMeshGroupObject(void)
{
}
	
void StaticMeshGroupObject::Create( MeshGroup *mesh, GeometryBuffer *gb )
{
	m_pMesh = mesh;
	m_pGB = gb;

	if ( m_pMesh->m_aaBoundingBox )
		m_pAABB = m_pMesh->m_aaBoundingBox->Clone();

	m_pMaterial = m_pScene->GetAssetsManager()->GetMaterial( m_pMesh->materialName.c_str() );

	if ( m_pMaterial ) {
		m_pGB->ReplaceDescription( m_pMaterial->GetDescription() );
	}
}

glm::mat4x4 StaticMeshGroupObject::GetModelMatrix()
{
	glm::mat4x4 mat = SceneObject::GetModelMatrix();
	
	return mat * m_pMesh->transform;
}

void StaticMeshGroupObject::Init( GLScene *scene )
{
	SceneObject::Init(scene);
}

void StaticMeshGroupObject::Update( float tickMS )
{
	SceneObject::Update(tickMS);
}

void StaticMeshGroupObject::Render(RenderPass *pass)
{
	if ( pass != nullptr )
		pass->BindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->BindForward();

	m_pGB->GetVertexBuffer()->Render( GL_TRIANGLES, m_pMesh->indices, m_pMesh->numIndices );
	
	if ( pass != nullptr )
		pass->UnbindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->UnbindForward();

}