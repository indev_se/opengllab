#pragma once

class VertexDescription;

class VertexDataElement
{
public:
	VertexDataElement(void) : 
	  AttribLocation(0), 
	  Name(""),
	  Size(0),
	  Enabled(true) {}

	uint32 AttribLocation;
	std::string Name;
	DataType Type;

	uint32 Size;
	uint32 Offset;
	uint32 ByteSize;

	bool Enabled;

	VertexDescription *ptrdesc;

	GLenum GetType()
	{
		switch(Type)
		{
		case Byte:
			return GL_BYTE;
		case Integer:
			return GL_INT;
		case Float:
			return GL_FLOAT;
		}

		return GL_FLOAT;
	}

	void ParseTypeSize(std::string typeSize)
	{
		if ( typeSize == "float4" ) {
			Type = Float; Size = 4;
		}
		else if ( typeSize == "float3" ) {
			Type = Float; Size = 3;
		}
		else if ( typeSize == "float2" ) {
			Type = Float; Size = 2;
		}
		else {
			printf("VertexDataElement - Unknown TypeSize %s", typeSize.c_str());
		}
	}

public:

	// Insertion operator
	friend std::ostream& operator<<(std::ostream& os, const VertexDataElement& s)
	{
		SWRITE(os,s.AttribLocation);
		uint32 len = s.Name.length();
		SWRITE(os,len);
		SWRITE_C(os,s.Name[0],len);
		SWRITE(os,s.Type);

		SWRITE(os,s.Size);
		SWRITE(os,s.Offset);
		SWRITE(os,s.ByteSize);

		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, VertexDataElement& s)
	{
		SREAD(is,s.AttribLocation);
		uint32 len;
		SREAD(is,len);

		s.Name.resize(len);
		SREAD_C(is,s.Name[0],len);

		uint32 Type;
		SREAD(is,Type);
		s.Type = static_cast<DataType>(Type);

		SREAD(is,s.Size);
		SREAD(is,s.Offset);
		SREAD(is,s.ByteSize);

		return is;
	}

};

class VertexDescription
{
public:
	VertexDescription(void);
	~VertexDescription(void);

	void AddElement( VertexDataElement *elem );
	void RemoveElement( VertexDataElement *elem );
	
	std::vector<VertexDataElement*> GetElements() { return m_Elements; }

	VertexDataElement *GetElement( std::string Name );

	void Lock();
	void Unlock();

	uint32 VertexSize() { return m_iVertexSize; }

	void BindShaderAttribs(ShaderProgram *shader);

	static VertexDescription* GetDefault( std::string type );

private:

	uint32 m_iVertexSize;

	bool m_bLocked;
	std::vector<VertexDataElement*> m_Elements;

	void CalculateValues();

public:

	// Insertion operator
	friend std::ostream& operator<<(std::ostream& os, const VertexDescription& s)
	{
		SWRITE(os,s.m_iVertexSize);

		uint32 numElements = s.m_Elements.size();
		SWRITE(os,numElements);
		for ( uint32 i = 0; i < numElements; ++i )
			os << *s.m_Elements[i];

		return os;
	}

	
	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, VertexDescription& s)
	{
		SREAD(is, s.m_iVertexSize);

		uint32 numElements;
		SREAD(is,numElements);

		for ( uint32 i = 0; i < numElements; ++i ) {
			VertexDataElement *element = new VertexDataElement();
			is >> *element;

			s.m_Elements.push_back(element);
		}

		s.Lock();
		return is;
	}
};

class VertexData
{
public:
	VertexData(void);
	~VertexData(void);

	void set( const VertexDataElement &elem, uint8 *data );
	void set( VertexDataElement *elem, uint8 *data );
	void set( VertexDataElement *elem, glm::vec3 data );
	void set( VertexDataElement *elem, glm::vec2 data );

	void clear();

	uint8 *data() { return ptrdata; }

	void data(uint8* data, uint32 size);

private:

	uint8 *ptrdata;
};