#include "StdAfx.h"
#include "FramebufferObject.h"


FramebufferObject::FramebufferObject(void)
{
	m_pTexture = NULL;
}


FramebufferObject::~FramebufferObject(void)
{
	if (m_pTexture)
		delete m_pTexture;

	m_pTexture = NULL;

	glDeleteFramebuffers(1, &m_FBOID);
}
