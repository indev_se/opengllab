#pragma once
#include "framebufferobject.h"
class DeferredFramebuffers :
	public FramebufferObject
{
public:
	DeferredFramebuffers(void);
	~DeferredFramebuffers(void);

	void Create2D( unsigned int Width, unsigned int Height );

	TextureAsset *GetDepthTexture() { return m_pDepthTexture; }
	TextureAsset *GetColorTexture() { return m_pColorTexture; }
	TextureAsset *GetNormalTexture() { return m_pNormalTexture; }
	TextureAsset *GetPositionTexture() { return m_pPositionTexture; }
	TextureAsset *GetShadowTexture() { return m_pShadowTexture; }

private:

	TextureAsset *CreateDepthTexture();

	TextureAsset *CreateColorTexture();

	TextureAsset *m_pDepthTexture;
	TextureAsset *m_pColorTexture;
	TextureAsset *m_pNormalTexture;
	TextureAsset *m_pPositionTexture;
	TextureAsset *m_pShadowTexture;
};