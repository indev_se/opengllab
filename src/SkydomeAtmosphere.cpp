#include "StdAfx.h"
#include "SkydomeAtmosphere.h"

// http://www.bitblit.org/articles/starfield/index.shtml
// http://www.cs.utah.edu/~shirley/papers/sunsky/sunsky.pdf
SkydomeAtmosphere::SkydomeAtmosphere( uint32 _Width, uint32 _Height )
{
	m_Width = _Width;
	m_Height = _Height;
}

SkydomeAtmosphere::~SkydomeAtmosphere(void)
{
}

void SkydomeAtmosphere::Generate()
{
}