// Strategy.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "GLGameWindow.h"
#include "GameApplication.h"
#include "StrategyGLScene.h"
#include "DreamPlayerScene.h"
#include "SponzaScene.h"
#include "EmptyGLScene.h"
#include "BlendAnimationScene.h"
#include "SkydomeScene.h"
#include "Strategy.h"

int WINAPI WinMain(	HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	GLGameWindow *gameWindow;
	GameApplication *app;
	//StrategyGLScene *scene;
	SponzaScene *scene;
	//DreamPlayerScene *scene;
	//EmptyGLScene *scene;
	//BlendAnimationScene *scene;
	//SkydomeScene *scene;

	gameWindow = new GLGameWindow();
	gameWindow->Create( 800, 600, false );
	gameWindow->ActivateConsole();

	//scene = new EmptyGLScene();
	//scene = new StrategyGLScene();
	scene = new SponzaScene();
	//scene = new DreamPlayerScene();
	//scene = new BlendAnimationScene();
	//scene = new SkydomeScene();

	app = new GameApplication();
	app->Run( gameWindow, scene );

	return 0;
}
