#pragma once
#include "VertexData.h"

class VertexBuffer
{
public:
	VertexBuffer(void);
	~VertexBuffer(void);

	void Destroy();

	void CreateVertexBuffer( VertexDescription *desc, size_t vCount, VertexData *data, GLenum usage );
	void CreateVertexBuffer( VertexDescription *desc, size_t vCount, void *data, GLenum usage );
	
	void CreateIndexBuffer( size_t iCount, uint16 *indices, GLenum usage );
	void CreateIndexBuffer( size_t iCount, uint32 *indices, GLenum usage );

	void SetVertexBuffer( int offset, size_t vCount, VertexData *data );
	void SetIndexBuffer( int offset, size_t iCount, unsigned short *indices );

	void Render( GLenum mode, uint16 indexOffset, uint16 indexCount);
	void Render( GLenum mode, uint32 indexOffset, uint32 indexCount);
	void Render( GLenum mode, uint16 *indices, uint32 indexCount );
	void Render( GLenum mode, uint32 *indices, uint32 indexCount );

	void Render( GLenum mode );

	size_t GetIndexSize() { return m_iIndexCount; }

private:

	void EnableVertexAttribs();
	void DisableVertexAttribs();
	void BindBuffers();

	VertexDescription	*m_VertexDesc;

	bool			m_bHaveVBID;
	bool			m_bHaveIBID;

	uint32			m_iVBID;
	uint32			m_iIBID;

	size_t			m_iVertexSize;
	size_t			m_iVertexCount;
	size_t			m_iIndexCount;
};