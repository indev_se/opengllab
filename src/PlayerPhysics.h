#pragma once

#define PLAYER_GRAVITY -9.81f
#define PLAYER_HALF_GRAVITY PLAYER_GRAVITY / 2.0f

#define PLAYER_JUMP_VELOCITY 4.0f
#define PLAYER_JUMP_MIN_VELOCITY -4.0f

#define PLAYER_RUN_MAX_VELOCITY 10.67f
#define PLAYER_RUN_ACCELERATION 2.2f
#define PLAYER_RUN_DEACCELERATION -102.2f
#define PLAYER_RUN_HALF_ACCELERATION PLAYER_RUN_ACCELERATION / 2.0f

class PlayerPhysicsSimulation
{
public:
	PlayerPhysicsSimulation(void);
	~PlayerPhysicsSimulation(void);

	void Create( glm::vec3 initialVelocity, glm::vec3 initialPosition, glm::vec3 acceleration );

	void SetVelocityBounds( glm::vec3 velocityMin, glm::vec3 velocityMax );

	void Run();
	void Pause();
	void Reset();

	void Simulate( float tickSec );

	void SetAcceleration( glm::vec3 acceleration );
	void SetAccelerationX( float acceleration );
	void SetAccelerationY( float acceleration );
	void SetAccelerationZ( float acceleration );

	glm::vec3 GetVelocity() { return m_Velocity; }
	glm::vec3 GetPosition() { return m_Position; }

	glm::vec3 GetForce() { return m_Force; }
	void SetForce( glm::vec3 force );
	void SetForceX( float force );
	void SetForceY( float force );
	void SetForceZ( float force );

	float GetForceY();

	void SetVelocity( const glm::vec3 axis, float velocity );

	void PushCurrentState();

	float GetSimulationTime() { return m_SimulationTime; }

private:

	bool m_Paused;

	float m_SimulationTime;
	glm::vec3 m_Velocity;
	glm::vec3 m_Acceleration;
	glm::vec3 m_HalfAcceleration;
	glm::vec3 m_Position;

	glm::vec3 m_InitialVelocity;
	glm::vec3 m_InitialPosition;

	glm::vec3 m_VelocityMin;
	glm::vec3 m_VelocityMax;

	glm::vec3 m_Force; // Not really a force, more like a direction :)

};

class PlayerPhysics
{
public:
	PlayerPhysics(void);
	~PlayerPhysics(void);

	void Init(glm::vec3 initialPosition);
	void Simulate( float tickMS );

	void DoJump();
	void StartRun( glm::vec3 direction );
	void StopRun();

	void ChangeDirection( glm::vec3 direction );

	glm::vec3 GetPosition();

private:

	PlayerPhysicsSimulation *m_PositionSim;

};