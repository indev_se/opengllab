#include "StdAfx.h"
#include "GLOverlay.h"


GLOverlay::GLOverlay(void)
{
}


GLOverlay::~GLOverlay(void)
{
}

void GLOverlay::Init()
{

}

void GLOverlay::SetOrtho( int Width, int Height )
{
	glm::mat4x4 tempProjectionMatrix;

	m_iWidth = Width, m_iHeight = Height;

	// Store the current projection matrix
	glMatrixMode(GL_PROJECTION);
	glGetFloatv(GL_PROJECTION_MATRIX, glm::value_ptr(tempProjectionMatrix));
	
	// Set the Ortho projection
	glLoadIdentity();
	glOrtho(0,Width,0,Height,1,10);
	
	// Save the ortho projection matrix
	glGetFloatv(GL_PROJECTION_MATRIX, glm::value_ptr(m_matOrthoProjection));
	
	// Set the projection matrix again
	glLoadMatrixf( glm::value_ptr(tempProjectionMatrix) );
}

void GLOverlay::Clear( GLbitfield bufferMask )
{
	
}

void GLOverlay::BeginRender()
{
	// Store the current projection matrix
	glMatrixMode(GL_PROJECTION);

	// Enable the ortho projection
	glLoadIdentity();
	glLoadMatrixf( glm::value_ptr(m_matOrthoProjection) );

	// Switch to modelview matrix
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity(); // clear since we don't want matrix from the camera
}

void GLOverlay::EndRender()
{
	// Set the values back as they were
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity(); 
}