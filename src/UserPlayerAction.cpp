#include "StdAfx.h"
#include "UserPlayerAction.h"


UserPlayerAction::UserPlayerAction( PlayerActionState State )
{
	m_State = State;
	m_IsCurrent = false;

	m_IsEndless = true;
	if ( m_State == STATE_JUMP )
		m_IsEndless = false;
}


UserPlayerAction::~UserPlayerAction(void)
{
}

PlayerActionState UserPlayerAction::GetState()
{
	return m_State;
}

bool UserPlayerAction::IsEndless()
{
	return m_IsEndless;
}

std::string UserPlayerAction::GetAnimationName()
{
	switch ( m_State )
	{
		case STATE_WALK_FORWARD:
		case STATE_WALK_BACKWARD:
			return "Walk";
		
		case STATE_RUN_FORWARD:
		case STATE_RUN_BACKWARD:
			return "Run";

		case STATE_JUMP:
			return "Jump";

		case STATE_NONE:
		case STATE_IDLE:
		default:
			return "Idle";
	}
}