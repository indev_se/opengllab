#include "StdAfx.h"
#include "CameraInput.h"
#include "MaterialImporter.h"
#include "ForwardRenderPass.h"
#include "SceneObjectFactory.h"
#include "SceneManager.h"
#include "Skydome.h"
#include "MeasureObject.h"
#include "SkydomeScene.h"

SkydomeScene::SkydomeScene(void)
{
	m_pDefaultFPSCamera = nullptr;
}

SkydomeScene::~SkydomeScene(void)
{
}

void SkydomeScene::SetViewport( int Width, int Height )
{
	GLScene::SetViewport( Width, Height );

	if (m_pDefaultFPSCamera != nullptr)
		m_pDefaultFPSCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );
}

void SkydomeScene::Init()
{
	GLScene::Init();
	
	// Setup the assetsmanager
	m_AssetsManager = new AssetsManager();

	// Create the camera
	m_pViewportCamera = m_pDefaultFPSCamera = new GLCamera();
	m_pViewportCamera->Init();

	if ( m_iWidth != 0 && m_iHeight != 0)
		m_pViewportCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );
	else
		m_pViewportCamera->SetPerspective( 45.0f, 4.f/3.f, 1.0f, 4000.0f );

	m_pViewportCamera->MoveEyeZ( -20 );

	// Setup the camera input
	m_pCameraInput = new CameraInput();
	m_pCameraInput->SetCamera( m_pViewportCamera );

	// Read all materials
	m_MaterialImporter = new MaterialImporter();
	m_MaterialImporter->ImportAssetsFromFile("Data/Materials/builtin.mtrl", m_AssetsManager);
	m_MaterialImporter->ImportAssetsFromFile("Data/Materials/skydome.mtrl", m_AssetsManager);
	
	// Create the scene objects
	m_RootObject = new SceneObject("root");

	// Create the forward rendering pass
	passForward = new ForwardRenderPass();
	passForward->Init(this);

	// Create the skydome
	Skydome *sky = new Skydome( 20, 20, METER * 20, "Skydome");
	sky->Init(this);
	m_RootObject->AddChild(sky);

	// Create a measure object
	MeasureObject *measure = new MeasureObject( "MeasurementFloor");
	measure->Init(this, 50.f );
	m_RootObject->AddChild(measure);	
}

void SkydomeScene::GetBounds( glm::vec3 &min, glm::vec3 &max )
{
	m_RootObject->GetBounds(min, max);
}

void SkydomeScene::Update( float tickMS )
{
	m_pCameraInput->Update( std::min(tickMS, 50.0f) );

	m_RootObject->Update( std::min(tickMS, 50.0f) );
}

void SkydomeScene::Render()
{
	Clear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	passForward->Bind();
	m_RootObject->Render( passForward );
	passForward->Unbind();
	
	AssertGLError();
}