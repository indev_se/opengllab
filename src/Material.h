#pragma once
#include "Asset.h"
#include "ShaderProgram.h"
#include "TextureAsset.h"
#include "VertexData.h"
#include "RenderPass.h"

struct MaterialProperty
{
	DataType Type;

	std::string ValueString;
	float ValueFloat;
	int32 ValueInteger;
	bool ValueBool;
};

class RenderPass;

class Material :
	public Asset
{
public:
	Material(void);
	~Material(void);

	VertexDescription *GetDescription() { return m_VertexDesc; }
	
	void SetShaderForward(ShaderProgram *shader) { m_pShaderForward = shader; }
	ShaderProgram *GetShaderForward() { return m_pShaderForward; }

	void SetShaderDeferred(ShaderProgram *shader) { m_pShaderDeferred = shader; }
	ShaderProgram *GetShaderDeferred() { return m_pShaderDeferred; }

	ShaderProgram *GetBoundShader() { return m_pBoundShader; }

	void AddTexture( const char *name, TextureAsset *tex );

	void BindForward(RenderPass *pass = nullptr);
	void UnbindForward(RenderPass *pass = nullptr);

	void BindDeferred(RenderPass *pass = nullptr);
	void UnbindDeferred(RenderPass *pass = nullptr);

	void BindShaderAttribs();

	void SyncMaterialDescription( Material *material );
	void UnsyncMaterialDescription();

public:

	void SetProperty( std::string name, float value );
	void SetProperty( std::string name, int32 value );
	void SetProperty( std::string name, std::string value );
	void SetProperty( std::string name, bool value );

	float GetFloatProperty( std::string name );
	int32 GetIntegerProperty( std::string name );
	std::string GetStringProperty( std::string name );
	bool GetBoolProperty( std::string name );

private:

	MaterialProperty* GetOrCreateProperty( std::string name );

	VertexDescription *m_VertexDesc;
	ShaderProgram *m_pShaderForward;
	ShaderProgram *m_pShaderDeferred;

	ShaderProgram *m_pBoundShader;

	std::map<std::string, TextureAsset *> m_Textures;

	std::map<std::string, MaterialProperty*> m_Properties;
};

