#include "StdAfx.h"
#include "GameTimer.h"


GameTimer::GameTimer(void)
{
	m_fFpsLastUpdate = 0;
	m_fFpsUpdateInterval = 0.5;
	m_iNumFrames = 0;
	m_fFPS = 0;
}

GameTimer::~GameTimer(void)
{
}

void GameTimer::Init()
{
	if( !QueryPerformanceFrequency((LARGE_INTEGER *)&m_iTicksPerSecond) )
		m_iTicksPerSecond = 1000;
  
	m_fTimeAtGameStart = 0;
	m_fTimeAtGameStart = GetElapsed();

	m_fLastTick = m_fTimeAtGameStart;
}

float GameTimer::GetElapsed()
{
  UINT64 ticks;
  float time;
  
  // This is the number of clock ticks since start
  if( !QueryPerformanceCounter((LARGE_INTEGER *)&ticks) )
    ticks = (UINT64)timeGetTime();

  // Divide by frequency to get the time in seconds
  time = (float)(__int64)ticks/(float)(__int64)m_iTicksPerSecond;
  
  // Subtract the time at game start to get
  // the time since the game started
  time -= m_fTimeAtGameStart;
  
  return time;
}

void GameTimer::Tick()
{
	m_iNumFrames++;
	float currentUpdate = GetElapsed();
	if( currentUpdate - m_fFpsLastUpdate > m_fFpsUpdateInterval )
	{
		m_fFPS = m_iNumFrames / (currentUpdate - m_fFpsLastUpdate);
		m_fFpsLastUpdate = currentUpdate;
		m_iNumFrames = 0;
	}

	m_fDelta = currentUpdate - m_fLastTick;

	m_fLastTick = currentUpdate;
}