#include "StdAfx.h"
#include "SphereObject.h"


SphereObject::SphereObject(float radius, uint32 segW, uint32 segH, const char *objectName) : SceneObject(objectName)
{
	m_fRadius = radius;
	m_iSegmentsW = segW;
	m_iSegmentsH = segH;
	m_pVB = nullptr;
}

SphereObject::~SphereObject(void)
{
	if ( m_pVB != nullptr ) {
		delete m_pVB;
		m_pVB = nullptr;
	}
}

void SphereObject::Init( GLScene *scene, Material *material, VertexDescription *description )
{
	SceneObject::Init( scene, material );

	m_pVB = new VertexBuffer();

	VertexDescription *usedDescription = nullptr;

	if ( material != nullptr )
		usedDescription = material->GetDescription();
	else if ( description != nullptr )
		usedDescription = description;
	else 
		usedDescription = VertexDescription::GetDefault("Position3Color3Normal3");

	VertexDataElement *POS = usedDescription->GetElement("InPosition");
	VertexDataElement *NOR = usedDescription->GetElement("InNormal");
	VertexDataElement *COL = usedDescription->GetElement("InColor");

	uint32 numVertices = ( m_iSegmentsH + 1 ) * ( m_iSegmentsW + 1 );
	uint32 numIndices = ( m_iSegmentsH - 1 ) * m_iSegmentsW * 6;

	uint32 vertexIndex = 0;
	uint32 idxIndex = 0;

	VertexData *vertices = new VertexData[numVertices];
	uint16 *indices = new uint16[numIndices];

	float horAngle, verAngle, ringRadius, normLen, x, y, z;

	for ( uint32 j = 0; j <= m_iSegmentsH; ++j )
	{
		horAngle = M_PI * j / m_iSegmentsH;
		z = -m_fRadius * cosf(horAngle);
		ringRadius = m_fRadius * sinf(horAngle);

		for ( uint32 i = 0; i <= m_iSegmentsW; ++i )
		{
			verAngle = 2 * M_PI * i / m_iSegmentsW;
			x = ringRadius * cosf(verAngle);
			y = ringRadius * sinf(verAngle);

			normLen = 1 / sqrt( x*x + y*y + z*z );

			vertices[vertexIndex].set(POS, glm::vec3(x,y,z));
			if (NOR != nullptr) vertices[vertexIndex].set(NOR, glm::vec3(x,y,z) * normLen);
			if (COL != nullptr) vertices[vertexIndex].set(COL, m_vColor);

			++vertexIndex;

			// set the indices
			if ( i > 0 && j > 0 ) 
			{
				uint16 a = (m_iSegmentsW+1) * j + i;
				uint16 b = (m_iSegmentsW+1) * j + i - 1;
				uint16 c = (m_iSegmentsW+1) * (j - 1) + i - 1;
				uint16 d = (m_iSegmentsW+1) * (j - 1) + i;

				if ( j == m_iSegmentsH ) {
					indices[idxIndex++] = a;
					indices[idxIndex++] = c;
					indices[idxIndex++] = d;
				}
				else if ( j == 1 ) {
					indices[idxIndex++] = a;
					indices[idxIndex++] = b;
					indices[idxIndex++] = c;
				}
				else {
					indices[idxIndex++] = a;
					indices[idxIndex++] = b;
					indices[idxIndex++] = c;
					indices[idxIndex++] = a;
					indices[idxIndex++] = c;
					indices[idxIndex++] = d;
				}
			}
		}
	}

	m_pVB->CreateVertexBuffer( usedDescription, numVertices, vertices, GL_STATIC_DRAW );
	m_pVB->CreateIndexBuffer( numIndices, indices, GL_STATIC_DRAW );

	delete []vertices;
	delete []indices;
}

void SphereObject::Update( float tickMS )
{
	SceneObject::Update(tickMS);
}

void SphereObject::Render(RenderPass *pass)
{
	if ( pass != nullptr )
		pass->BindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->BindForward();

	glPushMatrix();
	
	glMultMatrixf( glm::value_ptr(m_matTransform) );
	
	m_pVB->Render( GL_TRIANGLES, (uint16)0, (uint16)0 );
	
	SceneObject::Render(pass);

	glPopMatrix();

	if ( pass != nullptr )
		pass->UnbindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->UnbindForward();
}
