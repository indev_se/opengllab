#pragma once
#include "Sceneobject.h"
#include "VertexBuffer.h"

class SphereObject :
	public SceneObject
{
public:
	SphereObject(float radius, uint32 segW, uint32 segH, const char *objectName);
	~SphereObject(void);
	
	void Init( GLScene *scene, Material *material, VertexDescription *description = nullptr );
	void Update( float tickMS );
	void Render(RenderPass *pass);
	
	inline void SetColor( glm::vec3 color ) { m_vColor = color; }
	inline void GetColor( glm::vec3 &color ) { color = m_vColor; }

private:

	float m_fRadius;
	uint32 m_iSegmentsW;
	uint32 m_iSegmentsH;

	glm::vec3 m_vColor;

	VertexBuffer *m_pVB;
};

