#pragma once
class SkydomeClouds
{
public:
	SkydomeClouds( uint32 _Width, uint32 _Height );
	~SkydomeClouds(void);

	void Generate();

private:

	uint32 m_Width, m_Height;
};

