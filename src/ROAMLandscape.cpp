#include "StdAfx.h"
#include "BoundingBox.h"
#include "GLCamera.h"
#include "SceneManager.h"
#include "TextObject.h"
#include "VertexBufferFixed.h"
#include "ROAMLandscape.h"


/*****************************
 ** 	ROAM Patch			**
 *****************************/
ROAMPatch::ROAMPatch(void)
{
	m_iMapScale = 1;
}

ROAMPatch::~ROAMPatch(void)
{
}

// ---------------------------------------------------------------------
// Split a single Triangle and link it into the mesh.
// Will correctly force-split diamonds.
//
void ROAMPatch::Split(TriTreeNode *tri)
{
	// We are already split, no need to do it again.
	if (tri->LeftChild)
		return;

	// If this triangle is not in a proper diamond, force split our base neighbor
	if ( tri->BaseNeighbor && (tri->BaseNeighbor->BaseNeighbor != tri) )
		Split(tri->BaseNeighbor);

	// Create children and link into mesh
	tri->LeftChild  = ROAMLandscape::AllocateTri();
	tri->RightChild = ROAMLandscape::AllocateTri();

	// If creation failed, just exit.
	if ( !tri->LeftChild )
		return;

	// Fill in the information we can get from the parent (neighbor pointers)
	tri->LeftChild->BaseNeighbor  = tri->LeftNeighbor;
	tri->LeftChild->LeftNeighbor  = tri->RightChild;

	tri->RightChild->BaseNeighbor  = tri->RightNeighbor;
	tri->RightChild->RightNeighbor = tri->LeftChild;

	// Link our Left Neighbor to the new children
	if (tri->LeftNeighbor != NULL)
	{
		if (tri->LeftNeighbor->BaseNeighbor == tri)
			tri->LeftNeighbor->BaseNeighbor = tri->LeftChild;
		else if (tri->LeftNeighbor->LeftNeighbor == tri)
			tri->LeftNeighbor->LeftNeighbor = tri->LeftChild;
		else if (tri->LeftNeighbor->RightNeighbor == tri)
			tri->LeftNeighbor->RightNeighbor = tri->LeftChild;
		else
			;// Illegal Left Neighbor!
	}

	// Link our Right Neighbor to the new children
	if (tri->RightNeighbor != NULL)
	{
		if (tri->RightNeighbor->BaseNeighbor == tri)
			tri->RightNeighbor->BaseNeighbor = tri->RightChild;
		else if (tri->RightNeighbor->RightNeighbor == tri)
			tri->RightNeighbor->RightNeighbor = tri->RightChild;
		else if (tri->RightNeighbor->LeftNeighbor == tri)
			tri->RightNeighbor->LeftNeighbor = tri->RightChild;
		else
			;// Illegal Right Neighbor!
	}

	// Link our Base Neighbor to the new children
	if (tri->BaseNeighbor != NULL)
	{
		if ( tri->BaseNeighbor->LeftChild )
		{
			tri->BaseNeighbor->LeftChild->RightNeighbor = tri->RightChild;
			tri->BaseNeighbor->RightChild->LeftNeighbor = tri->LeftChild;
			tri->LeftChild->RightNeighbor = tri->BaseNeighbor->RightChild;
			tri->RightChild->LeftNeighbor = tri->BaseNeighbor->LeftChild;
		}
		else
			Split( tri->BaseNeighbor);  // Base Neighbor (in a diamond with us) was not split yet, so do that now.
	}
	else
	{
		// An edge triangle, trivial case.
		tri->LeftChild->RightNeighbor = NULL;
		tri->RightChild->LeftNeighbor = NULL;
	}
}

// ---------------------------------------------------------------------
// Tessellate a Patch.
// Will continue to split until the variance metric is met.
//
void ROAMPatch::RecursTessellate( TriTreeNode *tri,
							 int leftX,  int leftY,
							 int rightX, int rightY,
							 int apexX,  int apexY,
							 int node )
{
	float TriVariance;
	int centerX = (leftX + rightX)>>1; // Compute X coordinate of center of Hypotenuse
	int centerY = (leftY + rightY)>>1; // Compute Y coord...

	glm::vec3 vecCameraEye;
	m_pLandscape->GetCamera()->GetEye( vecCameraEye );
	
	if ( node < (1<<VARIANCE_DEPTH) )
	{
		// Extremely slow distance metric (sqrt is used).
		// Replace this with a faster one!
		float f1 = SQR( ( (float)centerX - (-vecCameraEye.x) ) );
		float f2 = SQR( ( (float)centerY - (-vecCameraEye.z) ) );
		float distance = 1.0f + sqrtf( f1 + f2 );
		
		// Ignore split if it's to far away, depends of use of the landscape, might wanna remove it
		//if ( distance > MAX_SPLIT_DISTANCE ) return;

		// Egads!  A division too?  What's this world coming to!
		// This should also be replaced with a faster operation.
		TriVariance = ((float)m_CurrentVariance[node] * MAP_SIZE * 2)/distance;	// Take both distance and variance into consideration
	}

	if ( (node >= (1<<VARIANCE_DEPTH)) ||	// IF we do not have variance info for this node, then we must have gotten here by splitting, so continue down to the lowest level.
		 (TriVariance > m_pLandscape->GetFrameTriangleVariance() ))	// OR if we are not below the variance tree, test for variance.
	{
		Split(tri);														// Split this triangle.
		m_bHasSplit = true;
		
		// Mark the neighbours as split aswell
		if (NeighborPatches[0]) NeighborPatches[0]->SetHasSplit(true);
		if (NeighborPatches[1]) NeighborPatches[1]->SetHasSplit(true);
		if (NeighborPatches[2]) NeighborPatches[2]->SetHasSplit(true);
		if (NeighborPatches[3]) NeighborPatches[3]->SetHasSplit(true);

		if (tri->LeftChild &&											// If this triangle was split, try to split it's children as well.
			((abs(leftX - rightX) >= 3) || (abs(leftY - rightY) >= 3)))	// Tessellate all the way down to one vertex per height field entry
		{
			RecursTessellate( tri->LeftChild,   apexX,  apexY, leftX, leftY, centerX, centerY,    node<<1  );
			RecursTessellate( tri->RightChild, rightX, rightY, apexX, apexY, centerX, centerY, 1+(node<<1) );
		}
	}
}

// ---------------------------------------------------------------------
// Render the tree.  Simple no-fan method.
//
void ROAMPatch::RecursRender( TriTreeNode *tri, int leftX, int leftY, int rightX, int rightY, int apexX, int apexY )
{
	if ( tri->LeftChild )					// All non-leaf nodes have both children, so just check for one
	{
		int centerX = (leftX + rightX)>>1;	// Compute X coordinate of center of Hypotenuse
		int centerY = (leftY + rightY)>>1;	// Compute Y coord...

		RecursRender( tri->LeftChild,  apexX,   apexY, leftX, leftY, centerX, centerY );
		RecursRender( tri->RightChild, rightX, rightY, apexX, apexY, centerX, centerY );
	}
	else									// A leaf node!  Output a triangle to be rendered.
	{
		m_pIndexData[m_iNumIndices++] = leftX + leftY * (PATCH_SIZE+1);
		m_pIndexData[m_iNumIndices++] = rightX + rightY * (PATCH_SIZE+1);
		m_pIndexData[m_iNumIndices++] = apexX + apexY * (PATCH_SIZE+1);

		/*
		GLfloat leftZ  = m_HeightMap[ ((leftY / m_iMapScale  * MAP_SIZE / m_iMapScale) + leftX/ m_iMapScale) ];
		GLfloat rightZ = m_HeightMap[ ((rightY / m_iMapScale * MAP_SIZE / m_iMapScale) + rightX/ m_iMapScale)  ];
		GLfloat apexZ  = m_HeightMap[ ((apexY / m_iMapScale  * MAP_SIZE / m_iMapScale) + apexX/ m_iMapScale)  ];

		// Output the LEFT VERTEX for the triangle
		glVertex3f(		(GLfloat) leftX,
						(GLfloat) leftZ,
						(GLfloat) leftY );

		// Output the RIGHT VERTEX for the triangle
		glVertex3f(		(GLfloat) rightX,
						(GLfloat) rightZ,
						(GLfloat) rightY );

		// Output the APEX VERTEX for the triangle
		glVertex3f(		(GLfloat) apexX,
						(GLfloat) apexZ,
						(GLfloat) apexY );
		*/
	}
}

// ---------------------------------------------------------------------
// Computes Variance over the entire tree.  Does not examine node relationships.
//
unsigned char ROAMPatch::RecursComputeVariance( int leftX,  int leftY,  unsigned char leftZ,
										    int rightX, int rightY, unsigned char rightZ,
											int apexX,  int apexY,  unsigned char apexZ,
											int node)
{
	//        /|\
	//      /  |  \
	//    /    |    \
	//  /      |      \
	//  ~~~~~~~*~~~~~~~  <-- Compute the X and Y coordinates of '*'
	//
	int centerX = (leftX + rightX) >>1;		// Compute X coordinate of center of Hypotenuse
	int centerY = (leftY + rightY) >>1;		// Compute Y coord...
	unsigned char myVariance;

	// Get the height value at the middle of the Hypotenuse
	unsigned char centerZ  = m_HeightMap[((centerY / m_iMapScale * MAP_SIZE / m_iMapScale) + centerX / m_iMapScale)];

	// Variance of this triangle is the actual height at it's hypotenuse midpoint minus the interpolated height.
	// Use values passed on the stack instead of re-accessing the Height Field.
	myVariance = abs((int)centerZ - (((int)leftZ + (int)rightZ)>>1) * MULT_SCALE);

	// Since we're after speed and not perfect representations,
	//    only calculate variance down to an 8x8 block
	if ( (abs(leftX - rightX) >= 8) ||
		 (abs(leftY - rightY) >= 8) )
	{
		// Final Variance for this node is the max of it's own variance and that of it's children.
		myVariance = std::max( myVariance, RecursComputeVariance( apexX,   apexY,  apexZ, leftX, leftY, leftZ, centerX, centerY, centerZ,    node<<1 ) );
		myVariance = std::max( myVariance, RecursComputeVariance( rightX, rightY, rightZ, apexX, apexY, apexZ, centerX, centerY, centerZ, 1+(node<<1)) );
	}

	// Store the final variance for this node.  Note Variance is never zero.
	if (node < (1<<VARIANCE_DEPTH))
		m_CurrentVariance[node] = 1 + myVariance;

	return myVariance;
}

// ---------------------------------------------------------------------
// Initialize a patch.
//
void ROAMPatch::Init( int heightX, int heightY, int worldX, int worldY, unsigned char *hMap, ROAMLandscape *landscape, unsigned int mapScale )
{
	m_pLandscape = landscape;
	m_iMapScale = mapScale;

	NeighborPatches[0] = NULL;
	NeighborPatches[1] = NULL;
	NeighborPatches[2] = NULL;
	NeighborPatches[3] = NULL;

	// Clear all the relationships
	m_BaseLeft.RightNeighbor = m_BaseLeft.LeftNeighbor = m_BaseRight.RightNeighbor = m_BaseRight.LeftNeighbor =
	m_BaseLeft.LeftChild = m_BaseLeft.RightChild = m_BaseRight.LeftChild = m_BaseLeft.LeftChild = NULL;

	// Attach the two m_Base triangles together
	m_BaseLeft.BaseNeighbor = &m_BaseRight;
	m_BaseRight.BaseNeighbor = &m_BaseLeft;

	// Store Patch offsets for the world and heightmap.
	m_WorldX = worldX;
	m_WorldY = worldY;

	// Store pointer to first byte of the height data for this patch.
	m_HeightMap = &hMap[ (heightY / m_iMapScale * MAP_SIZE / m_iMapScale + heightX / m_iMapScale) ];

	// Initialize flags
	m_VarianceDirty = 1;
	m_isVisible = 0;

	// Create the Boundingbox
	m_pBoundingBox = new BoundingBox();

	unsigned int PatchSize = PATCH_SIZE + 1;

	// Create the vertexbuffer
	m_pVB = new VertexBufferFixed();
	m_pVertexData = (PatchVertex*)malloc( sizeof(PatchVertex) * PatchSize * PatchSize );

	// Setup the index buffer
	m_pIndexData = (unsigned short*)malloc( PATCH_SIZE * PATCH_SIZE * sizeof(unsigned short) * 6 );
	memset(m_pIndexData, 0, PATCH_SIZE * PATCH_SIZE * sizeof(unsigned short) * 6 );
	m_iNumIndices = 0;

	unsigned short Y = 0;
	unsigned short X = 0;

	for ( Y = 0; Y < PatchSize; ++Y )
	{
		for ( X = 0; X < PatchSize; ++X )
		{
			PatchVertex *v = &m_pVertexData[ (Y * PatchSize) + X ];
			v->x = X + m_WorldX;
			v->y = m_HeightMap[ (Y * MAP_SIZE) + X ];
			v->z = Y + m_WorldY;
		}
	}

	
	for ( Y = 0; Y < PATCH_SIZE; ++Y )
	{
		for ( X = 0; X < PATCH_SIZE; ++X )
		{
			m_pIndexData[m_iNumIndices++] = X + Y * (PatchSize);
			m_pIndexData[m_iNumIndices++] = X + (Y + 1) * (PatchSize);
			m_pIndexData[m_iNumIndices++] = X + 1 + Y * (PatchSize);

			m_pIndexData[m_iNumIndices++] = X + 1 + Y * (PatchSize);
			m_pIndexData[m_iNumIndices++] = X + (Y + 1) * (PatchSize);
			m_pIndexData[m_iNumIndices++] = X + 1 + (Y + 1) * (PatchSize);
		}
	}
	
	/*
	m_pIndexData[m_iNumIndices++] = 0 + 32 * (PatchSize);
	m_pIndexData[m_iNumIndices++] = 32 + 0 * (PatchSize);
	m_pIndexData[m_iNumIndices++] = 0 + 0 * (PatchSize);

	m_pIndexData[m_iNumIndices++] = 32 + 0 * (PatchSize);
	m_pIndexData[m_iNumIndices++] = 0 + 32 * (PatchSize);
	m_pIndexData[m_iNumIndices++] = 32 + 32 * (PatchSize);
	*/
	m_pVB->CreateVertexBuffer( VDF_XYZ, PatchSize * PatchSize, m_pVertexData, sizeof(PatchVertex), GL_STATIC_DRAW );
	m_pVB->CreateIndexBuffer( m_iNumIndices, m_pIndexData, GL_STREAM_DRAW );

	m_bHasSplit = false;
}

// ---------------------------------------------------------------------
// Reset the patch.
//
void ROAMPatch::Reset()
{
	// Assume patch is not visible.
	m_isVisible = 0;

	// Reset the important relationships
	m_BaseLeft.LeftChild = m_BaseLeft.RightChild = m_BaseRight.LeftChild = m_BaseLeft.LeftChild = NULL;

	// Attach the two m_Base triangles together
	m_BaseLeft.BaseNeighbor = &m_BaseRight;
	m_BaseRight.BaseNeighbor = &m_BaseLeft;

	// Clear the other relationships.
	m_BaseLeft.RightNeighbor = m_BaseLeft.LeftNeighbor = m_BaseRight.RightNeighbor = m_BaseRight.LeftNeighbor = NULL;
	
	m_bHasSplit = false;

	m_iNumIndices = 0;
	m_pIndexData[m_iNumIndices++] = 0 + PATCH_SIZE * (PATCH_SIZE+1);
	m_pIndexData[m_iNumIndices++] = PATCH_SIZE + 0 * (PATCH_SIZE+1);
	m_pIndexData[m_iNumIndices++] = 0 + 0 * (PATCH_SIZE+1);

	m_pIndexData[m_iNumIndices++] = PATCH_SIZE + 0 * (PATCH_SIZE+1);
	m_pIndexData[m_iNumIndices++] = 0 + PATCH_SIZE * (PATCH_SIZE+1);
	m_pIndexData[m_iNumIndices++] = PATCH_SIZE + PATCH_SIZE * (PATCH_SIZE+1);
	
	m_pVB->SetIndexBuffer( 0, m_iNumIndices, m_pIndexData );
}

// ---------------------------------------------------------------------
// Compute the variance tree for each of the Binary Triangles in this patch.
//
void ROAMPatch::ComputeVariance()
{
	// Compute variance on each of the base triangles...

	m_CurrentVariance = m_VarianceLeft;
	RecursComputeVariance(	0,          PATCH_SIZE, m_HeightMap[ (PATCH_SIZE  / m_iMapScale * MAP_SIZE  / m_iMapScale) ],
							PATCH_SIZE, 0,          m_HeightMap[ PATCH_SIZE / m_iMapScale ],
							0,          0,          m_HeightMap[ 0 ],
							1);

	m_CurrentVariance = m_VarianceRight;
	RecursComputeVariance(	PATCH_SIZE, 0,          m_HeightMap[ PATCH_SIZE / m_iMapScale ],
							0,          PATCH_SIZE, m_HeightMap[ (PATCH_SIZE / m_iMapScale * MAP_SIZE / m_iMapScale) ],
							PATCH_SIZE, PATCH_SIZE, m_HeightMap[ ((PATCH_SIZE / m_iMapScale * MAP_SIZE / m_iMapScale) + PATCH_SIZE / m_iMapScale)],
							1);

	// Clear the dirty flag for this patch
	m_VarianceDirty = 0;
}

// ---------------------------------------------------------------------
// Set patch's visibility flag.
//
void ROAMPatch::CheckVisibility()
{
	// Set the bounds of the boundingbox
	m_pBoundingBox->SetBounds( glm::vec3( m_WorldX, -60, m_WorldY ), glm::vec3( m_WorldX+PATCH_SIZE, 60, m_WorldY+PATCH_SIZE ) );
	//m_pBoundingBox->Multiply( m_pLandscape->GetCamera()->GetMatrix() );
	m_isVisible = m_pLandscape->GetCamera()->GetFrustum()->AABBInFrustum( m_pBoundingBox, glm::mat4x4() );
}

// ---------------------------------------------------------------------
// Create an approximate mesh.
//
void ROAMPatch::Tessellate()
{
	// Split each of the base triangles
	m_CurrentVariance = m_VarianceLeft;
	RecursTessellate (	&m_BaseLeft,
						m_WorldX,				m_WorldY+PATCH_SIZE,
						m_WorldX+PATCH_SIZE,	m_WorldY,
						m_WorldX,				m_WorldY,
						1 );
					
	m_CurrentVariance = m_VarianceRight;
	RecursTessellate(	&m_BaseRight,
						m_WorldX+PATCH_SIZE,	m_WorldY,
						m_WorldX,				m_WorldY+PATCH_SIZE,
						m_WorldX+PATCH_SIZE,	m_WorldY+PATCH_SIZE,
						1 );
}

// ---------------------------------------------------------------------
// Render the mesh.
//
void ROAMPatch::Render()
{
	//m_pBoundingBox->Render();

	// Store old matrix
	//glPushMatrix();
	
	if ( m_bHasSplit ) 
	{
		glColor3f(1.0,0.0,0.0);

		//memset(m_pIndexData, 0, PATCH_SIZE * PATCH_SIZE * sizeof(unsigned short) * 6 );
		m_iNumIndices = 0;

		// Build the new index buffer
		RecursRender ( &m_BaseLeft,
				0,				PATCH_SIZE,
				PATCH_SIZE,		0,
				0,				0); 
	
		RecursRender( &m_BaseRight,
				PATCH_SIZE,		0,
				0,				PATCH_SIZE,
				PATCH_SIZE,		PATCH_SIZE);
	

		// Upload the new indexbuffer
		m_pVB->SetIndexBuffer( 0, m_iNumIndices, m_pIndexData );
	}
	else 
	{
		glColor3f(0.0,1.0,0.0);
	}

	// Render
	m_pVB->Render( GL_TRIANGLES, (unsigned int)0, (unsigned int)0 );

	/*
	// Translate the patch to the proper world coordinates
	glTranslatef( (GLfloat)m_WorldX, 0, (GLfloat)m_WorldY );
	glBegin(GL_TRIANGLES);
		
		RecursRender (	&m_BaseLeft,
			0,				PATCH_SIZE,
			PATCH_SIZE,		0,
			0,				0);
		
		RecursRender(	&m_BaseRight,
			PATCH_SIZE,		0,
			0,				PATCH_SIZE,
			PATCH_SIZE,		PATCH_SIZE);
	
	glEnd();
	*/
	// Restore the matrix
	//glPopMatrix();
	
	
}

/*****************************
 ** 	ROAM Landscape		**
 *****************************/
int         ROAMLandscape::m_NextTriNode;
TriTreeNode ROAMLandscape::m_TriPool[POOL_SIZE];

ROAMLandscape::ROAMLandscape(void)
{
	m_iNumDesiredTriangles = 10000;
	m_fFrameTriangleVariance = 0;
	m_iMapScale = 1;
}

ROAMLandscape::~ROAMLandscape(void)
{
}

// ---------------------------------------------------------------------
// Allocate a TriTreeNode from the pool.
//
TriTreeNode *ROAMLandscape::AllocateTri()
{
	TriTreeNode *pTri;

	// IF we've run out of TriTreeNodes, just return NULL (this is handled gracefully)
	if ( m_NextTriNode >= POOL_SIZE ) {
	 	//printf("Triangle allication pool is empty");
		return NULL;
	}

	pTri = &(m_TriPool[m_NextTriNode++]);
	pTri->LeftChild = pTri->RightChild = NULL;

	return pTri;
}

// ---------------------------------------------------------------------
// Initialize all patches
//
void ROAMLandscape::Init(unsigned char *hMap, GLCamera *Camera, unsigned int mapScale )
{
	ROAMPatch *patch;
	int X, Y;

	// Store the camera
	m_pCamera = Camera;

	m_iMapScale = mapScale;

	// Store the Height Field array
	m_HeightMap = hMap;

	// Initialize all terrain patches
	for ( Y=0; Y < NUM_PATCHES_PER_SIDE; Y++)
		for ( X=0; X < NUM_PATCHES_PER_SIDE; X++ )
		{
			patch = &(m_Patches[Y][X]);
			patch->Init( X*PATCH_SIZE, Y*PATCH_SIZE, X*(PATCH_SIZE), (Y*(PATCH_SIZE)), hMap, this, m_iMapScale );
			patch->ComputeVariance();
		}
}

// ---------------------------------------------------------------------
// Reset all patches, recompute variance if needed
//
void ROAMLandscape::Reset()
{
	int X, Y;
	ROAMPatch *patch;

	// Set the next free triangle pointer back to the beginning
	SetNextTriNode(0);

	// Go through the patches performing resets, compute variances, and linking.
	for ( Y=0; Y < NUM_PATCHES_PER_SIDE; Y++ )
		for ( X=0; X < NUM_PATCHES_PER_SIDE; X++)
		{
			patch = &(m_Patches[Y][X]);
			
			// Reset the patch
			patch->Reset();
			patch->CheckVisibility();

			// Check to see if this patch has been deformed since last frame.
			// If so, recompute the varience tree for it.
			if ( patch->isDirty() )
				patch->ComputeVariance();

			if ( patch->isVisibile() )
			{
				// Link all the patches together.
				if ( X > 0 ) {
					patch->GetBaseLeft()->LeftNeighbor = m_Patches[Y][X-1].GetBaseRight();
					patch->NeighborPatches[0] = &m_Patches[Y][X-1];
				}
				else
					patch->GetBaseLeft()->LeftNeighbor = NULL;		// Link to bordering Landscape here..

				if ( X < (NUM_PATCHES_PER_SIDE-1) ) {
					patch->GetBaseRight()->LeftNeighbor = m_Patches[Y][X+1].GetBaseLeft();
					patch->NeighborPatches[1] = &m_Patches[Y][X+1];
				}
				else
					patch->GetBaseRight()->LeftNeighbor = NULL;		// Link to bordering Landscape here..

				if ( Y > 0 ) {
					patch->GetBaseLeft()->RightNeighbor = m_Patches[Y-1][X].GetBaseRight();
					patch->NeighborPatches[2] = &m_Patches[Y-1][X];
				}
				else
					patch->GetBaseLeft()->RightNeighbor = NULL;		// Link to bordering Landscape here..

				if ( Y < (NUM_PATCHES_PER_SIDE-1) ) {
					patch->GetBaseRight()->RightNeighbor = m_Patches[Y+1][X].GetBaseLeft();
					patch->NeighborPatches[3] = &m_Patches[Y+1][X];
				}
				else
					patch->GetBaseRight()->RightNeighbor = NULL;	// Link to bordering Landscape here..
			}
		}

}

// ---------------------------------------------------------------------
// Create an approximate mesh of the landscape.
//
void ROAMLandscape::Tessellate()
{
	// Perform Tessellation
	int nCount;
	ROAMPatch *patch = &(m_Patches[0][0]);
	for (nCount=0; nCount < NUM_PATCHES_PER_SIDE*NUM_PATCHES_PER_SIDE; nCount++, patch++ )
	{
		if (patch->isVisibile())
			patch->Tessellate( );
	}
}

// ---------------------------------------------------------------------
// Render each patch of the landscape & adjust the frame variance.
//
void ROAMLandscape::Render()
{
	int nCount;
	int nVisiblePatches = 0;
	ROAMPatch *patch = &(m_Patches[0][0]);

	// Scale the terrain by the terrain scale specified at compile time.
	glScalef( 1.0f, MULT_SCALE, 1.0f );

	for (nCount=0; nCount < NUM_PATCHES_PER_SIDE*NUM_PATCHES_PER_SIDE; nCount++, patch++ )
	{
		if (patch->isVisibile()) {
			++nVisiblePatches;
			patch->Render();
		}
	}

	char *stats = new char[128];
	sprintf_s( stats, 128, "Visible patches: %d", nVisiblePatches );
	((TextObject*)SceneManager::Inst()->GetSceneObject( "statsText" ))->SetText( stats );
	

	// Check to see if we got close to the desired number of triangles.
	// Adjust the frame variance to a better value.
	if ( GetNextTriNode() != m_iNumDesiredTriangles )
		m_fFrameTriangleVariance += ((float)GetNextTriNode() - (float)m_iNumDesiredTriangles) / (float)m_iNumDesiredTriangles;

	// Bounds checking.
	if ( m_fFrameTriangleVariance < 0 )
		m_fFrameTriangleVariance = 0;
}
