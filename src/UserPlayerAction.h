#pragma once

enum PlayerActionState
{
	STATE_NONE,
	STATE_IDLE,
	STATE_WALK_FORWARD,
	STATE_RUN_FORWARD,
	STATE_WALK_BACKWARD,
	STATE_RUN_BACKWARD,
	STATE_JUMP
};

class UserPlayerAction
{
public:
	UserPlayerAction( PlayerActionState State );
	~UserPlayerAction(void);

	PlayerActionState GetState();
	
	bool IsEndless();

	std::string GetAnimationName();

private:

	bool m_IsEndless;
	bool m_IsCurrent;
	PlayerActionState m_State;

};

