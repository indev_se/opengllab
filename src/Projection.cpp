#include "StdAfx.h"
#include "Projection.h"


Projection::Projection(void)
{
}


Projection::~Projection(void)
{
}

void Projection::SetPerspective( float Fov, float AspectRatio, float zNear, float zFar )
{
	m_Fov = Fov;
	m_AspectRatio = AspectRatio;
	m_zNear = zNear;
	m_zFar = zFar;

	m_matProjection = glm::perspective( m_Fov, m_AspectRatio, m_zNear, m_zFar );
}

void Projection::SetNearFar( float zNear, float zFar )
{
	this->SetPerspective( m_Fov, m_AspectRatio, zNear, zFar );
}

void Projection::SetZNear( float zNear )
{
	this->SetPerspective( m_Fov, m_AspectRatio, zNear, m_zFar );
}

void Projection::SetZFar( float zFar )
{
	this->SetPerspective( m_Fov, m_AspectRatio, m_zNear, zFar );
}

void Projection::SetFov( float Fov )
{
	this->SetPerspective( Fov, m_AspectRatio, m_zNear, m_zFar );
}

void Projection::SetAspectRatio( float AspectRatio )
{
	this->SetPerspective( m_Fov, AspectRatio, m_zNear, m_zFar );
}

glm::mat4x4 Projection::GetMatrix()
{
	return m_matProjection;
}

Projection *Projection::Clone()
{
	Projection *clone = new Projection();
	clone->SetPerspective( m_Fov, m_AspectRatio, m_zNear, m_zFar );
	return clone;
}