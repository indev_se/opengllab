#include "StdAfx.h"
#include "AssetsManager.h"
#include "BitmapDataAsset.h"
#include "StrategyGLScene.h"
#include "LandscapeObject.h"

LandscapeObject::LandscapeObject(const char *objectName) : SceneObject(objectName)
{
}

LandscapeObject::~LandscapeObject(void)
{
}

void LandscapeObject::Init( GLScene *scene )
{
	SceneObject::Init( scene );

	AssetsManager *assetsManager = ((StrategyGLScene*)scene)->GetAssetsManager();

	BitmapDataAsset *bmpdata = assetsManager->GetBitmapDataAsset("heightmap_05");
	bmpdata->ConvertTo8Bits();
	
	int size = bmpdata->Width() * bmpdata->Height() * sizeof(unsigned char);
	m_HeightmapData = (unsigned char *)malloc(size);
	memcpy( m_HeightmapData, bmpdata->Bits(), size );

	m_pGeoMipMapTerrain = new GeoMipMapTerrain();
	m_pGeoMipMapTerrain->Init( m_HeightmapData, bmpdata->Width(), 65, 128, 90, 15);
	m_pGeoMipMapTerrain->SetCamera( ((StrategyGLScene*)scene)->GetCamera() );
	
	TextureInfo *info = new TextureInfo();
	info->genMipMap = true;

	TextureInfoParam infoParam1 = { GL_TEXTURE_WRAP_S, GL_REPEAT };
	info->params.push_back( &infoParam1 );
	TextureInfoParam infoParam2 = { GL_TEXTURE_WRAP_T, GL_REPEAT };
	info->params.push_back( &infoParam2 );
	TextureInfoParam infoParam3 = { GL_TEXTURE_MAG_FILTER, GL_LINEAR };
	info->params.push_back( &infoParam3 );
	TextureInfoParam infoParam4	= { GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR };
	info->params.push_back( &infoParam4 );

	m_GrounLeaf01Texture = assetsManager->CreateTextureAsset("GrounLeaf_01_Texture");
	m_GrounLeaf01Texture->CreateFromBitmapData( assetsManager->GetBitmapDataAsset("ground_leaf_01"), info );

	m_MaskTexture = assetsManager->CreateTextureAsset("Terrain_MaskTexture");
	info->format = GL_RGBA;
	m_MaskTexture->CreateFromBitmapData( assetsManager->GetBitmapDataAsset("terrain_mask02"), info );

	m_TerrainNormalMap = assetsManager->CreateTextureAsset("Terrain_NormalMap");
	m_TerrainNormalMap->CreateFromBitmapData( assetsManager->GetBitmapDataAsset("terrain_normal05"), info );
	
	info->format = GL_RGB;
	m_Ground01Texture = assetsManager->CreateTextureAsset("Terrain_Ground01Texture");
	m_Ground01Texture->CreateFromBitmapData( assetsManager->GetBitmapDataAsset("terrain_ground01"), info );

	m_Ground02Texture = assetsManager->CreateTextureAsset("Terrain_Ground02Texture");
	m_Ground02Texture->CreateFromBitmapData( assetsManager->GetBitmapDataAsset("terrain_ground02"), info );

	m_Ground03Texture = assetsManager->CreateTextureAsset("Terrain_Ground03Texture");
	m_Ground03Texture->CreateFromBitmapData( assetsManager->GetBitmapDataAsset("terrain_ground03"), info );

	m_Ground04Texture = assetsManager->CreateTextureAsset("Terrain_Ground04Texture");
	m_Ground04Texture->CreateFromBitmapData( assetsManager->GetBitmapDataAsset("terrain_ground04"), info );

	
	m_PointLight = new PointLight(1000, "PointLight_01");
	m_PointLight->SetPosition( glm::vec3(100,100,100) );

	m_LandscapeShader = assetsManager->CreateShaderProgram("landscape_shader");
	m_LandscapeShader->CreateProgram( "Data/Shaders/Terrain.vert", "Data/Shaders/Terrain.frag" );

	scene->AssertGLError();
}

void LandscapeObject::Update( float tickMS )
{
	m_pGeoMipMapTerrain->Update(tickMS);
}

void LandscapeObject::Render(RenderPass *pass)
{
	glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	
	GLCamera *cam = ((StrategyGLScene*)m_pScene)->GetCamera();
	glm::vec3 eye;
	cam->GetEye(eye);
	//m_PointLight->SetPosition( Vector3f(-eye.x,30000,-eye.z) );
	
	//m_PointLight->BindLight( GL_LIGHT0 );
	
	glEnable(GL_BLEND);
	
	glEnable(GL_TEXTURE_2D);
	
	glUseProgramObjectARB(m_LandscapeShader->Program());
	m_LandscapeShader->SendUniform1i("maskMap", 0);
	m_LandscapeShader->SendUniform1i("tex01Map", 1);
	m_LandscapeShader->SendUniform1i("tex03Map", 3);
	m_LandscapeShader->SendUniform1i("tex04Map", 4);
	
	//m_LandscapeShader->SendUniform1i("tex02Map", 2);
	
	m_LandscapeShader->SendUniform1i("normalMap", 5);

	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_MaskTexture->TextureID());
	
	glActiveTextureARB(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_Ground01Texture->TextureID());
	
	glActiveTextureARB(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_Ground02Texture->TextureID());

	glActiveTextureARB(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_Ground03Texture->TextureID());

	glActiveTextureARB(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_Ground04Texture->TextureID());
	
	glActiveTextureARB(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, m_TerrainNormalMap->TextureID());

	glUseProgramObjectARB(m_LandscapeShader->Program());
	glPushMatrix();
	m_pGeoMipMapTerrain->Render();
	glPopMatrix();
	glUseProgramObjectARB(0);
	
	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTextureARB(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTextureARB(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTextureARB(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTextureARB(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTextureARB(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTextureARB(GL_TEXTURE0);
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glUseProgramObjectARB(0);
	
	//m_PointLight->UnbindLight();
	
	/*
	glPushMatrix();
	m_pGeoMipMapTerrain->RenderDebugNormals();
	glPopMatrix();
	*/

	/*
	glPushMatrix();
	m_pGeoMipMapTerrain->RenderDebugBB();
	glPopMatrix();
	*/

	/*
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPushMatrix();
	m_pGeoMipMapTerrain->Render();
	glPopMatrix();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	*/
}
	