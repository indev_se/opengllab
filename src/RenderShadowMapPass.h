#pragma once
#include "RenderPass.h"
#include "LightObject.h"
#include "ShadowMapping.h"

class RenderShadowMapPass :
	public RenderPass
{
public:
	RenderShadowMapPass(void);
	~RenderShadowMapPass(void);

	void Init( LightObject *light, GLScene *scene );
	void Bind();
	void Unbind();

	void BindObject( SceneObject *object );
	void UnbindObject( SceneObject *object );

	void BindJob( RenderJob *job ) {}
	void UnbindJob( RenderJob *job ) {}

	ShadowMapping *GetShadowMap() { return m_pShadowMap; }

protected:

	ShadowMapping *m_pShadowMap;
};

