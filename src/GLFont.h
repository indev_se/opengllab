#pragma once
#include "ft2build.h"
#include "freetype/freetype.h"
#include "freetype/ftglyph.h"
#include "freetype/ftoutln.h"
#include "freetype/fttrigon.h"

class GLFont
{
public:
	GLFont(void);
	~GLFont(void);

	void Init(const char *fontName, unsigned int fontSize);

	inline GLuint GetGlyphList() { return m_iGlyphLists; }
	inline unsigned int GetFontSize() { return m_iFontSize; }

protected:

	unsigned int m_iFontSize;

	GLuint *m_pGlyphTextures;
    GLuint m_iGlyphLists;

private:

	// This Function Gets The First Power Of 2 >= Int.
	inline int next_p2 (int a )
	{
		int rval=1;
		while(rval<a) rval<<=1;
		return rval;
	}

	void MakeGlyphLists( FT_Face face, const wchar_t * charcodes );
};

