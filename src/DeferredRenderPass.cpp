#include "StdAfx.h"
#include "StrategyGLScene.h"
#include "Material.h"
#include "DeferredRenderPass.h"


DeferredRenderPass::DeferredRenderPass(void)
{
	m_pFBOs = NULL;
}


DeferredRenderPass::~DeferredRenderPass(void)
{
	if ( m_pFBOs != NULL )
		delete m_pFBOs;

	m_pFBOs = NULL;
}

void DeferredRenderPass::Init( GLScene *scene )
{
	RenderPass::Init(scene);

	m_pFBOs = new DeferredFramebuffers();
	m_pFBOs->Create2D(scene->Width(), scene->Height());
}

void DeferredRenderPass::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_pFBOs->GetFBOID() );

	GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };

	glDrawBuffers(3, buffers);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glDisable(GL_LIGHTING);
}

void DeferredRenderPass::Unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0 );
}
	
void DeferredRenderPass::BindObject( SceneObject *object )
{
	// Bind the shader
	Material *mtrl = object->GetMaterial();
	glEnable(GL_TEXTURE_2D);

	if ( mtrl->GetShaderDeferred() ) {
		mtrl->BindDeferred();

		mtrl->GetBoundShader()->SendUniformMatrix4fv("matModel", 1, false, (float*)&object->GetModelMatrix());
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matView", 1, false, (float*)&m_pScene->GetCamera()->GetViewMatrix());
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matPersp", 1, false, (float*)&m_pScene->GetProjectionMatrix());

		mtrl->GetBoundShader()->SendUniform1i("IsShadowDepthPass", 0);
	}
}

void DeferredRenderPass::UnbindObject( SceneObject *object )
{
	Material *mtrl = object->GetMaterial();

	if ( mtrl->GetShaderDeferred() ) {
		mtrl->UnbindDeferred();

		glActiveTextureARB(GL_TEXTURE0);
	}
}

void DeferredRenderPass::BindJob( RenderJob *job )
{
	// Bind the shader
	Material *mtrl = job->GetMaterial();
	
	if ( mtrl->GetShaderDeferred() ) {
		mtrl->BindDeferred();

		mtrl->GetBoundShader()->SendUniformMatrix4fv("matModel", 1, false, (float*)&job->GetMatrix());
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matView", 1, false, (float*)&m_pScene->GetCamera()->GetViewMatrix());
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matPersp", 1, false, (float*)&m_pScene->GetProjectionMatrix());

		mtrl->GetBoundShader()->SendUniform1i("IsShadowDepthPass", 0);
	}
}

void DeferredRenderPass::UnbindJob( RenderJob *job )
{
	Material *mtrl = job->GetMaterial();

	if ( mtrl->GetShaderDeferred() ) {
		mtrl->UnbindDeferred();

		glActiveTextureARB(GL_TEXTURE0);
	}
}

uint32 DeferredRenderPass::GetColorTextureId()
{
	return m_pFBOs->GetColorTexture()->TextureID();
}

uint32 DeferredRenderPass::GetNormalTextureId()
{
	return m_pFBOs->GetNormalTexture()->TextureID();
}

uint32 DeferredRenderPass::GetDepthTextureId()
{
	return m_pFBOs->GetDepthTexture()->TextureID();
}

uint32 DeferredRenderPass::GetPositionTextureId()
{
	return m_pFBOs->GetPositionTexture()->TextureID();
}