#include "StdAfx.h"
#include "PointLight.h"
#include "DeferredRenderLightPass.h"


DeferredRenderLightPass::DeferredRenderLightPass(void)
{
	m_pPointLightVolume = nullptr;
	m_bBindTextures = false;
	m_pFBO = nullptr;
}


DeferredRenderLightPass::~DeferredRenderLightPass(void)
{
	if( m_pPointLightVolume != nullptr ) {
		delete m_pPointLightVolume;
		m_pPointLightVolume = nullptr;
	}

	if ( m_pFBO != nullptr ) {
		delete m_pFBO;
		m_pFBO = nullptr;
	}
}

void DeferredRenderLightPass::Init( GLScene *scene, DeferredFramebuffers *deferredFBO )
{
	RenderPass::Init(scene);

	m_pDeferredFBO = deferredFBO;

	m_pFBO = new ColorFramebufferObject();
	m_pFBO->Create2D( scene->Width(), scene->Height(), GL_COLOR_ATTACHMENT0 );

	// Bind a Depth buffer to the color FBO
	glBindFramebuffer(GL_FRAMEBUFFER, m_pFBO->GetFBOID());
    
    glGenRenderbuffers(1, &m_iDepthFBOID);
	glBindRenderbuffer(GL_RENDERBUFFER, m_iDepthFBOID);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, scene->Width(), scene->Height());
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_iDepthFBOID);

	// Make sure we're ok
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if ( status != GL_FRAMEBUFFER_COMPLETE ) {
		StdConsole::LogError( _STR"GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO");
	}

	// unbind the framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	AssetsManager *assets = scene->GetAssetsManager();
	
	// Volume for point light is a sphere
	std::string volumeObjectName("DeferredPointLightRenderVolume");
	m_pPointLightVolume = new SphereObject( 1, 10, 10, volumeObjectName.c_str() );
	m_pPointLightVolume->Init( scene, assets->GetMaterial("PointLightVolumeRender") );
}

void DeferredRenderLightPass::Bind()
{
	glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
	glCullFace(GL_FRONT);

	glBindFramebuffer(GL_FRAMEBUFFER, m_pFBO->GetFBOID());
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	glDepthMask(GL_FALSE);

}

void DeferredRenderLightPass::Unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glCullFace(GL_BACK);
    glDisable (GL_BLEND);

	glDepthMask(GL_TRUE);
}
	
void DeferredRenderLightPass::BindObject( SceneObject *object )
{
	glm::mat4x4 volumeTransform, viewMatrix, perspMatrix;
	AssetsManager *assets = m_pScene->GetAssetsManager();
	LightObject *light = (LightObject*)object;
	Material *mtrl = nullptr;
	glm::mat4x4 matScale;
	
	viewMatrix = m_pScene->GetCamera()->GetViewMatrix();
	perspMatrix = m_pScene->GetProjectionMatrix();

	switch( light->GetType() )
	{
	case LIGHT_POINT:
		mtrl = assets->GetMaterial("PointLightVolumeRender");
		matScale[0][0] = ((PointLight*)light)->LightSize();
		matScale[1][1] = ((PointLight*)light)->LightSize();
		matScale[2][2] = ((PointLight*)light)->LightSize();
		//volumeTransform = volumeTransform * ((PointLight*)light)->LightSize();
		//volumeTransform = light->GetModelMatrix() + volumeTransform;
		volumeTransform = light->GetModelMatrix() * matScale;
		
		break;

	case LIGHT_SPOT:
	case LIGHT_DIRECTIONAL:
	default:
		break;
	}

	if ( mtrl != nullptr )
	{
		glEnable(GL_TEXTURE_2D);

		mtrl->BindDeferred(this);

		mtrl->GetBoundShader()->SendUniform3f("lightColor", (float*)&light->GetDiffuse());
		mtrl->GetBoundShader()->SendUniform3f("lightPosition", (float*)&light->GetPosition());
		mtrl->GetBoundShader()->SendUniform1f("lightSize", ((PointLight*)light)->LightSize());
		
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matModel", 1, false, glm::value_ptr(volumeTransform) );
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matView", 1, false, glm::value_ptr(viewMatrix) );
		mtrl->GetBoundShader()->SendUniformMatrix4fv("matPersp", 1, false, glm::value_ptr(perspMatrix) );

		mtrl->GetBoundShader()->SendUniform1i("InNormalTexture", 0);
		mtrl->GetBoundShader()->SendUniform1i("InPositionTexture", 1);
		
		glActiveTextureARB(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_pDeferredFBO->GetNormalTexture()->TextureID());

		glActiveTextureARB(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_pDeferredFBO->GetPositionTexture()->TextureID());
	
	}
	m_pPointLightVolume->Render(nullptr);
}

void DeferredRenderLightPass::UnbindObject( SceneObject *object )
{
	AssetsManager *assets = m_pScene->GetAssetsManager();
	LightObject *light = (LightObject*)object;
	Material *mtrl = nullptr;

	switch( light->GetType() )
	{
	case LIGHT_POINT:
		mtrl = assets->GetMaterial("PointLightVolumeRender");
		break;

	case LIGHT_SPOT:
	case LIGHT_DIRECTIONAL:
	default:
		break;
	}

	if ( mtrl != nullptr )
	{
		mtrl->UnbindDeferred(this);
		
		glActiveTextureARB(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);

		glActiveTextureARB(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, 0);

		glDisable(GL_TEXTURE_2D);
	}

}