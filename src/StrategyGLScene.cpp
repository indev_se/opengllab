#include "StdAfx.h"
#include "InputManager.h"
#include "SceneManager.h"
#include "GLScene.h"
#include "SpotLight.h"
#include "RenderPass.h"
#include "MeasureObject.h"
#include "SceneObjectFactory.h"
#include "StrategyGLScene.h"

static bool OneTimeShadowVolume = false;

StrategyGLScene::StrategyGLScene(void)
{
	m_pOverlay = new StrategyGLOverlay();
	m_pDefaultFPSCamera = nullptr;
}


StrategyGLScene::~StrategyGLScene(void)
{
}

void StrategyGLScene::SetViewport( int Width, int Height )
{
	m_pOverlay->SetOrtho( Width, Height );

	GLScene::SetViewport( Width, Height );
	
	if (m_pDefaultFPSCamera != nullptr)
		m_pDefaultFPSCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );
}

void StrategyGLScene::Init()
{
	GLScene::Init();
	
	m_pOverlay->Init();

	// Setup the assetsmanager
	m_AssetsManager = new AssetsManager();

	// Create the camera
	m_pViewportCamera = m_pDefaultFPSCamera = new GLCamera();
	m_pViewportCamera->Init();

	if ( m_iWidth != 0 && m_iHeight != 0)
		m_pViewportCamera->SetPerspective( 45.0f, (float)m_iWidth/m_iHeight, 1.0f, 4000.0f );
	else
		m_pViewportCamera->SetPerspective( 45.0f, 4.f/3.f, 1.0f, 4000.0f );

//	m_pCamera->RotateEyeY( -135 );
	m_pViewportCamera->MoveEyeZ( -20 );
//	m_pCamera->MoveEyeY( -250 );

	// Setup the camera input
	m_pCameraInput = new CameraInput();
	m_pCameraInput->SetCamera( m_pViewportCamera );

	// Read all materials
	m_MaterialImporter = new MaterialImporter();
	m_MaterialImporter->ImportAssetsFromFile("Data/Materials/test.mtrl", m_AssetsManager);
	m_MaterialImporter->ImportAssetsFromFile("Data/Materials/farmhouse02.mtrl", m_AssetsManager);
	m_MaterialImporter->ImportAssetsFromFile("Data/Materials/builtin.mtrl", m_AssetsManager);

	// Create the scene objects
	m_RootObject = new SceneObject("root");

	MeasureObject *measure = new MeasureObject( "MeasurementFloor");
	measure->Init(this, 50.f );
	m_RootObject->AddChild(measure);

	CubeObject *normalCube = new CubeObject( 1, 1, 1, "normalizedCube");
	normalCube->Init(this, m_AssetsManager->GetMaterial("ColorMaterial") );

	// Create some cubes
	CubeObject *cube = new CubeObject( 10, 10, 10, "cube01" );
	cube->SetColor( glm::vec3(1.0,1.0,1.0) );
	cube->Init(this, m_AssetsManager->GetMaterial("ColorMaterial"));
	cube->SetPosition( glm::vec3(0,20,-30) );
	//m_RootObject->AddChild(cube);

	cube = new CubeObject( 10, 10, 10, "cube02" );
	cube->SetColor( glm::vec3(1.0,1.0,1.0) );
	cube->Init(this, m_AssetsManager->GetMaterial("ColorMaterial"));
	cube->SetPosition( glm::vec3(15,20,25) );
	//m_RootObject->AddChild(cube);

	cube = new CubeObject( 10, 10, 10, "cube03" );
	cube->SetColor( glm::vec3(1.0,1.0,1.0) );
	cube->Init(this, m_AssetsManager->GetMaterial("ColorMaterial"));
	cube->SetPosition( glm::vec3(20,40,20) );
	//m_RootObject->AddChild(cube);

	SphereObject *sphere = new SphereObject( 5, 20, 20, "sphere01" );
	sphere->SetColor( glm::vec3(1.0,1.0,1.0));
	sphere->Init(this, m_AssetsManager->GetMaterial("ColorMaterial"));
	sphere->SetPosition(glm::vec3(20.0,15.0,0.0));
	//m_RootObject->AddChild(sphere);

	// Create a fake floor
	CubeObject *floor = new CubeObject(1000.0, 2, 1000.0 ,"floor01");
	floor->SetColor( glm::vec3(1.0,1.0,1.0) );
	floor->Init(this, m_AssetsManager->GetMaterial("ColorMaterial"));
	floor->SetPosition( glm::vec3(0,-5,0) );
	//m_RootObject->AddChild(floor);

	// Create the light
	m_RootLightObject = new SceneObject("rootlight");

	// shadow casting light
	std::vector<LightObject *> globalLights;
	/*
	SpotLight *spot = new SpotLight("spot01");
	spot->SetPosition( glm::vec3(1,150,1) );
	glm::vec3 dir( glm::vec3(0,0,0) - glm::vec3(1,150,1) );
	dir = glm::normalize(dir);
	spot->SetDirection( dir );
	globalLights.push_back(spot);
	*/
	DirectionalLight *sun = new DirectionalLight("sun01");
	glm::vec3 dir( glm::vec3(0,0,0) - glm::vec3(0.f,50.f,150.f) );
	sun->SetDirection(glm::normalize(dir));
	globalLights.push_back(sun);
	
	// point lights
	/*
	PointLight *light = new PointLight( 60, "pointLight01");
	light->SetPosition( glm::vec3(10, 10, 10) );
	light->SetDiffuse( glm::vec3(0.0,1.0,0.0) );
	m_RootLightObject->AddChild(light);

	light = new PointLight( 20, "pointLight02");
	light->SetPosition( glm::vec3(10,10,0) );
	light->SetDiffuse( glm::vec3(1.0,0.0,0.0) );
	m_RootLightObject->AddChild(light);

	light = new PointLight( 60, "pointLight03");
	light->SetPosition( glm::vec3(15, 35, 15) );
	light->SetDiffuse( glm::vec3(0.0,0.0,1.0) );
	m_RootLightObject->AddChild(light);
	*/
	PointLight *light;
	/*
	for ( uint32 x = 0; x < 4; ++x )
	{
		for ( uint32 y = 0; y < 4; ++y )
		{
			light = new PointLight( 20, "pointLight");
			light->SetPosition( glm::vec3( -45.0f + ( (float)x * 30.0f ), 10, -45.0f + ( (float)y * 30.0f )) );
			light->SetDiffuse( glm::vec3(1.0,0.0,0.0) );
			m_RootLightObject->AddChild(light);
		}
	}
	*/

	// Light for the archer
	light = new PointLight( 20, "archerPointLight");
	light->SetPosition( glm::vec3(-10.0,6,2) );
	light->SetDiffuse( glm::vec3(1.0,1.0,1.0) );
	m_RootLightObject->AddChild(light);

	// Light for the farmhouse
	light = new PointLight( 200, "farmhouseointLight");
	light->SetPosition( glm::vec3(5.0f,369.0f,315.0f) );
	light->SetDiffuse( glm::vec3(1.0,1.0,0.0) );
	m_RootLightObject->AddChild(light);

	// Create a render pass for the spotlight
	passRenderShadowMap = new RenderShadowMapPass();
	passRenderShadowMap->Init(sun, this);

	// Create a render pass for the cascading shadow maps
	passRenderCascadedShadowMap = new RenderCascadedShadowMapPass();
	passRenderCascadedShadowMap->Init(sun, this, 4);

	// Create pass that will apply shadow to the scene
	passApplyShadowMap = new ApplyShadowMapPass();
	passApplyShadowMap->Init(passRenderShadowMap->GetShadowMap(), this);

	// Create thje deferred rendering pass
	passDeferred = new DeferredRenderPass();
	passDeferred->Init(this);
	
	
	// Create the pass that will render the shadow to a deferred fbo
	passDeferredRenderShadow = new DeferredRenderShadowPass();
	passDeferredRenderShadow->Init(this, passDeferred->GetFramebuffer(), nullptr, passRenderCascadedShadowMap->GetShadowMap() );
	//passDeferredRenderShadow->Init(this, passDeferred->GetFramebuffer(), passRenderShadowMap->GetShadowMap(), nullptr );


	passDeferredLight = new DeferredRenderLightPass();
	passDeferredLight->Init(this, passDeferred->GetFramebuffer());

	passDeferredFinal = new DeferredRenderFinal();
	passDeferredFinal->Init(this, passDeferred->GetFramebuffer(), passDeferredRenderShadow->GetFramebuffer(), passDeferredLight->GetFBO(), globalLights );
	
	// Create the forward rendering pass
	passForward = new ForwardRenderPass();
	passForward->Init(this);

	passGaussianBlur = new GaussianBlurPass();
	passGaussianBlur->Init(this);

	// Load the dragon model
	StaticMeshObject *dragon =SceneObjectFactory::Inst()->CreateStaticMeshObject("dragon", "Data/Models/Characters/dragon_nrm.fbx", this);
	dragon->Init( this );
	dragon->SetPosition( glm::vec3(18,365,593) );
	m_RootObject->AddChild(dragon);
	
	
	// Load the the sphere model
	//StaticMeshObject *FbxNormalTest = SceneObjectFactory::Inst()->CreateStaticMeshObject("FbxNormalTest", "Data/Models/Characters/Normals.fbx", this);
	//FbxNormalTest->SetPosition( glm::vec3(0,4,-4) );
	//m_RootObject->AddChild(FbxNormalTest);

	
	// Load all textures
	/*
	m_AssetsManager->CreateBitmapDataAsset( "ground_leaf_01", "Data/Textures/ground_leaf_01.png" );
	m_AssetsManager->CreateBitmapDataAsset( "heightmap_01", "Data/Heightmaps/151009heightmapsr_bed.png" );
	m_AssetsManager->CreateBitmapDataAsset( "heightmap_02", "Data/Heightmaps/England_Wales_1024.png" );
	m_AssetsManager->CreateBitmapDataAsset( "heightmap_03", "Data/Heightmaps/pyramid.png" );
	m_AssetsManager->CreateBitmapDataAsset( "heightmap_04", "Data/Heightmaps/hill.png" );
	m_AssetsManager->CreateBitmapDataAsset( "heightmap_05", "Data/Heightmaps/mountains512.png" );
	m_AssetsManager->CreateBitmapDataAsset( "heightmap_06", "Data/Heightmaps/test.png" );
	m_AssetsManager->CreateBitmapDataAsset( "heightmap_07", "Data/Heightmaps/Heightmap.png" );
	
	m_AssetsManager->CreateBitmapDataAsset( "terrain_mask01", "Data/Textures/terrain_mask.png" );
	m_AssetsManager->CreateBitmapDataAsset( "terrain_mask02", "Data/Textures/alphamap.png" );
	m_AssetsManager->CreateBitmapDataAsset( "terrain_mask03", "Data/Textures/red_mask.png" );
	m_AssetsManager->CreateBitmapDataAsset( "terrain_ground01", "Data/Textures/grass-texture.jpg" );
	m_AssetsManager->CreateBitmapDataAsset( "terrain_ground04", "Data/Textures/Pavement.png" );
	m_AssetsManager->CreateBitmapDataAsset( "terrain_ground02", "Data/Textures/Grass1.png" );
	m_AssetsManager->CreateBitmapDataAsset( "terrain_ground03", "Data/Textures/Rock9.png" );
	m_AssetsManager->CreateBitmapDataAsset( "terrain_normal05", "Data/Heightmaps/mountains512_normal.png" );
	*/
	/*
	// Setup the landscape
	m_pLandscape = new LandscapeObject("landscape");
	m_pLandscape->Init(this);
	*/
	
	// Load a tree
	/*
	StaticMeshObject *tree01 = SceneObjectFactory::Inst()->CreateStaticMeshObject("tree01", "Data/Models/Trees/trees_0.fbx", this);
	tree01->SetPosition( glm::vec3(10,0,-5) );
	m_RootObject->AddChild(tree01);
	
	
	AnimatedMeshObject *archer = SceneObjectFactory::Inst()->CreateAnimatedMeshObject("archer", "Data/Models/Characters/archer_version_3.fbx", this);
	archer->SetPosition( glm::vec3(-10.0,8,-4) );
	m_RootObject->AddChild(archer);
	*/
	StaticMeshObject *farmhouse = SceneObjectFactory::Inst()->CreateStaticMeshObject("farmhouse", "Data/Models/Buildings/farmhouse_maya.fbx", this);
	farmhouse->SetPosition( glm::vec3(0.0f,1 * METER,0.0f) );
	m_RootObject->AddChild(farmhouse);

	AnimatedMeshObject *runner = SceneObjectFactory::Inst()->CreateAnimatedMeshObject("runner", "Data/Models/Characters/humanoid.fbx", this);
	runner->SetPosition( glm::vec3(0.0,3.2 * METER,400.0) );
	runner->SetRotationY( -90.0f );
	runner->SetAnimationByName("run", true);
	runner->GetAnimationState()->Play();
	m_RootObject->AddChild(runner);
}

void StrategyGLScene::GetBounds( glm::vec3 &min, glm::vec3 &max )
{
	m_RootObject->GetBounds(min, max);
}

static float spotAngle = 0;
void StrategyGLScene::Update( float tickMS )
{
	if ( InputManager::Inst()->GetKey( KEY_O ) ) return;

	m_pCameraInput->Update( tickMS );

	//m_pLandscape->Update( tickMS );

	m_pOverlay->Update( tickMS );

	m_RootObject->Update( tickMS );

	//SceneManager::Inst()->GetSceneObject("archer")->Update(tickMS);
	//SceneManager::Inst()->GetSceneObject("cube02")->SetRotationX( sinf(tickMS * 20) );
	SceneManager::Inst()->GetSceneObject("cube02")->SetRotationZ( sinf(tickMS * 20) );

	((AnimatedMeshObject*)SceneManager::Inst()->GetSceneObject("runner"))->GetAnimationState()->Update(tickMS);

	spotAngle += tickMS;
	/*
	glm::vec3 lookat(40.0,0.0,40.0);
	lookat.x = lookat.x * sinf(spotAngle);
	lookat.z = lookat.z * cosf(spotAngle);

	glm::vec3 dir( lookat - glm::vec3(10,120,30) );
	dir = glm::normalize(dir);
	*/
	glm::vec3 dir( glm::vec3(0,0,0) - glm::vec3(-40.f * sinf(spotAngle) ,50.f,150.f) );
	
	((DirectionalLight*)SceneManager::Inst()->GetSceneObject("sun01"))->SetDirection( glm::normalize(dir) );

//((SpotLight*)SceneManager::Inst()->GetSceneObject("spot01"))->SetDirection( dir );
}

void StrategyGLScene::Render()
{
	Clear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	/*
	passRenderShadowMap->Bind();
	m_RootObject->Render( passRenderShadowMap );
	passRenderShadowMap->Unbind();
	
	passApplyShadowMap->Bind();
	m_RootObject->Render( passApplyShadowMap );
	passApplyShadowMap->Unbind();
	*/
	
	//m_RootObject->Render( nullptr );
	
	passDeferred->Bind();
	m_RootObject->Render( passDeferred );
	passDeferred->Unbind();
	
	for ( uint32 i = 0; i < passRenderCascadedShadowMap->GetNumSplits(); ++i )
	{
		passRenderCascadedShadowMap->SetSplitIndex(i);
		passRenderCascadedShadowMap->Bind();

		m_RootObject->Render( passRenderCascadedShadowMap );

		passRenderCascadedShadowMap->Unbind();
	}
	
	passDeferredLight->Bind();
	m_RootLightObject->Render( passDeferredLight );
	passDeferredLight->Unbind();
	
	/*
	if ( !OneTimeShadowVolume ) {
		passRenderCascadedShadowMap->GetShadowMap()->CreateDebugVolumes();
		OneTimeShadowVolume = true;
	}
	*/
	
	passDeferredRenderShadow->Bind();
	passDeferredRenderShadow->RenderScreenQuad();
	passDeferredRenderShadow->Unbind();
	
	//passGaussianBlur->ApplyBlur( passDeferredRenderShadow->GetFramebuffer()->GetTexture() );

	passDeferredFinal->Bind();
	passDeferredFinal->RenderScreenQuad();
	passDeferredFinal->Unbind();
	

	glUseProgramObjectARB(0);
	/*
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,m_iWidth,0,m_iHeight,1,20);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor4f(1,1,1,1);
	
	glActiveTextureARB(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D,passDeferredLight->GetFBO()->GetTextureId());
	//glBindTexture(GL_TEXTURE_2D,passDeferred->GetDepthTextureId());
	//glBindTexture(GL_TEXTURE_2D,passDeferred->GetNormalTextureId());
	
	glBindTexture(GL_TEXTURE_2D,passDeferredRenderShadow->GetFramebuffer()->GetTextureId());
	//glBindTexture(GL_TEXTURE_2D,passGaussianBlur->GetPass1TextureId());
	
	glEnable(GL_TEXTURE_2D);
	glTranslated(0,0,-1);
	glBegin(GL_QUADS);
	glTexCoord2d(0,0);glVertex3f(0,0,0);
	glTexCoord2d(1,0);glVertex3f(m_iWidth,0,0);
	glTexCoord2d(1,1);glVertex3f(m_iWidth,m_iHeight,0);
	glTexCoord2d(0,1);glVertex3f(0,m_iHeight,0);
	glEnd();
	
	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	*/
	//passRenderCascadedShadowMap->GetShadowMap()->RenderDebug();
	//passRenderShadowMap->GetShadowMap()->RenderDepthBuffer();
	
	glDisable(GL_TEXTURE_2D);
	
	passForward->Bind();
	m_RootDebugObject->Render( passForward );
	passForward->Unbind();
	
	//
	/*
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST); 

	m_RootObject->RenderBoundingBoxChildren( m_pViewportCamera->GetViewMatrix(), m_pViewportCamera->GetProjection()->GetMatrix() );
	
	glEnable(GL_DEPTH_TEST); 
	glDepthMask(GL_TRUE);
	*/
	//

	m_pOverlay->Render();
	AssertGLError();

}
