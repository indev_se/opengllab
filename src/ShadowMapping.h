#pragma once
#include "SceneObject.h"
#include "LightObject.h"
#include "Frustum.h"
#include "DepthFramebufferObject.h"

class ShadowMapping
{
public:
	ShadowMapping(void);
	~ShadowMapping(void);

	void Create( LightObject *light, GLScene *Scene, unsigned int FBWidth, unsigned int FBHeight );

	void BindFramebuffer();
	void UnbindFramebuffer();

	void GetTextureMatrix( glm::mat4x4 &textureMatrix ) { textureMatrix = m_TextureMatrix; }

	void RenderDepthBuffer();
	
	void SetSpotLightValues();
	void SetDirectionalLightValues();

	DepthFramebufferObject *GetFramebuffer() { return m_pFPO; }

	LightObject *GetLight() { return m_pLight; }

	glm::mat4x4 GetProjectionMatrix() { return m_ProjectionMatrix; }
	glm::mat4x4 GetViewMatrix() { return m_ViewMatrix; }

protected:

	std::list<SceneObject *> m_ShadowEmitters;
	LightObject *m_pLight;

	DepthFramebufferObject *m_pFPO;

	Frustum *m_pFrustum;

	GLScene *m_pScene;

private:

	void CalculateTextureMatrix();
	
	glm::mat4x4 m_TextureMatrix;

	glm::mat4x4 m_ProjectionMatrix;
	glm::mat4x4 m_ViewMatrix;
};

