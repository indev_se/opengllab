#pragma once
#include "SceneObject.h"
#include "GLScene.h"

class SceneManager
{
public:
	~SceneManager(void);
	static SceneManager* Inst();

	inline void SetScene( GLScene *scene ) { m_pScene = scene; }
	inline GLScene *GetScene() { return m_pScene; }

	void RegisterObject( const char *, SceneObject *obj );
	void UnregisterObject( const char * );

	SceneObject *GetSceneObject( const char *objectName );

protected:
	SceneManager(void);
	
private:
	static SceneManager* m_pInst;

	GLScene *m_pScene;

	std::map< const char *, SceneObject*> m_mapObjects;
};

