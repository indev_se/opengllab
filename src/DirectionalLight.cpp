#include "StdAfx.h"
#include "DirectionalLight.h"


DirectionalLight::DirectionalLight(const char *objectName = nullptr) : LightObject(objectName)
{
	m_LightType = LIGHT_DIRECTIONAL;
}


DirectionalLight::~DirectionalLight(void)
{
}

void DirectionalLight::SetDirection( const glm::vec3 &vDirection )
{
	m_vDirection = vDirection;
	if( m_vDirection.length() != 1.0 )
		m_vDirection = glm::normalize(m_vDirection);
}

glm::vec3 DirectionalLight::GetDirection()
{
	return m_vDirection;
}

void DirectionalLight::GetDirection( glm::vec3 &direction )
{
	direction = m_vDirection;
}

void DirectionalLight::SetShaderUniform( char LightVar[60], ShaderProgram *shader )
{
	char LightVarStruct[60];
	
	// LightSource.Type
	sprintf_s(LightVarStruct, "%s.Type", LightVar);
	shader->SendUniform1i( LightVarStruct, m_LightType );
	
	// LightSource.Position
	//sprintf_s(LightVarStruct, "%s.Position", LightVar);
	//shader->SendUniform3f( LightVarStruct, (float*)&m_vecPosition );
	
	// LightSource.Position
	sprintf_s(LightVarStruct, "%s.Direction", LightVar);
	shader->SendUniform3f( LightVarStruct, (float*)&m_vDirection );
	/*
	// LightSource.Color
	sprintf_s(LightVarStruct, "%s.Color", LightVar);
	shader->SendUniform3f( LightVarStruct, (float*)&Vector3f(m_vDiffuse.x, m_vDiffuse.y, m_vDiffuse.z) );
	*/
}