#include "StdAfx.h"
#include "InputManager.h"

InputManager *InputManager::m_pInst = NULL;
InputManager::InputManager(void)
{
	memset( m_bKeys, 0, sizeof(m_bKeys) );

	m_iCurrentMouseX = 0, m_iCurrentMouseY = 0;

	m_iWindowWidth = 0, m_iWindowHeight = 0;

	m_iWindowPosX = 0, m_iWindowPosY = 0;

	m_iWindowCenterX = 0, m_iWindowCenterY = 0;

	m_bIsMouseNavigationActive = false;
	m_bIgnoreMouseMoveEvent = false;
}

InputManager::~InputManager(void)
{
}

InputManager *InputManager::Inst()
{
	if ( m_pInst == NULL )
		m_pInst = new InputManager();

	return m_pInst;
}

void InputManager::SetWindowSize( int Width, int Height )
{
	m_iWindowWidth = Width;
	m_iWindowHeight = Height;

	m_iWindowCenterX = m_iWindowPosX + (m_iWindowWidth / 2);
	m_iWindowCenterY = m_iWindowPosY + (m_iWindowHeight / 2);
}

void InputManager::SetWindowPosition( int PosX, int PosY )
{
	m_iWindowPosX = PosX;
	m_iWindowPosY = PosY;

	m_iWindowCenterX = m_iWindowPosX + (m_iWindowWidth / 2);
	m_iWindowCenterY = m_iWindowPosY + (m_iWindowHeight / 2);
}

void InputManager::GetCurrentMousePos( int &X, int &Y )
{
	if ( m_bIsMouseNavigationActive )
	{
		X = m_iCurrentMouseX;
		Y = m_iCurrentMouseY;
		m_iCurrentMouseX = 0;
		m_iCurrentMouseY = 0;
	}
	else
	{
		X = m_iCurrentMouseX = 0;
		Y = m_iCurrentMouseY = 0;
	}
}

LRESULT InputManager::ProcessMessage( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	switch (uMsg)
	{
		case WM_KEYDOWN:
		{
			// only bother about non repetetive events
			if ( (lParam & 0x40000000) == 0 )
			{
				m_bKeys[wParam] = TRUE;
			
				DispatchKeyDownEvent(wParam);
			}

			return 0;
		}

		case WM_KEYUP:
		{
			// only bother about non repetetive events
			if ( (lParam & 0x40000000) != 0 )
			{
				m_bKeys[wParam] = FALSE;
		
				DispatchKeyUpEvent(wParam);

				if ( wParam == VK_SPACE )
				{
					m_bIsMouseNavigationActive = !m_bIsMouseNavigationActive;
					SetCursorPos(m_iWindowCenterX, m_iWindowCenterY);
				}
			}

			return 0;
		}

		case WM_INPUT:
		{
			return 0;
		}
		
		case WM_MOUSEMOVE:
		{
			if ( m_bIgnoreMouseMoveEvent )
			{
				m_bIgnoreMouseMoveEvent = false;
			}
			else
			{
				if ( m_bIsMouseNavigationActive )
				{
					POINT tempPoint;
					GetCursorPos(&tempPoint);

					m_iCurrentMouseX = tempPoint.x - m_iWindowCenterX;
					m_iCurrentMouseY = tempPoint.y - m_iWindowCenterY;

					m_bIgnoreMouseMoveEvent = true;

					if ( m_iCurrentMouseX || m_iCurrentMouseY )
						SetCursorPos(m_iWindowCenterX, m_iWindowCenterY);
				}
			}

			return 0;
		}
	}

	return 0;
}

void InputManager::DispatchKeyUpEvent( uint16 key )
{
	for ( uint16 i = 0; i < m_Listeners_KeyUp.size(); ++i ) {
		EventKeyPair pair = m_Listeners_KeyUp[i];
		(pair.first->*pair.second)(key);
	}
}

void InputManager::DispatchKeyDownEvent( uint16 key )
{
	for ( uint16 i = 0; i < m_Listeners_KeyDown.size(); ++i ) {
		EventKeyPair pair = m_Listeners_KeyDown[i];
		(pair.first->*pair.second)(key);
	}
}

void InputManager::DispatchDeltaEvent( uint16 x, uint16 y )
{
	for ( uint16 i = 0; i < m_Listeners_Delta.size(); ++i ) {
		EventDeltaPair pair = m_Listeners_Delta[i];
		(pair.first->*pair.second)(x, y);
	}
}

void InputManager::AddKeyUpListener( InputEventListener* scope, InputEventKeyFuncPtr func)
{
	m_Listeners_KeyUp.push_back( EventKeyPair(scope, func) );
}

void InputManager::RemoveKeyUpListener( InputEventListener* scope, InputEventKeyFuncPtr func)
{
	EventKeyPair find(scope, func);
	std::remove(m_Listeners_KeyUp.begin(), m_Listeners_KeyUp.end(), find);
}

void InputManager::AddKeyDownListener( InputEventListener* scope, InputEventKeyFuncPtr func)
{
	m_Listeners_KeyDown.push_back( EventKeyPair(scope, func) );
}

void InputManager::RemoveKeyDownListener( InputEventListener* scope, InputEventKeyFuncPtr func)
{
	EventKeyPair find(scope, func);
	std::remove(m_Listeners_KeyDown.begin(), m_Listeners_KeyDown.end(), find);
}

void InputManager::AddDeltaListener( InputEventListener* scope, InputEventDeltaFuncPtr func)
{
	m_Listeners_Delta.push_back( EventDeltaPair(scope, func) );
}

void InputManager::RemoveDeltaListener( InputEventListener* scope, InputEventDeltaFuncPtr func)
{
	EventDeltaPair find(scope, func);
	std::remove(m_Listeners_Delta.begin(), m_Listeners_Delta.end(), find);
}
