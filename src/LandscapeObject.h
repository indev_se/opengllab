#pragma once
#include "SceneObject.h"
#include "GeoMipMapTerrain.h"
#include "TextureAsset.h"
#include "PointLight.h"
#include "ShaderProgram.h"

class LandscapeObject :
	public SceneObject
{
public:
	LandscapeObject(const char *objectName);
	~LandscapeObject(void);

	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);
	
private:
	
	unsigned char *m_HeightmapData;
	//ROAMLandscape *m_pRoamLandscape;

	TextureAsset *m_GrounLeaf01Texture;

	TextureAsset *m_MaskTexture;
	TextureAsset *m_Ground01Texture;
	TextureAsset *m_Ground02Texture;
	TextureAsset *m_Ground03Texture;
	TextureAsset *m_Ground04Texture;
	TextureAsset *m_TerrainNormalMap;

	PointLight	*m_PointLight;

	GeoMipMapTerrain *m_pGeoMipMapTerrain;

	ShaderProgram *m_LandscapeShader;
};

