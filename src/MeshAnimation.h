#pragma once
#include "Skeleton.h"
#include "Bone.h"

struct AnimationKeyFrame
{
	// In bone space
	glm::mat4x4 LocalTransform;
	glm::mat4x4 GlobalTransform;
	// In global space
	glm::mat4x4 ModelTransform;
	glm::mat4x4 ModelTransformInv;

	float Time;

	friend std::ostream& operator<<(std::ostream& os, const AnimationKeyFrame& s)
	{
		streamwrite_glm_mat4x4(os, s.LocalTransform);
		streamwrite_glm_mat4x4(os, s.GlobalTransform);
		
		streamwrite_glm_mat4x4(os, s.ModelTransform);
		streamwrite_glm_mat4x4(os, s.ModelTransformInv);

		SWRITE(os, s.Time);

		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, AnimationKeyFrame& s)
	{
		s.LocalTransform = streamread_glm_mat4x4(is);
		s.GlobalTransform = streamread_glm_mat4x4(is);

		s.ModelTransform = streamread_glm_mat4x4(is);
		s.ModelTransformInv = streamread_glm_mat4x4(is);

		SREAD(is, s.Time);

		return is;
	}
};

typedef std::vector<AnimationKeyFrame *> KeyFramesVector;
typedef std::map<const Bone*, KeyFramesVector> KeyFramesMap;

class MeshAnimation
{
public:
	MeshAnimation(void);
	~MeshAnimation(void);

	void SetKeyFrames( const Bone* bone, const KeyFramesVector& frames );
	void AddKeyFrame( const Bone* bone, AnimationKeyFrame* frame );
	KeyFramesVector GetKeyFrames( const Bone* bone );

	uint32 GetNumKeyFrames();

	void SetSkeleton( Skeleton *skeleton ) { m_pSkeleton = skeleton; }
	Skeleton* GetSkeleton() { return m_pSkeleton; }

	std::string Name;
	float Duration;

private:
	
	KeyFramesMap	m_KeyFrameMap;
	KeyFramesVector m_KeyFrames;

	Skeleton *m_pSkeleton;

public:

	// Insertion operator
	friend std::ostream& operator<<(std::ostream& os, const MeshAnimation& s)
	{
		uint32 len, count;
		len = s.Name.size();
		SWRITE(os, len);
		SWRITE_C(os, s.Name[0], len);
		SWRITE(os,s.Duration);

		count = s.m_KeyFrameMap.size();
		SWRITE(os, count);

		KeyFramesMap::const_iterator iter = s.m_KeyFrameMap.begin();
		KeyFramesVector::iterator iter2;
		KeyFramesVector frames;

		// Write all bones and their keyframes
		for ( iter; iter != s.m_KeyFrameMap.end(); ++iter ) {
			const Bone *bone = iter->first;
			SWRITE(os, bone->Index);
			frames = iter->second;

			// Write the keyframes that belong to this bone
			count = frames.size();
			SWRITE(os, count);

			iter2 = frames.begin();
			for ( iter2; iter2 != frames.end(); ++iter2 ) {

				os << **iter2;

			}
		}
		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, MeshAnimation& s)
	{
		uint32 i, j, count, count2, len;
		SREAD(is, len);
		s.Name.resize(len);
		SREAD_C(is, s.Name[0], len);
		SREAD(is, s.Duration);

		SREAD(is,count);

		uint32 boneIndex;
		const Bone *bone;
		for ( i = 0; i < count; ++i )
		{
			SREAD(is, boneIndex);
			bone = s.m_pSkeleton->GetBone(boneIndex);

			SREAD(is,count2);
			for ( j = 0; j < count2; ++j )
			{
				AnimationKeyFrame *frame = new AnimationKeyFrame();
				is >> *frame;

				s.AddKeyFrame(bone, frame);
			}
		}
		
		return is;
	}
};

