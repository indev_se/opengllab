#pragma once
#include "SceneObject.h"

class VertexBuffer;
class Material;
class VertexDescription;

class Skydome :
	public SceneObject
{
public:
	Skydome( uint32 _iLatitude, uint32 _iLongitude, float _fRadius, const char *objectName );
	~Skydome(void);

	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);

private:

	void InitGeometry();
	void InitVertexDescription();

	float m_fRadius;
	uint32 m_iLatitude, m_iLongitude;

	VertexBuffer		*m_pVB;
	VertexDescription	*m_pVertexDesc;

	std::vector<glm::vec3> test;
};

