#include "StdAfx.h"
#include "VertexData.h"


VertexData::VertexData(void)
{
	ptrdata = NULL;
}


VertexData::~VertexData(void)
{
	if ( ptrdata != NULL )
		free(ptrdata);

	ptrdata = NULL;
}

void VertexData::set( const VertexDataElement &elem, uint8 *data )
{
	if(ptrdata == NULL) {
		clear();

		ptrdata = (uint8*)malloc( elem.ptrdesc->VertexSize() );

		if (ptrdata == NULL) {
			StdConsole::LogError( _STR"Out of memory!");
			return;
		}
	}
	memcpy(ptrdata, data, elem.ByteSize);
}

void VertexData::set( VertexDataElement *elem, uint8 *data )
{
	if(ptrdata == NULL) {
		clear();

		int size = elem->ptrdesc->VertexSize();
		ptrdata = (uint8*)malloc( size );

		if (ptrdata == NULL) {
			StdConsole::LogError( _STR"Out of memory!");
			return;
		}
	}

	memcpy(ptrdata + elem->Offset, data, elem->ByteSize);
}

void VertexData::set( VertexDataElement *elem, glm::vec3 data )
{
	this->set(elem, (uint8*) glm::value_ptr(data));
}

void VertexData::set( VertexDataElement *elem, glm::vec2 data )
{
	this->set(elem, (uint8*) glm::value_ptr(data));
}

void VertexData::clear()
{
	if ( ptrdata != NULL )
		free(ptrdata);
	ptrdata = NULL;
}

void VertexData::data(uint8* data, uint32 size)
{
	clear();

	ptrdata = (uint8*)malloc( size );

	if (ptrdata == NULL) {
		StdConsole::LogError( _STR"Out of memory!");
		return;
	}

	memcpy(ptrdata, data, size);
}

/**
 * VertexDescription
 **/
VertexDescription::VertexDescription(void)
{
	m_bLocked = false;
}

VertexDescription::~VertexDescription(void)
{
}

void VertexDescription::AddElement( VertexDataElement *elem )
{
	if ( this->m_bLocked )
	{
		StdConsole::LogError( _STR"VertexDescription::AddElement - Description is locked");
		return;
	}

	elem->ptrdesc = this;
	m_Elements.push_back( elem );
}

void VertexDescription::RemoveElement( VertexDataElement *elem )
{
	if ( this->m_bLocked )
	{
		StdConsole::LogError( _STR"VertexDescription::RemoveElement - Description is locked");
		return;
	}

	std::vector<VertexDataElement*>::iterator iter = std::find(m_Elements.begin(), m_Elements.end(), elem);
	if(iter != m_Elements.end())
	   m_Elements.erase(iter);
}

VertexDataElement *VertexDescription::GetElement( std::string Name )
{
	for ( uint32 i = 0; i < m_Elements.size(); ++i )
		if ( m_Elements[i]->Name == Name )
			return m_Elements[i];

	return NULL;
}

void VertexDescription::Lock()
{
	m_bLocked = true;
	CalculateValues();
}

void VertexDescription::Unlock()
{
	m_bLocked = false;
}

void VertexDescription::CalculateValues()
{
	uint32 offset = 0;
	uint32 typeSize = 0;

	for ( uint32 i = 0; i < m_Elements.size(); ++i )
	{
		m_Elements[i]->Offset = offset;

		if ( m_Elements[i]->Type == Byte ) typeSize = sizeof(char);
		else if ( m_Elements[i]->Type == Integer ) typeSize = sizeof(int32);
		else if ( m_Elements[i]->Type == Float ) typeSize = sizeof(float);
		
		m_Elements[i]->ByteSize = typeSize * m_Elements[i]->Size;

		offset += m_Elements[i]->ByteSize;
	}

	m_iVertexSize = offset;
}

void VertexDescription::BindShaderAttribs(ShaderProgram *shader)
{
	for ( uint32 i = 0; i < m_Elements.size(); ++i )
	{
		m_Elements[i]->AttribLocation = shader->GetAttribLocation( (char*)m_Elements[i]->Name.c_str() );
		m_Elements[i]->Enabled = (m_Elements[i]->AttribLocation != -1);
	}
}

/** static **/
VertexDescription* VertexDescription::GetDefault( std::string type )
{
	VertexDescription *desc = new VertexDescription();
	VertexDataElement *POS, *COL, *NOR;
	if ( type == "Position3Color3Normal3" )
	{
		POS = new VertexDataElement();
		COL = new VertexDataElement();
		NOR = new VertexDataElement();

		POS->Name = "InPosition";
		COL->Name = "InColor";
		NOR->Name = "InNormal";

		POS->ParseTypeSize("float3");
		COL->ParseTypeSize("float3");
		NOR->ParseTypeSize("float3");

		desc->AddElement(POS);
		desc->AddElement(COL);
		desc->AddElement(NOR);

		desc->Lock();
	}
	else if ( type == "Position3" )
	{
		POS = new VertexDataElement();

		POS->Name = "InPosition";

		POS->ParseTypeSize("float3");

		desc->AddElement(POS);

		desc->Lock();
	}

	return desc;
}