#include "StdAfx.h"
#include "CubeMapFramebufferObject.h"


CubeMapFramebufferObject::CubeMapFramebufferObject(void)
{
	memset(m_pTextures, 0, 6);
}

CubeMapFramebufferObject::~CubeMapFramebufferObject(void)
{
	for ( uint8 i = 0; i < 6; ++i )
		if ( m_pTextures[i] != nullptr )
			delete m_pTextures[i];

	memset(m_pTextures, 0, 6);
}

void CubeMapFramebufferObject::Create( unsigned int Width, unsigned int Height )
{
	TextureInfo *info = new TextureInfo();
	info->genMipMap = false;
	info->format = GL_RGBA16F;
	info->valueType = GL_FLOAT;

	TextureInfoParam infoParam1 = { GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE };
	info->params.push_back( &infoParam1 );
	TextureInfoParam infoParam2 = { GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE };
	info->params.push_back( &infoParam2 );
	TextureInfoParam infoParam3 = { GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE };
	info->params.push_back( &infoParam3 );
	TextureInfoParam infoParam4 = { GL_TEXTURE_MAG_FILTER, GL_LINEAR };
	info->params.push_back( &infoParam4 );
	TextureInfoParam infoParam5	= { GL_TEXTURE_MIN_FILTER, GL_LINEAR };
	info->params.push_back( &infoParam5 );
}