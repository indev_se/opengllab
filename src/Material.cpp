#include "StdAfx.h"
#include "AssetsManager.h"
#include "Material.h"


Material::Material(void)
{
	m_pShaderForward = nullptr;
	m_pShaderDeferred = nullptr;
	m_VertexDesc = new VertexDescription();
	m_pBoundShader = nullptr;
}


Material::~Material(void)
{
	if ( m_pShaderForward ) {
		m_pManager->RemoveAndDestroyAsset(m_pShaderForward);
	}
	if ( m_pShaderDeferred ) {
		m_pManager->RemoveAndDestroyAsset(m_pShaderDeferred);
	}

	if ( m_VertexDesc ) {
		delete m_VertexDesc;
	}

	std::map<std::string, TextureAsset*>::iterator iter = m_Textures.begin();
	while ( iter != m_Textures.end() ) {
		m_pManager->RemoveAndDestroyAsset(iter->second);
		++iter;
	}

	m_pShaderForward = nullptr;
	m_pShaderDeferred = nullptr;
	m_VertexDesc = nullptr;
	m_Textures.clear();
}

void Material::AddTexture( const char *name, TextureAsset *tex )
{
	m_Textures[name] = tex;
}

void Material::BindForward(RenderPass *pass)
{
	bool bindTextures = true;
	if ( pass != nullptr && !pass->BindTextures() )
		bindTextures = false;

	if ( m_pShaderForward != NULL ) {
		m_VertexDesc->BindShaderAttribs(m_pShaderForward);
		
		if ( m_Textures.size() > 0 && bindTextures ) glEnable(GL_TEXTURE_2D);

		glUseProgramObjectARB(m_pShaderForward->Program());

		if ( m_Textures.size() > 0 && bindTextures ) {
			GLenum textureIndices[] = { GL_TEXTURE0, GL_TEXTURE1, GL_TEXTURE2 };
			uint32 textureIndex = 0;
			std::map<std::string, TextureAsset*>::iterator iter = m_Textures.begin();
			while ( iter != m_Textures.end() ) {
			
				m_pShaderForward->SendUniform1i((char*)iter->first.c_str(), textureIndex);
	
				glActiveTextureARB(textureIndices[textureIndex]);
				glBindTexture(GL_TEXTURE_2D, ((TextureAsset*)iter->second)->TextureID());

				++textureIndex;
				++iter;
			}
		}
		
	}

	m_pBoundShader = m_pShaderForward;
}

void Material::UnbindForward(RenderPass *pass)
{
	bool bindTextures = true;
	if ( pass != nullptr && !pass->BindTextures() )
		bindTextures = false;

	if ( m_pShaderForward != NULL ) {
		glUseProgramObjectARB(0);
		
		if ( m_Textures.size() > 0 && bindTextures ) {
			GLenum textureIndices[] = { GL_TEXTURE0, GL_TEXTURE1, GL_TEXTURE2 };
			uint32 textureIndex = 0;
		
			std::map<std::string, TextureAsset*>::iterator iter = m_Textures.begin();
			while ( iter != m_Textures.end() ) {
				glActiveTextureARB(textureIndices[textureIndex]);
				glBindTexture(GL_TEXTURE_2D, 0);

				++textureIndex;
				++iter;
			}

			glActiveTextureARB(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, 0);
			glDisable(GL_TEXTURE_2D);
		}
	}

	m_pBoundShader = nullptr;
}

void Material::BindDeferred(RenderPass *pass)
{
	bool bindTextures = true;
	if ( pass != nullptr && !pass->BindTextures() )
		bindTextures = false;

	if ( m_pShaderDeferred != NULL ) {
		m_VertexDesc->BindShaderAttribs(m_pShaderDeferred);

		if ( m_Textures.size() > 0 && bindTextures ) glEnable(GL_TEXTURE_2D);

		glUseProgramObjectARB(m_pShaderDeferred->Program());

		if ( m_Textures.size() > 0 && bindTextures ) {
			GLenum textureIndices[] = { GL_TEXTURE0, GL_TEXTURE1, GL_TEXTURE2 };
			uint32 textureIndex = 0;
			std::map<std::string, TextureAsset*>::iterator iter = m_Textures.begin();
			while ( iter != m_Textures.end() ) {
			
				m_pShaderDeferred->SendUniform1i((char*)iter->first.c_str(), textureIndex);
	
				glActiveTextureARB(textureIndices[textureIndex]);
				glBindTexture(GL_TEXTURE_2D, ((TextureAsset*)iter->second)->TextureID());

				++textureIndex;
				++iter;
			}
		}
	}

	m_pBoundShader = m_pShaderDeferred;
}

void Material::UnbindDeferred(RenderPass *pass)
{
	bool bindTextures = true;
	if ( pass != nullptr && !pass->BindTextures() )
		bindTextures = false;

	if ( m_pShaderDeferred != NULL ) {
		glUseProgramObjectARB(0);

		if ( m_Textures.size() > 0 && bindTextures ) {
			GLenum textureIndices[] = { GL_TEXTURE0, GL_TEXTURE1, GL_TEXTURE2 };
			uint32 textureIndex = 0;
		
			std::map<std::string, TextureAsset*>::iterator iter = m_Textures.begin();
			while ( iter != m_Textures.end() ) {
				glActiveTextureARB(textureIndices[textureIndex]);
				glBindTexture(GL_TEXTURE_2D, 0);

				++textureIndex;
				++iter;
			}

			glActiveTextureARB(GL_TEXTURE0);
			glDisable(GL_TEXTURE_2D);
		}
	}

	m_pBoundShader = nullptr;
}

void Material::BindShaderAttribs()
{
	if ( m_pShaderForward != nullptr )
		m_VertexDesc->BindShaderAttribs(m_pShaderForward);
	else if ( m_pShaderDeferred != nullptr )
		m_VertexDesc->BindShaderAttribs(m_pShaderDeferred);
}

/** This requires that the material to sync with contains same or less attributes than this one **/
void Material::SyncMaterialDescription( Material *material )
{
	uint32 i;

	// First set all elements as inactive
	std::vector<VertexDataElement*> elems = m_VertexDesc->GetElements();
	for( i = 0; i < elems.size(); ++i )
	{
		elems[i]->Enabled = false;
	}

	// Now set all of the elemts that exists in both descriptions as active
	std::vector<VertexDataElement*> syncElems = material->GetDescription()->GetElements();
	for ( i = 0; i < syncElems.size(); ++i )
	{
		if ( m_VertexDesc->GetElement(syncElems[i]->Name) && material->GetDescription()->GetElement(syncElems[i]->Name) )
		{
			elems[i]->Enabled = true;
			elems[i]->AttribLocation = syncElems[i]->AttribLocation;
		}
	}
}

void Material::UnsyncMaterialDescription()
{
	uint32 i;

	// Set all elements as active
	std::vector<VertexDataElement*> elems = m_VertexDesc->GetElements();
	for( i = 0; i < elems.size(); ++i )
	{
		elems[i]->Enabled = true;
	}

	// Update shader attribute positions
	BindShaderAttribs();
}


/** Properties **/
MaterialProperty* Material::GetOrCreateProperty( std::string name )
{
	if ( m_Properties[name] == nullptr ) {
		m_Properties[name] = new MaterialProperty();
	}
	return m_Properties[name];
}

void Material::SetProperty( std::string name, float value )
{
	MaterialProperty *prop = GetOrCreateProperty(name);

	prop->Type = DataType::Float;
	prop->ValueFloat = value;
}

void Material::SetProperty( std::string name, int32 value )
{
	MaterialProperty *prop = GetOrCreateProperty(name);

	prop->Type = DataType::Integer;
	prop->ValueInteger = value;
}

void Material::SetProperty( std::string name, std::string value )
{
	MaterialProperty *prop = GetOrCreateProperty(name);

	prop->Type = DataType::String;
	prop->ValueString = value;
}

void Material::SetProperty( std::string name, bool value )
{
	MaterialProperty *prop = GetOrCreateProperty(name);

	prop->Type = DataType::Boolean;
	prop->ValueBool = value;
}

float Material::GetFloatProperty( std::string name )
{
	MaterialProperty *prop = m_Properties[name];
	if ( prop == nullptr ) {
		StdConsole::LogWarning( _STR"Found no property named: " << name );
		return 0.0f;
	}
	if ( prop->Type != DataType::Float ) {
		StdConsole::LogWarning( _STR"Property is another type: " << name);
		return false;
	}

	return prop->ValueFloat;
}

int32 Material::GetIntegerProperty( std::string name )
{
	MaterialProperty *prop = m_Properties[name];
	if ( prop == nullptr ) {
		StdConsole::LogWarning( _STR"Found no property named: " << name);
		return 0;
	}
	if ( prop->Type != DataType::Integer ) {
		StdConsole::LogWarning( _STR"Property is another type: " << name);
		return false;
	}

	return prop->ValueInteger;
}

std::string Material::GetStringProperty( std::string name )
{
	MaterialProperty *prop = m_Properties[name];
	if ( prop == nullptr ) {
		StdConsole::LogWarning( _STR"Found no property named: " << name);
		return "Error";
	}
	if ( prop->Type != DataType::String ) {
		StdConsole::LogWarning( _STR"Property is another type: " << name);
		return false;
	}

	return prop->ValueString;
}

bool Material::GetBoolProperty( std::string name )
{
	MaterialProperty *prop = m_Properties[name];
	if ( prop == nullptr ) {
		StdConsole::LogWarning( _STR"Found no property named: " << name);
		return false;
	}
	if ( prop->Type != DataType::Boolean ) {
		StdConsole::LogWarning( _STR"Property is another type: " << name);
		return false;
	}

	return prop->ValueBool;
}