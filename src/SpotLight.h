#pragma once
#include "DirectionalLight.h"

class SpotLight :
	public DirectionalLight
{
public:
	SpotLight( const char *objectName );
	~SpotLight(void);

	void Render();
	
	void SetShaderUniform( char LightVar[60], ShaderProgram *shader );

};

