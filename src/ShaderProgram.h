#pragma once
#include "asset.h"
class ShaderProgram :
	public Asset
{
public:
	ShaderProgram(void);
	~ShaderProgram(void);

	GLhandleARB CreateProgram( const char *vertexShaderFile, const char *fragmentShaderFile );

	// Uniform functions, send values to the shader program
	void SendUniform1i(char variable[60], int value);
	void SendUniform1f(char variable[60], float value);
	void SendUniform2f(char variable[60], float value[2]);
	void SendUniform3f(char variable[60], float value[3]);
	void SendUniform4f(char variable[60], float value[4]);
	void SendUniformMatrix4fv(char variable[60], int count, bool transpose, float *value);

	void BindFragDataLocation(char variable[60], int value);

	int32 GetAttribLocation(char variable[60] );

	inline GLhandleARB Program() { return m_Program; }

protected:

	GLhandleARB	m_VertexShader;
	GLhandleARB	m_FragmentShader;

	GLhandleARB	m_Program;

	char *GetSource( const char *sourceFile );
	void PrintProgramInfoLog();
};

