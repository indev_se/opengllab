#include "StdAfx.h"
#include "BlendAnimationStates.h"
#include "AnimatedMeshGroupObject.h"

AnimatedMeshGroupObject::AnimatedMeshGroupObject( const char *objectName ) : SceneObject(objectName)
{
	m_pAnimationState = nullptr;
}

AnimatedMeshGroupObject::~AnimatedMeshGroupObject(void)
{
}

void AnimatedMeshGroupObject::Create( MeshGroup *mesh, Mesh *parent, GeometryBuffer *gb )
{
	m_pMesh = mesh;
	m_pMeshParent = parent;
	m_pGB = gb;

	m_pMaterial = m_pScene->GetAssetsManager()->GetMaterial( m_pMesh->name.c_str() );

	if ( m_pMaterial == nullptr )
		m_pMaterial = m_pScene->GetAssetsManager()->GetMaterial( m_pMesh->name.c_str() );

	if ( m_pMaterial ) {
		m_pGB->ReplaceDescription( m_pMaterial->GetDescription() );
	}
}

void AnimatedMeshGroupObject::SetAnimationState( AnimationState* state )
{
	m_pAnimationState = state;

	if ( m_pAnimationState->IsBlend() )
		PreApplyBlendAnimation();
	else
		PreApplyAnimation();
}

glm::mat4x4 AnimatedMeshGroupObject::GetModelMatrix()
{
	glm::mat4x4 mat = SceneObject::GetModelMatrix();
	
	return mat * m_pMesh->transform;
}

void AnimatedMeshGroupObject::Init( GLScene *scene )
{
	SceneObject::Init(scene);
}

void AnimatedMeshGroupObject::Update( float tickMS )
{
	SceneObject::Update(tickMS);
}

void AnimatedMeshGroupObject::Render(RenderPass *pass)
{
	if ( pass != nullptr )
		pass->BindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->BindForward();

	ApplyAnimation();

	if (m_pMaterial->GetBoundShader())
		m_pMaterial->GetBoundShader()->SendUniformMatrix4fv("SkinMatrices", m_BoneMatrices.size(), false, (float*)&m_BoneMatrices[0]);

	m_pGB->GetVertexBuffer()->Render( GL_TRIANGLES, m_pMesh->indices, m_pMesh->numIndices );

	if ( pass != nullptr )
		pass->UnbindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->UnbindForward();

	SceneObject::Render(pass);
}

void AnimatedMeshGroupObject::PreApplyAnimation()
{
	if ( nullptr == m_pAnimationState ) {
		PreApplyNoAnimation();
		return;
	}

	Skeleton *skeleton = m_pMeshParent->GetSkeleton();
	std::vector<Bone*> bones = skeleton->GetBones();
	const Bone *bone;
	uint32 i, j;

	MeshAnimation *animation = m_pAnimationState->GetMeshAnimation();
	KeyFramesVector keyframes;
	
	for( i = 0; i < bones.size(); ++i ) 
	{
		bone = bones[i];

		keyframes = animation->GetKeyFrames(bone);

		for ( j = 0; j < keyframes.size(); ++j )
		{
			AnimationKeyFrame *keyframe = keyframes.at(j);

			glm::mat4x4 transform1 = bone->GlobalMatrixInv * bone->LocalToGlobalMatrix;
			glm::mat4x4 transform2 = m_pMesh->transformInv * keyframe->GlobalTransform;
			glm::mat4x4 transform = transform2 * transform1;

			if ( m_BoneFrameMatrices.size() < keyframes.size() )
				m_BoneFrameMatrices.resize(keyframes.size());

			if ( m_BoneFrameMatrices[j].size() < bone->Index + 1 )
				m_BoneFrameMatrices[j].resize( bone->Index + 1 );

			m_BoneFrameMatrices[j][bone->Index] = transform;
		}
	}
}

void AnimatedMeshGroupObject::PreApplyBlendAnimation()
{
	if ( nullptr == m_pAnimationState ) {
		PreApplyNoAnimation();
		return;
	}

	Skeleton *skeleton = m_pMeshParent->GetSkeleton();
	std::vector<Bone*> bones = skeleton->GetBones();
	const Bone *bone;
	uint32 i, j, totalFrames;
	float blendFactor;

	AnimationState *pSourceState = nullptr, *pDestState = nullptr;
	MeshAnimation *srcAnimation, *dstAnimation;

	((BlendAnimationStates*)m_pAnimationState)->GetAnimationStates( &pSourceState, &pDestState );

	srcAnimation = pSourceState->GetMeshAnimation();
	dstAnimation = pDestState->GetMeshAnimation();

	AnimationKeyFrame *srcKF, *dstKF;

	totalFrames = m_pAnimationState->GetTotalFrames();

	for( i = 0; i < bones.size(); ++i ) 
	{
		bone = bones[i];

		srcKF = srcAnimation->GetKeyFrames(bone).at( pSourceState->GetCurrentFrame() );
		dstKF = dstAnimation->GetKeyFrames(bone).at( 0 );

		for ( j = 0; j < totalFrames; ++j )
		{
			blendFactor = (float)j / (float)totalFrames;

			glm::mat4x4 blendTransform = (srcKF->GlobalTransform * (1.0f-blendFactor)) + (dstKF->GlobalTransform * blendFactor);

			glm::mat4x4 transform1 = bone->GlobalMatrixInv * bone->LocalToGlobalMatrix;
			glm::mat4x4 transform2 = m_pMesh->transformInv * blendTransform;
			glm::mat4x4 transform = transform2 * transform1;

			if ( m_BoneFrameMatrices.size() < totalFrames )
				m_BoneFrameMatrices.resize(totalFrames);

			if ( m_BoneFrameMatrices[j].size() < bone->Index + 1 )
				m_BoneFrameMatrices[j].resize( bone->Index + 1 );

			m_BoneFrameMatrices[j][bone->Index] = transform;
		}
	}

	
}

void AnimatedMeshGroupObject::PreApplyNoAnimation()
{
	Skeleton *skeleton = m_pMeshParent->GetSkeleton();
	std::vector<Bone*> bones = skeleton->GetBones();
	const Bone *bone;
	uint32 i;

	// Single frame "animation" with identity matrix
	m_BoneFrameMatrices.resize(1);

	for( i = 0; i < bones.size(); ++i ) {
		bone = bones[i];

		if ( m_BoneFrameMatrices[0].size() < bone->Index + 1 )
			m_BoneFrameMatrices[0].resize( bone->Index + 1 );

		m_BoneFrameMatrices[0][bone->Index] = glm::mat4x4();
	}
}

void AnimatedMeshGroupObject::ApplyAnimation()
{
	if ( m_pAnimationState == nullptr ) {
		m_BoneMatrices = m_BoneFrameMatrices[0];
		return;
	}

	if ( m_pAnimationState->NeedUpdate() )
		m_BoneMatrices = m_BoneFrameMatrices[ m_pAnimationState->GetCurrentFrame() ];
}

void AnimatedMeshGroupObject::RenderSkeleton(MeshAnimation *animation, uint32 frame, RenderPass *pass)
{
	Skeleton *skeleton = m_pMeshParent->GetSkeleton();
	std::vector<Bone*> bones = skeleton->GetBones();
	KeyFramesVector keyframes;
	const Bone *bone;

	for (uint32 i = 0; i < bones.size(); ++i )
	{
		bone = bones[i];
		keyframes = animation->GetKeyFrames(bone);

		bone->DebugShape->SetTransformMatrix( ((AnimationKeyFrame*)keyframes.at(frame))->GlobalTransform );
		bone->DebugShape->Render(pass);
	}
}
