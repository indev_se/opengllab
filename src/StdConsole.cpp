#include "StdAfx.h"
#include "StdConsole.h"

HANDLE StdConsole::stdoutHandle = nullptr;

StdConsoleListenerList StdConsole::m_Listeners_ProcessCommand;

StdConsole::StdConsole(void)
{
}

StdConsole::~StdConsole(void)
{
}

void StdConsole::SetStdoutHandle( HANDLE stdoutHandle )
{
	StdConsole::stdoutHandle = stdoutHandle;
}

void StdConsole::LogInfo( std::ostream &stream )
{
	StdConsole::SetTextColor( FOREGROUND_GREEN );
	std::cout << "INFO: ";
	StdConsole::SetTextColor( FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY );
	
	std::ostringstream& s = dynamic_cast<std::ostringstream&>(stream);
    std::cout << s.str() << std::endl;
}

void StdConsole::LogError( std::ostream &stream )
{
	StdConsole::SetTextColor( FOREGROUND_RED );
	std::cout << "ERROR: ";
	StdConsole::SetTextColor( FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
	
	std::ostringstream& s = dynamic_cast<std::ostringstream&>(stream);
    std::cout << s.str() << std::endl;
}

void StdConsole::LogWarning( std::ostream &stream )
{
	StdConsole::SetTextColor( FOREGROUND_RED | FOREGROUND_GREEN );
	std::cout << "WARNING: ";
	StdConsole::SetTextColor( FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);

	std::ostringstream& s = dynamic_cast<std::ostringstream&>(stream);
    std::cout << s.str() << std::endl;
}

void StdConsole::SetTextColor(WORD wColor)
{
	if ( StdConsole::stdoutHandle == nullptr )
		return;

	WORD wAttributes = wColor;
	SetConsoleTextAttribute( StdConsole::stdoutHandle, wAttributes );
}

bool StdConsole::ProcessCommand( std::string command, std::string &response )
{
	if ( command == "exit" ) {
		response = "Good bye!";
		return true;
	}

	bool exit = false;
	for ( uint16 i = 0; i < StdConsole::m_Listeners_ProcessCommand.size(); ++i ) {
		StdConsoleListenerPair pair = StdConsole::m_Listeners_ProcessCommand[i];
		exit |= (pair.first->*pair.second)( command, response );
	}

	return exit;
}

void StdConsole::AddCommandListener( StdConsoleEventListener* scope, ProcessCommandFuncPtr func )
{
	StdConsole::m_Listeners_ProcessCommand.push_back( StdConsoleListenerPair(scope, func) );
}

void StdConsole::RemoveCommandListener( StdConsoleEventListener* scope, ProcessCommandFuncPtr func )
{
	StdConsoleListenerPair find(scope, func);
	std::remove(StdConsole::m_Listeners_ProcessCommand.begin(), StdConsole::m_Listeners_ProcessCommand.end(), find);
}