#include "StdAfx.h"
#include "md5.h"
#include "SceneObjectFactory.h"

SceneObjectFactory *SceneObjectFactory::m_pInst = NULL;
SceneObjectFactory::SceneObjectFactory(void)
{
}

SceneObjectFactory::~SceneObjectFactory(void)
{
}

SceneObjectFactory *SceneObjectFactory::Inst()
{
	if ( m_pInst == NULL )
		m_pInst = new SceneObjectFactory();

	return m_pInst;
}

StaticMeshObject* SceneObjectFactory::CreateStaticMeshObject( const char *name, const char *filename, GLScene *scene )
{
	StaticMeshObject *obj;

	std::string cacheFile = GetCachedFile(filename);

	if ( cacheFile != "" )
	{
		StdConsole::LogInfo( _STR"Found cached file for: " << filename);
		obj = new StaticMeshObject(name);
		obj->Init(scene);
		obj->LoadFromCache( cacheFile.c_str() );
	}
	else
	{
		obj = new StaticMeshObject(name);
		obj->Init(scene);
		obj->LoadFromFile( filename );
	}

	return obj;
}

AnimatedMeshObject* SceneObjectFactory::CreateAnimatedMeshObject( const char *name, const char *filename, GLScene *scene )
{
	AnimatedMeshObject *obj;

	std::string cacheFile = GetCachedFile(filename);

	if ( cacheFile != "" )
	{
		StdConsole::LogInfo( _STR"Found cached file for: " << filename);
		obj = new AnimatedMeshObject(name);
		obj->Init(scene);
		obj->LoadFromCache( cacheFile.c_str() );
	}
	else
	{
		obj = new AnimatedMeshObject(name);
		obj->Init(scene);
		obj->LoadFromFile(filename);
	}

	return obj;
}

ModelRenderBatchObject* SceneObjectFactory::CreateModelRenderBatchObject( const char *name, const char *filename, GLScene *scene )
{
	ModelRenderBatchObject *obj;

	std::string cacheFile = GetCachedFile(filename);

	if ( cacheFile != "" )
	{
		StdConsole::LogInfo( _STR"Found cached file for: " << filename);
		obj = new ModelRenderBatchObject(name);
		obj->Init(scene);
		obj->LoadFromCache( cacheFile.c_str() );
	}
	else
	{
		obj = new ModelRenderBatchObject(name);
		obj->Init(scene);
		obj->LoadFromFile(filename);
	}

	return obj;
}

std::string SceneObjectFactory::GetCacheFilepath(const char *filename)
{
	std::string md5hash = md5(filename);
	std::string cacheFile = "Data/Cache/" + md5hash;
	return cacheFile;
}

std::string SceneObjectFactory::GetCachedFile(const char *filename)
{
	std::string cacheFile = GetCacheFilepath(filename);

	// Check and see if the cached file exist
	FILE *fileHandle = NULL;
	fopen_s(&fileHandle, cacheFile.c_str(), "r");

	if ( fileHandle == NULL )
		return "";

	return cacheFile;
}