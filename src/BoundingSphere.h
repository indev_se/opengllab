#pragma once
class BoundingSphere
{
public:
	BoundingSphere(void);
	BoundingSphere(const glm::vec3 &center, const float radius);

	~BoundingSphere(void);

	void Multiply( const glm::mat4x4 &transformMatrix );

	void SetValues(const glm::vec3 &center, const float radius);
	void GetValues(glm::vec3 &center, float &radius);
	
	BoundingSphere* Clone();

private:

	glm::vec3 m_vecCenter;
	float m_fRadius;
	
};

