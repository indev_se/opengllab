#pragma once
#include "RenderPass.h"
#include "ShadowMapping.h"

class ApplyShadowMapPass :
	public RenderPass
{
public:
	ApplyShadowMapPass(void);
	~ApplyShadowMapPass(void);

	void Init( ShadowMapping *shadowmap, GLScene *scene );
	void Bind();
	void Unbind();

	void BindObject( SceneObject *object );
	void UnbindObject( SceneObject *object );

	void BindJob( RenderJob *job ) {}
	void UnbindJob( RenderJob *job ) {}

protected:

	ShadowMapping *m_pShadowMap;

};

