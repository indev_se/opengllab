#include "StdAfx.h"
#include "StrategyGLScene.h"
#include "AssetsManager.h"

#include "FbxFileContext.h"
#include "MeshAnimation.h"
#include "FbxImporter.h"


FbxImporter::FbxImporter(void)
{
	m_bIncludeAnimation = false;
	m_pMesh = nullptr;
	m_pContext = nullptr;
}


FbxImporter::~FbxImporter(void)
{
}

FbxFileContext *FbxImporter::LoadFromFile( const char *filename, GLScene *glscene, bool includeAnimation )
{
	m_pScene = glscene;
	m_bIncludeAnimation = includeAnimation;

	FbxManager *mgr = FbxManager::Inst();
    
	// Allocate and load the scene from file
	mgr->AllocateScene(m_pFbxScene);

	mgr->LoadScene(m_pFbxScene, filename, m_bIncludeAnimation);

	if ( m_pFbxScene == NULL )
	{
		StdConsole::LogError( _STR"Failed to load scene: " << filename);
		return nullptr;
	}

	KFbxAxisSystem sceneAxisSystem = m_pFbxScene->GetGlobalSettings().GetAxisSystem();
	KFbxAxisSystem engineAxisSystem(KFbxAxisSystem::OpenGL);
	
	if (sceneAxisSystem != engineAxisSystem)
	{
		engineAxisSystem.ConvertScene(m_pFbxScene);     // convert the scene to engine specs
	}


	m_pContext = new FbxFileContext();

	int i;
	m_pRootNode = m_pFbxScene->GetRootNode();
	
	if(m_pRootNode)
    {
		m_pMesh = new Mesh();
		m_pMesh->Name = m_pRootNode->GetName();

		if ( m_bIncludeAnimation ) {
			ReadSkeleton( FindRoot(m_pRootNode, KFbxNodeAttribute::eSKELETON));
		}

		ReadMeshNodes( m_pRootNode );
	}

	if ( m_pMesh ) {
		m_pMesh->CalculateBoundsFromGroups();
		m_pContext->SetMesh(m_pMesh);
	}

	return m_pContext;
}

void FbxImporter::ReadMeshNodes( KFbxNode *_pParentNode )
{
	int32 i;
	KFbxNode *pNode;
	
	for(i = 0; i < _pParentNode->GetChildCount(); i++)
    {
		pNode = _pParentNode->GetChild(i);

		KFbxNodeAttribute::EAttributeType attributeType;

		if ( pNode->GetChildCount() > 0 )
			ReadMeshNodes( pNode );

		if(pNode->GetNodeAttribute() == NULL) continue;
			
		attributeType = pNode->GetNodeAttribute()->GetAttributeType();

		switch (attributeType)
		{
			case KFbxNodeAttribute::eMESH:      
				ReadSubMesh(pNode);
			break;

			case KFbxNodeAttribute::eLIGHT:
				ReadLight(pNode);
			break;
		}
    }
}

void FbxImporter::ReadSubMesh( KFbxNode *node )
{
	int i;
	KFbxMesh *fbxMesh = (KFbxMesh*) node->GetMesh();
	
	// Read a mesh group for each material
	const int materialCount = fbxMesh->GetNode()->GetMaterialCount();
	
	std::string name;
	for ( i = 0; i < materialCount; ++i ) {
		ReadSubMeshGroup( node, i );
	}

}

void FbxImporter::ReadSubMeshGroup( KFbxNode *node, const int materialIndex )
{
	int i, j;

	TriangulateNode(node);

	KFbxMesh *fbxMesh = (KFbxMesh*) node->GetMesh();
	AssetsManager *assetsManager = ((StrategyGLScene*)m_pScene)->GetAssetsManager();
	
	// Read the skinning
	int skinCount = fbxMesh->GetDeformerCount(KFbxDeformer::eSKIN);
	std::vector<std::vector<int>> boneIndices;
	std::vector<std::vector<double>> weightIndices;

	if ( skinCount > 0 && m_bIncludeAnimation )
	{
		boneIndices.resize( fbxMesh->GetControlPointsCount() );
		weightIndices.resize( fbxMesh->GetControlPointsCount() );
	
		for( i = 0; i < skinCount; ++i )
		{
			KFbxSkin *skin = (KFbxSkin*)fbxMesh->GetDeformer(i, KFbxDeformer::eSKIN);

			int clusterCount = skin->GetClusterCount();
			for ( j = 0; j < clusterCount; ++j )
			{
				KFbxCluster *cluster = skin->GetCluster(j);
				if ( !cluster ) continue;

				KFbxNode *boneNode = cluster->GetLink();

				if ( !boneNode ) continue;

				Bone *bone = m_FbxSkeletonMap[boneNode];

				// Calculate the bones transform matrices
				KFbxXMatrix boneBindingMatrix;
				cluster->GetTransformLinkMatrix( boneBindingMatrix );
				bone->GlobalMatrix = ConvertMatrix( boneBindingMatrix );
				bone->GlobalMatrixInv = glm::inverse(bone->GlobalMatrix);

				const KFbxVector4 lT = fbxMesh->GetNode()->GetGeometricTranslation(KFbxNode::eSOURCE_SET);
				const KFbxVector4 lR = fbxMesh->GetNode()->GetGeometricRotation(KFbxNode::eSOURCE_SET);
				const KFbxVector4 lS = fbxMesh->GetNode()->GetGeometricScaling(KFbxNode::eSOURCE_SET);
				KFbxXMatrix meshGeometry = KFbxXMatrix(lT, lR, lS);

				KFbxXMatrix meshGlobal;
				cluster->GetTransformMatrix(meshGlobal);
				meshGlobal *= meshGeometry;

				bone->LocalToGlobalMatrix = ConvertMatrix( meshGlobal );
				
				if (cluster->GetLinkMode() == KFbxLink::eADDITIVE && cluster->GetAssociateModel())
				{
					StdConsole::LogWarning( _STR"Cluster is additive, not supported yet");
				}
				else if (cluster->GetLinkMode() == KFbxLink::eNORMALIZE)
				{
					
				}
				else if (cluster->GetLinkMode() == KFbxLink::eTOTAL1)
				{
					
				}

				// Bind the bone index to the control points
				int numPoints = cluster->GetControlPointIndicesCount(); // number of vertices this bone influence
				int *indices = cluster->GetControlPointIndices();
				double *weights = cluster->GetControlPointWeights();

				for ( int point = 0; point < numPoints; ++point )
				{
					int index = indices[ point ];
					double weight = weights[ point ];

					weightIndices[ index ].push_back(weight);
					boneIndices[ index ].push_back(bone->Index);
				}
			}
		}
	}
	
	// Setup the geometry buffer
	int indexPtrIndex = 0;
	int currentIndex = 0; // This should be updated depending on GB and vertex description

	int uvCount = fbxMesh->GetElementUVCount();

	VertexDescription *pVertexDesc;
	Material *pMaterial;
	GeometryBuffer *pGB;

	std::string materialName;

	pMaterial = GetMaterial(fbxMesh, materialIndex, materialName);

	pVertexDesc = pMaterial->GetDescription();

	if (m_GBCache[pVertexDesc])
		pGB = m_GBCache[pVertexDesc];
	else {
		pGB = new GeometryBuffer();
		pGB->SetDescription(pVertexDesc);
		m_GBCache[pVertexDesc] = pGB;
	}

	currentIndex = m_pMesh->GetNumIndices(pGB);
	
	// Setup the vertex description elements
	VertexDataElement *POS = nullptr;
	VertexDataElement *NOR = nullptr;
	VertexDataElement *SKIN_IDX = nullptr;
	VertexDataElement *SKIN_WEIGHT = nullptr;
	VertexDataElement *TAN = nullptr;
	std::vector<VertexDataElement *> uvDescs;

	POS = pVertexDesc->GetElement("InPosition");
	NOR = pVertexDesc->GetElement("InNormal");
	SKIN_IDX = pVertexDesc->GetElement("InSkinIndices");
	SKIN_WEIGHT = pVertexDesc->GetElement("InSkinWeights");
	TAN = pVertexDesc->GetElement("InTangent");

	for ( i = 0; i < uvCount; ++i )
	{
		std::ostringstream oss;
		oss << "InTex" << i;
		if ( pVertexDesc->GetElement(oss.str()) != nullptr )
			uvDescs.push_back( pVertexDesc->GetElement(oss.str()) );
	}

	// Make sure we have the necessary vertex elements
	if ( ( skinCount > 0 && m_bIncludeAnimation ) && (SKIN_IDX == nullptr || SKIN_WEIGHT == nullptr) )
	{
		StdConsole::LogWarning( _STR"Have skeleton data but not Vertex elements");
		return;
	}

	// Setup the mesh index and vertex count
	int polygonCount = fbxMesh->GetPolygonCount();
	int indexCount  = 0;//polygonCount * 3;
	
	KFbxLayerElementArrayTemplate<int>* materialIndices = NULL;
	fbxMesh->GetMaterialIndices( &materialIndices );

		/** ugly as hell **/
		for (i = 0; i < polygonCount; i++)
		{
			for (j = 0; j < 3; ++j )
			{
				int vertexIndex = fbxMesh->GetPolygonVertex(i, j);

				// make sure this vertex index is part of the current material
				if ( materialIndices ) {
					const int matIdx = materialIndices->GetAt(vertexIndex);
					if ( matIdx != materialIndex && j == 0 )
						break;
					
					++indexCount;
				}
			}
		}

	int vertexCount = indexCount;//fbxMesh->GetControlPointsCount();
	
	// create the mesh group
	MeshGroup *group = new MeshGroup();
	m_pMesh->AddGroup(group);
	m_pMesh->SetGeometryBuffer(group, pGB);

	group->numIndices = indexCount;
	group->indices = new uint16[ group->numIndices ];
	group->name= node->GetName();
	group->materialName = materialName;
	
	// Use these to calculate the bounding box
	glm::vec3 min, max;

	// Read the vertices
	KFbxVector4* controlPoints = fbxMesh->GetControlPoints(); 
	KFbxMatrix transform = GetNodeTransform(node);
	
	group->transform = ConvertMatrix(transform);
	group->transformInv = glm::inverse(group->transform);

	// store the triangle data so that we can calcuate a smooth tangent later
	std::vector<std::vector<int>> triangleVertexData;
	std::list<int> triangleIndexData;

	if ( TAN != nullptr )
		triangleVertexData.resize( vertexCount );

	// Read the indices
	KFbxVector4 fbx_vec;
	glm::vec3 vec;

	for (i = 0; i < polygonCount; i++)
	{
		for (j = 0; j < 3; ++j )
		{
			int vertexIndex = fbxMesh->GetPolygonVertex(i, j);

			// make sure this vertex index is part of the current material
			if ( materialIndices ) {
				const int matIdx = materialIndices->GetAt(vertexIndex);
				if ( matIdx != materialIndex && j == 0 )
					break; // index not part of this material, skip
			}
			
			VertexData *v = new VertexData();
			/*
			if ( TAN != nullptr ) {
				triangleVertexData[vertexIndex].push_back( currentIndex + j );
				triangleIndexData.push_back(vertexIndex);
			}*/

			fbx_vec = controlPoints[vertexIndex];
			vec.x = fbx_vec.GetAt(0);
			vec.y = fbx_vec.GetAt(1);
			vec.z = fbx_vec.GetAt(2);

			v->set( POS, vec );

			if ( vec.x < min.x ) min.x = vec.x;
			if ( vec.x > max.x ) max.x = vec.x;
			if ( vec.y < min.y ) min.y = vec.y;
			if ( vec.y > max.y ) max.y = vec.y;
			if ( vec.z < min.z ) min.z = vec.z;
			if ( vec.z > max.z ) max.z = vec.z;

			// Read the texture coordinates
			for ( int uvIndex = 0; uvIndex < uvCount; ++uvIndex )
			{
				KFbxGeometryElementUV* uvElem = fbxMesh->GetElementUV(uvIndex);
				if ( uvElem != NULL && uvDescs.size() > uvIndex )
				{
					VertexDataElement *TEX = uvDescs[uvIndex];
					if ( TEX != nullptr )
					{
						switch (uvElem->GetMappingMode())
						{
							case KFbxGeometryElement::eBY_CONTROL_POINT:
							{
								switch (uvElem->GetReferenceMode())
								{
									case KFbxGeometryElement::eDIRECT:
									{
										v->set(TEX, glm::vec2( uvElem->GetDirectArray().GetAt(i).GetAt(0), 
															uvElem->GetDirectArray().GetAt(i).GetAt(1) ) );
									} break;
									case KFbxGeometryElement::eINDEX_TO_DIRECT:
									{
										v->set(TEX, glm::vec2( uvElem->GetDirectArray().GetAt( uvElem->GetIndexArray().GetAt(i) ).GetAt(0), 
															uvElem->GetDirectArray().GetAt( uvElem->GetIndexArray().GetAt(i) ).GetAt(1) ) );
									} break;
								}
							}
							break;

							case KFbxGeometryElement::eBY_POLYGON_VERTEX:
							{
								switch (uvElem->GetReferenceMode())
								{
									case KFbxGeometryElement::eDIRECT:
									{
										v->set(TEX, glm::vec2( uvElem->GetDirectArray().GetAt(i).GetAt(0), 
															uvElem->GetDirectArray().GetAt(i).GetAt(1) ) );
									} break;
									case KFbxGeometryElement::eINDEX_TO_DIRECT:
									{
										v->set(TEX, glm::vec2( uvElem->GetDirectArray().GetAt( fbxMesh->GetTextureUVIndex(i, j) ).GetAt(0), 
															uvElem->GetDirectArray().GetAt( fbxMesh->GetTextureUVIndex(i, j) ).GetAt(1) ) );
									} break;
								}
							}
							break;
						}
					}
				}
			}
			// Read the normals (only support one normal for now)
			if ( NOR != nullptr ) {
				KFbxVector4 outNormal;
	
				fbxMesh->GetPolygonVertexNormal(i, j, outNormal);
				v->set(NOR, glm::vec3(	outNormal.GetAt(0), 
										outNormal.GetAt(1),
										outNormal.GetAt(2)));
			}

			// animation setup
			if ( skinCount > 0 && m_bIncludeAnimation )
			{
				float bones[4], weights[4];
				memset(&bones, -1, sizeof(float) * 4);
				memset(&weights, 0, sizeof(float) * 4);

				if ( boneIndices[ vertexIndex ].size() > 0 )
				{
					Skeleton *skeleton = m_pMesh->GetSkeleton();
			
					for ( int k = 0; k != boneIndices[ vertexIndex ].size(); ++k )
					{
						if ( k >= 4 ) continue;

						int boneIndex = boneIndices[ vertexIndex ][k];
						double weight = weightIndices[ vertexIndex ][k];
						const Bone *bone = skeleton->GetBone( boneIndex );
					
						bones[k] = static_cast<float>(boneIndex);
						weights[k] = static_cast<float>(weight);
					}

					v->set(SKIN_IDX, (uint8*)&bones);
					v->set(SKIN_WEIGHT, (uint8*)&weights);
				}
			}
			
			// Add the vertex to the Geometry buffer
			pGB->Add( v, pVertexDesc->VertexSize() );
			
			delete v;

			group->indices[indexPtrIndex++] = currentIndex++;
		}
	}

	// Now that the vertex data is read we can set the bounding box
	group->m_aaBoundingBox = new BoundingBox(min, max);

	// Calculate the tangens
	if ( TAN != nullptr ) 
	{
		/*
		std::vector<glm::vec3> tanSum;
		glm::vec3 tan, avgTan;
		int idx0, idx1, idx2;
		uint8 *buf;

		tanSum.resize( triangleIndexData.size() );

		// Calculate the sum of the tangens for each vertex
		std::list<int>::iterator _iter1 = triangleIndexData.begin();
		while ( _iter1 != triangleIndexData.end() )
		{
			idx0 = *_iter1; ++_iter1;
			idx1 = *_iter1; ++_iter1;
			idx2 = *_iter1; ++_iter1;

			// get the tangent of the triangle
			tan = CalculateTangentBiNormal(	pGB->GetAt(triangleVertexData[idx0].at(0)),
											pGB->GetAt(triangleVertexData[idx1].at(0)), 
											pGB->GetAt(triangleVertexData[idx2].at(0)),
											pVertexDesc);
											
			tanSum[idx0] += tan;
			tanSum[idx1] += tan;
			tanSum[idx2] += tan;
		}

		// Calcuate the average tangent and apply it to all vertices that originally had the same index
		// size that changed when we read the data
		for ( i = 0; i < triangleVertexData.size(); ++i ) {

			avgTan = tanSum[i] / (float)triangleVertexData[i].size();

			std::vector<int>::iterator _iter2 = triangleVertexData[i].begin();
			while ( _iter2 != triangleVertexData[i].end() )
			{
				// Apply the avg tan to each vertex associated with this index
				buf = pGB->GetAt( *_iter2 );

				memcpy( buf + TAN->Offset, (uint8 *) &avgTan, TAN->ByteSize );

				++_iter2;
			}
		}
		*/
	}
}

void FbxImporter::ReadLight( KFbxNode *_pNode )
{
	KFbxLight *pFbxLight = (KFbxLight*)_pNode->GetLight();

	// Get the light transform
	glm::mat4x4 mat = ConvertMatrix( GetNodeTransform(_pNode) );

	if ( pFbxLight->LightType == KFbxLight::ePOINT ) {
		PointLight *pPointLight = new PointLight(100, nullptr);
		//pPointLight->SetPosition( mat );
	}
	else if ( pFbxLight->LightType == KFbxLight::eSPOT ) {
		PointLight *pPointLight = new PointLight(100, nullptr);
	
	}
	else if ( pFbxLight->LightType == KFbxLight::eDIRECTIONAL ) {
		PointLight *pPointLight = new PointLight(100, nullptr);
	
	}
}

Material *FbxImporter::GetMaterial( KFbxMesh *fbxMesh, const int materialIndex, std::string &materialName )
{
	AssetsManager *assetsManager = ((StrategyGLScene*)m_pScene)->GetAssetsManager();
	
	materialName = this->FindMaterialName( fbxMesh, materialIndex );

	Material *mtrl = assetsManager->GetMaterial( materialName.c_str() );
	if ( mtrl == nullptr )
		mtrl = assetsManager->GetMaterial("NotFound");

	return mtrl;
}

glm::vec3 FbxImporter::CalculateTangentBiNormal( uint8 *buf0, uint8 *buf1, uint8 *buf2, VertexDescription *desc )
{
	VertexDataElement *elPos = desc->GetElement("InPosition");
	VertexDataElement *elUV = desc->GetElement("InTex0");

	// Read the triangle values we need to calculate the Tangent and BiNormal
	glm::vec3 v0, v1, v2;
	glm::vec2 uv0, uv1, uv2;
	
	memcpy( &v0, buf0 + elPos->Offset, elPos->ByteSize );
	memcpy( &v1, buf1 + elPos->Offset, elPos->ByteSize );
	memcpy( &v2, buf2 + elPos->Offset, elPos->ByteSize );

	memcpy( &uv0, buf0 + elUV->Offset, elUV->ByteSize );
	memcpy( &uv1, buf1 + elUV->Offset, elUV->ByteSize );
	memcpy( &uv2, buf2 + elUV->Offset, elUV->ByteSize );

	// Now calculate the tangent and binormal
	glm::vec3 tan;

	glm::vec3 pos1 = v1 - v0;
	glm::vec3 pos2 = v2 - v0;

	glm::vec2 tex1 = uv1 - uv0;
	glm::vec2 tex2 = uv2 - uv0;

	float r = 1.0f / (tex1.x * tex2.y - tex1.y * tex2.x);

	tan = ( (pos1 * tex2.y) - (pos2 - tex1.y) ) * r;

	return tan;
}

std::string FbxImporter::FindMaterialName( KFbxMesh *fbxMesh, const int materialIndex )
{
	AssetsManager *assetsManager = ((StrategyGLScene*)m_pScene)->GetAssetsManager();
	
	// Find the name of the material for this node, just interested in the first one that will
	// be used to find a shader and vertex description
	std::string materialName("");
	
	const int materialCount = fbxMesh->GetNode()->GetMaterialCount();
	int idx = materialIndex;

	if ( idx > materialCount )
		idx = materialCount;
	
	int a = fbxMesh->GetLayerCount();
	int test = fbxMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetCount();

	materialName = fbxMesh->GetNode()->GetMaterial(idx)->GetName();

	return materialName;
}

void FbxImporter::ProcessBoneAnimation( Bone *bone, KFbxNode *node )
{
	//http://apoc3dcpp.googlecode.com/svn/trunk/APBuild/MeshBuild/FbxImporter.cpp

	// Read the animation
	KArrayTemplate<KString*> animStackNameArray;
	m_pFbxScene->FillAnimStackNameArray(animStackNameArray);
	const int animStackCount = animStackNameArray.GetCount();
	int i;

	float fFrameRate = (float)KTime::GetFrameRate(m_pFbxScene->GetGlobalSettings().GetTimeMode());

	//printf("Skeleton name: %s\n", node->GetName() );
    
	for(i = 0; i < animStackCount; i++)
	{
		KFbxAnimStack *stack = m_pFbxScene->FindMember(FBX_TYPE(KFbxAnimStack), animStackNameArray[i]->Buffer());
		
		if ( stack == NULL ) continue;

		m_pFbxScene->GetEvaluator()->SetContext(stack);
		m_pFbxScene->ActiveAnimStackName.Set(*animStackNameArray[i]);
		
		KFbxTakeInfo *takeInfo = m_pFbxScene->GetTakeInfo(*(animStackNameArray[i]));
		//printf("Process take: \"%s\"\n", takeInfo->mName.Buffer());
		
		MeshAnimation *animation = nullptr;
		if ( m_Animations[takeInfo->mName.Buffer()] ) {
			animation = m_Animations[takeInfo->mName.Buffer()];
		}
		else {
			animation = new MeshAnimation();
			animation->Name = takeInfo->mName.Buffer();
			animation->Duration = 0;

			m_Animations[animation->Name] = animation;
			m_pMesh->AddAnimation( animation );
		}

		KFbxAnimLayer *layer = stack->GetMember(FBX_TYPE(KFbxAnimLayer), 0);
		
		KTime timeStart = takeInfo->mLocalTimeSpan.GetStart();
		KTime timeEnd = takeInfo->mLocalTimeSpan.GetStop();

		float fStart = static_cast<float>(timeStart.GetSecondDouble());
		float fEnd = static_cast<float>(timeEnd.GetSecondDouble());

		if( fStart < fEnd )
		{
			double fTime = 0;
			while( fTime <= fEnd )
			{
				KTime takeTime;
				takeTime.SetSecondDouble(fTime);

				AnimationKeyFrame *frame = new AnimationKeyFrame();
				frame->Time = static_cast<float>(fTime);
				frame->LocalTransform = ConvertMatrix(m_pFbxScene->GetEvaluator()->GetNodeLocalTransform(node, takeTime)); //GetAbsoluteTransformFromCurrentTake(node, takeTime);
				frame->GlobalTransform = ConvertMatrix(m_pFbxScene->GetEvaluator()->GetNodeGlobalTransform(node, takeTime)); //GetAbsoluteTransformFromCurrentTake(node, takeTime);
				fTime += 1.0f/fFrameRate;

				animation->AddKeyFrame( bone, frame );
			}
		}
	}
}

void FbxImporter::ReadSkeleton( KFbxNode *node )
{
	// http://www.assembla.com/code/vapor/subversion/nodes/src/Pipeline/ImporterFBX.cpp
	// http://code.google.com/p/meshimport/source/browse/trunk/src/MeshImportFBX/FBXSceneFuncs.cpp
	KFbxSkeleton *fbxSkeleton = (KFbxSkeleton*) node->GetSkeleton();

	m_pSkeleton = new Skeleton();
	m_pMesh->SetSkeleton(m_pSkeleton);
		
	ReadSkeletonRecursive( fbxSkeleton->GetNode(), -1, m_pSkeleton );
}

void FbxImporter::ReadSkeletonRecursive( KFbxNode *node, int parentIndex, Skeleton *skeleton )
{
	int numChildren;

	KFbxSkeleton *fbxSkeleton = (KFbxSkeleton*) node->GetNodeAttribute();
	if ( fbxSkeleton == nullptr ) {
		numChildren = node->GetChildCount();
		return;
	}

	KFbxSkeleton::ESkeletonType type;
	type = fbxSkeleton->GetSkeletonType();
	
	Bone *bone = new Bone();
	bone->Name = std::string( node->GetName() );
	bone->Index = skeleton->Size();
	bone->ParentIndex = parentIndex;
	bone->DebugColor = glm::vec3( (float)(255*rand()/(RAND_MAX + 1.0))/255, (float)(255*rand()/(RAND_MAX + 1.0))/255, (float)(255*rand()/(RAND_MAX + 1.0))/255 );

	ProcessBoneAnimation(bone, node);

	m_FbxSkeletonMap[node] = bone;

	KFbxNodeAttribute::EAttributeType attributeType;
	attributeType = node->GetNodeAttribute()->GetAttributeType();
	
	// Read the animation
	//KTime takeTime;
	//takeTime.SetSecondDouble(3.5);
	//bone->GlobalMatrix = GetAbsoluteTransformFromCurrentTake(node, takeTime); //ConvertMatrix( scene->GetEvaluator()->GetNodeGlobalTransform(node, 4.6141043, KFbxNode::eSOURCE_SET, false, true) );
	//bone->LocalMatrix = ConvertMatrix( scene->GetEvaluator()->GetNodeLocalTransform(node) );
	/*
	bone->DebugShape = new CubeObject( 0.5, 0.5, 0.5, NULL );
	bone->DebugShape->SetColor( bone->DebugColor );
	bone->DebugShape->SetTransformMatrix( ConvertMatrix(scene->GetEvaluator()->GetNodeGlobalTransform(node)) );
	bone->DebugShape->Init(m_pScene, NULL);
	*/
	skeleton->AddBone( bone );

	numChildren = node->GetChildCount();
	for ( int i = 0; i < numChildren; ++i )
	{
		KFbxNode *child = node->GetChild( i );
		ReadSkeletonRecursive( child, bone->Index, skeleton );
	}
}

glm::mat4x4 FbxImporter::GetAbsoluteTransformFromCurrentTake(KFbxNode* node, KTime time)
{
	KFbxMatrix matGlobal = node->EvaluateGlobalTransform(time);
	KFbxVector4 T = node->GetGeometricTranslation(KFbxNode::eSOURCE_SET);
	KFbxVector4 R = node->GetGeometricRotation(KFbxNode::eSOURCE_SET);
	KFbxVector4 S = node->GetGeometricScaling(KFbxNode::eSOURCE_SET);

	KFbxXMatrix matFBXGeometryOffset;
	matFBXGeometryOffset.SetIdentity();
	matFBXGeometryOffset.SetT(T);
	matFBXGeometryOffset.SetR(R);
	matFBXGeometryOffset.SetS(S);

	return ConvertMatrix( matGlobal * matFBXGeometryOffset );
}

glm::mat4x4 FbxImporter::ConvertMatrix( KFbxMatrix src )
{
	glm::mat4x4 dst = glm::mat4();
	
	for( int r = 0; r < 4; ++r )
	{
		for( int c = 0; c < 4; ++c )
		{
			dst[c][r] = static_cast<float>(src.Get(c,r));
		}
	}
	return dst;
}

KFbxNode* FbxImporter::FindRoot(KFbxNode* root, KFbxNodeAttribute::EAttributeType attrib)
{
	if ( root->GetNodeAttribute() )
	{
		if ( root->GetNodeAttribute()->GetAttributeType() == attrib )
			return root;
	}

    uint32 numChildren = root->GetChildCount();
    if(!numChildren)
        return NULL;

    KFbxNode* child = NULL;
    for(unsigned c = 0; c < numChildren; c++)
    {
        child = root->GetChild(c);
        if(!child->GetNodeAttribute())
            continue;
        if(child->GetNodeAttribute()->GetAttributeType() != attrib)
            continue;
        
        return child;
    }

    KFbxNode* rootJoint = NULL;
    for(unsigned c = 0; c < numChildren; c++)
    {
        child = root->GetChild(c);
        rootJoint = FindRoot(child, attrib);
        if(rootJoint)
            break;
    }
    return rootJoint;
}

KFbxMatrix FbxImporter::GetNodeTransform(KFbxNode *node)
{
	return m_pFbxScene->GetEvaluator()->GetNodeLocalTransform(node);
}

void FbxImporter::TriangulateNode( KFbxNode *node )
{
	KFbxMesh* mesh = (KFbxMesh*) node->GetNodeAttribute();
	if( mesh->IsTriangleMesh())
		return;

	KFbxGeometryConverter converter( FbxManager::Inst()->SdkManager() );
	bool status = converter.TriangulateInPlace(node);

	if( !status)
		StdConsole::LogError( _STR"TriangulateNode() - failed to triangulate!");
}