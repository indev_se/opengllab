#pragma once

class InputEventListener
{
public:
	InputEventListener(void);
	~InputEventListener(void);

	virtual void Event_KeyUp( uint16 key ) {}
	virtual void Event_KeyDown( uint16 key ) {}

	virtual void Event_MouseDelta( int16 deltax, int16 deltay ) {}
};

typedef void (InputEventListener::*InputEventKeyFuncPtr) ( uint16 );
typedef void (InputEventListener::*InputEventDeltaFuncPtr) ( uint16, uint16 );

