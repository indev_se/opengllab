#include "StdAfx.h"
#include "DeferredRenderFinal.h"


DeferredRenderFinal::DeferredRenderFinal(void)
{
	m_pShader = NULL;
}

DeferredRenderFinal::~DeferredRenderFinal(void)
{
	if (m_pShader)
		delete m_pShader;

	m_pShader = NULL;
	m_pFBOs = NULL;
	m_pLightFBO = NULL;
}

void DeferredRenderFinal::Init( GLScene *scene, DeferredFramebuffers *fbo, ColorFramebufferObject *shadowFBO, ColorFramebufferObject *lightFBO, std::vector<LightObject*> globalLights )
{
	RenderPass::Init(scene);
	m_pFBOs = fbo;
	m_pLightFBO = lightFBO;
	m_pShadowFBO = shadowFBO;

	m_GlobalLights = globalLights;

	AssetsManager *assets = scene->GetAssetsManager();

	m_pShader = assets->CreateShaderProgram("DeferredFinalShader");
	m_pShader->CreateProgram( "Data/Shaders/DeferredFinal.vert", "Data/Shaders/DeferredFinal.frag" );

	TextureInfo *info = new TextureInfo();
	info->genMipMap = false;

	TextureInfoParam infoParam1 = { GL_TEXTURE_WRAP_S, GL_REPEAT };
	info->params.push_back( &infoParam1 );
	TextureInfoParam infoParam2 = { GL_TEXTURE_WRAP_T, GL_REPEAT };
	info->params.push_back( &infoParam2 );
	TextureInfoParam infoParam3 = { GL_TEXTURE_MAG_FILTER, GL_NEAREST };
	info->params.push_back( &infoParam3 );
	TextureInfoParam infoParam4	= { GL_TEXTURE_MIN_FILTER, GL_NEAREST };
	info->params.push_back( &infoParam4 );

	m_pScene->GetAssetsManager()->CreateBitmapDataAsset( "ssao_noise", "Data/Textures/ssao_noise.png" );
	
	m_pSSAONoiseTexture = m_pScene->GetAssetsManager()->CreateTextureAsset("SSAONoiseTexture");
	m_pSSAONoiseTexture->CreateFromBitmapData( m_pScene->GetAssetsManager()->GetBitmapDataAsset("ssao_noise"), info );


}

void DeferredRenderFinal::Bind()
{
	glEnable(GL_TEXTURE_2D);

	glUseProgramObjectARB(m_pShader->Program());
	
	// Bind the light to the shader (only one global light for now, the sun as a spotlight)
	m_GlobalLights[0]->SetShaderUniform( "GlobalLight0", m_pShader );

	// Set the uniform matrices
	glm::mat4x4 matView = m_pScene->GetCamera()->GetViewMatrix();
		
	m_pShader->SendUniformMatrix4fv("matView", 1, false, glm::value_ptr(matView));
	
	// Bind the texture uniforms
	m_pShader->SendUniform1i("InColorTexture", 0);
	m_pShader->SendUniform1i("InDepthTexture", 1);
	m_pShader->SendUniform1i("InLightTexture", 2);
	m_pShader->SendUniform1i("InPositionTexture", 3);
	m_pShader->SendUniform1i("InNormalTexture", 4);
	m_pShader->SendUniform1i("InShadowTexture", 5);
	m_pShader->SendUniform1i("InSSAONoiseTexture", 6);
	
	// Set the textures
	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_pFBOs->GetColorTexture()->TextureID());
	
	glActiveTextureARB(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_pFBOs->GetDepthTexture()->TextureID());

	glActiveTextureARB(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_pLightFBO->GetTextureId());

	glActiveTextureARB(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_pFBOs->GetPositionTexture()->TextureID());

	glActiveTextureARB(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_pFBOs->GetNormalTexture()->TextureID());

	glActiveTextureARB(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, m_pShadowFBO->GetTextureId());

	glActiveTextureARB(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, m_pSSAONoiseTexture->TextureID());
}

void DeferredRenderFinal::Unbind()
{
	glUseProgramObjectARB(0);

	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glActiveTextureARB(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTextureARB(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTextureARB(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTextureARB(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glActiveTextureARB(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTextureARB(GL_TEXTURE0);
	glDisable(GL_TEXTURE_2D);
}

void DeferredRenderFinal::BindObject( SceneObject *object )
{
}

void DeferredRenderFinal::UnbindObject( SceneObject *object )
{
}
