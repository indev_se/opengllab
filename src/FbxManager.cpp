#include "StdAfx.h"
#include "FbxManager.h"

FbxManager *FbxManager::m_pInst = NULL;
FbxManager::FbxManager(void)
{
	CreateManager();
}

FbxManager::~FbxManager(void)
{
}

FbxManager *FbxManager::Inst()
{
	if ( m_pInst == NULL )
		m_pInst = new FbxManager();

	return m_pInst;
}

void FbxManager::AllocateScene( KFbxScene*& pScene )
{
	pScene = KFbxScene::Create(m_pSdkManager,"");
}

bool FbxManager::LoadScene(KFbxDocument* pScene, const char* pFilename, bool includeAnimation)
{
	int lFileMajor, lFileMinor, lFileRevision;
    int lSDKMajor,  lSDKMinor,  lSDKRevision;
    //int lFileFormat = -1;
    int i, lAnimStackCount;
    bool lStatus;

    // Get the file version number generate by the FBX SDK.
    KFbxSdkManager::GetFileFormatVersion(lSDKMajor, lSDKMinor, lSDKRevision);

    // Create an importer.
    KFbxImporter* lImporter = KFbxImporter::Create(m_pSdkManager,"");

	// Initialize the importer by providing a filename.
    const bool lImportStatus = lImporter->Initialize(pFilename, -1, m_pSdkManager->GetIOSettings());
    lImporter->GetFileVersion(lFileMajor, lFileMinor, lFileRevision);

	if( !lImportStatus )
    {
        return false;
    }

	if (lImporter->IsFBX())
    {
        StdConsole::LogInfo( _STR"FBX version number for file "<<pFilename<<" is "<<lFileMajor<<"."<<lFileMinor<<"."<<lFileRevision);

        StdConsole::LogInfo( _STR"Animation Stack Information");

        lAnimStackCount = lImporter->GetAnimStackCount();

        StdConsole::LogInfo( _STR"    Number of Animation Stacks: "<< lAnimStackCount);
        StdConsole::LogInfo( _STR"    Current Animation Stack: "<< lImporter->GetActiveAnimStackName().Buffer() );
        printf("\n");

        for(i = 0; i < lAnimStackCount; i++)
        {
            KFbxTakeInfo* lTakeInfo = lImporter->GetTakeInfo(i);

            StdConsole::LogInfo( _STR"    Animation Stack " << i);
            StdConsole::LogInfo( _STR"         Name: "<<lTakeInfo->mName.Buffer());
            StdConsole::LogInfo( _STR"         Description: " << lTakeInfo->mDescription.Buffer());

            // Change the value of the import name if the animation stack should be imported 
            // under a different name.
            StdConsole::LogInfo( _STR"         Import Name: " << lTakeInfo->mImportName.Buffer());

            // Set the value of the import state to false if the animation stack should be not
            // be imported. 
            StdConsole::LogInfo( _STR"         Import State: "<< (lTakeInfo->mSelect ? "true" : "false") );
            
        }

        // Set the import states. By default, the import states are always set to 
        // true. The code below shows how to change these states.
        IOS_REF.SetBoolProp(IMP_FBX_MATERIAL,        true);
        IOS_REF.SetBoolProp(IMP_FBX_TEXTURE,         true);
        IOS_REF.SetBoolProp(IMP_FBX_LINK,            true);
        IOS_REF.SetBoolProp(IMP_FBX_SHAPE,           true);
        IOS_REF.SetBoolProp(IMP_FBX_GOBO,            true);
        IOS_REF.SetBoolProp(IMP_FBX_ANIMATION,       includeAnimation);
        IOS_REF.SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, true);
    }

    // Import the scene.
    lStatus = lImporter->Import(pScene);

	return lStatus;
}

void FbxManager::CreateManager()
{
	m_pSdkManager = KFbxSdkManager::Create();
	if (!m_pSdkManager)
    {
        StdConsole::LogError( _STR"Unable to create the FBX SDK manager");
        exit(0);
    }

	// create an IOSettings object
	KFbxIOSettings * ios = KFbxIOSettings::Create(m_pSdkManager, IOSROOT);
	m_pSdkManager->SetIOSettings(ios);
}