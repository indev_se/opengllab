#include "StdAfx.h"
#include "BoundingSphere.h"


BoundingSphere::BoundingSphere(void)
{
	m_vecCenter = glm::vec3(0.0f);
	m_fRadius = 1.0f;
}

BoundingSphere::BoundingSphere(const glm::vec3 &center, const float radius)
{
	m_vecCenter = center;
	m_fRadius = radius;
}

void BoundingSphere::Multiply( const glm::mat4x4 &transformMatrix )
{
	glm::vec4 center( m_vecCenter.x, m_vecCenter.y, m_vecCenter.z, 1 );

	center = transformMatrix * center;

	m_vecCenter.x = center.x;
	m_vecCenter.y = center.y;
	m_vecCenter.z = center.z;

	// multiply radius with scale here
}

void BoundingSphere::SetValues(const glm::vec3 &center, const float radius)
{
	m_vecCenter = center;
	m_fRadius = radius;
}

void BoundingSphere::GetValues(glm::vec3 &center, float &radius)
{
	center = m_vecCenter;
	radius = m_fRadius;
}

BoundingSphere::~BoundingSphere(void)
{
}

BoundingSphere* BoundingSphere::Clone()
{
	return new BoundingSphere( m_vecCenter, m_fRadius );
}
