#include "StdAfx.h"
#include "Mesh.h"
#include "MeshAnimation.h"
#include "FbxImporter.h"
#include "AssetsManager.h"
#include "StrategyGLScene.h"
#include "AnimatedMeshGroupObject.h"
#include "SceneObjectFactory.h"
#include "FbxFileContext.h"
#include "AnimatedMeshObject.h"

AnimatedMeshObject::AnimatedMeshObject( const char *objectName ) : SceneObject(objectName)
{
	CacheType = CACHE_ANIMATEDMESH;
}

AnimatedMeshObject::~AnimatedMeshObject(void)
{
}

bool AnimatedMeshObject::LoadFromFile( const char *filename )
{
	FbxImporter *importer = new FbxImporter();
	m_pFbxContext = importer->LoadFromFile( filename, m_pScene, true );
	m_pMesh = m_pFbxContext->GetMesh();
	delete importer;

	if ( m_pMesh != nullptr )
	{
		// Create the children
		MeshGroup *meshgroup;
		for( uint32 i = 0; i < m_pMesh->GetGroups().size(); ++i ) 
		{
			meshgroup = m_pMesh->GetGroups()[i];

			AnimatedMeshGroupObject *child = new AnimatedMeshGroupObject(meshgroup->name.c_str());
			child->Init(m_pScene);
			child->Create(meshgroup, m_pMesh, m_pMesh->GetGeometryBuffer(meshgroup));

			this->AddChild(child);
		}
	}

	if ( m_pMesh != nullptr ) {
		SaveCachedVersion(filename);
		return true;
	}

	return m_pMesh != nullptr;
}

bool AnimatedMeshObject::LoadFromCache( const char *cachedFilename )
{
	std::ifstream ifs(cachedFilename, std::ios::binary);

	SREAD(ifs, CacheType);

	m_pFbxContext = new FbxFileContext();
	ifs >> *m_pFbxContext;

	m_pMesh = m_pFbxContext->GetMesh();

	if ( m_pMesh != nullptr )
	{
		// Create the children
		MeshGroup *meshgroup;
		for( uint32 i = 0; i < m_pMesh->GetGroups().size(); ++i ) 
		{
			meshgroup = m_pMesh->GetGroups()[i];

			AnimatedMeshGroupObject *child = new AnimatedMeshGroupObject(meshgroup->name.c_str());
			child->Init(m_pScene);
			child->Create(meshgroup, m_pMesh, m_pMesh->GetGeometryBuffer(meshgroup));

			this->AddChild(child);
		}
	}

	return true;
}

void AnimatedMeshObject::SaveCachedVersion(const char *filename)
{
	std::string cachedFilename = SceneObjectFactory::Inst()->GetCacheFilepath(filename);
	std::ofstream ofs(cachedFilename, std::ios::binary);

	SWRITE(ofs, CacheType)

	ofs << *m_pFbxContext;

	ofs.close();
}

void AnimatedMeshObject::SetAnimationByName( std::string name, bool loop )
{
	MeshAnimation *anim = m_pMesh->GetAnimation( name );
	if ( anim == nullptr ) {
		StdConsole::LogWarning( _STR"Found no animation with name: " << name );
		return;
	}

	SetAnimationState( new AnimationState(anim, loop) );
}

void AnimatedMeshObject::SetAnimationState( AnimationState* state )
{
	m_pAnimationState = state;

	for( uint32 i = 0; i < m_Children.size(); ++i ) 
		((AnimatedMeshGroupObject*)m_Children[i])->SetAnimationState( state );
}

AnimationState* AnimatedMeshObject::GetAnimationState()
{
	return m_pAnimationState;
}

void AnimatedMeshObject::DeleteAnimationState()
{
	if ( m_pAnimationState != nullptr )
		delete m_pAnimationState;

	m_pAnimationState = nullptr;
	SetAnimationState(nullptr);
}

Mesh* AnimatedMeshObject::GetMesh()
{
	return m_pMesh;
}

void AnimatedMeshObject::Init( GLScene *scene )
{
	m_pScene = scene;
}

void AnimatedMeshObject::Update( float tickMS )
{
	if ( m_pAnimationState != nullptr )
		m_pAnimationState->Update(tickMS);

	SceneObject::Update(tickMS);
}

void AnimatedMeshObject::Render(RenderPass *pass)
{
	SceneObject::Render(pass);
}