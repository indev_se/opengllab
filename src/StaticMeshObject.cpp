#include "StdAfx.h"
#include "StrategyGLScene.h"
#include "FbxImporter.h"
#include "Mesh.h"
#include "SceneObjectFactory.h"
#include "StaticMeshGroupObject.h"
#include "FbxFileContext.h"
#include "StaticMeshObject.h"


StaticMeshObject::StaticMeshObject( const char *objectName ) : 
	SceneObject(objectName), 
	CachedObject()
{
	CacheType = CACHE_STATICMESH;
}

StaticMeshObject::~StaticMeshObject(void)
{
}

bool StaticMeshObject::LoadFromFile( const char *filename )
{
	FbxImporter *importer = new FbxImporter();
	m_pFbxContext = importer->LoadFromFile( filename, m_pScene, false );
	m_pMesh = m_pFbxContext->GetMesh();

	if ( m_pMesh != nullptr )
	{
		// Create the children
		MeshGroup *meshgroup;
		for( uint32 i = 0; i < m_pMesh->GetGroups().size(); ++i ) 
		{
			meshgroup = m_pMesh->GetGroups()[i];

			StaticMeshGroupObject *child = new StaticMeshGroupObject(meshgroup->name.c_str());
			child->Init(m_pScene);
			child->Create(meshgroup, m_pMesh->GetGeometryBuffer(meshgroup));

			this->AddChild(child);
		}
		
		if ( m_pMesh->GetBoundingBox() )
			m_pAABB = m_pMesh->GetBoundingBox()->Clone();

		SaveCachedVersion(filename);
		return true;
	}

	return false;
}

bool StaticMeshObject::LoadFromCache( const char *cachedFilename )
{
	std::ifstream ifs(cachedFilename, std::ios::binary);

	SREAD(ifs, CacheType);

	m_pFbxContext = new FbxFileContext();
	ifs >> *m_pFbxContext;

	m_pMesh = m_pFbxContext->GetMesh();

	if ( m_pMesh != nullptr )
	{
		// Create the children
		MeshGroup *meshgroup;
		for( uint32 i = 0; i < m_pMesh->GetGroups().size(); ++i ) 
		{
			meshgroup = m_pMesh->GetGroups()[i];

			StaticMeshGroupObject *child = new StaticMeshGroupObject(meshgroup->name.c_str());
			child->Init(m_pScene);
			child->Create(meshgroup, m_pMesh->GetGeometryBuffer(meshgroup));

			this->AddChild(child);
		}

		if ( m_pMesh->GetBoundingBox() )
			m_pAABB = m_pMesh->GetBoundingBox()->Clone();
	}

	return true;
}

void StaticMeshObject::SaveCachedVersion(const char *filename)
{
	std::string cachedFilename = SceneObjectFactory::Inst()->GetCacheFilepath(filename);
	std::ofstream ofs(cachedFilename, std::ios::binary);

	SWRITE(ofs, CacheType)

	ofs << *m_pFbxContext;

	ofs.close();
}

void StaticMeshObject::Init( GLScene *scene )
{
	m_pScene = scene;
}

void StaticMeshObject::Update( float tickMS )
{
	SceneObject::Update(tickMS);
}

void StaticMeshObject::Render(RenderPass *pass)
{
	SceneObject::Render(pass);
}