#pragma once
#include "GLCamera.h"
#include "VertexBufferFixed.h"

struct PatchVertex
{
	float x, y, z;
	float nx, ny, nz;
	float s0, t0; // tex0
	float s1, t1; // tex1
	float tanx, tany, tanz; // tex2
	float padding[3]; // pad to 64 bytes
};

class GeoMipMapPatch
{
public:
	GeoMipMapPatch(void);
	~GeoMipMapPatch(void);

	UINT m_LOD;
	UINT m_MinLOD;

	unsigned int *m_Indices;
	unsigned int m_IndexCount;

	glm::vec3	m_vCenter;
	glm::vec3	m_vMin;
	glm::vec3	m_vMax;

	bool		m_Visible;

	BoundingBox *m_BB;

	GeoMipMapPatch  *m_Top,
					*m_Right,
					*m_Bottom,
					*m_Left;
};

class GeoMipMapTerrain
{
public:
	GeoMipMapTerrain(void);
	~GeoMipMapTerrain(void);

	void Init(unsigned char *HeightMap, UINT Size, UINT PatchSize, float Step, float Scale, int SmoothingPasses );
	void Update( float tickMS );
	void Render();
	void RenderDebugBB();
	void RenderDebugNormals();

	void SetCamera( GLCamera *Camera );

protected:

	void Dispose();
	void CalculateLOD();
	void UpdateIndices();

	void BoxFilterHeightMap(int SmoothingPasses);

	UINT			*m_LODDistances;
	unsigned char	*m_HeightMap;
	float			*m_SmoothHeightMap;
	UINT			m_PatchSize;
	float			m_Step;
	float			m_Scale;
	UINT			m_MaxLOD;
	UINT			m_PatchCount;
	UINT			m_Size;

	GeoMipMapPatch  *m_Patches;
	PatchVertex		*m_VertexData;
	GLCamera		*m_Camera;

	VertexBufferFixed	*m_VBO;

private:

	inline unsigned int getIndex( UINT PatchX, UINT PatchZ, UINT X, UINT Z, GeoMipMapPatch *patch );

};

