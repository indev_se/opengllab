#include "StdAfx.h"
#include "Plane.h"


Plane::Plane(void)
{

}

Plane::~Plane(void)
{
}

void Plane::CalculatePlane( const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2 )
{
	float rx1 = v1.x - v0.x;
	float ry1 = v1.y - v0.y;
	float rz1 = v1.z - v0.z;
			
	float rx2 = v2.x - v0.x;
	float ry2 = v2.y - v0.y;
	float rz2 = v2.z - v0.z;

	m_vecNormal.x = ry1 * rz2 - ry2 * rz1;
	m_vecNormal.y = rz1 * rx2 - rz2 * rx1;
	m_vecNormal.z = rx1 * ry2 - rx2 * ry1;
			
	m_vecNormal /= m_vecNormal.length();
			
	m_fDistance = m_vecNormal.x * v1.x + m_vecNormal.y * v1.y + m_vecNormal.z * v1.z;
}

void Plane::SetCoefficients( float a, float b, float c, float d )
{
	// set the normal vector
	m_vecNormal.x	= a;
	m_vecNormal.y	= b;
	m_vecNormal.z	= c;

	// Set the distance
	m_fDistance		= d;
			
	Normalize();
}

void Plane::Normalize()
{
	//compute the length of the vector
	float length = m_vecNormal.length();
	if ( length == 0 ) return;
			
	// normalize
	m_vecNormal /= length;
	m_fDistance /= length;
}

float Plane::DistanceToPlane(const glm::vec3 &v)
{
	return glm::dot(m_vecNormal, v) + m_fDistance;
}

ClassificationPointsEnum Plane::ClassifyPoint(const glm::vec3 &v)
{
	float distance = DistanceToPlane(v);

	if ( distance > PLANE_DENOM )
		return CP_FRONT;
	else if ( distance < -PLANE_DENOM )
		return CP_BACK;

	return CP_ONPLANE;
}

ClassificationPointsEnum Plane::ClassifyPoints( std::list<const glm::vec3> vertices )
{
	int numpos = 0;
	int numneg = 0;

	std::list<const glm::vec3>::iterator iter;

	for( iter = vertices.begin(); iter != vertices.end(); iter++ )
	{
		int point = ClassifyPoint( (*iter) );
		if( point == CP_FRONT )
			numpos++;
		else if( point == CP_BACK )
			numneg++;
	}

	if( numpos > 0 && numneg == 0 )
		return CP_FRONT;
	else if( numpos == 0 && numneg > 0 )
		return CP_BACK;
	else if( numpos > 0 && numneg > 0 )
		return CP_SPLIT;
	else
		return CP_ONPLANE;

}

bool Plane::GetPointIntersection( Plane &p1, Plane &p2, Plane &p3, glm::vec3 *intersectionPoint )
{
	// We're only interested in the result if the planes give us a single point intersection
	glm::vec3 cx = glm::cross(p2.GetNormal(), p3.GetNormal());
	float f = glm::dot(p1.GetNormal(), cx );
	if ( f == 0 )
		return false; // These planes do not intersect into a single point

	// Find the intersected point
	glm::vec3 cx1 = -p1.GetDistance() * glm::cross( p2.GetNormal(), p3.GetNormal() );
	glm::vec3 cx2 = p2.GetDistance() * glm::cross( p3.GetNormal(), p1.GetNormal() );
	glm::vec3 cx3 = p3.GetDistance() * glm::cross( p1.GetNormal(), p2.GetNormal() );

	cx = cx1 - cx2 - cx3;

	*intersectionPoint = cx / f;

	return true;
}