#pragma once
#include "sceneobject.h"
class NodeObject :
	public SceneObject
{
public:
	NodeObject(const char *objectName);
	~NodeObject(void);

	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);
};

