#pragma once
#include "RenderPass.h"
#include "SphereObject.h"
#include "ColorFramebufferObject.h"
#include "DeferredFramebuffers.h"

class DeferredRenderLightPass :
	public RenderPass
{
public:
	DeferredRenderLightPass(void);
	~DeferredRenderLightPass(void);

	void Init( GLScene *scene, DeferredFramebuffers *deferredFBO );
	void Bind();
	void Unbind();
	
	void BindObject( SceneObject *object );
	void UnbindObject( SceneObject *object );

	void BindJob( RenderJob *job ) {}
	void UnbindJob( RenderJob *job ) {}

	ColorFramebufferObject *GetFBO() { return m_pFBO; }

protected:

	DeferredFramebuffers *m_pDeferredFBO;
	ColorFramebufferObject *m_pFBO;

	uint32 m_iDepthFBOID;

	SphereObject* m_pPointLightVolume;
};

