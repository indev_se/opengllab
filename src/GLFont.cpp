#include "StdAfx.h"
#include "GLFont.h"


GLFont::GLFont(void)
{
}


GLFont::~GLFont(void)
{
}

void GLFont::Init(const char *fontName, unsigned int fontSize)
{
	m_iFontSize = fontSize;
	m_pGlyphTextures = new GLuint[128]; // Max 128 characters for now
    FT_Face face;

	// Create And Initilize A FreeType Font Library.
    FT_Library library;
    if (FT_Init_FreeType( &library )) {
        StdConsole::LogError( _STR"FT_Init_FreeType failed");
		return;
	}

	// Load Face
	if (FT_New_Face( library, fontName, 0, &face )) {
        StdConsole::LogError( _STR"FT_New_Face failed (there is probably a problem with your font file)");
		return;
	}
	
	
	// Select charmap
    if( FT_Select_Charmap( face, FT_ENCODING_UNICODE ) )
    {
        StdConsole::LogError( _STR"FT_Select_Charmap failed");
		return;
    }
	
	
	// Set character size
    FT_Set_Char_Size( face, m_iFontSize << 6, m_iFontSize << 6, 72, 72);

	// Here We Ask OpenGL To Allocate Resources For
    // All The Textures And Display Lists Which We
    // Are About To Create. 
	m_iGlyphLists = glGenLists(128);
    glGenTextures( 128, m_pGlyphTextures );

	// This Is Where We Actually Create Each Of The Font Glyphs Display Lists.
	const wchar_t *chars = L"!\"#$%&'()*+,-./0123456789:;<=>?"
                           L"@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
                           L"`abcdefghijklmnopqrstuvwxyz{|}~";

	MakeGlyphLists(face, chars);
}

void GLFont::MakeGlyphLists( FT_Face face, const wchar_t * charcodes )
{	
	// Create the display list for each glyph (this could be made so much nicer, VBO for starter, but works for now)
    for( unsigned int i=0; i < 128; ++i )
	{
		unsigned int index = FT_Get_Char_Index( face, i );

		// Load The Glyph For Our Character.
		if(FT_Load_Glyph( face, index, FT_LOAD_DEFAULT )) {
			StdConsole::LogError( _STR"FT_Load_Glyph failed");
			return;
		}

		// Move the Face Glyph into a glyph object of its own
		FT_Glyph glyph;
		if(FT_Get_Glyph( face->glyph, &glyph )) {
			StdConsole::LogError( _STR"FT_Get_Glyph failed");
			return;
		}

		// Convert the glyph to a bitmap
		FT_Glyph_To_Bitmap( &glyph, FT_RENDER_MODE_NORMAL, 0, 1 );
		FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph)glyph;

		// Get the bitmap
		FT_Bitmap& bitmap = bitmap_glyph->bitmap;

		int glyphWidth = bitmap.width;
		int glyphHeight = bitmap.rows;

		// Allocate Memory For The Texture Data.
		unsigned char* textureData = new unsigned char[glyphWidth * glyphHeight];
		memset( textureData, 0, glyphWidth * glyphHeight * sizeof(unsigned char));

		// Copy the data from the bitmap the a temporary array
		memcpy( textureData, bitmap.buffer, bitmap.width * bitmap.rows * sizeof(unsigned char) );
		
		// Now We Just Setup Some Texture Parameters.
		glPixelStorei( GL_PACK_ALIGNMENT, 1 );
		glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

		glBindTexture( GL_TEXTURE_2D, m_pGlyphTextures[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexImage2D( GL_TEXTURE_2D, 0, GL_ALPHA, glyphWidth, glyphHeight, 0, GL_ALPHA, GL_UNSIGNED_BYTE, textureData );

		// cleanup
		delete [] textureData;

		// Now We Create The Display List
		glNewList(m_iGlyphLists+i, GL_COMPILE);

		glBindTexture(GL_TEXTURE_2D, m_pGlyphTextures[i]);
 
		glPushMatrix();
 
		glTranslatef(bitmap_glyph->left,0,0);
		glTranslatef(0,bitmap_glyph->top-bitmap.rows,0);

		float x = 1;//(float)glyphWidth;
		float y = 1;//(float)glyphHeight;

		glBegin(GL_QUADS);
		glTexCoord2d(0,0); glVertex2f(0,bitmap.rows);
		glTexCoord2d(0,y); glVertex2f(0,0);
		glTexCoord2d(x,y); glVertex2f(bitmap.width,0);
		glTexCoord2d(x,0); glVertex2f(bitmap.width,bitmap.rows);
		glEnd();

		glPopMatrix();

		glTranslatef( face->glyph->advance.x >> 6, 0, 0);

		// Finish The Display List
		glEndList();
	}
}