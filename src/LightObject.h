#pragma once
#include "SceneObject.h"
#include "ShaderProgram.h"

#define LIGHT_POINT			1
#define LIGHT_SPOT			2
#define LIGHT_DIRECTIONAL	3

class LightObject :
	public SceneObject
{
public:
	LightObject( const char *objectName );
	~LightObject(void);

	void Init( GLScene *scene ) {}
	void Update( float tickMS ) {}
	void Render(RenderPass *pass) {}

	void BindLight( GLenum lightID ) {}
	void UnbindLight() {}

	virtual void SetShaderUniform( char LightVar[60], ShaderProgram *shader ) = 0;

	inline unsigned int GetType() { return m_LightType; }

	void SetDiffuse( glm::vec3 diffuse ) { m_vDiffuse = diffuse; } 
	glm::vec3 GetDiffuse() { return m_vDiffuse; } 

protected:

	glm::vec3	m_vDiffuse;
	glm::vec3	m_vAmbient;
	glm::vec3	m_vSpecular;
	float		m_fShininess;

	GLenum		m_LightID;

	unsigned int m_LightType;
};

