#include "StdAfx.h"
#include "VertexBufferFixed.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

VertexBufferFixed::VertexBufferFixed(void)
{
	m_bHaveVBID = false;
	m_bHaveIBID = false;
}

VertexBufferFixed::~VertexBufferFixed(void)
{
	Destroy();
}

void VertexBufferFixed::Destroy()
{
	if ( m_bHaveVBID )
		glDeleteBuffers(1, &m_iVBID);
	if ( m_bHaveIBID )
		glDeleteBuffers(1, &m_iIBID);
	GLuint
	m_bHaveVBID = m_bHaveIBID = false;
}

void VertexBufferFixed::CreateVertexBuffer( uint32 fvf, size_t vCount, void *vertices, size_t vertexSize, GLenum usage )
{
	if ( m_bHaveVBID == false )
	{
		glGenBuffers( 1, &m_iVBID );
		m_bHaveVBID = true;
	}

	m_iVertexCount = vCount;
	m_iVertexSize = vertexSize;
	m_iVertexFormat = fvf;

	// Bind the buffer and start setting the data
	glBindBuffer( GL_ARRAY_BUFFER, m_iVBID );

	// Allocate memory and send the data to GL
	glBufferData( GL_ARRAY_BUFFER,  m_iVertexCount * m_iVertexSize, vertices, usage);

	// unbind
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

void VertexBufferFixed::SetVertexBuffer( int offset, size_t vCount, void *vertices )
{
	if ( m_bHaveVBID == false )
		return;

	m_iVertexCount = vCount;
	
	glBindBuffer( GL_ARRAY_BUFFER, m_iVBID );

	glBufferSubData( GL_ARRAY_BUFFER, 0, m_iVertexCount * m_iVertexSize, vertices );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

void VertexBufferFixed::CreateIndexBuffer( size_t iCount, uint16 *indices, GLenum usage )
{
	if ( m_bHaveIBID == false )
	{
		glGenBuffers( 1, &m_iIBID );
		m_bHaveIBID = true;
	}

	m_iIndexCount = iCount;
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iIBID);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iCount * sizeof(uint16), indices, usage);

	// unbind
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}

void VertexBufferFixed::CreateIndexBuffer( size_t iCount, uint32 *indices, GLenum usage )
{
	if ( m_bHaveIBID == false )
	{
		glGenBuffers( 1, &m_iIBID );
		m_bHaveIBID = true;
	}

	m_iIndexCount = iCount;
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iIBID);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iCount * sizeof(uint32), indices, usage);

	// unbind
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}


void VertexBufferFixed::SetIndexBuffer( int offset, size_t iCount, uint16 *indices )
{
	if ( m_bHaveIBID == false )
		return;

	m_iIndexCount = iCount;
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iIBID);

	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, m_iIndexCount * sizeof(uint16), indices);

	// unbind
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}

void VertexBufferFixed::Render( GLenum mode, uint16 indexOffset, uint16 indexCount )
{
	if ( m_bHaveVBID == false )
	{
		return;
	}

	EnableClientStates();
	uint32 pointerOffset = BindBuffers();

	if ( m_iIndexCount > 0 && m_bHaveIBID )
	{
		glEnableClientState(GL_INDEX_ARRAY);

		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_iIBID );
		
		if ( indexOffset == 0 && indexCount == 0)
		{
			glDrawElements( mode, m_iIndexCount, GL_UNSIGNED_SHORT, 0 );
		}
		else
		{
			glDrawRangeElements( mode, indexOffset, indexOffset+indexCount, indexCount, GL_UNSIGNED_SHORT, (const GLvoid*)(indexOffset*sizeof(GLshort)) );
		}

		glDisableClientState(GL_INDEX_ARRAY);
	}
	else
	{
		glDrawArrays( mode, 0, m_iVertexCount );
	}

	// unbind 
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	DisableClientStates();
}

void VertexBufferFixed::Render( GLenum mode, uint32 indexOffset, uint32 indexCount )
{
	if ( m_bHaveVBID == false )
	{
		return;
	}

	EnableClientStates();
	uint32 pointerOffset = BindBuffers();

	if ( m_iIndexCount > 0 && m_bHaveIBID )
	{
		glEnableClientState(GL_INDEX_ARRAY);

		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_iIBID );
		
		if ( indexOffset == 0 && indexCount == 0)
		{
			glDrawElements( mode, m_iIndexCount, GL_UNSIGNED_INT, 0 );
		}
		else
		{
			glDrawRangeElements( mode, indexOffset, indexOffset+indexCount, indexCount, GL_UNSIGNED_INT, (const GLvoid*)(indexOffset*sizeof(GLint)) );
		}

		glDisableClientState(GL_INDEX_ARRAY);
	}
	else
	{
		glDrawArrays( mode, 0, m_iVertexCount );
	}

	// unbind 
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	DisableClientStates();
}

void VertexBufferFixed::Render( GLenum mode, uint16 *indices, uint16 indexCount )
{
	if ( m_bHaveVBID == false )
	{
		return;
	}

	EnableClientStates();
	uint32 pointerOffset = BindBuffers();

	if ( indexCount > 0 && indices != NULL )
	{
		glDrawElements( mode, indexCount, GL_UNSIGNED_SHORT, indices );
	}

	// unbind 
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	DisableClientStates();
}

void VertexBufferFixed::Render( GLenum mode, uint32 *indices, uint32 indexCount )
{
	if ( m_bHaveVBID == false )
	{
		return;
	}

	EnableClientStates();
	uint32 pointerOffset = BindBuffers();

	if ( indexCount > 0 && indices != NULL )
	{
		glDrawElements( mode, indexCount, GL_UNSIGNED_INT, indices );
	}

	// unbind 
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
    
	DisableClientStates();
}

uint32 VertexBufferFixed::BindBuffers()
{
	glBindBuffer( GL_ARRAY_BUFFER, m_iVBID );

	// Set the vertex format
	uint32 pointerOffset = 0;

	// Set XYZ
	if ( m_iVertexFormat & VDF_XYZ )
	{
		glVertexPointer(3, GL_FLOAT, m_iVertexSize, 0);
		pointerOffset += sizeof(GL_FLOAT) * 3;
	}
	else
	{
		return 0; // Might be acceptable in the future, who knows
	}

	// Bind normal
	if ( m_iVertexFormat & VDF_NORMAL )
	{
		glNormalPointer(GL_FLOAT, m_iVertexSize, BUFFER_OFFSET(pointerOffset));
		pointerOffset += sizeof(GL_FLOAT) * 3;
	}
	// Bind RGB color
	if ( m_iVertexFormat & VDF_COLOR_RGB )
	{
		glColorPointer(3, GL_FLOAT, m_iVertexSize, BUFFER_OFFSET(pointerOffset) );
		pointerOffset += sizeof(GL_FLOAT) * 3;
	}
	// Bind RGBA color (can't have both RGB and RGBA)
	else if ( m_iVertexFormat & VDF_COLOR_RGBA )
	{
		glColorPointer(4, GL_FLOAT, m_iVertexSize, BUFFER_OFFSET(pointerOffset) );
		pointerOffset += sizeof(GL_FLOAT) * 4;
	}

	// Set texture coordinates
	pointerOffset += SetTextureCoordPointer(VDF_TEX0, pointerOffset, 0);
	pointerOffset += SetTextureCoordPointer(VDF_TEX1, pointerOffset, 1);
	pointerOffset += SetTextureCoordPointer(VDF_TEX2, pointerOffset, 2);
	pointerOffset += SetTextureCoordPointer(VDF_TEX3, pointerOffset, 3);
	pointerOffset += SetTextureCoordPointer(VDF_TEX4, pointerOffset, 4);
	pointerOffset += SetTextureCoordPointer(VDF_TEX5, pointerOffset, 5);
	pointerOffset += SetTextureCoordPointer(VDF_TEX6, pointerOffset, 6);
	pointerOffset += SetTextureCoordPointer(VDF_TEX7, pointerOffset, 7);

	return pointerOffset;
}

uint32 VertexBufferFixed::SetTextureCoordPointer( uint32 texCoordMask, int pointerOffset, int texCoordIndex )
{
	if ( m_iVertexFormat & texCoordMask )
	{
		int coordSize = GetTextureCoordSize( m_iVertexFormat, texCoordIndex );
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glClientActiveTexture( GetTextureEnumFromMask(texCoordMask) );
		glTexCoordPointer(coordSize, GL_FLOAT, m_iVertexSize, BUFFER_OFFSET(pointerOffset));

		return sizeof(GL_FLOAT) * coordSize;
	}
	return 0;
}

uint32 VertexBufferFixed::GetTextureCoordSize( uint32 format, uint32 texCoordIndex )
{
	int textureFormat = (format >> (16 + texCoordIndex * 2)) & 0x3;
	if ( textureFormat == VDF_TEXTUREFORMAT1 ) return 1;
	else if ( textureFormat == VDF_TEXTUREFORMAT3 ) return 3;
	else if ( textureFormat == VDF_TEXTUREFORMAT4 ) return 4;
	else return 2; // Default FVF_TEXTUREFORMAT2
}

GLenum VertexBufferFixed::GetTextureEnumFromMask( uint32 texCoordMask  )
{
	switch( texCoordMask )
	{
		case VDF_TEX0: return GL_TEXTURE0;
		case VDF_TEX1: return GL_TEXTURE1;
		case VDF_TEX2: return GL_TEXTURE2;
		case VDF_TEX3: return GL_TEXTURE3;
		case VDF_TEX4: return GL_TEXTURE4;
		case VDF_TEX5: return GL_TEXTURE5;
		case VDF_TEX6: return GL_TEXTURE6;
		case VDF_TEX7: return GL_TEXTURE7;
	}

	return GL_TEXTURE0;
}

void VertexBufferFixed::EnableClientStates()
{
	if( m_iVertexFormat & VDF_XYZ )			glEnableClientState(GL_VERTEX_ARRAY);
	if( m_iVertexFormat & VDF_COLOR_RGB || 
		m_iVertexFormat & VDF_COLOR_RGBA )	glEnableClientState(GL_COLOR_ARRAY);
	if( m_iVertexFormat & VDF_NORMAL )		glEnableClientState(GL_NORMAL_ARRAY);
	if( m_iVertexFormat & VDF_TEX0 ||
		m_iVertexFormat & VDF_TEX1 ||
		m_iVertexFormat & VDF_TEX2 ||
		m_iVertexFormat & VDF_TEX3 ||
		m_iVertexFormat & VDF_TEX4 ||
		m_iVertexFormat & VDF_TEX5 ||
		m_iVertexFormat & VDF_TEX6 ||
		m_iVertexFormat & VDF_TEX7 )		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}

void VertexBufferFixed::DisableClientStates()
{
	if( m_iVertexFormat & VDF_XYZ )			glDisableClientState(GL_VERTEX_ARRAY);
	if( m_iVertexFormat & VDF_COLOR_RGB || 
		m_iVertexFormat & VDF_COLOR_RGBA )	glDisableClientState(GL_COLOR_ARRAY);
	if( m_iVertexFormat & VDF_NORMAL )		glDisableClientState(GL_NORMAL_ARRAY);
	if( m_iVertexFormat & VDF_TEX0 ||
		m_iVertexFormat & VDF_TEX1 ||
		m_iVertexFormat & VDF_TEX2 ||
		m_iVertexFormat & VDF_TEX3 ||
		m_iVertexFormat & VDF_TEX4 ||
		m_iVertexFormat & VDF_TEX5 ||
		m_iVertexFormat & VDF_TEX6 ||
		m_iVertexFormat & VDF_TEX7 )		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}