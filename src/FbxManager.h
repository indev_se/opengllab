#pragma once
#include <fbxsdk.h>
#include <fbxfilesdk\kfbxplugins\kfbxlayer.h>

#ifdef IOS_REF
	#undef  IOS_REF
	#define IOS_REF (*(m_pSdkManager->GetIOSettings()))
#endif

class FbxManager
{
public:
	~FbxManager(void);
	static FbxManager* Inst();

	void AllocateScene( KFbxScene*& pScene );
	bool LoadScene(KFbxDocument* pScene, const char* pFilename, bool includeAnimation);

	inline KFbxSdkManager *SdkManager() { return m_pSdkManager; }

protected:
	FbxManager(void);

private:
	static FbxManager* m_pInst;

	KFbxSdkManager *m_pSdkManager;

	void CreateManager();
};

