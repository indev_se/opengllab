#pragma once
#include "GLOverlay.h"
#include "TextObject.h"

class StrategyGLOverlay :
	public GLOverlay
{
public:
	StrategyGLOverlay(void);
	~StrategyGLOverlay(void);

	void Init();
	void Update( float tickMS );
	void Render();

private:

	TextObject *m_pStatsText;
	TextObject *m_pTimeText;
	GLFont *m_Font_Arial16;
};

