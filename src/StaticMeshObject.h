#pragma once
#include "CachedObject.h"
#include "Mesh.h"

class FbxFileContext;

class StaticMeshObject :
	public SceneObject, public CachedObject
{
public:
	StaticMeshObject( const char *objectName );
	~StaticMeshObject(void);

	void Init( GLScene *scene );
	void Update( float tickMS );
	void Render(RenderPass *pass);
	
	bool LoadFromFile( const char *filename );
	bool LoadFromCache( const char *cachedFilename );

protected:

	void SaveCachedVersion(const char *filename);

private:

	Mesh *m_pMesh;
	FbxFileContext *m_pFbxContext;
};

