#include "StdAfx.h"
#include "MeasureObject.h"
#include "AssetsManager.h"

MeasureObject::MeasureObject( const char *objectName ) : SceneObject(objectName)
{
	m_pMaterial = nullptr;
}

MeasureObject::~MeasureObject(void)
{
	AssetsManager *manager = m_pScene->GetAssetsManager();

	manager->RemoveAndDestroyAsset(m_pMaterial);

	m_pMaterial = nullptr;
}

void MeasureObject::Init( GLScene *scene, float sizeMeter )
{
	m_pScene = scene;
	
	AssetsManager *manager = m_pScene->GetAssetsManager();

	m_pMaterial = manager->GetMaterial("Measure8DM");

	VertexDescription *desc = m_pMaterial->GetDescription();
	VertexDataElement *POS = desc->GetElement("InPosition");
	VertexDataElement *TEX = desc->GetElement("InTex0");
	VertexDataElement *NOR = desc->GetElement("InNormal");
	VertexDataElement *TAN = desc->GetElement("InTangent");

	float units = sizeMeter * METER;
	float halfUnits = units * .5;

	float textureScale = sizeMeter * ( 1.0f / 0.8f );

	VertexData *vertices = new VertexData[4];

	vertices[0].set(POS, glm::vec3( -halfUnits, .0f, -halfUnits ) );
	vertices[1].set(POS, glm::vec3( -halfUnits, .0f, halfUnits ) );
	vertices[2].set(POS, glm::vec3( halfUnits, .0f, halfUnits ) );
	vertices[3].set(POS, glm::vec3( halfUnits, .0f, -halfUnits ) );
	
	vertices[0].set(NOR, glm::vec3(0.0f,1.0f,0.0f) );
	vertices[1].set(NOR, glm::vec3(0.0f,1.0f,0.0f) );
	vertices[2].set(NOR, glm::vec3(0.0f,1.0f,0.0f) );
	vertices[3].set(NOR, glm::vec3(0.0f,1.0f,0.0f) );

	vertices[0].set(TEX, glm::vec2(0.0f,0.0f) );
	vertices[1].set(TEX, glm::vec2(0.0f,textureScale) );
	vertices[2].set(TEX, glm::vec2(textureScale,textureScale) );
	vertices[3].set(TEX, glm::vec2(textureScale,0.0f) );

	vertices[0].set(TAN, glm::vec3(0.0f,0.0f,1.0f) );
	vertices[1].set(TAN, glm::vec3(0.0f,0.0f,1.0f) );
	vertices[2].set(TAN, glm::vec3(0.0f,0.0f,1.0f) );
	vertices[3].set(TAN, glm::vec3(0.0f,0.0f,1.0f) );

	uint16 indices[6];
	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;

	indices[3] = 0;
	indices[4] = 2;
	indices[5] = 3;
	
	m_pVB = new VertexBuffer();
	m_pVB->CreateVertexBuffer( desc, 4, vertices, GL_STATIC_DRAW );
	m_pVB->CreateIndexBuffer( 6, indices, GL_STATIC_DRAW );

	m_pAABB = new BoundingBox( glm::vec3(-halfUnits, -1.0f, -halfUnits), glm::vec3(halfUnits, 1.0f, halfUnits) );
}

void MeasureObject::Update( float tickMS )
{
	SceneObject::Update(tickMS);
}

void MeasureObject::Render(RenderPass *pass)
{
	if ( pass != nullptr )
		pass->BindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->BindForward();

	m_pVB->Render(GL_TRIANGLES, (uint16)0, (uint16)0);

	if ( pass != nullptr )
		pass->UnbindObject(this);
	else
		if (m_pMaterial != nullptr) m_pMaterial->UnbindForward();

	SceneObject::Render(pass);
}