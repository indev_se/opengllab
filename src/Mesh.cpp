#include "StdAfx.h"
#include "Mesh.h"


Mesh::Mesh(void)
{
	m_Skeleton = nullptr;
	m_aaBoundingBox = nullptr;
}


Mesh::~Mesh(void)
{
	if ( m_aaBoundingBox != nullptr )
		delete m_aaBoundingBox;

	m_aaBoundingBox = nullptr;
}

void Mesh::SetBounds( const glm::vec3 &min, const glm::vec3 &max )
{
	if ( m_aaBoundingBox == nullptr )
		m_aaBoundingBox = new BoundingBox();

	m_aaBoundingBox->SetBounds(min, max);
}

void Mesh::CalculateBoundsFromGroups()
{
	// Calculate the bounds of the whole mesh from the groups
	if ( m_aaBoundingBox == nullptr )
		m_aaBoundingBox = new BoundingBox();

	uint32 i;
	glm::vec3 min, max, tmpMin, tmpMax;

	if (m_Groups.size() == 0) {
		StdConsole::LogError( _STR"Mesh::CalculateBoundsFromGroups - No groups");
		return;
	}

	if (m_Groups[0]->m_aaBoundingBox == nullptr) {
		StdConsole::LogError( _STR"Mesh::CalculateBoundsFromGroups - Group have no boundingbox");
		return;
	}

	m_Groups[0]->m_aaBoundingBox->GetBounds(min, max);

	for ( i = 1; i < m_Groups.size(); ++i )
	{
		m_Groups[i]->m_aaBoundingBox->GetBounds(tmpMin, tmpMax);

		if ( tmpMin.x < min.x ) min.x = tmpMin.x;
		if ( tmpMax.x > max.x ) min.x = tmpMax.x;
		if ( tmpMin.y < min.y ) min.y = tmpMin.y;
		if ( tmpMax.y > max.y ) min.y = tmpMax.y;
		if ( tmpMin.z < min.z ) min.z = tmpMin.z;
		if ( tmpMax.z > max.z ) min.z = tmpMax.z;
	}

	m_aaBoundingBox->SetBounds(min, max);
}

BoundingBox* Mesh::GetBoundingBox()
{
	return m_aaBoundingBox;
}

void Mesh::SetSkeleton( Skeleton *skeleton )
{
	m_Skeleton = skeleton;
}

void Mesh::SetGeometryBuffer( MeshGroup *group, GeometryBuffer *buffer )
{
	group->pGeometryBuffer = buffer;

	// Add it to the geometry list if it's not already there
	if( std::find(m_GeometryBuffers.begin(), m_GeometryBuffers.end(), buffer) == m_GeometryBuffers.end() )
		m_GeometryBuffers.push_back( buffer );
}

GeometryBuffer* Mesh::GetGeometryBuffer(MeshGroup *group)
{
	return group->pGeometryBuffer;
}

std::vector<MeshGroup*> Mesh::GetGroups()
{
	return m_Groups;
}

std::vector<MeshAnimation*> Mesh::GetAnimations()
{
	return m_Animations;
}

MeshAnimation* Mesh::GetAnimation( std::string name )
{
	for ( uint32 i = 0; i < m_Animations.size(); ++i )
		if ( m_Animations[i]->Name == name )
			return m_Animations[i];

	return nullptr;
}

Skeleton* Mesh::GetSkeleton()
{
	return m_Skeleton;
}

void Mesh::AddAnimation( MeshAnimation *animation )
{
	m_Animations.push_back( animation );
}

void Mesh::AddGroup( MeshGroup *group )
{
	m_Groups.push_back(group);
}

uint16 Mesh::GetNumIndices()
{
	uint16 indexCount = 0;
	for ( uint32 i = 0; i < m_Groups.size(); ++i )
		indexCount += m_Groups[i]->numIndices;

	return indexCount;
}

uint16 Mesh::GetNumIndices( GeometryBuffer *gb )
{
	uint16 indexCount = 0;
	for ( uint32 i = 0; i < m_Groups.size(); ++i ) {
		if ( m_Groups[i]->pGeometryBuffer == gb )
			indexCount += m_Groups[i]->numIndices;
	}

	return indexCount;
}
