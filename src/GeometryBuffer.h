#pragma once
#include "VertexData.h"
#include "VertexBuffer.h"

class GeometryBuffer
{
public:
	GeometryBuffer(void);
	~GeometryBuffer(void);

	void SetDescription( VertexDescription *vertexDesc );
	void ReplaceDescription( VertexDescription *vertexDesc );
	VertexDescription *GetDescription();

	VertexBuffer *GetVertexBuffer();

	void Set(VertexData *buffer, uint32 size);
	
	void Add(VertexData *buffer, uint32 size);
	void Add(uint8 *buffer, uint32 size);

	void Clear();

	uint8 *GetAt( uint32 offset );

	uint32 Count();

	inline void SetDirty() { m_IsDirty = true; }

private:

	void RebuildVertexBuffer();

	bool m_IsDirty;

	VertexDescription *m_VertexDesc;

	VertexBuffer *m_VertexBuffer;

	// Vertex data
	std::vector<uint8> m_Data;
	

public:

	uint32 UniqueCacheID;

	// Insertion operator
	friend std::ostream& operator<<(std::ostream& os, const GeometryBuffer& s)
	{
		SWRITE(os,s.UniqueCacheID);

		os << *s.m_VertexDesc;

		uint32 numVertexBytes = s.m_Data.size();

		SWRITE(os,numVertexBytes);

		os.write((char*)&s.m_Data[0], numVertexBytes * sizeof(char));

		//for ( uint32 i = 0; i < numVertexBytes; ++i )
		//	SWRITE(os,s.m_Data[i]);

		return os;
	}

	// Extraction  operator
	friend std::istream& operator>>(std::istream& is, GeometryBuffer& s)
	{
		SREAD(is,s.UniqueCacheID);

		s.m_VertexDesc = new VertexDescription();
		is >> *s.m_VertexDesc;

		uint32 numVertexBytes;
		SREAD(is,numVertexBytes);

		s.m_Data.resize(numVertexBytes);
		is.read((char*)&s.m_Data[0], numVertexBytes * sizeof(char));
		//for ( uint32 i = 0; i < numVertexBytes; ++i )
		//	SREAD(is,s.m_Data[i]);

		return is;
	}
};


