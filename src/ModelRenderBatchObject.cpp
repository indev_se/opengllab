#include "StdAfx.h"
#include "FbxImporter.h"
#include "Mesh.h"
#include "VertexBuffer.h"
#include "SceneObjectFactory.h"
#include "FbxFileContext.h"
#include "ModelRenderBatchObject.h"


ModelRenderBatchObject::ModelRenderBatchObject( const char *objectName ) : 
	SceneObject(objectName), 
	CachedObject()
{
	CacheType = CACHE_MODELRENDERBATCH;
	m_pRenderBatch = nullptr;
}


ModelRenderBatchObject::~ModelRenderBatchObject(void)
{
	if ( m_pRenderBatch != nullptr )
		delete m_pRenderBatch;

	m_pRenderBatch = nullptr;
}

bool ModelRenderBatchObject::LoadFromFile( const char *filename )
{
	FbxImporter *importer = new FbxImporter();
	m_pFbxContext = importer->LoadFromFile( filename, m_pScene, false );
	m_pMesh = m_pFbxContext->GetMesh();

	if ( m_pMesh != nullptr )
	{
		m_pRenderBatch = new RenderBatch();
		
		// Create the render Jobs
		MeshGroup *meshgroup;
		VertexBuffer *vb;
		RenderJob *job;
		Material *mtrl;

		std::map<VertexBuffer*, RenderJob*> vbJobMap;

		for( uint32 i = 0; i < m_pMesh->GetGroups().size(); ++i ) 
		{
			meshgroup = m_pMesh->GetGroups()[i];
			vb = meshgroup->pGeometryBuffer->GetVertexBuffer();
			
			// Get or create the Job associated with this VB
			if ( vbJobMap[vb] ) {
				job = vbJobMap[vb];
			}
			else {
				job = new RenderJob();
				job->SetVertexBuffer(vb);
				
				mtrl = m_pScene->GetAssetsManager()->GetMaterial( meshgroup->materialName.c_str() );
				if ( mtrl == nullptr )
					mtrl = m_pScene->GetAssetsManager()->GetMaterial( "NotFound" );

				job->SetMaterial( mtrl );
				m_pRenderBatch->AddJob(job);
				vbJobMap[vb] = job;
			}

			printf("%s %s %d\n", meshgroup->name.c_str(), meshgroup->materialName.c_str(), meshgroup->numIndices );

			// Add the index from the group to the Job
			//job->AddIndexRange( meshgroup->indices, meshgroup->numIndices );
		}
		
		m_pMesh->CalculateBoundsFromGroups();
		if ( m_pMesh->GetBoundingBox() )
			m_pAABB = m_pMesh->GetBoundingBox()->Clone();

		SaveCachedVersion(filename);
		return true;
	}

	return false;
}

bool ModelRenderBatchObject::LoadFromCache( const char *cachedFilename )
{
	std::ifstream ifs(cachedFilename, std::ios::binary);

	SREAD(ifs, CacheType);

	m_pFbxContext = new FbxFileContext();
	ifs >> *m_pFbxContext;

	m_pMesh = m_pFbxContext->GetMesh();

	if ( m_pMesh != nullptr )
	{
		m_pRenderBatch = new RenderBatch();
		
		// Create the render Jobs
		MeshGroup *meshgroup;
		VertexBuffer *vb;
		RenderJob *job;
		Material *mtrl;

		std::map<VertexBuffer*, RenderJob*> vbJobMap;

		for( uint32 i = 0; i < m_pMesh->GetGroups().size(); ++i ) 
		{
			meshgroup = m_pMesh->GetGroups()[i];
			vb = meshgroup->pGeometryBuffer->GetVertexBuffer();
			
			// Get or create the Job associated with this VB
			if ( vbJobMap[vb] ) {
				job = vbJobMap[vb];
			}
			else {
				job = new RenderJob();
				job->SetVertexBuffer(vb);
				
				mtrl = m_pScene->GetAssetsManager()->GetMaterial( meshgroup->materialName.c_str() );
				if ( mtrl == nullptr )
					mtrl = m_pScene->GetAssetsManager()->GetMaterial( "NotFound" );

				job->SetMaterial( mtrl );
				m_pRenderBatch->AddJob(job);
				vbJobMap[vb] = job;
			}

			// Add the index from the group to the Job
			//job->AddIndexRange( meshgroup->indices, meshgroup->numIndices );
		}
		
		m_pMesh->CalculateBoundsFromGroups();
		if ( m_pMesh->GetBoundingBox() )
			m_pAABB = m_pMesh->GetBoundingBox()->Clone();

		return true;
	}

	return false;

}

void ModelRenderBatchObject::SaveCachedVersion(const char *filename)
{
	std::string cachedFilename = SceneObjectFactory::Inst()->GetCacheFilepath(filename);
	std::ofstream ofs(cachedFilename, std::ios::binary);

	SWRITE(ofs, CacheType)

	ofs << *m_pFbxContext;

	ofs.close();
}

void ModelRenderBatchObject::Init( GLScene *scene )
{
	m_pScene = scene;
}

void ModelRenderBatchObject::Update( float tickMS )
{

	SceneObject::Update(tickMS);
}

void ModelRenderBatchObject::Render(RenderPass *pass)
{
	m_pRenderBatch->Render(pass);

	SceneObject::Render(pass);
}