#pragma once

// Vertex format bits
#define VDF_XYZ				1
#define VDF_NORMAL			1 << 1
#define VDF_COLOR_RGB		1 << 2
#define VDF_COLOR_RGBA		1 << 3

#define VDF_TEX0			1 << 5
#define VDF_TEX1			1 << 6
#define VDF_TEX2			1 << 7
#define VDF_TEX3			1 << 8
#define VDF_TEX4			1 << 9
#define VDF_TEX5			1 << 10
#define VDF_TEX6			1 << 11
#define VDF_TEX7			1 << 12

#define VDF_TEXTUREFORMAT1 3 // one floating point value
#define VDF_TEXTUREFORMAT2 0 // two floating point values
#define VDF_TEXTUREFORMAT3 1 // three floating point values
#define VDF_TEXTUREFORMAT4 2 // four floating point values

#define VDF_TEXCOORDSIZE1(CoordIndex) (VDF_TEXTUREFORMAT1 << (CoordIndex*2 + 16)) 
#define VDF_TEXCOORDSIZE2(CoordIndex) (VDF_TEXTUREFORMAT2) 
#define VDF_TEXCOORDSIZE3(CoordIndex) (VDF_TEXTUREFORMAT3 << (CoordIndex*2 + 16)) 
#define VDF_TEXCOORDSIZE4(CoordIndex) (VDF_TEXTUREFORMAT4 << (CoordIndex*2 + 16))

class VertexBufferFixed
{
public:
	VertexBufferFixed(void);
	~VertexBufferFixed(void);

	void Destroy();

	void CreateVertexBuffer( uint32 fvf, size_t vCount, void *vertices, size_t vertexSize, GLenum usage );
	void CreateIndexBuffer( size_t iCount, uint16 *indices, GLenum usage );
	void CreateIndexBuffer( size_t iCount, uint32 *indices, GLenum usage );

	void SetVertexBuffer( int offset, size_t vCount, void *vertices );
	void SetIndexBuffer( int offset, size_t iCount, unsigned short *indices );

	void Render( GLenum mode, uint16 indexOffset = 0, uint16 indexCount = 0);
	void Render( GLenum mode, uint32 indexOffset = 0, uint32 indexCount = 0 );
	void Render( GLenum mode, uint16 *indices, uint16 indexCount );
	void Render( GLenum mode, uint32 *indices, uint32 indexCount );

	static uint32 GetTextureCoordSize( uint32 format, uint32 texCoordIndex );

private:

	void EnableClientStates();
	void DisableClientStates();

	uint32 BindBuffers();

	uint32 SetTextureCoordPointer( uint32 texCoordMask, int pointerOffset, int texCoordIndex );
	GLenum GetTextureEnumFromMask( uint32 texCoordMask  );
	
	uint32			m_iVertexFormat;

	bool			m_bHaveVBID;
	bool			m_bHaveIBID;

	uint32			m_iVBID;
	uint32			m_iIBID;

	size_t			m_iVertexSize;
	size_t			m_iVertexCount;

	size_t			m_iIndexCount;
};

