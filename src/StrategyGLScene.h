#pragma once
#include "CameraInput.h"
#include "GLCamera.h"
#include "CubeObject.h"
#include "SphereObject.h"
#include "GLScene.h"
#include "StrategyGLOverlay.h"
#include "LandscapeObject.h"
#include "StaticMeshObject.h"
#include "RenderShadowMapPass.h"
#include "ApplyShadowMapPass.h"
#include "DeferredRenderPass.h"
#include "DeferredRenderLightPass.h"
#include "DeferredRenderFinal.h"
#include "RenderCascadedShadowMapPass.h"
#include "DeferredRenderShadowPass.h"
#include "ForwardRenderPass.h"
#include "AnimatedMeshObject.h"
#include "MaterialImporter.h"
#include "GaussianBlurPass.h"
#include "Projection.h"

class StrategyGLScene :
	public GLScene
{
public:
	StrategyGLScene(void);
	~StrategyGLScene(void);

	void SetViewport( int Width, int Height );

	void Init();
	void Update( float tickMS );
	void Render();

	void GetBounds( glm::vec3 &min, glm::vec3 &max );

protected:

	GLCamera *m_pDefaultFPSCamera;
	CameraInput *m_pCameraInput;
	LandscapeObject *m_pLandscape;
	StrategyGLOverlay *m_pOverlay;

	MaterialImporter *m_MaterialImporter;

	RenderShadowMapPass *passRenderShadowMap;
	ApplyShadowMapPass *passApplyShadowMap;
	DeferredRenderPass *passDeferred;
	DeferredRenderLightPass *passDeferredLight;
	RenderCascadedShadowMapPass *passRenderCascadedShadowMap;
	DeferredRenderShadowPass *passDeferredRenderShadow;
	ForwardRenderPass *passForward;
	GaussianBlurPass *passGaussianBlur;

	DeferredRenderFinal *passDeferredFinal;

	SceneObject *m_RootObject;
	SceneObject *m_RootLightObject;
};

