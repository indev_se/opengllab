#pragma once
#include "Frustum.h"
#include "Projection.h"

class GLCamera
{
public:
	GLCamera(void);
	~GLCamera(void);

	void Init();
	void Reset();

	void SetPerspective( float Fov, float AspectRatio, float zNear, float zFar );
	
	void RotateEyeByAxis( glm::vec3 &axis, float radians );
	inline void RotateEyeX( float radians ) { RotateEyeByAxis( glm::vec3(1,0,0), radians ); }
	inline void RotateEyeY( float radians ) { RotateEyeByAxis( glm::vec3(0,1,0), radians ); }
	inline void RotateEyeZ( float radians ) { RotateEyeByAxis( glm::vec3(0,0,1), radians ); }

	void MoveEyeByAxis( glm::vec3 &axis, float distance );
	inline void MoveEyeX( float distance ) { MoveEyeByAxis( glm::vec3(1,0,0), distance ); }
	inline void MoveEyeY( float distance ) { MoveEyeByAxis( glm::vec3(0,1,0), distance ); }
	inline void MoveEyeZ( float distance ) { MoveEyeByAxis( glm::vec3(0,0,1), distance ); }

	glm::mat4x4 GetViewNormalMatrix();
	glm::mat4x4 GetViewMatrix();

	inline void GetEye( glm::vec3 &eye ) { eye = m_vecEye; }
	inline void GetDirection( glm::vec3 &direction ) { direction = m_vecDirection; }

	Projection *GetProjection();
	Frustum* GetFrustum();

protected:

	void InvalidateRotationQuaternions();

	glm::mat4x4 m_matView;
	glm::mat4x4 m_matRotation;
	glm::mat4x4 m_matTranslation;

	glm::quat m_qRotation;

	glm::vec3 m_vecEye;
	glm::vec3 m_vecDirection;

	// The camera's frustum, used for culling and clipping
	Frustum *m_pFrustum;

	// The cameras projection 
	Projection *m_pProjection;
};

