#pragma once
class GameTimer
{
public:
	GameTimer(void);
	~GameTimer(void);

	void Init();
	float GetElapsed(); 

	void Tick();

	inline unsigned int GetFPS() { return m_fFPS; }
	inline float GetDelta() { return m_fDelta; }

private:

	float  m_fTimeAtGameStart;
	UINT64 m_iTicksPerSecond;

	float			m_fFpsLastUpdate;
	float			m_fFpsUpdateInterval;
	unsigned int	m_iNumFrames;
	float			m_fFPS;

	float m_fLastTick;
	float m_fDelta;
};

