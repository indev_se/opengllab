#include "StdAfx.h"
#include "GaussianBlurPass.h"


GaussianBlurPass::GaussianBlurPass(void)
{
	m_pPass1FBO = nullptr;
}

GaussianBlurPass::~GaussianBlurPass(void)
{
	if ( m_pPass1FBO != nullptr )
		delete m_pPass1FBO;

	m_pPass1FBO = nullptr;
}

void GaussianBlurPass::Init( GLScene *scene )
{
	RenderPass::Init(scene);

	m_pPass1FBO = new ColorFramebufferObject();
	m_pPass1FBO->Create2D( m_pScene->Width(), m_pScene->Height(), GL_COLOR_ATTACHMENT0 );

	m_pPass2FBO = new EmptyFramebufferObject();
	m_pPass2FBO->Create2D( m_pScene->Width(), m_pScene->Height() );

	AssetsManager *assets = m_pScene->GetAssetsManager();

	m_pShader = assets->CreateShaderProgram("GaussianBlurShader");
	m_pShader->CreateProgram( "Data/Shaders/Deferred/GaussianBlur.vert", "Data/Shaders/Deferred/GaussianBlur.frag" );	
}

void GaussianBlurPass::ApplyBlur( TextureAsset *texture )
{
	// First render the horizontal blur to our temporary texture

	// setup the temp pass 1 texture as render target
	glBindFramebuffer(GL_FRAMEBUFFER, m_pPass1FBO->GetFBOID() );
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	
	// bind the shader
	glUseProgramObjectARB(m_pShader->Program());
	
	// set shader values
	m_pShader->SendUniform1i("InTexture", 0);
	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->TextureID());
	m_pShader->SendUniform1i("IsHorizontal", 1);

	// apply the first pass
	this->RenderScreenQuad();

	// Set the dest texture to the framebuffer so we can draw to it
	m_pPass2FBO->BindTexture(texture->TextureID(), GL_COLOR_ATTACHMENT0);
	glBindFramebuffer(GL_FRAMEBUFFER, m_pPass2FBO->GetFBOID() );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	// Change the shader uniforms
	glBindTexture(GL_TEXTURE_2D, m_pPass1FBO->GetTextureId());
	m_pShader->SendUniform1i("IsHorizontal", 0);

	// apply the second pass with pass 1 as src and original as dest
	this->RenderScreenQuad();

	// unbind what we don't need anymore
	glBindFramebuffer(GL_FRAMEBUFFER, 0 );
	glUseProgramObjectARB(0);

	glBindTexture(GL_TEXTURE_2D, 0);
}

uint32 GaussianBlurPass::GetPass1TextureId()
{
	return m_pPass1FBO->GetTextureId();
}

uint32 GaussianBlurPass::GetPass2TextureId()
{
	return m_pPass2FBO->GetTextureId();
}