#pragma once
#include "DeferredFramebuffers.h"
#include "ShadowMapping.h"
#include "CascadedShadowMap.h"
#include "RenderPass.h"

class DeferredRenderPass :
	public RenderPass
{
public:
	DeferredRenderPass(void);
	~DeferredRenderPass(void);

	void Init( GLScene *scene );
	void Bind();
	void Unbind();
	
	void BindObject( SceneObject *object );
	void UnbindObject( SceneObject *object );

	void BindJob( RenderJob *job );
	void UnbindJob( RenderJob *job );

	DeferredFramebuffers *GetFramebuffer() { return m_pFBOs; }

	uint32 GetColorTextureId();
	uint32 GetNormalTextureId();
	uint32 GetDepthTextureId();
	uint32 GetPositionTextureId();

protected:

	DeferredFramebuffers *m_pFBOs;
	
	ShaderProgram *m_pColorShader;
	ShaderProgram *m_pTextureShader;
};

