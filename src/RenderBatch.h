#pragma once
#include "RenderJob.h"
#include "RenderPass.h"

class RenderBatch
{
public:
	RenderBatch(void);
	~RenderBatch(void);

	void AddJob( RenderJob* job );
	void RemoveJob( RenderJob* job );

	void Render( RenderPass *pass );

private:

	std::vector<RenderJob *> m_Jobs;
};

