#include "StdAfx.h"
#include "SceneManager.h"
#include "SceneObject.h"

SceneObject::SceneObject(const char *objectName)
{
	m_matTransform = glm::mat4();

	m_sObjectName = objectName;
	m_Visible = true;
	m_pMaterial = nullptr;
	
	m_pParent = nullptr;

	m_pAABB = nullptr;

	m_vecScale = glm::vec3(1.0f);

	if ( m_sObjectName != NULL )
		SceneManager::Inst()->RegisterObject(objectName, this);
}


SceneObject::~SceneObject(void)
{
	if ( m_sObjectName != NULL )
		SceneManager::Inst()->UnregisterObject(m_sObjectName);

	if ( m_pAABB != nullptr )
		delete m_pAABB;
}

void SceneObject::Init( GLScene *scene, Material *material )
{
	m_pScene = scene;
	m_pMaterial = material;
}

void SceneObject::Identity()
{
	m_matTransform = glm::mat4();
}

void SceneObject::SetRotation( const glm::vec3 axis, float radians )
{
	m_matTransform = glm::rotate( m_matTransform, radians, axis );
}

void SceneObject::SetPosition( glm::vec3 &position )
{
	m_matTransform = glm::translate( m_matTransform, position );
	m_vecPosition = position;
}

void SceneObject::SetScale( glm::vec3 &scale )
{
	m_matTransform = glm::scale( m_matTransform, scale );
	m_vecScale = scale;
}

void SceneObject::SetTransformMatrix( glm::mat4x4 &mat )
{
	m_matTransform = mat;
}

glm::mat4x4 SceneObject::GetModelMatrix()
{
	glm::mat4x4 mat = glm::mat4();
	
	if ( m_pParent != nullptr )
		mat = mat * m_pParent->GetModelMatrix();

	mat = mat * m_matTransform;

	return mat;
}

glm::mat4x4 SceneObject::GetMVNormalMatrix()
{
	glm::quat qrot = glm::toQuat( GetModelMatrix() );
	glm::mat4x4 rot = glm::toMat4(qrot);
	rot = glm::inverse( glm::transpose(rot) );
	
	return rot;
}

void SceneObject::Update( float tickMS )
{
	UpdateChildren(tickMS);
}

void SceneObject::Render(RenderPass *pass)
{
	RenderChildren(pass);
}

void SceneObject::RenderBoundingBox( glm::mat4x4 viewMatrix, glm::mat4x4 projection )
{
	if ( m_pAABB != nullptr )
		m_pAABB->Render( viewMatrix * GetModelMatrix(), projection );

	RenderBoundingBoxChildren(viewMatrix, projection);
}

void SceneObject::AddChild( SceneObject *object )
{
	object->SetParent(this);
	m_Children.push_back(object);
}

void SceneObject::RemoveChild( SceneObject *object )
{
	std::vector<SceneObject *>::iterator iter = std::find(m_Children.begin(), m_Children.end(), object);
	if(iter != m_Children.end()) {
		(*iter)->SetParent(nullptr);
		m_Children.erase(iter);
	}
}

void SceneObject::UpdateChildren(float tickMS)
{
	for( uint32 i = 0; i < m_Children.size(); ++i ) 
		m_Children[i]->Update(tickMS);
}

void SceneObject::RenderChildren(RenderPass *pass)
{
	for( uint32 i = 0; i < m_Children.size(); ++i ) {
		//if ( m_Children[i]->InFrustum() )
			m_Children[i]->Render(pass);
	}
}

void SceneObject::RenderBoundingBoxChildren( glm::mat4x4 viewMatrix, glm::mat4x4 projection )
{
	for( uint32 i = 0; i < m_Children.size(); ++i ) 
		m_Children[i]->RenderBoundingBox(viewMatrix, projection);
}

bool SceneObject::InFrustum()
{
	if ( m_pAABB == nullptr )
		return true; // always in frustum if we have to bb

	// Since we'll calculate the points in view space we just add the model transformations
	return m_pScene->GetCamera()->GetFrustum()->AABBInFrustum( m_pAABB, GetModelMatrix() );
}

void SceneObject::GetBounds( glm::vec3 &min, glm::vec3 &max )
{
	glm::vec3 lmin, lmax;

	if ( m_pAABB != nullptr )
		m_pAABB->GetBounds( lmin, lmax );
	
	if ( lmin.x < min.x ) min.x = lmin.x;
	if ( lmin.y < min.y ) min.y = lmin.y;
	if ( lmin.z < min.z ) min.z = lmin.z;
	if ( lmax.x > max.x ) max.x = lmax.x;
	if ( lmax.y > max.y ) max.y = lmax.y;
	if ( lmax.z > max.z ) max.z = lmax.z;

	for( uint32 i = 0; i < m_Children.size(); ++i ) {
		m_Children[i]->GetBounds(min, max);
	}
}